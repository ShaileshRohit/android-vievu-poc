package com.vievu.vievusolution;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.vievu.vievusolution.ptpip.dataset.CameraStatus;
import com.vievu.vievusolution.util.ActivityUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AssignedCameraActivity extends EventBusActivity {

    @Bind(R.id.connectedCameraTextView) TextView connectedCameraTextView;
    @Bind(R.id.progressBarOverlay) View progressBarOverlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUtil.tryBindNetworkAdapter(this);
        setContentView(R.layout.activity_assigned_camera);
        ButterKnife.bind(this);
        progressBarOverlay.setVisibility(View.VISIBLE);
        CameraClient.getInstance().getStorageInfoAsync();
    }

    private void displayConnectedCameraSSID() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        connectedCameraTextView.setText(
            Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 ?
                wifiInfo.getSSID() :
                wifiInfo.getSSID().replaceAll("^\"(.*)\"$", "$1"));
        progressBarOverlay.setVisibility(View.GONE);
    }

    private void displayNoConnectedCamera() {
        connectedCameraTextView.setText(R.string.text_camera_no_connected);
        progressBarOverlay.setVisibility(View.GONE);
    }

    public void onEventMainThread(CameraStatus cameraStatus) {
        displayConnectedCameraSSID();
    }

    public void onEventMainThread(CameraClient.Event event) {
        if (!event.isConnected()) {
            displayNoConnectedCamera();
        }
    }
}
