package com.vievu.vievusolution.data;

import android.content.Context;
import android.os.Environment;

import com.google.gson.stream.JsonWriter;
import com.vievu.vievusolution.bean.Permissions;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.json.Gsons;
import com.vievu.vievusolution.util.JsonCacheUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;

public class UserDataRepository {

    private static final String FILE_NAME = "user_data.json";

    private static UserDataRepository instance;

    public static UserDataRepository getInstance() {
        if (instance == null) {
            instance = new UserDataRepository();
            instance.permissions = new Permissions();
            instance.permissions.setVideosAccess(Permissions.VIDEOS_NONE);
            instance.permissions.setCasesAccess(Permissions.CASES_NONE);
            instance.permissions.setCanDownloadVideos(false);
            instance.permissions.setCanEditVideos(true);
            instance.permissions.setSetLockdown(true);
        }
        return instance;
    }

    private User user;
    private Permissions permissions;

    public void initialize(User user, Permissions permissions) {
        this.user = user;
        this.permissions = permissions;
    }

    public User getUser() {
        return user;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    public void clearData(Context context) {
        //Not supported in Veripatrol app
    }

    public void cacheCopy(Context context) {
        //Not supported in Veripatrol app
    }

    public static boolean restoreFromCache(Context context) {
        //Not supported in Veripatrol app
        return true;
    }
}
