package com.vievu.vievusolution.data;

import android.content.Context;

import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.camera.MetadataFtpClient;

import java.util.List;

public class CategoriesAsyncRepository extends CachingAsyncRepository<List<Category>> {

    private static final String REMOTE_FILE_NAME = "/VIDEO/Categories.json";

    private static CategoriesAsyncRepository instance;

    public static CategoriesAsyncRepository getInstance() {
        if (instance == null) {
            instance = new CategoriesAsyncRepository();
        }
        return instance;
    }

    @Override
    protected String getCacheFileName() {
        return null;
    }

    public static boolean restoreFromCache(Context context) {
        //Not supported in Veripatrol app
        return false;
    }

    @Override
    protected int getLoadEventAction() {
        //Not supported in Veripatrol app
        return 0;
    }

    @Override
    public boolean isDataReady() {
        return false;
    }

    @Override
    protected void loadData(EventBusCallback<List<Category>> callback) {
        MetadataFtpClient.getInstance().loadCategoriesAsync(REMOTE_FILE_NAME);
    }
}
