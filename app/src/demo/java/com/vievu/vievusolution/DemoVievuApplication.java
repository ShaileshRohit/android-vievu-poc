package com.vievu.vievusolution;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(mailTo = BuildConfig.FEEDBACK_EMAIL)
public class DemoVievuApplication extends VievuApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
    }
}