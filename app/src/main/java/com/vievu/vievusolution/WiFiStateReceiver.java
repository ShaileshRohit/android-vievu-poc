package com.vievu.vievusolution;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.vievu.vievusolution.util.ActivityUtil;

public class WiFiStateReceiver<T extends EventBusActivity & WiFiStateReceiver.CallbackActivity> extends BroadcastReceiver {

    public interface CallbackActivity {
        void onConnectedToWiFiNetwork(String ssid);
        void onWiFiConnectionLost();
    }

    private static final IntentFilter filter = new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION);

    private boolean useDefaultHandler;
    private T owner;
    private WifiManager wifiManager;

    public WiFiStateReceiver(boolean useDefaultHandler) {
        this.useDefaultHandler = useDefaultHandler;
    }

    public WiFiStateReceiver() {
        this(true);
    }

    public void registerWith(T owner) {
        this.owner = owner;
        this.wifiManager = (WifiManager) owner.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        this.owner.registerReceiver(this, filter);
    }

    public void unregister() {
        this.owner.unregisterReceiver(this);
        this.owner = null;
        this.wifiManager = null;
    }

    public void handleLostConnection() {
        CameraClient.getInstance().disconnectImmediately();
        if (owner != null && !owner.isInBackground) {
            ActivityUtil.replaceFragment(owner, NoCameraFragment.create(false));
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("WiFiStateReceiver", "intent received");
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (networkInfo != null) {
            if (networkInfo.isConnectedOrConnecting()) {
                String ssid = networkInfo.getExtraInfo();
                if (ssid == null || ssid.isEmpty()) {
                    ssid = wifiManager.getConnectionInfo().getSSID();
                }
                owner.onConnectedToWiFiNetwork(ssid);
            } else if (!networkInfo.isConnectedOrConnecting()) {
                if (useDefaultHandler && !owner.isInBackground()) {
                    ActivityUtil.replaceFragment(owner, NoCameraFragment.create(false));
                }
                owner.onWiFiConnectionLost();
            }
        }
    }
}
