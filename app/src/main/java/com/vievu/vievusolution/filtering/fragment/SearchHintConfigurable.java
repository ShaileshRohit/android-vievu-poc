package com.vievu.vievusolution.filtering.fragment;

public interface SearchHintConfigurable {
    void setSearchHintId(int searchHintId);
}
