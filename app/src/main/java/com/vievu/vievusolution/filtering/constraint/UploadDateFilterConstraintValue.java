package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.DateWrapper;
import com.vievu.vievusolution.net.WebClient;

public class UploadDateFilterConstraintValue extends BaseDateFilterConstraintValue {

    public UploadDateFilterConstraintValue(DateWrapper value) {
        super(R.id.filter_type_upload_date, null, value, 0);
        if (value.isStartDate()) {
            this.key = WebClient.Filters.UPLOAD_DATE_FROM;
            this.readableName = R.string.text_uploaded_from;
        } else {
            this.key = WebClient.Filters.UPLOAD_DATE_TO;
            this.readableName = R.string.text_uploaded_to;
        }
    }
}