package com.vievu.vievusolution.filtering.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.ListView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ui.ClearableEditText;
import com.vievu.vievusolution.ui.CommonListMixin;
import com.vievu.vievusolution.util.ActivityUtil;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnEditorAction;
import butterknife.OnItemClick;

public abstract class SearchableListConstraintFragment<T>  extends ConstraintProvider<T> implements AbsListView.OnScrollListener, SearchHintConfigurable {

    @Bind(R.id.searchEditText) ClearableEditText searchEditText;
    protected ListView listView;
    protected View progressFooter;
    protected CommonListMixin commonListMixin;

    protected int searchHintId;

    protected int currentAction = EventBusCallback.ACTION_NONE;
    protected int currentPage;
    protected String lastSearch;
    protected AtomicBoolean isLoading = new AtomicBoolean();

    protected ArrayListAdapter<T, ?> adapter;

    protected abstract ArrayListAdapter<T, ?> createAdapter();

    public void setSearchHintId(int searchHintId) {
        this.searchHintId = searchHintId;
        if (searchEditText != null) {
            searchEditText.setHint(searchHintId);
        }
    }

    public SearchableListConstraintFragment() {
        usesEvents = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_searchable_list_constraint, container, false);
        currentPage = 0;
        lastSearch = null;
        isLoading.set(false);
        ButterKnife.bind(this, view);
        commonListMixin = CommonListMixin.create(view);
        listView = commonListMixin.getListView();
        progressFooter = inflater.inflate(R.layout.view_part_progress_footer, null);
        listView.addFooterView(progressFooter);
        adapter = createAdapter();
        listView.setAdapter(adapter);
        listView.setOnScrollListener(this);
        if (searchHintId != 0) {
            searchEditText.setHint(searchHintId);
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        commonListMixin.unbind();
    }

    @OnItemClick(android.R.id.list)
    public void onValuePicked(int position) {
        ActivityUtil.hideKeyboard(getActivity());
        callOnValueChanged(adapter.getItem(position));
    }

    @OnEditorAction(R.id.searchEditText)
    public boolean onSearchClicked(int key) {
        if (EditorInfo.IME_ACTION_SEARCH == key) {
            String currentSearch = searchEditText.getText().toString();
            if (!currentSearch.equals(lastSearch)) {
                currentPage = 0;
                lastSearch = currentSearch;
                listView.setOnScrollListener(this);
            }
            ActivityUtil.hideKeyboard(getActivity());
            commonListMixin.setEmptyProgressView();
            currentAction = search(currentPage + 1);
            return true;
        }
        return false;
    }

    protected abstract int search(int currentPage);

    protected void setProgressFooterVisible(boolean isVisible) {
        progressFooter.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        int totalItemCount = listView.getAdapter().getCount();
        if (view.getLastVisiblePosition() == (totalItemCount - 1) && listView.getChildCount() > 0) {
            if (isLoading.compareAndSet(false, true)) {
                setProgressFooterVisible(true);
                currentAction = search(currentPage + 1);
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    //region Event handlers

    public void onEvent(RequestCompletedEvent<List<T>> event) {
        if (event.is(currentAction)) {
            currentAction = EventBusCallback.ACTION_NONE;
            adapter.addItems(event.getResult());
            int newItemsCount = event.getResult().size();
            currentPage++;
            commonListMixin.setEmptyMessageView();
            isLoading.set(false);
            setProgressFooterVisible(false);
            if (newItemsCount < WebClient.DEFAULT_PAGE_SIZE) {
                Log.d("PAGING", String.format("All entries loaded (%d < %d)", newItemsCount, WebClient.DEFAULT_PAGE_SIZE));
                listView.setOnScrollListener(null);
            }
        }
    }

    public void onEvent(NetworkErrorEvent event) {
        if (event.is(currentAction)) {
            if (isAdded()) {
                event.displayError(getActivity());
            }
            commonListMixin.setEmptyMessageView();
        }
    }

    //endregion
}