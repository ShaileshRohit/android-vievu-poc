package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.filtering.fragment.CategoriesConstraintFragment;
import com.vievu.vievusolution.net.WebClient;

public class CategoryFilterConstraint extends FilterConstraint<Category> {

    public CategoryFilterConstraint() {
        super(WebClient.Filters.CATEGORY,
              R.string.filter_type_category,
              R.drawable.ic_category,
              CategoriesConstraintFragment::new,
              R.id.filter_type_category);
    }

    @Override
    protected FilterConstraintValue<Category> createFilterConstraintValue(Category category) {
        return new CategoryFilterConstraintValue(category);
    }
}