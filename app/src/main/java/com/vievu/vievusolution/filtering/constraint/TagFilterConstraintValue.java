package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.net.WebClient;

public class TagFilterConstraintValue extends ComplexFilterConstraintValue<Tag> {

    public TagFilterConstraintValue(Tag value) {
        super(R.id.filter_type_tag, WebClient.Filters.TAG, value, R.string.filter_type_tag);
    }

    @Override
    protected String getNotNullQueryParam() {
        return String.valueOf(value.getTagId());
    }

    @Override
    protected String getNotNullReadableValue() {
        return value.getTagName();
    }

    @Override
    protected boolean compareValues(Tag otherValue) {
        return this.value.getTagId() == otherValue.getTagId();
    }
}