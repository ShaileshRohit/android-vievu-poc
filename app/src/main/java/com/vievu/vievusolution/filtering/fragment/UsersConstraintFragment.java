package com.vievu.vievusolution.filtering.fragment;

import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.UserAdapter;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;

public class UsersConstraintFragment extends SearchableListConstraintFragment<User> {

    @Override
    protected ArrayListAdapter<User, ?> createAdapter() {
        return new UserAdapter(getActivity(), null);
    }

    @Override
    protected int search(int currentPage) {
        int action = WebClient.Actions.unique(WebClient.Actions.USERS_FIND_BY_NAME);
        if (currentPage == WebClient.FIRST_PAGE_NUMBER) {
            adapter.swapItems(null);
        }
        String value = searchEditText.getText().toString();
        WebClient.request(currentPage).userBy(
                value.isEmpty() ? null : value,
                new EventBusCallback<>(action));
        return action;
    }
}
