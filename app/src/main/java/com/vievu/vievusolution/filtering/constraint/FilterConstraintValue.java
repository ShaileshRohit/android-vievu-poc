package com.vievu.vievusolution.filtering.constraint;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.Collection;

public class FilterConstraintValue<T> {

    protected int id;
    protected String key;
    protected T value;
    protected int readableName;

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getQueryParam() {
        return getReadableValue();
    }

    public int getReadableName() {
        return readableName;
    }

    public String getReadableValue() {
        return value != null ? value.toString() : "";
    }

    public FilterConstraintValue(int id, String key, T value, int readableName) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.readableName = readableName;
    }

    public FilterConstraintValue findDuplicate(Collection<FilterConstraintValue> constraints) {
        Optional<FilterConstraintValue> duplicate =
                Stream.of(constraints)
                        .filter(this::compareConstraints)
                        .findFirst();
        if (duplicate.isPresent()) {
            return duplicate.get();
        }
        return null;
    }

    protected boolean compareConstraints(FilterConstraintValue other) {
        return other != null && this.getKey().equals(other.getKey());
    }
}
