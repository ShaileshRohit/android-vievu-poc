package com.vievu.vievusolution.filtering.results;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.SparseArray;

import com.vievu.vievusolution.filtering.FilterResultsActivity;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.bean.LimitedArrayList;
import com.vievu.vievusolution.bean.UserRelatedBean;
import com.vievu.vievusolution.details.BaseDetailsActivity;
import com.vievu.vievusolution.filtering.FilterBundle;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.VievuService;
import com.vievu.vievusolution.net.WebClient;

import java.util.List;
import java.util.Map;

import retrofit.Callback;

public abstract class FilterResults<T extends UserRelatedBean> {

    protected interface WebRequest<T> {
        void apply(VievuService service, Map<String, String> queryParameters, Callback<LimitedArrayList<T>> callback);
    }

    private static SparseArray<FilterResults> results = new SparseArray<>();

    public static <T extends UserRelatedBean> FilterResults<T> resultsOf(int bundleId) {
        FilterResults item = results.get(bundleId);
        if (item == null) {
            switch (bundleId) {
                case FilterBundle.FILTER_BUNDLE_VIDEOS:
                    item = new VideoFilterResults();
                    break;
                case FilterBundle.FILTER_BUNDLE_CASES:
                    item = new CaseFilterResults();
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Unknown bundle id: %d", bundleId));
            }
            results.put(bundleId, item);
        }
        //noinspection unchecked
        return item;
    }

    private int titleId;
    private int webClientAction;
    private WebRequest<T> webRequest;

    public int getTitleId(Map<String, String> queryParameters) {
        return titleId;
    }

    public int load(int page, Map<String, String> queryParameters) {
        int action = WebClient.Actions.unique(webClientAction);
        webRequest.apply(WebClient.request(page), queryParameters, new EventBusCallback<>(action));
        return action;
    }

    protected FilterResults(
            int titleId,
            int webClientAction,
            WebRequest<T> webRequest) {

        this.titleId = titleId;
        this.webClientAction = webClientAction;
        this.webRequest = webRequest;
    }

    public abstract ArrayListAdapter<T, ?> createAdapter(Context context, List<T> initialValues);

    public abstract void handleItemClick(Activity context, T item);

    public void onActivityResult(FilterResultsActivity<T> activity, Intent data) {
        if (data != null && data.hasExtra(BaseDetailsActivity.EXTRA_ITEM)
                && activity.getSelectedItem() < activity.getAdapter().getCount()) {
            T item = data.getParcelableExtra(BaseDetailsActivity.EXTRA_ITEM);
            //noinspection unchecked
            item.copyTo(activity.getAdapter().getItem(activity.getSelectedItem()));
            activity.getAdapter().notifyDataSetChanged();
        }
    }
}
