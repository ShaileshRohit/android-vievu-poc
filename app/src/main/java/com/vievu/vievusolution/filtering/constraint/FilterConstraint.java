package com.vievu.vievusolution.filtering.constraint;

import com.annimon.stream.function.Function;
import com.vievu.vievusolution.filtering.fragment.ConstraintProvider;

import java.lang.ref.WeakReference;

public class FilterConstraint<TValue> implements ConstraintProvider.OnValueChangedListener<TValue> {

    public interface ProviderCreator<TValue> {
        ConstraintProvider<TValue> create();
    }

    public interface OnValueCreatedListener<TValue> {
        void onValueCreated(FilterConstraintValue<TValue> value);
    }

    protected int id;
    protected String key;
    protected int filterItemNameId;
    protected int filterIconId;
    protected ProviderCreator<TValue> providerCreator;
    protected Function<TValue, FilterConstraintValue<TValue>> valueCreator;
    protected WeakReference<ConstraintProvider<TValue>> constraintProviderReference;
    protected WeakReference<OnValueCreatedListener<TValue>> onValueCreatedListenerReference;

    public String getKey() {
        return key;
    }

    public int getFilterItemNameId() {
        return filterItemNameId;
    }

    public int getFilterIconId() {
        return filterIconId;
    }

    public void setOnValueCreatedListener(OnValueCreatedListener<TValue> onValueCreatedListener) {
        this.onValueCreatedListenerReference = new WeakReference<>(onValueCreatedListener);
    }

    public FilterConstraint(
            String key,
            int filterItemNameId,
            int filterIconId,
            ProviderCreator<TValue> providerCreator,
            int id) {
        this(key, filterItemNameId, filterIconId, providerCreator, null);
        this.id = id;
    }

    public FilterConstraint(
            String key,
            int filterItemNameId,
            int filterIconId,
            ProviderCreator<TValue> providerCreator,
            Function<TValue, FilterConstraintValue<TValue>> valueCreator) {

        this.id = filterItemNameId;
        this.key = key;
        this.filterItemNameId = filterItemNameId;
        this.filterIconId = filterIconId;
        this.providerCreator = providerCreator;
        this.valueCreator = valueCreator;
    }

    public ConstraintProvider<TValue> getConstraintProvider() {
        ConstraintProvider<TValue> constraintProvider = null;
        if (constraintProviderReference != null) {
            constraintProvider = constraintProviderReference.get();
        }
        if (constraintProvider == null) {
            constraintProvider = providerCreator.create();
            constraintProviderReference = new WeakReference<>(constraintProvider);
            constraintProvider.setOnValueChangedListener(this);
        }
        return constraintProvider;
    }

    @Override
    public void onValueChanged(TValue value) {
        OnValueCreatedListener<TValue> onValueCreatedListener =
            onValueCreatedListenerReference == null ? null : onValueCreatedListenerReference.get();
        if (onValueCreatedListener != null) {
            onValueCreatedListener.onValueCreated(createFilterConstraintValue(value));
        }
    }

    protected FilterConstraintValue<TValue> createFilterConstraintValue(TValue value) {
        if (valueCreator != null) {
            return valueCreator.apply(value);
        } else {
            return new FilterConstraintValue<>(id, key, value, filterItemNameId);
        }
    }
}
