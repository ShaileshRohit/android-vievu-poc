package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.net.WebClient;

public class UserFilterConstraintValue extends ComplexFilterConstraintValue<User> {

    public UserFilterConstraintValue(User value) {
        super(R.id.filter_type_users, WebClient.Filters.USERS, value, R.string.text_user);
    }

    @Override
    protected String getNotNullQueryParam() {
        return String.valueOf(value.getUserId());
    }

    @Override
    protected String getNotNullReadableValue() {
        return value.getUserName();
    }

    @Override
    protected boolean compareValues(User otherValue) {
        return this.value.getUserId() == otherValue.getUserId();
    }
}
