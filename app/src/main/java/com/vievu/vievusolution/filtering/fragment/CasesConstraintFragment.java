package com.vievu.vievusolution.filtering.fragment;

import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;

import java.util.HashMap;
import java.util.Map;

public class CasesConstraintFragment extends SearchableListConstraintFragment<Case> {

    @Override
    protected ArrayListAdapter<Case, ?> createAdapter() {
        return new OneLineArrayListAdapter<>(getActivity(), null, Case::getCaseName);
    }

    @Override
    protected int search(int currentPage) {
        if (currentPage == WebClient.FIRST_PAGE_NUMBER) {
            adapter.swapItems(null);
        }
        int action = WebClient.Actions.unique(WebClient.Actions.CASES_FIND);
        Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put(WebClient.Filters.NAME, searchEditText.getText().toString());
        WebClient.request(currentPage).cases(queryParameters, new EventBusCallback<>(action));
        return action;
    }
}
