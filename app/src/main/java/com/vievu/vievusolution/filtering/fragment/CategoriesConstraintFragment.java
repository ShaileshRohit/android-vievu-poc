package com.vievu.vievusolution.filtering.fragment;

import android.widget.AbsListView;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.data.CategoriesAsyncRepository;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.WebClient;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

public class CategoriesConstraintFragment extends SearchableListConstraintFragment<Category> {

    @Override
    protected ArrayListAdapter<Category, ?> createAdapter() {
        return new OneLineArrayListAdapter<>(getActivity(), null, Category::getCategoryName);
    }

    @Override
    protected int search(int currentPage) {
        CategoriesAsyncRepository.getInstance().getDataAsync(this::updateAdapter);
        return WebClient.Actions.LOAD_CATEGORIES;
    }

    private void updateAdapter(Collection<Category> items) {
        commonListMixin.setEmptyMessageView();
        if (items != null) {
            Pattern pattern = Pattern.compile(
                searchEditText.getText().toString(),
                Pattern.CASE_INSENSITIVE | Pattern.LITERAL);
            adapter.swapItems(
                Stream.of(items)
                    .filter(category -> pattern.matcher(category.getCategoryName()).find())
                    .collect(Collectors.toList()));
        } else if (isAdded()) {
            getVievuActivity().shortToast(R.string.error_cannot_load_categories);
        }
    }

    @Override
    public void onEvent(RequestCompletedEvent<List<Category>> event) {

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // Just suppress base handler
    }
}
