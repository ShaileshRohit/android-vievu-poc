package com.vievu.vievusolution.filtering.results;

import android.app.Activity;
import android.content.Context;

import com.vievu.vievusolution.details.cases.CaseDetailsActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.CaseAdapter;
import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.net.VievuService;
import com.vievu.vievusolution.net.WebClient;

import java.util.List;

public class CaseFilterResults extends FilterResults<Case> {

    protected CaseFilterResults() {
        super(
            R.string.title_activity_filter_cases,
            WebClient.Actions.CASES_FIND,
            VievuService::cases);
    }

    @Override
    public ArrayListAdapter<Case, ?> createAdapter(Context context, List<Case> initialValues) {
        return new CaseAdapter(context, initialValues);
    }

    @Override
    public void handleItemClick(Activity context, Case item) {
        CaseDetailsActivity.start(context, item);
    }
}
