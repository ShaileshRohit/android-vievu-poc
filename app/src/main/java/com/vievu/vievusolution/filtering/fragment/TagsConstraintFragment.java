package com.vievu.vievusolution.filtering.fragment;

import android.widget.AbsListView;
import android.widget.Toast;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.data.TagsAsyncRepository;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.WebClient;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

public class TagsConstraintFragment extends SearchableListConstraintFragment<Tag> {

    @Override
    protected ArrayListAdapter<Tag, ?> createAdapter() {
        return new OneLineArrayListAdapter<>(getActivity(), null, Tag::getTagName);
    }

    @Override
    protected int search(int currentPage) {
        TagsAsyncRepository.getInstance().getDataAsync(this::updateAdapter);
        return WebClient.Actions.LOAD_TAGS;
    }

    private void updateAdapter(Collection<Tag> items) {
        commonListMixin.setEmptyMessageView();
        if (items != null) {
            Pattern pattern = Pattern.compile(
                    searchEditText.getText().toString(),
                    Pattern.CASE_INSENSITIVE | Pattern.LITERAL);
            adapter.swapItems(
                    Stream.of(items)
                            .filter(tag -> pattern.matcher(tag.getTagName()).find())
                            .collect(Collectors.toList()));
        } else if (isAdded()) {
            getVievuActivity().shortToast(R.string.error_cannot_load_tags);
        }
    }

    @Override
    public void onEvent(RequestCompletedEvent<List<Tag>> event) {

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // Just suppress base handler
    }
}
