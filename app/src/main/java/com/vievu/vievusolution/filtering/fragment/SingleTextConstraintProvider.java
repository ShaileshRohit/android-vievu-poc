package com.vievu.vievusolution.filtering.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.ui.ClearableEditText;
import com.vievu.vievusolution.util.ActivityUtil;
import com.vievu.vievusolution.util.ValidationUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class SingleTextConstraintProvider extends ConstraintProvider<String> {

    @Bind(R.id.searchEditText)
    ClearableEditText searchEditText;

    @Bind(R.id.errorTextView)
    TextView errorTextView;

    private boolean isWatcherAdded = false;
    private TextWatcher validationWatcher = new ValidationUtil.SimpleWatcher(){

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() > 0) {
                errorTextView.setVisibility(View.GONE);
                if (isWatcherAdded){
                    isWatcherAdded = false;
                    searchEditText.removeTextChangedListener(validationWatcher);
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single_text_constraint, container, false);
        ButterKnife.bind(this, view);
        searchEditText.requestFocus();
        return view;
    }

    @OnEditorAction(R.id.searchEditText)
    boolean onEditorAction(int key) {
        if (EditorInfo.IME_ACTION_DONE == key) {
            addSearchConstraint();
            return true;
        }
        return false;
    }

    @OnClick(R.id.addButton)
    void addSearchConstraint() {
        if (searchEditText.getText().length() > 0) {
            ActivityUtil.hideKeyboard(getActivity());
            if (onValueChangedListener != null) {
                if (callOnValueChanged(searchEditText.getText().toString())) {
                    searchEditText.setText(null);
                    searchEditText.setError(null);
                }
            }
        } else {
            errorTextView.setVisibility(View.VISIBLE);
            if (!isWatcherAdded) {
                isWatcherAdded = true;
                searchEditText.addTextChangedListener(validationWatcher);
            }
        }
    }
}