package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.net.WebClient;

public class CaseFilterConstrainValue extends ComplexFilterConstraintValue<Case> {

    public CaseFilterConstrainValue(Case value) {
        super(R.id.filter_type_cases, WebClient.Filters.CASES, value, R.string.text_case);
    }

    @Override
    protected String getNotNullQueryParam() {
        return String.valueOf(value.getCaseId());
    }

    @Override
    protected String getNotNullReadableValue() {
        return value.getCaseName();
    }

    @Override
    protected boolean compareValues(Case otherValue) {
        return value.getCaseId() == otherValue.getCaseId();
    }
}
