package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.net.WebClient;

public class CategoryFilterConstraintValue extends ComplexFilterConstraintValue<Category> {

    public CategoryFilterConstraintValue(Category value) {
        super(R.id.filter_type_category, WebClient.Filters.CATEGORY, value, R.string.filter_type_category);
    }

    @Override
    protected String getNotNullQueryParam() {
        return String.valueOf(value.getCategoryId());
    }

    @Override
    protected String getNotNullReadableValue() {
        return value.getCategoryName();
    }

    @Override
    protected boolean compareValues(Category otherValue) {
        return this.value.getCategoryId() == otherValue.getCategoryId();
    }
}
