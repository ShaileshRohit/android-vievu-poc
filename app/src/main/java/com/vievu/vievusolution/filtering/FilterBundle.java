package com.vievu.vievusolution.filtering;

import android.util.SparseArray;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.filtering.constraint.CaseFilterConstrainValue;
import com.vievu.vievusolution.filtering.constraint.CaseStatusFilterConstrainValue;
import com.vievu.vievusolution.filtering.constraint.CategoryFilterConstraint;
import com.vievu.vievusolution.filtering.constraint.CreationDateFilterConstraintValue;
import com.vievu.vievusolution.filtering.constraint.FilterConstraint;
import com.vievu.vievusolution.filtering.constraint.IncidentDateFilterConstraintValue;
import com.vievu.vievusolution.filtering.constraint.IntWrapperConstraint;
import com.vievu.vievusolution.filtering.constraint.UnitFilterConstrainValue;
import com.vievu.vievusolution.filtering.constraint.RecordDateFilterConstraintValue;
import com.vievu.vievusolution.filtering.constraint.TagFilterConstraint;
import com.vievu.vievusolution.filtering.constraint.UploadDateFilterConstraintValue;
import com.vievu.vievusolution.filtering.constraint.UserFilterConstraint;
import com.vievu.vievusolution.filtering.fragment.CaseStatusConstraintFragment;
import com.vievu.vievusolution.filtering.fragment.CasesConstraintFragment;
import com.vievu.vievusolution.filtering.fragment.DateConstraintFragment;
import com.vievu.vievusolution.filtering.fragment.UnitsConstraintFragment;
import com.vievu.vievusolution.filtering.fragment.SingleTextConstraintProvider;
import com.vievu.vievusolution.net.WebClient;

public class FilterBundle {

    public static final int FILTER_BUNDLE_VIDEOS = R.string.title_activity_filter_videos;
    public static final int FILTER_BUNDLE_CASES = R.string.title_activity_filter_cases;

    private static SparseArray<FilterBundle> bundles = new SparseArray<>();

    public static FilterBundle create(int bundleId) {
        FilterBundle bundle = bundles.get(bundleId);
        if (bundle == null) {
            SparseArray<FilterConstraint> constraints;
            switch (bundleId) {
                case FILTER_BUNDLE_VIDEOS:
                    constraints = new SparseArray<>(10);
                    constraints.put(R.id.filter_type_users, new UserFilterConstraint());
                    constraints.put(R.id.filter_type_record_date, new FilterConstraint<>(
                            WebClient.Filters.RECORD_DATE_FROM,
                            R.string.filter_type_record_date,
                            R.drawable.ic_calendar,
                            DateConstraintFragment::new,
                            RecordDateFilterConstraintValue::new
                    ));
                    constraints.put(R.id.filter_type_upload_date, new FilterConstraint<>(
                            WebClient.Filters.UPLOAD_DATE_FROM,
                            R.string.filter_type_upload_date,
                            R.drawable.ic_calendar,
                            DateConstraintFragment::new,
                            UploadDateFilterConstraintValue::new
                    ));
                    constraints.put(R.id.filter_type_category, new CategoryFilterConstraint());
                    constraints.put(R.id.filter_type_tag, new TagFilterConstraint());
                    constraints.put(R.id.filter_type_cases, new FilterConstraint<>(
                            WebClient.Filters.CASES,
                            R.string.filter_type_cases,
                            R.drawable.ic_case_folder,
                            CasesConstraintFragment::new,
                            CaseFilterConstrainValue::new
                    ));
                    constraints.put(R.id.filter_type_report, new FilterConstraint<>(
                            WebClient.Filters.REPORT_NUMBER,
                            R.string.filter_type_report,
                            R.drawable.ic_report,
                            SingleTextConstraintProvider::new,
                            R.id.filter_type_report
                    ));
                    constraints.put(R.id.filter_type_event_number, new FilterConstraint<>(
                            WebClient.Filters.EVENT_NUMBER,
                            R.string.filter_type_event_number,
                            R.drawable.ic_report,
                            SingleTextConstraintProvider::new,
                            R.id.filter_type_event_number
                    ));
                    constraints.put(R.id.filter_type_units, new FilterConstraint<>(
                            WebClient.Filters.LOCATIONS,
                            R.string.filter_type_units,
                            R.drawable.ic_units,
                            UnitsConstraintFragment::new,
                            UnitFilterConstrainValue::new
                    ));
                    constraints.put(R.id.filter_type_lockdown, new IntWrapperConstraint(
                            WebClient.Filters.LOCKDOWN,
                            R.string.filter_type_lockdown,
                            R.drawable.ic_lockdown,
                            R.array.filter_spinner_values_lockdown,
                            R.array.filter_picked_values_yes_no,
                            R.id.filter_type_lockdown
                    ));
                    constraints.put(R.id.filter_type_never_delete, new IntWrapperConstraint(
                            WebClient.Filters.NEVER_DELETE,
                            R.string.filter_type_never_delete,
                            R.drawable.ic_never_delete,
                            R.array.filter_spinner_values_never_delete,
                            R.array.filter_picked_values_yes_no,
                            R.id.filter_type_never_delete
                    ));
                    constraints.put(R.id.filter_type_filename, new FilterConstraint<>(
                            WebClient.Filters.FILE_NAME,
                            R.string.filter_type_filename,
                            R.drawable.ic_filename,
                            SingleTextConstraintProvider::new,
                            R.id.filter_type_filename
                    ));
                    constraints.put(R.id.filter_type_comment, new FilterConstraint<>(
                            WebClient.Filters.COMMENT,
                            R.string.filter_type_comment,
                            R.drawable.ic_comment,
                            SingleTextConstraintProvider::new,
                            R.id.filter_type_comment
                    ));
                    constraints.put(R.id.filter_type_shared, new IntWrapperConstraint(
                            WebClient.Filters.SHARED,
                            R.string.filter_type_shared,
                            R.drawable.ic_shared,
                            R.array.filter_spinner_values_shared_videos,
                            R.id.filter_type_shared
                    ));
                    bundle = new FilterBundle(
                            FILTER_BUNDLE_VIDEOS,
                            constraints,
                            new int[] {
                                    R.id.filter_type_users,
                                    R.id.filter_type_record_date,
                                    R.id.filter_type_upload_date,
                                    R.id.filter_type_category,
                                    R.id.filter_type_tag,
                                    R.id.filter_type_cases,
                                    R.id.filter_type_report,
                                    R.id.filter_type_event_number,
                                    R.id.filter_type_units,
                                    R.id.filter_type_lockdown,
                                    R.id.filter_type_never_delete,
                                    R.id.filter_type_filename,
                                    R.id.filter_type_comment,
                                    R.id.filter_type_shared
                            });
                    break;
                case FILTER_BUNDLE_CASES:
                    constraints = new SparseArray<>(10);
                    constraints.put(R.id.filter_type_users, new UserFilterConstraint());
                    constraints.put(R.id.filter_type_cases, new FilterConstraint<>(
                            WebClient.Filters.CASES,
                            R.string.filter_type_cases,
                            R.drawable.ic_case_folder,
                            CasesConstraintFragment::new,
                            CaseFilterConstrainValue::new
                    ));
                    constraints.put(R.id.filter_type_report, new FilterConstraint<>(
                            WebClient.Filters.REPORT_NUMBER,
                            R.string.filter_type_report,
                            R.drawable.ic_report,
                            SingleTextConstraintProvider::new,
                            R.id.filter_type_report
                    ));
                    constraints.put(R.id.filter_type_event_number, new FilterConstraint<>(
                            WebClient.Filters.EVENT_NUMBER,
                            R.string.filter_type_event_number,
                            R.drawable.ic_report,
                            SingleTextConstraintProvider::new,
                            R.id.filter_type_event_number
                    ));
                    constraints.put(R.id.filter_type_case_status, new FilterConstraint<>(
                            WebClient.Filters.CASE_STATUSES,
                            R.string.filter_type_case_status,
                            R.drawable.ic_case_status,
                            CaseStatusConstraintFragment::new,
                            CaseStatusFilterConstrainValue::new
                    ));
                    constraints.put(R.id.filter_type_tag, new TagFilterConstraint());
                    constraints.put(R.id.filter_type_incident_date, new FilterConstraint<>(
                            WebClient.Filters.INCIDENT_DATE_FROM,
                            R.string.filter_type_incident_date,
                            R.drawable.ic_calendar,
                            DateConstraintFragment::new,
                            IncidentDateFilterConstraintValue::new
                    ));
                    constraints.put(R.id.filter_type_creation_date, new FilterConstraint<>(
                            WebClient.Filters.CREATION_DATE_FROM,
                            R.string.filter_type_creation_date,
                            R.drawable.ic_calendar,
                            DateConstraintFragment::new,
                            CreationDateFilterConstraintValue::new
                    ));
                    constraints.put(R.id.filter_type_summary, new FilterConstraint<>(
                            WebClient.Filters.SUMMARY,
                            R.string.filter_type_case_summary,
                            R.drawable.ic_filename,
                            SingleTextConstraintProvider::new,
                            R.id.filter_type_summary
                    ));
                    constraints.put(R.id.filter_type_comment, new FilterConstraint<>(
                            WebClient.Filters.COMMENT,
                            R.string.filter_type_comment,
                            R.drawable.ic_comment,
                            SingleTextConstraintProvider::new,
                            R.id.filter_type_comment
                    ));
                    constraints.put(R.id.filter_type_shared, new IntWrapperConstraint(
                            WebClient.Filters.SHARED,
                            R.string.filter_type_shared,
                            R.drawable.ic_shared,
                            R.array.filter_spinner_values_shared_cases,
                            R.id.filter_type_shared
                    ));
                    bundle = new FilterBundle(
                            FILTER_BUNDLE_CASES,
                            constraints,
                            new int[] {
                                    R.id.filter_type_users,
                                    R.id.filter_type_cases,
                                    R.id.filter_type_report,
                                    R.id.filter_type_event_number,
                                    R.id.filter_type_case_status,
                                    R.id.filter_type_tag,
                                    R.id.filter_type_incident_date,
                                    R.id.filter_type_creation_date,
                                    R.id.filter_type_summary,
                                    R.id.filter_type_comment,
                                    R.id.filter_type_shared
                            });
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Unknown bundle id: %d", bundleId));
            }
            bundles.put(bundleId, bundle);
        }
        return bundle;
    }

    protected int bundleId;
    protected int[] orderedIds;
    protected SparseArray<FilterConstraint> constraints;

    public int getBundleId() {
        return bundleId;
    }

    public int[] getOrderedIds() {
        return orderedIds;
    }

    public FilterConstraint getConstraint(int id) {
        return constraints.get(id);
    }

    protected FilterBundle(int bundleId, SparseArray<FilterConstraint> constraints, int[] orderedIds) {
        this.bundleId = bundleId;
        this.orderedIds = orderedIds;
        this.constraints = constraints;
    }
}
