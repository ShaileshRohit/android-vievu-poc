package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.filtering.fragment.TagsConstraintFragment;
import com.vievu.vievusolution.net.WebClient;

public class TagFilterConstraint extends FilterConstraint<Tag> {

    public TagFilterConstraint() {
        super(WebClient.Filters.TAG,
                R.string.filter_type_tag,
                R.drawable.ic_tag,
                TagsConstraintFragment::new,
                R.id.filter_type_tag);
    }

    @Override
    protected FilterConstraintValue<Tag> createFilterConstraintValue(Tag tag) {
        return new TagFilterConstraintValue(tag);
    }
}