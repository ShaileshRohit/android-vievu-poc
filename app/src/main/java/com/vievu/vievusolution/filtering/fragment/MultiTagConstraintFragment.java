package com.vievu.vievusolution.filtering.fragment;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ListView;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.MultiTagAdapter;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.data.TagsAsyncRepository;
import com.vievu.vievusolution.ui.ClearableEditText;
import com.vievu.vievusolution.ui.CommonListMixin;
import com.vievu.vievusolution.ui.VievuCheckedTextView;
import com.vievu.vievusolution.util.ActivityUtil;

import java.util.List;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;

public class MultiTagConstraintFragment extends ConstraintProvider<List<Tag>> implements /*AdapterView.OnItemClickListener, */VievuCheckedTextView.OnCheckedChangedListener {

    public static MultiTagConstraintFragment create(List<Tag> selectedItems) {
        MultiTagConstraintFragment fragment = new MultiTagConstraintFragment();
        fragment.selectedItems = new SparseBooleanArray(selectedItems.size());
        for (Tag tag : selectedItems) {
            fragment.selectedItems.append(tag.getTagId(), true);
        }
        return fragment;
    }

    @Bind(R.id.searchEditText) ClearableEditText searchEditText;
    protected ListView listView;
    protected CommonListMixin commonListMixin;

    protected ArrayListAdapter<Tag, ?> adapter;

    protected SparseBooleanArray selectedItems;

    public MultiTagConstraintFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_searchable_list_constraint, container, false);
        ButterKnife.bind(this, view);
        commonListMixin = CommonListMixin.create(view);
        listView = commonListMixin.getListView();
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        searchEditText.setHint(R.string.text_tags);
        searchEditText.setEnabled(false);
        adapter = new MultiTagAdapter(getActivity(), null, this);
        listView.setAdapter(adapter);
        TagsAsyncRepository.getInstance().getDataAsync(data -> {
            onDataChanged(data);
            searchEditText.setEnabled(true);
        });
        return view;
    }

    private void onDataChanged(List<Tag> data) {
        listView.clearChoices();
        adapter.replaceItems(data);
        for (int i = 0; i < data.size(); i++) {
            if (selectedItems.get(data.get(i).getTagId())) {
                listView.setItemChecked(i, true);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit, menu);
        menu.findItem(R.id.action_save).setEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            ActivityUtil.hideKeyboard(getActivity());
            callOnValueChanged(
                Stream.of(TagsAsyncRepository.getInstance().getData())
                    .filter(tag -> selectedItems.get(tag.getTagId()))
                    .collect(Collectors.toList()));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        commonListMixin.unbind();
    }

    @OnEditorAction(R.id.searchEditText)
    public boolean onSearchClicked(int key) {
        if (EditorInfo.IME_ACTION_SEARCH == key) {
            final String currentSearch = searchEditText.getText().toString();
            ActivityUtil.hideKeyboard(getActivity());
            if (!currentSearch.isEmpty()) {
                Pattern pattern = Pattern.compile(currentSearch, Pattern.CASE_INSENSITIVE | Pattern.LITERAL);
                onDataChanged(
                    Stream.of(TagsAsyncRepository.getInstance().getData())
                        .filter(tag -> pattern.matcher(tag.getTagName()).find())
                        .collect(Collectors.toList()));
            } else {
                onDataChanged(TagsAsyncRepository.getInstance().getData());
            }
            return true;
        }
        return false;
    }

    @Override
    public void onCheckedChanged(VievuCheckedTextView view) {
        selectedItems.put(((MultiTagAdapter.ViewHolder) view.getTag()).getId(), view.isChecked());
    }
}
