package com.vievu.vievusolution.filtering.fragment;

import com.vievu.vievusolution.BaseVievuFragment;

public abstract class ConstraintProvider<T> extends BaseVievuFragment {

    public interface OnValueChangedListener<T> {
        void onValueChanged(T value);
    }

    protected OnValueChangedListener<T> onValueChangedListener;

    public OnValueChangedListener getOnValueChangedListener() {
        return onValueChangedListener;
    }

    public void setOnValueChangedListener(OnValueChangedListener<T> onValueChangedListener) {
        this.onValueChangedListener = onValueChangedListener;
    }

    public boolean callOnValueChanged(T newValue) {
        if (this.onValueChangedListener != null) {
            this.onValueChangedListener.onValueChanged(newValue);
            return true;
        }
        return false;
    }
}
