package com.vievu.vievusolution.filtering;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.EventBusActivity;
import com.vievu.vievusolution.MainActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.bean.LimitedArrayList;
import com.vievu.vievusolution.bean.UserRelatedBean;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.PingCompletedEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.filtering.results.FilterResults;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ui.CommonListMixin;
import com.vievu.vievusolution.util.ActivityUtil;

import java.net.UnknownHostException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnItemClick;

public class FilterResultsActivity<T extends UserRelatedBean> extends EventBusActivity implements AbsListView.OnScrollListener {

    public static final String EXTRA_QUERY_PARAMETERS = "query_params";

    public static void start(Activity caller, int filterBundleId, Map<String, String> queryParameters) {
        Intent intent = new Intent(caller, FilterResultsActivity.class);
        ActivityUtil.putMapExtra(intent, FilterResultsActivity.EXTRA_QUERY_PARAMETERS, queryParameters);
        intent.putExtra(CommonConstants.EXTRA_FILTER_BUNDLE_ID, filterBundleId);
        caller.startActivity(intent);
    }

    protected ListView listView;
    protected CommonListMixin commonListMixin;
    @Bind(R.id.progressFooter) View progressFooter;
    @Bind(R.id.errorOverlayLayout) View errorOverlayLayout;
    private TextView footer;

    private ArrayListAdapter<T, ?> adapter;
    private Map<String, String> queryParameters;
    private FilterResults<T> filterBundleResults;

    private int currentPage = 0;
    private AtomicBoolean isLoading = new AtomicBoolean(false);

    private int selectedItem;

    public int getSelectedItem() {
        return selectedItem;
    }

    public ArrayListAdapter<T, ?> getAdapter() {
        return adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_results);
        ButterKnife.bind(this);
        errorOverlayLayout.setVisibility(View.GONE);
        commonListMixin = CommonListMixin.create(this);
        listView = commonListMixin.getListView();
        queryParameters = ActivityUtil.getMapExtra(
                savedInstanceState != null ?
                        savedInstanceState :
                        getIntent().getExtras(),
                EXTRA_QUERY_PARAMETERS,
                null);
        int bundleId = getIntent().getIntExtra(CommonConstants.EXTRA_FILTER_BUNDLE_ID, 0);
        filterBundleResults = FilterResults.resultsOf(bundleId);
        setTitle(filterBundleResults.getTitleId(queryParameters));
        commonListMixin.setEmptyProgressView();
        adapter = filterBundleResults.createAdapter(this, null);
        footer = (TextView) getLayoutInflater().inflate(R.layout.view_part_search_results_footer, null);
        listView.addFooterView(footer);
        listView.setAdapter(adapter);
        listView.setOnScrollListener(this);
        currentAction = WebClient.Actions.unique(0);
        WebClient.pingAsync(currentAction);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_menu) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnItemClick(android.R.id.list)
    void onItemClick(int position) {
        // Check added because this handler is also called for footer view on 4.4.4 and above
        if (position < adapter.getCount()) {
            this.selectedItem = position;
            filterBundleResults.handleItemClick(this, adapter.getItem(position));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        filterBundleResults.onActivityResult(this, data);
    }

    public void onEvent(RequestCompletedEvent<LimitedArrayList<T>> event) {
        if (event.getAction() == currentAction && !keepEvent(event)) {
            setErrorOverlayVisible(false);
            adapter.addItems(event.getResult());
            currentPage++;
            commonListMixin.setEmptyMessageView();
            isLoading.set(false);
            setProgressFooterVisible(false);
            int limit = event.getResult().getLimit();
            if (adapter.getCount() == limit) {
                listView.setOnScrollListener(null);
                footer.setText(getResources().getQuantityString(
                        R.plurals.template_results_footer, limit, limit));
            }
        }
    }

    @Override
    public void onEvent(NetworkErrorEvent event) {
        if (event.is(currentAction)) {
            commonListMixin.setEmptyMessageView();
        }
        super.onEvent(event);
    }

    public void onEvent(PingCompletedEvent event) {
        if (event.getAction() == currentAction) {
            if (event.isConnected()) {
                currentAction = filterBundleResults.load(WebClient.FIRST_PAGE_NUMBER, queryParameters);
            } else {
                setErrorOverlayVisible(true);
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        int totalItemCount = listView.getAdapter().getCount();
        if (view.getLastVisiblePosition() == (totalItemCount - 1) && listView.getChildCount() > 0) {
            if (isLoading.compareAndSet(false, true)) {
                setProgressFooterVisible(true);
                currentAction = filterBundleResults.load(currentPage + 1, queryParameters);
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    private void setProgressFooterVisible(boolean isVisible) {
        progressFooter.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    private void setErrorOverlayVisible(boolean isVisible) {
        if (isVisible) {
            errorOverlayLayout.setVisibility(View.VISIBLE);
            if (errorOverlayLayout.getTag() == null) {
                GestureDetector gestureDetector = new GestureDetector(this, new GestureListener());
                ButterKnife.<ImageView>findById(errorOverlayLayout, R.id.errorImageView).setImageResource(R.drawable.no_wifi);
                TextView errorTextView = ButterKnife.<TextView>findById(errorOverlayLayout, R.id.errorTextView);
                errorTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_primary));
                errorTextView.setText(R.string.error_no_internet);
                ButterKnife.<TextView>findById(errorOverlayLayout, R.id.tapsHintTextView).setText(R.string.text_taps_no_camera);
                errorOverlayLayout.setOnTouchListener((v, event) -> {
                    gestureDetector.onTouchEvent(event);
                    return true;
                });
                errorOverlayLayout.setTag(true);
            }
        } else {
            errorOverlayLayout.setVisibility(View.GONE);
        }
        commonListMixin.setVisible(!isVisible);
    }

    private void retryFiltering() {
        commonListMixin.setEmptyProgressView();
        setErrorOverlayVisible(false);
        adapter = filterBundleResults.createAdapter(this, null);
        listView.setAdapter(adapter);
        currentAction = WebClient.Actions.unique(0);
        WebClient.pingAsync(currentAction);
        currentPage = 0;
        isLoading.set(false);
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            retryFiltering();
            return super.onSingleTapConfirmed(e);
        }


        @Override
        public boolean onDoubleTap(MotionEvent e) {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            return true;
        }
    }
}
