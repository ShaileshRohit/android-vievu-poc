package com.vievu.vievusolution.filtering;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.annimon.stream.Stream;
import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.EventBusActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.collector.GroupByReduceCollector;
import com.vievu.vievusolution.collector.JoinWithSeparatorCollector;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.filtering.constraint.FilterConstraint;
import com.vievu.vievusolution.filtering.constraint.FilterConstraintValue;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ui.SearchConstraintsFragment;
import com.vievu.vievusolution.util.ActivityUtil;

import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class FilterActivity extends EventBusActivity implements RadioGroup.OnCheckedChangeListener, FilterConstraint.OnValueCreatedListener {

    public static void start(Context caller, int filterBundleId) {
        Intent intent = new Intent(caller, FilterActivity.class);
        intent.putExtra(CommonConstants.EXTRA_FILTER_BUNDLE_ID, filterBundleId);
        caller.startActivity(intent);
    }

    private static SparseIntArray layoutMapping = new SparseIntArray(2);
    static {
        layoutMapping.put(FilterBundle.FILTER_BUNDLE_VIDEOS, R.layout.activity_video_filter);
        layoutMapping.put(FilterBundle.FILTER_BUNDLE_CASES, R.layout.activity_case_filter);
    }

    @Bind(R.id.filterTabs) RadioGroup filterTabs;

    @Nullable
    @Bind(R.id.fileTypeFilterSegment)
    RadioGroup fileTypeFilterSegment;

    private FilterBundle filterBundle;
    private SearchConstraintsFragment searchConstraintsFragment;

    private MenuItem searchItem;

    public FilterBundle getFilterBundle() {
        return filterBundle;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int bundleId = getIntent().getIntExtra(CommonConstants.EXTRA_FILTER_BUNDLE_ID, 0);

        setContentView(layoutMapping.get(bundleId));
        ButterKnife.bind(this);

        filterBundle = FilterBundle.create(bundleId);
        RadioButton radioButton;
        FilterConstraint constraint;
        for (int id : filterBundle.getOrderedIds()) {
            radioButton = (RadioButton) getLayoutInflater().
                    inflate(R.layout.view_part_filter_tab_item, filterTabs, false);
            constraint = filterBundle.getConstraint(id);
            radioButton.setText(constraint.getFilterItemNameId());
            radioButton.setCompoundDrawablesWithIntrinsicBounds(0, constraint.getFilterIconId(), 0, 0);
            radioButton.setId(id);
            filterTabs.addView(radioButton);
        }
        filterTabs.setOnCheckedChangeListener(this);
        ((RadioButton) filterTabs.getChildAt(0)).setChecked(true);
        searchConstraintsFragment = (SearchConstraintsFragment) getFragmentManager().findFragmentById(R.id.searchConstraintsFragment);

        if (fileTypeFilterSegment != null) {
            fileTypeFilterSegment.check(R.id.fileTypeFilterAll);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        searchItem = menu.findItem(R.id.action_search);
        searchItem.setEnabled(isInOnlineMode());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.action_search:
                Map<String, String> queryParameters = Stream
                        .of(searchConstraintsFragment.getConstraints())
                        .collect(new GroupByReduceCollector<>(
                                FilterConstraintValue::getKey,
                                new JoinWithSeparatorCollector<>(",", FilterConstraintValue::getQueryParam)
                        ));
                if (fileTypeFilterSegment != null) {
                    int currentFileType = fileTypeFilterSegment.getCheckedRadioButtonId();
                    if (currentFileType == R.id.fileTypeFilterVideos) {
                        queryParameters.put(WebClient.Filters.FILE_TYPE, WebClient.Filters.FileType.VIDEOS);
                    } else if (currentFileType == R.id.fileTypeFilterFiles) {
                        queryParameters.put(WebClient.Filters.FILE_TYPE, WebClient.Filters.FileType.FILES);
                    }
                }
                FilterResultsActivity.start(this, filterBundle.getBundleId(), queryParameters);
                return true;
            case android.R.id.home :
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        FilterConstraint<?> filterConstraint = filterBundle.getConstraint(checkedId);
        //noinspection unchecked
        filterConstraint.setOnValueCreatedListener(this);
        ActivityUtil.replaceFragment(this,
                filterConstraint.getConstraintProvider(), false, R.id.searchFragmentContainer);
    }

    @Override
    public void onValueCreated(FilterConstraintValue value) {
        searchConstraintsFragment.addConstraint(value);
    }

    public void onEvent(OfflineModeChangedEvent event) {
        if (searchItem != null) {
            searchItem.setEnabled(true);
            searchItem.setEnabled(event.isInOnlineMode());
        }
    }
}
