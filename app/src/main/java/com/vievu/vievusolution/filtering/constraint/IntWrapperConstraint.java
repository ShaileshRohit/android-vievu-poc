package com.vievu.vievusolution.filtering.constraint;

import android.support.annotation.ArrayRes;

import com.vievu.vievusolution.bean.IntWrapper;
import com.vievu.vievusolution.filtering.fragment.SpinnerConstraintFragment;

public class IntWrapperConstraint extends FilterConstraint<IntWrapper> {

    public IntWrapperConstraint(
            String key,
            int filterItemNameId,
            int filterIconId,
            @ArrayRes int spinnerValuesArrayId,
            int id) {

        super(key, filterItemNameId, filterIconId,
                () -> SpinnerConstraintFragment.create(spinnerValuesArrayId, spinnerValuesArrayId), id);
    }

    public IntWrapperConstraint(
            String key,
            int filterItemNameId,
            int filterIconId,
            @ArrayRes int spinnerValuesArrayId,
            @ArrayRes int pickedValuesArrayId,
            int id) {

        super(key, filterItemNameId, filterIconId,
            () -> SpinnerConstraintFragment.create(spinnerValuesArrayId, pickedValuesArrayId), id);
    }

    @Override
    protected FilterConstraintValue<IntWrapper> createFilterConstraintValue(IntWrapper value) {
        return new IntWrapperConstraintValue(id, key, value, filterItemNameId);
    }
}
