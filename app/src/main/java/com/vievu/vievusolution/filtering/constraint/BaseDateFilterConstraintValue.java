package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.bean.DateWrapper;
import com.vievu.vievusolution.util.DateUtil;

public abstract class BaseDateFilterConstraintValue extends FilterConstraintValue<DateWrapper> {

    public BaseDateFilterConstraintValue(int id, String key, DateWrapper value, int readableName) {
        super(id, key, value, readableName);
    }

    @Override
    public String getReadableValue() {
        return DateUtil.Formatters.DATE_TIME_SHORT.format(value.getDate());
    }

    @Override
    public String getQueryParam() {
        return DateUtil.Formatters.UTC.PARTIAL_ISO.format(value.getDate());
    }
}
