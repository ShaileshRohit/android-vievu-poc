package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.filtering.fragment.UsersConstraintFragment;
import com.vievu.vievusolution.net.WebClient;

public class UserFilterConstraint extends FilterConstraint<User> {

    public UserFilterConstraint() {
        super(WebClient.Filters.USERS,
              R.string.filter_type_users,
              R.drawable.ic_user,
              UsersConstraintFragment::new,
              R.id.filter_type_users);
    }

    @Override
    protected FilterConstraintValue<User> createFilterConstraintValue(User user) {
        return new UserFilterConstraintValue(user);
    }
}
