package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.DateWrapper;
import com.vievu.vievusolution.net.WebClient;

public class RecordDateFilterConstraintValue extends BaseDateFilterConstraintValue {

    public RecordDateFilterConstraintValue(DateWrapper value) {
        super(R.id.filter_type_record_date, null, value, 0);
        if (value.isStartDate()) {
            this.key = WebClient.Filters.RECORD_DATE_FROM;
            this.readableName = R.string.text_recorded_from;
        } else {
            this.key = WebClient.Filters.RECORD_DATE_TO;
            this.readableName = R.string.text_recorded_to;
        }
    }
}
