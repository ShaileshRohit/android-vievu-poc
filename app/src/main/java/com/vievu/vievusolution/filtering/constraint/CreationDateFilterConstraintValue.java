package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.DateWrapper;
import com.vievu.vievusolution.net.WebClient;

public class CreationDateFilterConstraintValue extends BaseDateFilterConstraintValue {

    public CreationDateFilterConstraintValue(DateWrapper value) {
        super(R.id.filter_type_creation_date, null, value, 0);
        if (value.isStartDate()) {
            this.key = WebClient.Filters.CREATION_DATE_FROM;
            this.readableName = R.string.text_created_from;
        } else {
            this.key = WebClient.Filters.CREATION_DATE_TO;
            this.readableName = R.string.text_created_to;
        }
    }
}