package com.vievu.vievusolution.filtering.results;

import android.app.Activity;
import android.content.Context;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.details.files.FileDetailsActivity;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.FileAdapter;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.net.VievuService;
import com.vievu.vievusolution.net.WebClient;

import java.util.List;
import java.util.Map;

public class VideoFilterResults extends FilterResults<File> {

    protected VideoFilterResults() {
        super(
            R.string.title_activity_filter_videos,
            WebClient.Actions.VIDEOS_FIND,
            VievuService::files);
    }

    @Override
    public ArrayListAdapter<File, ?> createAdapter(Context context, List<File> initialValues) {
        return new FileAdapter(context, initialValues);
    }

    @Override
    public void handleItemClick(Activity context, File item) {
        FileDetailsActivity.start(context, item);
    }

    @Override
    public int getTitleId(Map<String, String> queryParameters) {
        if (queryParameters != null) {
            String fileType = queryParameters.get(WebClient.Filters.FILE_TYPE);
            if (fileType != null) {
                if (WebClient.Filters.FileType.VIDEOS.equals(fileType)) {
                    return R.string.title_activity_filter_videos;
                } else if (WebClient.Filters.FileType.FILES.equals(fileType)) {
                    return R.string.title_activity_filter_files;
                }
            }
        }
        return R.string.title_activity_filter_all;
    }
}
