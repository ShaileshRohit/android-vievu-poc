package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.CaseStatus;
import com.vievu.vievusolution.net.WebClient;

public class CaseStatusFilterConstrainValue extends ComplexFilterConstraintValue<CaseStatus> {

    public CaseStatusFilterConstrainValue(CaseStatus value) {
        super(R.id.filter_type_case_status, WebClient.Filters.CASE_STATUSES, value, R.string.filter_type_case_status);
    }

    @Override
    protected String getNotNullQueryParam() {
        return String.valueOf(value.getCaseStatusId());
    }

    @Override
    protected String getNotNullReadableValue() {
        return value.getName();
    }

    @Override
    protected boolean compareValues(CaseStatus otherValue) {
        return value.getCaseStatusId() == otherValue.getCaseStatusId();
    }
}
