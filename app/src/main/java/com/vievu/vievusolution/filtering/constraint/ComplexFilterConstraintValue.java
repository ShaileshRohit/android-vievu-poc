package com.vievu.vievusolution.filtering.constraint;

import com.annimon.stream.Objects;

public abstract class ComplexFilterConstraintValue<T> extends FilterConstraintValue<T> {

    public ComplexFilterConstraintValue(int id, String key, T value, int readableName) {
        super(id, key, value, readableName);
    }

    public String getQueryParam() {
        return value != null ? getNotNullQueryParam() : "";
    }

    @Override
    public String getReadableValue() {
        return value != null ? getNotNullReadableValue() : "";
    }

    protected boolean compareConstraints(FilterConstraintValue other) {
        if (other != null && Objects.equals(this.key, other.key)) {
            if (this.value != null) {
                //noinspection unchecked
                return compareValues((T) other.value);
            } else {
                return other.value == null;
            }
        }
        return false;
    }

    protected abstract String getNotNullQueryParam();
    protected abstract String getNotNullReadableValue();
    protected abstract boolean compareValues(T otherValue);
}