package com.vievu.vievusolution.filtering.fragment;

import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.IntWrapper;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SpinnerConstraintFragment extends ConstraintProvider<IntWrapper> {

    public static SpinnerConstraintFragment create(@ArrayRes int spinnerValuesArrayId, @ArrayRes int pickedValuesArrayId) {
        SpinnerConstraintFragment fragment = new SpinnerConstraintFragment();
        fragment.spinnerValuesArrayId = spinnerValuesArrayId;
        fragment.pickedValuesArrayId = pickedValuesArrayId;
        return fragment;
    }

    @Bind(R.id.spinner)
    Spinner spinner;

    private int spinnerValuesArrayId;
    private int pickedValuesArrayId;
    private String[] pickedValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spinner_constraint, container, false);
        ButterKnife.bind(this, view);
        List<String> items = Arrays.asList(getResources().getStringArray(spinnerValuesArrayId));
        pickedValues = getResources().getStringArray(pickedValuesArrayId);
        ArrayListAdapter<String, ?> adapter = new OneLineArrayListAdapter<>(getActivity(), items);
        spinner.setAdapter(adapter);
        spinner.setVisibility(View.VISIBLE);
        return view;
    }


    @OnClick(R.id.addButton)
    void addSearchConstraint() {
        int position = spinner.getSelectedItemPosition();
        callOnValueChanged(new IntWrapper(position + 1, pickedValues[position]));
    }
}