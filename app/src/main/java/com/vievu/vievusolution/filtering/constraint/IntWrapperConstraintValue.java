package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.bean.IntWrapper;

public class IntWrapperConstraintValue extends FilterConstraintValue<IntWrapper> {

    public IntWrapperConstraintValue(int id, String key, IntWrapper value, int readableName) {
        super(id, key, value, readableName);
    }

    @Override
    public String getReadableValue() {
        return value.getReadableValue();
    }

    @Override
    public String getQueryParam() {
        return String.valueOf(value.getValue());
    }
}
