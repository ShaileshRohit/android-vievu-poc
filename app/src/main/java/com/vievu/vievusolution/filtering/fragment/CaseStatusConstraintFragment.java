package com.vievu.vievusolution.filtering.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.CaseStatus;
import com.vievu.vievusolution.data.CaseStatusesAsyncRepository;

import java.util.Collection;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;

public class CaseStatusConstraintFragment extends ConstraintProvider<CaseStatus> {

    @Bind(R.id.progressBar) ProgressBar progressBar;
    @Bind(R.id.caseStatusSpinner) Spinner caseStatusSpinner;
    @Bind(R.id.addButton) Button addButton;

    private ArrayListAdapter<CaseStatus, ?> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_case_status_constraint, container, false);
        ButterKnife.bind(this, view);
        adapter = new OneLineArrayListAdapter<>(getActivity(), null, CaseStatus::getName);
        caseStatusSpinner.setAdapter(adapter);
        CaseStatusesAsyncRepository.getInstance().getDataAsync(this::updateAdapter);
        return view;
    }

    private void updateAdapter(Collection<CaseStatus> items) {
        progressBar.setVisibility(View.GONE);
        caseStatusSpinner.setVisibility(View.VISIBLE);
        if (items != null) {
            addButton.setEnabled(true);
            adapter.swapItems(items);
        } else if (isAdded()) {
            Toast.makeText(getActivity(), R.string.error_cannot_load_statuses, Toast.LENGTH_SHORT).show();
        }
    }


    @OnClick(R.id.addButton)
    void addSearchConstraint() {
        callOnValueChanged(adapter.getItem(caseStatusSpinner.getSelectedItemPosition()));
    }
}