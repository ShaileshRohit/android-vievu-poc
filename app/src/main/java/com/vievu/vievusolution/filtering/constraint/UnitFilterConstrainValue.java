package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Location;
import com.vievu.vievusolution.net.WebClient;

public class UnitFilterConstrainValue extends ComplexFilterConstraintValue<Location> {

    public UnitFilterConstrainValue(Location value) {
        super(R.id.filter_type_units, WebClient.Filters.LOCATIONS, value, R.string.filter_type_units);
    }

    @Override
    protected String getNotNullQueryParam() {
        return String.valueOf(value.getLocationId());
    }

    @Override
    protected String getNotNullReadableValue() {
        return value.getLocationName();
    }

    @Override
    protected boolean compareValues(Location otherValue) {
        return value.getLocationId() == otherValue.getLocationId();
    }
}