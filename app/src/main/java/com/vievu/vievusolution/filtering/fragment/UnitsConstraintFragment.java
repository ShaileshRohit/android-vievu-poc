package com.vievu.vievusolution.filtering.fragment;

import com.vievu.vievusolution.adapter.ArrayListAdapter;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.Location;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;

public class UnitsConstraintFragment extends SearchableListConstraintFragment<Location> {

    @Override
    protected ArrayListAdapter<Location, ?> createAdapter() {
        return new OneLineArrayListAdapter<>(getActivity(), null, Location::getLocationName);
    }

    @Override
    protected int search(int currentPage) {
        if (currentPage == WebClient.FIRST_PAGE_NUMBER) {
            adapter.swapItems(null);
        }
        int action = WebClient.Actions.unique(WebClient.Actions.LOCATIONS_FIND);
        WebClient.request(currentPage).locations(searchEditText.getText().toString(), new EventBusCallback<>(action));
        return action;
    }
}