package com.vievu.vievusolution.filtering.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.DateWrapper;
import com.vievu.vievusolution.dialog.DateTimeDialogFragment;
import com.vievu.vievusolution.ui.ActionDrawableEditText;
import com.vievu.vievusolution.util.DateUtil;

import org.joda.time.DateTime;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class DateConstraintFragment extends ConstraintProvider<DateWrapper> {

    protected static class ActionDrawableClickListener implements ActionDrawableEditText.OnActionDrawableClickListener, DateTimeDialogFragment.OnDateSelectedListener {

        public interface OnDatePickedListener {
            void onDatePicked(Date date);
        }

        private DateTimeDialogFragment fragment;
        private OnDatePickedListener listener;
        private Date date;

        public Date getDate() {
            return date;
        }

        public DateTimeDialogFragment getFragment() {
            return fragment;
        }

        public void setMaxDate(Date maxDate) {
            this.fragment.setMaxDate(maxDate);
            if (dateIsPicked() && date.after(maxDate)) {
                fragment.setDate(maxDate);
                onDateSelected(maxDate);
            }
        }

        public void setMinDate(Date minDate) {
            this.fragment.setMinDate(minDate);
            if (dateIsPicked() && date.before(minDate)) {
                fragment.setDate(minDate);
                onDateSelected(minDate);
            }
        }

        public boolean dateIsPicked() {
            return date != null;
        }

        public ActionDrawableClickListener(int titleId, DateTime defaultDate, OnDatePickedListener listener) {
            this.listener = listener;
            this.fragment = new DateTimeDialogFragment();
            if (defaultDate.getMillisOfSecond() == 0) {
                fragment.setMaxDate(defaultDate);
            } else {
                fragment.useUpperBoundAlignment(true);
            }
            fragment.setTitleId(titleId);
            fragment.setDateAndTime(defaultDate);
            fragment.setOnDateSelectedListener(this);
        }

        @Override
        public void onActionDrawableClicked(ActionDrawableEditText view) {
            fragment.showSingle(((Activity) view.getContext()).getFragmentManager());
        }

        @Override
        public void onDateSelected(Date date) {
            this.date = date;
            fragment.setDate(this.date);
            listener.onDatePicked(this.date);
        }
    }

    @Bind(R.id.fromDateEditText) ActionDrawableEditText fromDateEditText;
    @Bind(R.id.toDateEditText) ActionDrawableEditText toDateEditText;
    @Bind(R.id.addButton) Button searchButton;

    private ActionDrawableClickListener fromDrawableClickListener;
    private ActionDrawableClickListener toDrawableClickListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_date_constraint, container, false);
        ButterKnife.bind(this, view);
        fromDrawableClickListener = new ActionDrawableClickListener(R.string.text_from,
                DateTime.now().withTime(0, 0, 0, 0),
                (date) -> {
                    fromDateEditText.setText(DateUtil.Formatters.DATE_TIME_SHORT.format(date));
                    toDrawableClickListener.setMinDate(date);
                });
        fromDrawableClickListener.setMaxDate(DateTime.now().withMillisOfSecond(0).toDate());
        toDrawableClickListener = new ActionDrawableClickListener(R.string.text_to,
                DateTime.now().withTime(23, 59, 59, 999),
                (date) -> {
                    toDateEditText.setText(DateUtil.Formatters.DATE_TIME_SHORT.format(date));
                    fromDrawableClickListener.setMaxDate(date);
                });
        fromDateEditText.setOnActionDrawableClickListener(fromDrawableClickListener);
        toDateEditText.setOnActionDrawableClickListener(toDrawableClickListener);
        return view;
    }

    @OnClick(R.id.addButton)
    void onAddClick() {
        if (fromDrawableClickListener.dateIsPicked()) {
            callOnValueChanged(new DateWrapper(fromDrawableClickListener.getDate(), true));
        }
        if (toDrawableClickListener.dateIsPicked()) {
            callOnValueChanged(new DateWrapper(toDrawableClickListener.getDate(), false));
        }
    }

    @OnTextChanged(value = {R.id.fromDateEditText, R.id.toDateEditText}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void onDateSet() {
        searchButton.setEnabled(
            fromDateEditText.getText().length() > 0 ||
              toDateEditText.getText().length() > 0
        );
    }
}