package com.vievu.vievusolution.filtering.constraint;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.DateWrapper;
import com.vievu.vievusolution.net.WebClient;

public class IncidentDateFilterConstraintValue extends BaseDateFilterConstraintValue {

    public IncidentDateFilterConstraintValue(DateWrapper value) {
        super(R.id.filter_type_incident_date, null, value, 0);
        if (value.isStartDate()) {
            this.key = WebClient.Filters.INCIDENT_DATE_FROM;
            this.readableName = R.string.text_incident_from;
        } else {
            this.key = WebClient.Filters.INCIDENT_DATE_TO;
            this.readableName = R.string.text_incident_to;
        }
    }
}