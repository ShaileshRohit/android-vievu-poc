package com.vievu.vievusolution.net.camera;

import android.os.AsyncTask;
import android.util.Log;

import com.annimon.stream.function.Supplier;
import com.google.gson.reflect.TypeToken;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.json.Gsons;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;

public class MetadataFtpClient {

    public static class Event {

        private boolean isEdited;
        private File metadata;

        public Event(File metadata, boolean isEdited) {
            this.metadata = metadata;
            this.isEdited = isEdited;
        }

        public File getMetadata() {
            return metadata;
        }

        public boolean isSuccessful() {
            return metadata != null;
        }

        public boolean isEdited() {
            return isEdited;
        }
    }

    private static MetadataFtpClient instance;

    public static MetadataFtpClient getInstance() {
        if (instance == null) {
            instance = new MetadataFtpClient();
        }
        return instance;
    }

    private FTPClient ftpClient = new FTPClient();
    private Executor executor;

    private Executor getExecutor() {
        if (executor == null) {
            executor = Executors.newSingleThreadExecutor();
        }
        return executor;
    }

    private void prepareFtpSession() throws IOException {
        ftpClient.connect(InetAddress.getByName(CommonConstants.DEFAULT_CAMERA_IP));
        ftpClient.login(CommonConstants.DEFAULT_FTP_LOGIN, CommonConstants.DEFAULT_FTP_PASSWORD);
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();
    }

    private void closeFtpSession() throws IOException {
        ftpClient.logout();
        ftpClient.disconnect();
    }

    private String getMetadataFileName(String videoFileName) {
        return String.format("/VIDEO/%s%s.JSON", videoFileName, BuildConfig.METADATA_SUFFIX);
    }

    public File saveMetadata(String videoFileName, File metadata) {
        File result = null;
        try {
            Log.d("MetadataFtpClient", "Attempting to save metadata for " + videoFileName);
            prepareFtpSession();
            String json = Gsons.CAMEL_CASE_FTP_ONLY.toJson(metadata);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(json.getBytes());
            String metadataFileName = getMetadataFileName(videoFileName);
            if (ftpClient.deleteFile(metadataFileName)) {
                Log.d("MetadataFtpClient", "Deleted old " + metadataFileName);
            }
            if (ftpClient.storeFile(metadataFileName, inputStream)) {
                result = metadata;
                Log.d("MetadataFtpClient", "Metadata saved");
            } else {
                Log.d("MetadataFtpClient", "Metadata save failed with code " + String.valueOf(ftpClient.getReplyCode()));
            }
            inputStream.close();
            closeFtpSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public File loadMetadata(String videoFileName) {
        Log.d("MetadataFtpClient", "Attempting to load metadata for " + videoFileName);
        return loadData(getMetadataFileName(videoFileName), File.class, () -> {
            File metadata = new File();
            metadata.setUserId(-1);
            metadata.setCaseId(-1);
            return metadata;
        });
    }

    public List<Category> loadCategories(String fileName) {
        Log.d("MetadataFtpClient", "Attempting to load categories from " + fileName);
        return loadData(fileName, new TypeToken<List<Category>>() {}.getType(), null);
    }

    private <T> T loadData(String fileName, Type dataType, Supplier<T> onFailure) {
        T data = null;
        try {
            prepareFtpSession();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ftpClient.retrieveFile(fileName, outputStream);
            if (ftpClient.getReplyCode() == 450 || ftpClient.getReplyCode() == 501) {
                Log.d("MetadataFtpClient", "No file with name " + fileName);
                if (onFailure != null) {
                    data = onFailure.get();
                }
            } else {
                String json = new String(outputStream.toByteArray());
                Log.d("MetadataFtpClient", String.format("Downloaded %s from %s", json, fileName));
                data = Gsons.CAMEL_CASE_WITH_LISTS_UNWRAPPING.fromJson(json, dataType);
            }
            outputStream.close();
            closeFtpSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public void saveMetadataAsync(String videoFileName, File metadata) {
        new AsyncTask<Void, Void, File>() {

            @Override
            protected File doInBackground(Void... params) {
                return saveMetadata(videoFileName, metadata);
            }

            @Override
            protected void onPostExecute(File result) {
                EventBus.getDefault().post(new Event(result, true));
            }
        }.executeOnExecutor(getExecutor());
    }

    public void loadMetadataAsync(String videoFileName) {
        new AsyncTask<Void, Void, File>() {

            @Override
            protected File doInBackground(Void... params) {
                return loadMetadata(videoFileName);
            }

            @Override
            protected void onPostExecute(File result) {
                EventBus.getDefault().post(new Event(result, false));
            }
        }.executeOnExecutor(getExecutor());
    }

    public void loadCategoriesAsync(String fileName) {
        new AsyncTask<Void, Void, List<Category>>() {

            @Override
            protected List<Category> doInBackground(Void... params) {
                return loadCategories(fileName);
            }

            @Override
            protected void onPostExecute(List<Category> result) {
                EventBus.getDefault().post(new RequestCompletedEvent<>(result));
            }
        }.executeOnExecutor(getExecutor());
    }
}
