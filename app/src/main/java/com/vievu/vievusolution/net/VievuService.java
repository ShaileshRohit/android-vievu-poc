package com.vievu.vievusolution.net;

import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.bean.CaseFile;
import com.vievu.vievusolution.bean.CaseStatus;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.bean.FileInfo;
import com.vievu.vievusolution.bean.LimitedArrayList;
import com.vievu.vievusolution.bean.Location;
import com.vievu.vievusolution.bean.LoginErrorDescription;
import com.vievu.vievusolution.bean.NewFileInfo;
import com.vievu.vievusolution.bean.NewVideo;
import com.vievu.vievusolution.bean.ResultDescription;
import com.vievu.vievusolution.bean.ShareLink;
import com.vievu.vievusolution.bean.ShareRequest;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.bean.TokenData;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.bean.VideoFileInfo;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.openid_provider.OpenIDAccountModel;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;
import retrofit.http.QueryMap;

public interface VievuService {

    @FormUrlEncoded
    @POST("/token/mobile")
    void login(@Field("username") String userName, @Field("password") String password, Callback<TokenData> callback);

    @FormUrlEncoded
    @POST("/token/mobile")
    void login(@Field("username") String userName, @Field("password") String password, @Field("2fa-token") String mfaToken, @Field("grant_type") String grantType, Callback<TokenData> callback);

    @FormUrlEncoded
    @POST("/token/securityQuestion")
    void login(@Field("username") String userName, @Field("answer") String answer, @Field("SecurityQuestionId") int questionId, Callback<TokenData> callback);

    @FormUrlEncoded
    @POST("/token/mobile")
    void verify(@Field("otp") String verificationCode, @Field("2fa-token") String mfaToken, @Field("2fa-provider") String mfaProvider, @Field("grant_type") String grantType, Callback<TokenData> callback);

    @FormUrlEncoded
    @POST("/token/sendSecurityCode")
    void securityCode(@Field("2fa-token") String mfaToken, @Field("2fa-provider") String mfaProvider, Callback<LoginErrorDescription> callback);

    @GET("/cases/getCategories")
    void categories(Callback<List<Category>> callback);

    @GET("/cases/getCaseStatus")
    void caseStatuses(Callback<List<CaseStatus>> callback);

    @GET("/users/getUsers/lightweight")
    void userBy(@Query("Name") String name, Callback<List<User>> callback);

    @GET("/settings/Tags")
    void tags(Callback<List<Tag>> callback);

    @GET("/videos/files")
    void files(@QueryMap Map<String, String> queryParameters, Callback<LimitedArrayList<File>> callback);

    @GET("/videos/getVideoUrl")
    void convertedVideoFileInfo(@Query("FileName") String fileName, Callback<VideoFileInfo> callback);

    @PUT("/videos/edit")
    void editVideo(@Body File file, Callback<ResultDescription> callback);

    @GET("/cases/getCases")
    void cases(@QueryMap Map<String, String> queryParameters, Callback<LimitedArrayList<Case>> callback);

    @GET("/cases/getFiles")
    void filesBy(@Query("CaseId") int caseId, Callback<List<CaseFile>> callback);

    @GET("/cases/downloadFile")
    void caseFileInfo(@Query("CaseId") int caseId, @Query("FileName") String fileName, Callback<FileInfo> callback);

    @GET("/cases/generateFile")
    NewFileInfo newFileSync(@Query("UserId") int userId, @Query("CaseId") int caseId, @Query("FileName") String fileName);

    @POST("/cases/commit")
    ResultDescription commitSync(@Query("FileName") String fileName);

    @PUT("/cases/edit")
    void editCase(@Body Case item, Callback<ResultDescription> callback);

    @GET("/videos/generate")
    NewFileInfo newVideoSync(@Query("FileName") String fileName, @Query("DateOfRecord") String dateOfRecord);

    @POST("/videos/commit")
    ResultDescription commitSync(@Body NewVideo video);

    @POST("/videos/share")
    void shareVideoLink(@Body ShareRequest request, Callback<ShareLink> callback);

    @GET("/videos/locations")
    void locations(@Query("Location") String name, Callback<List<Location>> callback);

    @GET("/cameras/getAssignedCameras")
    void assignedCameras(Callback<List<String>> callback);

    @GET("/account/openidconfig")
        void getOpenIDInfo(Callback<OpenIDAccountModel> callback);
    @GET("/users/GetPermissions")
    void getPermissoin(Callback<UserDataRepository> callback);
}



/*https://192.168.3.121:444/v1/account/openidconfig
https://192.168.3.121:444/v1/users/GetPermissions*/