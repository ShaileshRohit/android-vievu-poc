package com.vievu.vievusolution.net;

import com.vievu.vievusolution.bean.ErrorDescription;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EventBusCallback<T> implements Callback<T> {

    public static final int ACTION_NONE = 0;

    protected int action;

    public EventBusCallback() {
        this.action = ACTION_NONE;
    }

    public EventBusCallback(int action) {
        this.action = action;
    }

    @Override
    public void success(T result, Response response) {
        EventBus.getDefault().post(new RequestCompletedEvent<>(action, result));
    }

    @Override
    public void failure(RetrofitError error) {
        Throwable cause = error.getCause() != null ? error.getCause() : error;
        if (error.getResponse() != null) {
            try {
                ErrorDescription errorDescription = (ErrorDescription) error.getBodyAs(ErrorDescription.class);
                EventBus.getDefault().post(new NetworkErrorEvent(action, cause, errorDescription));
                return;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        EventBus.getDefault().post(new NetworkErrorEvent(action, cause));
    }
}
