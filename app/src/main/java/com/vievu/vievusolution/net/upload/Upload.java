package com.vievu.vievusolution.net.upload;

import android.content.Context;

import com.vievu.vievusolution.bean.NewFileInfo;
import com.vievu.vievusolution.bean.ResultDescription;

import java.io.FileNotFoundException;
import java.io.InputStream;

interface Upload {
    InputStream getInputStream(Context context) throws FileNotFoundException;
    long getFileSize();
    NewFileInfo requestNewFileInfo();
    ResultDescription commitFile();
    int getErrorMessageId();
}
