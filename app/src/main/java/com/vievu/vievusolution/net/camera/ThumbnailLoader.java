package com.vievu.vievusolution.net.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;
import com.vievu.vievusolution.CameraClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ThumbnailLoader extends RequestHandler {

    @SuppressLint("StaticFieldLeak")
    private static Picasso instance;
    private static LruCache cameraThumbnailsCache;

    public static Picasso getInstance() {
        return instance;
    }

    public static void init(@NonNull Context context) {
        cameraThumbnailsCache = new LruCache(context);
        instance = new Picasso.Builder(context)
                .memoryCache(cameraThumbnailsCache)
                .executor(CameraClient.getInstance().getExecutor())
                .addRequestHandler(new ThumbnailLoader())
                .build();
    }

    private final Pattern idPattern = Pattern.compile("(\\d+)$");

    private static final String SCHEME = "vf";

    public static String getUri(int fileId) {
        return String.format("%s://%d", SCHEME, fileId);
    }

    @Override
    public boolean canHandleRequest(Request data) {
        return SCHEME.equals(data.uri.getScheme());
    }

    @Override
    public Result load(Request request, int networkPolicy) throws IOException {
        Matcher matcher = idPattern.matcher(request.uri.toString());
        if (matcher.find()) {
            int id = Integer.parseInt(matcher.group(1));
            File tempFile = File.createTempFile("vievuTemp", null);
            FileOutputStream outputStream = new FileOutputStream(tempFile);
            CameraClient.getInstance().getPtpIpClient().getThumbnail(id, outputStream);
            outputStream.close();
            FileInputStream inputStream = new FileInputStream(tempFile);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            return new Result(bitmap, Picasso.LoadedFrom.NETWORK);
        }
        return null;
    }

    public static void invalidateCache() {
        if (cameraThumbnailsCache != null) {
            cameraThumbnailsCache.clear();
        }
    }
}
