package com.vievu.vievusolution.net.upload;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.microsoft.azure.storage.Constants;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.RetryLinearRetry;
import com.microsoft.azure.storage.RetryPolicy;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobRequestOptions;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.NewFileInfo;
import com.vievu.vievusolution.bean.ResultDescription;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.event.UploadCompletedEvent;
import com.vievu.vievusolution.event.UploadProgressEvent;
import com.vievu.vievusolution.net.WebClient;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;

public class UploadIntentService extends IntentService {

    private static final String ACTION_UPLOAD_CASE_FILE = "ACTION_UPLOAD_CASE_FILE";
    private static final String ACTION_UPLOAD_VIDEO = "ACTION_UPLOAD_VIDEO";
    public static final String EXTRA_LOGGED_USER_ID = "logged_user_id";
    public static final String EXTRA_CASE_ID = "case_id";
    public static final String EXTRA_FILE_URI = "file_uri";
    public static final String EXTRA_VIDEO = "video";
    public static final String EXTRA_EVENT_ACTION = "event_action";

    private static final int BUFFER_SIZE = 128 * 1024;

    public static int uploadCaseFile(Context context, int userId, int caseId, Uri fileUri) {
        int action = WebClient.Actions.unique(WebClient.Actions.CASES_NEW_FILE);
        Intent intent = new Intent(context, UploadIntentService.class);
        intent.setAction(ACTION_UPLOAD_CASE_FILE);
        intent.putExtra(EXTRA_LOGGED_USER_ID, userId);
        intent.putExtra(EXTRA_CASE_ID , caseId);
        intent.putExtra(EXTRA_FILE_URI, fileUri);
        intent.putExtra(EXTRA_EVENT_ACTION, action);
        context.startService(intent);
        return action;
    }

    public static int uploadVideo(Context context, Uri fileUri, File video) {
        int action = WebClient.Actions.unique(WebClient.Actions.CASES_NEW_FILE);
        Intent intent = new Intent(context, UploadIntentService.class);
        intent.setAction(ACTION_UPLOAD_VIDEO);
        intent.putExtra(EXTRA_FILE_URI, fileUri);
        intent.putExtra(EXTRA_VIDEO , video);
        intent.putExtra(EXTRA_EVENT_ACTION, action);
        context.startService(intent);
        return action;
    }

    private int currentEventAction;

    public UploadIntentService() {
        super("UploadIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            currentEventAction = intent.getIntExtra(EXTRA_EVENT_ACTION, 0);
            if (ACTION_UPLOAD_CASE_FILE.equals(action)) {
                execute(UploadCaseFile.from(intent));
            } else if (ACTION_UPLOAD_VIDEO.equals(action)) {
                execute(UploadFile.from(intent));
            }
        }
    }

    private void execute(Upload upload) {
        UploadCompletedEvent event = null;
        try {
            InputStream in = upload.getInputStream(this);
            if (in != null) {
                NewFileInfo info = upload.requestNewFileInfo();
                if (info != null) {
                    String uri = info.buildFileUri(null).toString();
                    CloudBlockBlob blob = new CloudBlockBlob(new URI(uri));
                    BlobRequestOptions options = new BlobRequestOptions();
                    options.setUseTransactionalContentMD5(false);
                    options.setDisableContentMD5Validation(true);
                    options.setRetryPolicyFactory(new RetryLinearRetry(3000, RetryPolicy.DEFAULT_CLIENT_RETRY_COUNT));
                    OperationContext operationContext = new OperationContext();
                    if (BuildConfig.DEBUG) {
                        Log.d("UploadIntentService", "Starting upload to " + uri);
                        operationContext.setLogLevel(Log.DEBUG);
                    }
                    blob.setStreamWriteSizeInBytes(3 * Constants.MB);
                    OutputStream out = blob.openOutputStream(null, options, operationContext);
                    copyStreamDataWithReports(out, in,
                            upload.getFileSize() > 0 ? upload.getFileSize() : in.available());
                    in.close();
                    out.close();
                    Log.d("UploadIntentService", "Upload completed");
                    ResultDescription result = upload.commitFile();
                    if (result.isApplied()) {
                        event = new UploadCompletedEvent(currentEventAction, info.getStorageFileName());
                    } else {
                        event = new UploadCompletedEvent(currentEventAction, result);
                    }
                }
            } else {
                event = new UploadCompletedEvent(currentEventAction,
                        ResultDescription.forError(this, R.string.error_cannot_open_file));
            }
        } catch (FileNotFoundException e) {
            event = new UploadCompletedEvent(currentEventAction,
                    ResultDescription.forError(this, R.string.error_cannot_open_file));
        } catch (IOException | RetrofitError e) {
            e.printStackTrace();
            if (   e.getCause() instanceof UnknownHostException
                || e.getCause() instanceof ConnectException
                || e.getCause() instanceof StorageException && e.getCause().getCause() instanceof UnknownHostException) {
                event = new UploadCompletedEvent(
                        currentEventAction,
                        ResultDescription.forError(this, R.string.error_no_internet));
            }
        } catch (StorageException | URISyntaxException e) {
            e.printStackTrace();
        }
        if (event == null) {
            event = new UploadCompletedEvent(
                    currentEventAction,
                    ResultDescription.forError(this, upload.getErrorMessageId()));
        }
        EventBus.getDefault().post(event);
    }

    private void copyStreamDataWithReports(OutputStream out, InputStream in, long fileSize) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = BUFFER_SIZE;
        UploadProgressEvent event = new UploadProgressEvent(currentEventAction, fileSize);
        EventBus bus = EventBus.getDefault();
        while (bytesRead == BUFFER_SIZE) {
            bytesRead = in.read(buffer, 0, BUFFER_SIZE);
            out.write(buffer, 0, bytesRead);
            event.addBytesUploaded(bytesRead);
            bus.post(event);
        }
        event.setUploadCompleted(true);
        bus.post(event);
    }
}
