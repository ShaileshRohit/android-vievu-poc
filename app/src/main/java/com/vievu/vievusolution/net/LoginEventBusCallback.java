package com.vievu.vievusolution.net;

import com.vievu.vievusolution.bean.LoginErrorDescription;
import com.vievu.vievusolution.bean.SecurityQuestion;
import com.vievu.vievusolution.event.NetworkErrorEvent;

import java.net.HttpURLConnection;

import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;

public class LoginEventBusCallback<T> extends EventBusCallback<T> {

    @Override
    public void failure(RetrofitError error) {
        if (error.getResponse() != null) {
            try {
                if (error.getResponse().getStatus() == HttpURLConnection.HTTP_FORBIDDEN) {
                    SecurityQuestion securityQuestion = (SecurityQuestion) error.getBodyAs(SecurityQuestion.class);
                    EventBus.getDefault().post(securityQuestion);
                } else {
                    LoginErrorDescription errorDescription = (LoginErrorDescription) error.getBodyAs(LoginErrorDescription.class);
                    EventBus.getDefault().post(new NetworkErrorEvent(action,
                            error.getCause() != null ? error.getCause() : error, errorDescription));
                }
                return;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        super.failure(error);
    }
}
