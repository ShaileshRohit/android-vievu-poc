package com.vievu.vievusolution.net.upload;

import android.content.Intent;
import android.net.Uri;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.NewFileInfo;
import com.vievu.vievusolution.bean.ResultDescription;
import com.vievu.vievusolution.net.WebClient;

class UploadCaseFile extends UploadBase {

    public static UploadCaseFile from(Intent intent) {
        return new UploadCaseFile(
                intent.getIntExtra(UploadIntentService.EXTRA_LOGGED_USER_ID, 0),
                intent.getIntExtra(UploadIntentService.EXTRA_CASE_ID, 0),
                intent.getParcelableExtra(UploadIntentService.EXTRA_FILE_URI));
    }

    private int userId;
    private int caseId;
    private String storageFileName;

    private UploadCaseFile(int userId, int caseId, Uri fileUri) {
        super(fileUri);
        this.userId = userId;
        this.caseId = caseId;
    }

    @Override
    public NewFileInfo requestNewFileInfo() {
        NewFileInfo info = WebClient.request().newFileSync(userId, caseId, localFileName);
        this.storageFileName = info.getStorageFileName();
        return info;
    }

    @Override
    public ResultDescription commitFile() {
        return WebClient.request().commitSync(storageFileName);
    }

    @Override
    public int getErrorMessageId() {
        return R.string.error_upload_file;
    }
}
