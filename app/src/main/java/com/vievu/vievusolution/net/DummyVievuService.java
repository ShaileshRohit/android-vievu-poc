package com.vievu.vievusolution.net;

import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.bean.CaseFile;
import com.vievu.vievusolution.bean.CaseStatus;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.bean.FileInfo;
import com.vievu.vievusolution.bean.LimitedArrayList;
import com.vievu.vievusolution.bean.Location;
import com.vievu.vievusolution.bean.LoginErrorDescription;
import com.vievu.vievusolution.bean.NewFileInfo;
import com.vievu.vievusolution.bean.NewVideo;
import com.vievu.vievusolution.bean.ResultDescription;
import com.vievu.vievusolution.bean.ShareLink;
import com.vievu.vievusolution.bean.ShareRequest;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.bean.TokenData;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.bean.VideoFileInfo;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.openid_provider.OpenIDAccountModel;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Field;

public class DummyVievuService implements VievuService {

    @Override
    public void login(String userName, String password, Callback<TokenData> callback) {
    }

    @Override
    public void login(@Field("username") String userName, @Field("password") String password, @Field("2fa-token") String mfaToken, @Field("grant_type") String grantType, Callback<TokenData> callback) {

    }

    @Override
    public void login(@Field("username") String userName, @Field("answer") String answer, @Field("SecurityQuestionId") int questionId, Callback<TokenData> callback) {

    }

    @Override
    public void verify(@Field("otp") String verificationCode, @Field("2fa-token") String mfaToken, @Field("2fa-provider") String mfaProvider, @Field("grant_type") String grantType, Callback<TokenData> callback) {

    }

    @Override
    public void securityCode(@Field("2fa-token") String mfaToken, @Field("2fa-provider") String mfaProvider, Callback<LoginErrorDescription> callback) {

    }


    @Override
    public void categories(Callback<List<Category>> callback) {

    }

    @Override
    public void caseStatuses(Callback<List<CaseStatus>> callback) {

    }

    @Override
    public void userBy(String name, Callback<List<User>> callback) {

    }

    @Override
    public void tags(Callback<List<Tag>> callback) {

    }

    @Override
    public void files(Map<String, String> queryParameters, Callback<LimitedArrayList<File>> callback) {

    }

    @Override
    public void convertedVideoFileInfo(String fileName, Callback<VideoFileInfo> callback) {

    }

    @Override
    public void editVideo(File file, Callback<ResultDescription> callback) {

    }

    @Override
    public void cases(Map<String, String> queryParameters, Callback<LimitedArrayList<Case>> callback) {

    }

    @Override
    public void filesBy(int caseId, Callback<List<CaseFile>> callback) {

    }

    @Override
    public void caseFileInfo(int caseId, String fileName, Callback<FileInfo> callback) {

    }

    @Override
    public NewFileInfo newFileSync(int userId, int caseId, String fileName) {
        return null;
    }

    @Override
    public ResultDescription commitSync(String fileName) {
        return null;
    }

    @Override
    public void editCase(Case item, Callback<ResultDescription> callback) {

    }

    @Override
    public NewFileInfo newVideoSync(String fileName, String dateOfRecord) {
        return null;
    }

    @Override
    public ResultDescription commitSync(NewVideo video) {
        return null;
    }

    @Override
    public void shareVideoLink(ShareRequest request, Callback<ShareLink> callback) {

    }

    @Override
    public void locations(String name, Callback<List<Location>> callback) {

    }

    @Override
    public void assignedCameras(Callback<List<String>> callback) {

    }

    @Override
    public void getOpenIDInfo(Callback<OpenIDAccountModel> callback) {
    }
    @Override
    public void getPermissoin(Callback<UserDataRepository> callback) {
    }
}
