package com.vievu.vievusolution.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.EventBusActivity;
import com.vievu.vievusolution.VievuApplication;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.bean.TokenData;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.event.LoginEvent;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.PingCompletedEvent;
import com.vievu.vievusolution.json.Gsons;
import com.vievu.vievusolution.util.LogoutUtil;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.ClientAuthentication;
import net.openid.appauth.TokenRequest;
import net.openid.appauth.TokenResponse;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

public class WebClient  implements RequestInterceptor, ErrorHandler {

    private static final String KEY_AUTH_STATE = "authState";
    private static final String KEY_USER_INFO = "userInfo";

    private static final String EXTRA_AUTH_SERVICE_DISCOVERY = "authServiceDiscovery";
    private static final String EXTRA_AUTH_STATE = "authState";
    private static final String TAG = "WebClient";
    static Context context;
    public static  AuthState mAuthState;
    public  static AuthorizationService mAuthService;
    public  static AuthorizationResponse response;
    public  static AuthorizationException ex;
    private static JSONObject mUserInfoJson;
    private static SharedPreferences preferences;
    public   static final String KEY_TOKEN_EXPIRATION_TIME = "expiration_time";
    public static boolean isIDRefreshed=false;


    public static class Login {
        public static final String REQUEST_CODE_METHOD_EMAIL = "EmailCode";
        public static final String REQUEST_CODE_METHOD_SMS = "TextMessageCode";
    }

    public static class Filters {
        public static final String FILE_TYPE = "FileType";
        public static final String USERS = "UserIds";
        public static final String COMMENT = "Comment";
        public static final String CATEGORY = "CategoryIds";
        public static final String TAG = "TagIds";
        public static final String RECORD_DATE_FROM = "RecordDateFrom";
        public static final String RECORD_DATE_TO = "RecordDateTo";
        public static final String UPLOAD_DATE_FROM = "UploadDateFrom";
        public static final String UPLOAD_DATE_TO = "UploadDateTo";
        public static final String CREATION_DATE_FROM = "CreationDateFrom";
        public static final String CREATION_DATE_TO = "CreationDateTo";
        public static final String INCIDENT_DATE_FROM = "IncidentDateFrom";
        public static final String INCIDENT_DATE_TO = "IncidentDateTo";
        public static final String REPORT_NUMBER = "ReportNumber";
        public static final String EVENT_NUMBER = "EventNumber";
        public static final String CASES = "CaseIds";
        public static final String NAME = "Name";
        public static final String CASE_STATUSES = "StatusIds";
        public static final String LOCATIONS = "LocationIds";
        public static final String FILE_NAME = "FileName";
        public static final String SUMMARY = "Summary";
        public static final String LOCKDOWN = "Lockdown";
        public static final String NEVER_DELETE = "NeverDelete";
        public static final String SHARED = "SharedLink";

        public static class FileType {
            public static final String ALL = "0";
            public static final String VIDEOS = "1";
            public static final String FILES = "2";
        }
    }

    public static class Actions {
        public static final int USERS_FIND_BY_NAME = 0x11000000;
        public static final int USERS_LOAD_CURRENT_INFO = 0x12000000;
        public static final int LOAD_CATEGORIES = 0x21000000;
        public static final int LOAD_CASE_STATUSES = 0x22000000;
        public static final int LOAD_TAGS = 0x23000000;
        public static final int LOAD_ASSIGNED_CAMERAS = 0x24000000;
        public static final int VIDEOS_FIND = 0x31000000;
        public static final int VIDEOS_CONVERTED_FILE_NAME = 0x32000000;
        public static final int VIDEOS_ORIGINAL_FILE_NAME = 0x33000000;
        public static final int VIDEOS_EDIT = 0x34000000;
        public static final int VIDEOS_SHARE = 0x35000000;
        public static final int REPORTS_FIND_BY_NUMBER = 0x41000000;
        public static final int CASES_FIND = 0x51000000;
        public static final int CASES_GET_FILES = 0x52000000;
        public static final int CASES_GET_FILE_NAME = 0x53000000;
        public static final int CASES_NEW_FILE = 0x54000000;
        public static final int CASES_COMMIT_FILE = 0x55000000;
        public static final int CASES_EDIT = 0x56000000;
        public static final int LOCATIONS_FIND = 0x61000000;
        public static final int FILES_FIND = 0x71000000;

        public static int unique(int action) {
            return action | (int) (System.currentTimeMillis() & 0xFFFFFF);
        }
        public static int common(int action) {
            return action & 0xFF000000;
        }
    }
    private static class PingTask extends AsyncTask<Void, Void, Boolean> {
        private static ConcurrentLinkedQueue<Integer> pendingActions = new ConcurrentLinkedQueue<>();
        private int action;
        PingTask(int action) {
            this.action = action;
            pendingActions.add(action);
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Log.d("PingTask", "Executing external process");
                return Runtime.getRuntime().exec("/system/bin/ping -c 1 8.8.8.8").waitFor() == 0;
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        @Override
        protected void onPostExecute(Boolean isConnected) {
            for (int action : pendingActions) {
                Log.d("PingTask", String.format("Finished %d with result: %b", action, isConnected));
                EventBus.getDefault().post(new PingCompletedEvent(isConnected, action));
            }
            pendingActions.clear();
            WebClient.currentPingTask = null;

        }
    }
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String HEADER_ACCEPT = "Accept";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String AUTHORIZATION_PREFIX = "Bearer ";

    private static final String QUERY_PAGE_SIZE = "PageSize";
    private static final String QUERY_PAGE_NUMBER = "PageNumber";

    public static final int FIRST_PAGE_NUMBER = 1;
    public static final int DEFAULT_PAGE_SIZE = 20;

    private static WebClient instance;
    private static PingTask currentPingTask;
    private  static boolean isIDPDone=false;

    public static void pingAsync(int action) {
        if (currentPingTask != null) {
            Log.d("PingTask",  String.format("PingTask %d cancelled due to next ping task", currentPingTask.action));
            currentPingTask.cancel(true);
        }
        Log.d("PingTask",  String.format("Executing PingTask %d", action));
        currentPingTask  = new PingTask(action);
        currentPingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private static String getApiUrl() {
        if (instance != null) {
            //return getInstance().baseUrl;
            return getInstance(null).baseUrl + "/api/v1";
        }
        return null;
    }

    public static String getAvatarUrl(int userId, int width, int height) {
        return getImageUrl("/users/getAvatar", "UserId", userId, width, height);
    }

    public static String getThumbnail(File file, int width, int height) {
        String url = getImageUrl("/videos/getThumbnail", "videoId", file.getVideoId(), width, height);
        if (file.isRedactedVideo()) {
            url = String.format("%s&redactionVideoId=%d", url, file.getRedactVideoId());
        }
        return url;
    }

    private static String getImageUrl(String path, String idParameterName, int id, int width, int height) {
        String apiUrl = getApiUrl();
        if (apiUrl != null) {
            StringBuilder builder = new StringBuilder(apiUrl);
            builder.append(path).append('?');
            builder.append(idParameterName).append('=').append(id);
            if (width != 0) {
                builder.append("&width=").append(width);
            }
            if (height != 0) {
                builder.append("&height=").append(height);
            }
            return builder.toString();
        }
        return null;
    }


    public static WebClient getInstance(String accountName) {
        if (accountName != null) {
            instance = new WebClient();
            instance.accountName = accountName;
            final int minimumDomainLevels = 3;
            if(accountName.split("\\.").length < minimumDomainLevels){
                accountName = String.format("%s.%s", accountName, BuildConfig.DOMAIN);
            }
            instance.baseUrl = "https://" + accountName;
            //instance.vievuService = new DummyVievuService();
            //noinspection ConstantConditions
            instance.vievuService = new RestAdapter.Builder()
                    .setEndpoint(getApiUrl())
                    .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                    .setConverter(new GsonConverter(Gsons.CAMEL_CASE_WITH_LISTS_UNWRAPPING))
                    .setRequestInterceptor(instance)
                    .setErrorHandler(instance)
                    .build().create(VievuService.class);
        }
        return instance;
    }

    public static void isIDPDONE(){
        isIDPDone=true;

    }
    public static WebClient getInstances(Context context) {

        //String baseURL="http://192.168.3.121:4545";
        String baseURL="http://vaultapi.demo.wwhnetwork.net";

        instance = new WebClient();
        instance.baseUrl =baseURL;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        RequestInterceptor requestInterceptor=null;
             requestInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON);
                    request.addHeader(HEADER_ACCEPT, CONTENT_TYPE_JSON);
                    //request.addHeader(HEADER_AUTHORIZATION,token);
                    if(!TextUtils.isEmpty(preferences.getString(CommonConstants.KEY_ID_TOKEN,"")))
                    {
                        request.addHeader(HEADER_AUTHORIZATION, preferences.getString(CommonConstants.KEY_ID_TOKEN, null));
                    }
                    if (offset > -1) {
                        request.addQueryParam(QUERY_PAGE_NUMBER, String.valueOf(offset));
                        offset = -1;
                    }
                    if (count > 0) {
                        request.addQueryParam(QUERY_PAGE_SIZE, String.valueOf(count));
                        count = 0;
                    }
                }
            };
            /*.setEndpoint(!isIDPDone ? baseURL : baseURL+ "/v1")*/
                instance.vievuService = new RestAdapter.Builder()
                        .setEndpoint(baseURL+ "/v1")
                        .setLogLevel(RestAdapter.LogLevel.FULL)
                        .setConverter(new GsonConverter(Gsons.CAMEL_CASE_WITH_LISTS_UNWRAPPING))
                        .setRequestInterceptor(requestInterceptor)
                        .setErrorHandler(instance)

                        .build().create(VievuService.class);
        return instance;

    }

    public static boolean restoreInstance(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        getInstance(preferences.getString(CommonConstants.KEY_ACCOUNT_NAME, null));
        if (instance != null) {
            instance.authHeader  = AUTHORIZATION_PREFIX + preferences.getString(CommonConstants.KEY_AUTH_TOKEN, null);
            return true;
        }
        return false;
    }

    public static VievuService request(){

        checkExpirationTimeOfToken();

        return instance != null ? instance.vievuService : new DummyVievuService();
    }
    public static void setInstanceNull(){
        instance = null;
    }


    public static VievuService request(int from){

        checkExpirationTimeOfToken();

        return request(from, DEFAULT_PAGE_SIZE);
    }
    public static  void checkExpirationTimeOfToken(){
        context= VievuApplication.getContext();
        mAuthService = new AuthorizationService(context);
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long currentTime = System.currentTimeMillis();

        if(!isIDRefreshed && preferences.contains(CommonConstants.KEY_AUTH_TOKEN)
                && preferences.getLong(KEY_TOKEN_EXPIRATION_TIME, currentTime) < currentTime){

            refreshAccessToken();
        }
    }

    public static VievuService request(int from, int count){
        WebClient client = getInstance(null);
        checkExpirationTimeOfToken();
        if (client != null) {
            client.offset = from;
            client.count = count;

            return client.vievuService;
        } else {
            return new DummyVievuService();
        }
    }

    public static void destroy() {
        instance = null;
    }

    private VievuService vievuService;

    private String baseUrl;
    private String accountName;
    private String authHeader;

    private static int offset = -1;
    private static int count;

    public String getAuthHeader() {
        return authHeader;
    }

    @Override
    public void intercept(RequestFacade request) {
        if (authHeader != null) {
            request.addHeader(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON);
            request.addHeader(HEADER_ACCEPT, CONTENT_TYPE_JSON);
            request.addHeader(HEADER_AUTHORIZATION, authHeader);
        }
        if (offset > -1) {
            request.addQueryParam(QUERY_PAGE_NUMBER, String.valueOf(offset));
            offset = -1;
        }
        if (count > 0) {
            request.addQueryParam(QUERY_PAGE_SIZE, String.valueOf(count));
            count = 0;
        }
    }

    @Override
    public Throwable handleError(RetrofitError cause) {
        Log.e("Got","ERROR");
        cause.printStackTrace();
        Response r = cause.getResponse();
        if (r != null && r.getStatus() == 401) {
            isIDPDone=false;
            isIDRefreshed=false;
            LogoutUtil.logOut(context);
        }
        return cause;
    }

    public void authorizeWithCode(String verificationCode, String mfaToken, String mfaProvider) {
        authHeader = null;
        vievuService.verify(verificationCode, mfaToken, mfaProvider, "two_factor_auth", new AuthLoginEventBusCallback());
    }

    public void authorizeWithPassword(String login, String password, String mfaToken) {
        authHeader = null;
        vievuService.login(login, password, mfaToken, "password", new AuthLoginEventBusCallback());
    }

    public void authorizeWithQuestion(String login, String answer, int questionId) {
        authHeader = null;
        vievuService.login(login, answer, questionId, new SecurityQuestionEventBusCallback());
    }
    private static class AuthLoginEventBusCallback extends LoginEventBusCallback<TokenData> {

        @Override
        public void success(TokenData tokenData, Response response) {
            if (tokenData != null && tokenData.isValid()) {
                WebClient.getInstance(null).authHeader = AUTHORIZATION_PREFIX + tokenData.getAccessToken();
                UserDataRepository.getInstance().initialize(
                        tokenData.getLoggedUser(), tokenData.getPermissions());
                EventBus.getDefault().post(new LoginEvent(WebClient.getInstance(null).accountName, tokenData));
            }
        }
    }

    private static class SecurityQuestionEventBusCallback extends AuthLoginEventBusCallback {

        @Override
        public void success(TokenData tokenData, Response response) {
            EventBus.getDefault().post(new LoginEvent(WebClient.getInstance(null).accountName, null));
        }
    }


    private  static void refreshUi() {


        if (mAuthState.isAuthorized()) {

            if (mAuthState.getAccessToken() != null) {

                isIDRefreshed=true;

                preferences.edit().putString("TokenAccessed","1").apply();
                preferences.edit().putString(CommonConstants.KEY_ID_TOKEN,mAuthState.getIdToken()).apply();
                preferences.edit().putString(CommonConstants.KEY_AUTH_TOKEN,mAuthState.getIdToken()).apply();

                Long expiresAt = mAuthState.getAccessTokenExpirationTime();
                preferences.edit().putLong(LogoutUtil.KEY_TOKEN_EXPIRATION_TIME, System.currentTimeMillis() + expiresAt * 1000).apply();
                getInstances(context);

            }
        }
    }
    private static void receivedTokenResponse(
            @Nullable TokenResponse tokenResponse,
            @Nullable AuthorizationException authException) {
        Log.d(TAG, "Token request complete");
        mAuthState.update(tokenResponse, authException);

        refreshUi();
    }
    private static void refreshAccessToken() {
        Log.d("TokenRefreshRequest",""+mAuthState.createTokenRefreshRequest());
        mAuthState.setNeedsTokenRefresh(true);
        performTokenRequest(mAuthState.createTokenRefreshRequest());
    }

    private static void exchangeAuthorizationCode(AuthorizationResponse authorizationResponse) {
        performTokenRequest(authorizationResponse.createTokenExchangeRequest());
    }

    private static void performTokenRequest(TokenRequest request) {
        ClientAuthentication clientAuthentication;

        try {
            clientAuthentication = mAuthState.getClientAuthentication();
            Log.d("clientAuthentication",""+mAuthState.getClientAuthentication());


        } catch (ClientAuthentication.UnsupportedAuthenticationMethod ex) {
            Log.d(TAG, "Token request cannot be made, client authentication for the token "
                    + "endpoint could not be constructed (%s)", ex);
            return;
        }

        mAuthService.performTokenRequest(
                request,
                clientAuthentication,
                new AuthorizationService.TokenResponseCallback() {
                    @Override
                    public void onTokenRequestCompleted(
                            @Nullable TokenResponse tokenResponse,
                            @Nullable AuthorizationException ex) {
                        receivedTokenResponse(tokenResponse, ex);
                    }
                });
    }


}



/*Log.d(TAG,"Time expire(currentTime): "+currentTime);
        Log.d(TAG,"Time expire(KEY_TOKEN_EXPIRATION_TIME): "+preferences.getLong(KEY_TOKEN_EXPIRATION_TIME, currentTime) );*/
            /*Log.d(TAG,"Time expire(currentTime): Not require to refresh");*/
 /* OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60000, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(6000, TimeUnit.SECONDS);    // socket timeout
        client.setWriteTimeout(6000, TimeUnit.SECONDS);
*/