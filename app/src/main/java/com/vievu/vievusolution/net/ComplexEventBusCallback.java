package com.vievu.vievusolution.net;

import com.annimon.stream.function.BiFunction;
import com.vievu.vievusolution.event.RequestCompletedEvent;

import de.greenrobot.event.EventBus;
import retrofit.client.Response;

public class ComplexEventBusCallback<TValue, TEvent extends RequestCompletedEvent<TValue>> extends EventBusCallback<TValue> {

    private BiFunction<Integer, TValue, TEvent> eventCreator;

    public ComplexEventBusCallback(BiFunction<Integer, TValue, TEvent> eventCreator) {
        super(ACTION_NONE);
        this.eventCreator = eventCreator;
    }

    public ComplexEventBusCallback(int action, BiFunction<Integer, TValue, TEvent> eventCreator) {
        super(action);
        this.eventCreator = eventCreator;
    }

    @Override
    public void success(TValue result, Response response) {
        EventBus.getDefault().post(eventCreator.apply(action, result));
    }
}
