package com.vievu.vievusolution.net;

import com.vievu.vievusolution.bean.ResultDescription;
import com.vievu.vievusolution.event.EditRequestCompletedEvent;

import de.greenrobot.event.EventBus;
import retrofit.client.Response;

public class EditEvenBusCallback<TEntity> extends EventBusCallback<ResultDescription> {

    private TEntity entity;

    public TEntity getEntity() {
        return entity;
    }

    public EditEvenBusCallback(int action, TEntity entity) {
        super(action);
        this.entity = entity;
    }

    @Override
    public void success(ResultDescription result, Response response) {
        EventBus.getDefault().post(new EditRequestCompletedEvent<>(action, result, entity));
    }
}
