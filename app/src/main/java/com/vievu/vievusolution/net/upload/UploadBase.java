package com.vievu.vievusolution.net.upload;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.OpenableColumns;
import android.util.Log;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.vievu.vievusolution.util.FileUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@SuppressWarnings("WeakerAccess")
public abstract class UploadBase implements Upload {

    protected Uri fileUri;
    protected String localFileName;
    private long localFileSize;

    public UploadBase(Uri fileUri) {
        this.fileUri = fileUri;
    }

    @Override
    public InputStream getInputStream(Context context) throws FileNotFoundException {
        InputStream result = null;
        Log.d("UploadFile", "Fetching metadata for URI: " + fileUri.toString());
        ContentResolver contentResolver = context.getContentResolver();
        //noinspection NewApi
        Cursor cursor =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
            && FileUtils.isMediaDocument(fileUri)?
                FileUtil.getMediaCursor(contentResolver, fileUri) :
                contentResolver.query(fileUri, null, null, null, null);
        if (cursor != null) {
            getDataFromCursor(cursor);
            cursor.close();
            result = contentResolver.openInputStream(fileUri);
        } else {
            Log.d("UploadFile", "Cannot get a cursor");
            java.io.File file = FileUtils.getFile(context, fileUri);
            if (file != null) {
                if (file.length() > 0) {
                    getDataFromFile(file);
                    result = new FileInputStream(file);
                } else {
                    Log.d("UploadFile", "File but has no content");
                }
            }
        }
        if (result != null) {
            Log.d("UploadFile", String.format(
                    "Uploading %s (%d bytes)", this.localFileName, this.localFileSize));
        } else {
            Log.d("UploadFile", "Nothing was found (no cursor and no file)");
        }
        return result;
    }

    protected void getDataFromCursor(Cursor cursor) throws FileNotFoundException {
        if (cursor.moveToFirst()) {
            this.localFileName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
            this.localFileSize = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
        } else {
            Log.d("UploadFile", "Nothing was found (empty cursor)");
        }
    }

    protected void  getDataFromFile(File file) throws FileNotFoundException {
        this.localFileName = file.getName();
        this.localFileSize = file.length();
    }

    @Override
    public long getFileSize() {
        return this.localFileSize;
    }
}
