package com.vievu.vievusolution.net.upload;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.bean.NewFileInfo;
import com.vievu.vievusolution.bean.NewVideo;
import com.vievu.vievusolution.bean.ResultDescription;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.DateUtil;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

class UploadFile extends UploadBase {

    public static UploadFile from(Intent intent) {
        return new UploadFile(
                intent.getParcelableExtra(UploadIntentService.EXTRA_FILE_URI),
                intent.getParcelableExtra(UploadIntentService.EXTRA_VIDEO));
    }

    private NewVideo newVideo;

    private UploadFile(Uri fileUri, File video) {
        super(fileUri);
        this.fileUri = fileUri;
        this.newVideo = video.getNewVideoProxy();
    }

    private Date readDate(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATE_TAKEN);
        if (columnIndex < 0) {
            Log.d("UploadFile", "Cursor: No column for name: " + MediaStore.Video.Media.DATE_TAKEN);
            @SuppressLint("InlinedApi")
            String backupColumn =
                Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT ?
                    MediaStore.Files.FileColumns.DATE_MODIFIED :
                    DocumentsContract.Document.COLUMN_LAST_MODIFIED;
            columnIndex = cursor.getColumnIndex(backupColumn);
            if (columnIndex < 0) {
                Log.d("UploadFile", "Cursor: No backup column for name: " + backupColumn);
                return new Date();
            }
        }
        return new Date(cursor.getLong(columnIndex));
    }

    @Override
    protected void getDataFromCursor(Cursor cursor) throws FileNotFoundException {
        super.getDataFromCursor(cursor);
        newVideo.setRecordDate(readDate(cursor));
    }

    @Override
    protected void getDataFromFile(java.io.File file) throws FileNotFoundException {
        super.getDataFromFile(file);
        newVideo.setRecordDate(new Date(file.lastModified()));
    }

    @Override
    public InputStream getInputStream(Context context) throws FileNotFoundException {
        InputStream result = super.getInputStream(context);
        newVideo.setCameraFileName(this.localFileName);
        return result;
    }

    @Override
    public NewFileInfo requestNewFileInfo() {
        NewFileInfo info = WebClient.request().newVideoSync(
                newVideo.getCameraFileName(),
                DateUtil.Formatters.UTC.PARTIAL_ISO.format(newVideo.getRecordDate()));
        if (info != null) {
            newVideo.setStorageFileName(info.getStorageFileName());
        }
        return info;
    }

    @Override
    public ResultDescription commitFile() {
        return WebClient.request().commitSync(newVideo);
    }

    @Override
    public int getErrorMessageId() {
        return R.string.error_upload_file;
    }
}
