package com.vievu.vievusolution.json;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Base64BitmapAdapter extends TypeAdapter<Bitmap> {

    @Override
    public void write(JsonWriter out, Bitmap bitmap) throws IOException {
        if (bitmap == null) {
            out.nullValue();
        } else {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream);
            out.value(Base64.encodeToString(stream.toByteArray(), Base64.NO_WRAP));
            stream.close();
        }
    }

    @Override
    public Bitmap read(JsonReader in) throws IOException {
        byte[] decodedString = Base64.decode(in.nextString(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }
}
