package com.vievu.vievusolution.json;

import com.annimon.stream.function.Function;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.vievu.vievusolution.bean.LimitedArrayList;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

public class ListUnwrapAdapterFactory implements TypeAdapterFactory {

    public static class ListUnwrapAdapter<T> extends TypeAdapter<T> {

        public static final String FIELD_TOTAL_ITEMS_COUNT = "TotalItemsCount";

        protected Type type;

        public ListUnwrapAdapter(TypeToken<T> typeToken) {
            this.type = typeToken.getType();
        }

        @Override
        public void write(JsonWriter out, T value) throws IOException {
            Gsons.CAMEL_CASE_PURE.toJson(value, type, out);
        }

        @Override
        public T read(JsonReader in) throws IOException {
            T data = null;
            if (in.peek() == JsonToken.BEGIN_OBJECT) {
                in.beginObject();
                while (in.hasNext()) {
                    if (FIELD_TOTAL_ITEMS_COUNT.equals(in.nextName())) {
                        in.skipValue();
                    } else{
                        data = Gsons.CAMEL_CASE_PURE.fromJson(in, type);
                    }
                }
                in.endObject();
            } else {
                data = Gsons.CAMEL_CASE_PURE.fromJson(in, type);
            }
            return data;
        }
    }

    public static class LimitedListUnwrapAdapter extends ListUnwrapAdapter<LimitedArrayList> {

        public LimitedListUnwrapAdapter(TypeToken<LimitedArrayList> typeToken) {
            super(typeToken);
        }

        @Override
        public LimitedArrayList<?> read(JsonReader in) throws IOException {
            LimitedArrayList<?> data = new LimitedArrayList<>();
            in.beginObject();
            while (in.hasNext()) {
                if (FIELD_TOTAL_ITEMS_COUNT.equals(in.nextName())) {
                    data.setLimit(in.nextInt());
                } else{
                    data.addAll(Gsons.CAMEL_CASE_PURE.fromJson(in, type));
                }
            }
            in.endObject();
            return data;
        }
    }

    private HashMap<Type, ListUnwrapAdapter> adapterCache = new HashMap<>();
    private HashMap<Type, LimitedListUnwrapAdapter> limitedAdapterCache = new HashMap<>();


    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        if (type.getRawType() == LimitedArrayList.class) {
            //noinspection unchecked
            return create(type, limitedAdapterCache, t -> new LimitedListUnwrapAdapter((TypeToken<LimitedArrayList>)t));
        }
        if (type.getRawType() == List.class) {
            return create(type, adapterCache, ListUnwrapAdapter::new);
        }
        return null;
    }

    private <T, TUnwrapAdapter extends ListUnwrapAdapter> TypeAdapter<T> create(
            TypeToken<T> type,
            HashMap<Type, TUnwrapAdapter> cache,
            Function<TypeToken<T>, TUnwrapAdapter> creator) {

        TUnwrapAdapter adapter = cache.get(type.getType());
        if (adapter == null) {
            adapter = creator.apply(type);
            cache.put(type.getType(), adapter);
        }
        //noinspection unchecked
        return (TypeAdapter<T>)adapter;
    }
}
