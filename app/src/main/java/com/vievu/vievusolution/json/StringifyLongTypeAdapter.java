package com.vievu.vievusolution.json;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class StringifyLongTypeAdapter extends TypeAdapter<Long> {

    @Override
    public void write(JsonWriter out, Long value) throws IOException {
        if (value != null) {
            out.value(value.toString());
        }
    }

    @Override
    public Long read(JsonReader in) throws IOException {
        return in.nextLong();
    }
}
