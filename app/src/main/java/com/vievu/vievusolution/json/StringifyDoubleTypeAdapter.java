package com.vievu.vievusolution.json;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class StringifyDoubleTypeAdapter extends TypeAdapter<Double> {
    @Override
    public void write(JsonWriter out, Double value) throws IOException {
        if (value != null) {
            out.value(value.toString());
        }
    }

    @Override
    public Double read(JsonReader in) throws IOException {
        return in.nextDouble();
    }
}