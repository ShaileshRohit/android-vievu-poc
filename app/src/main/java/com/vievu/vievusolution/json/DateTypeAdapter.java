package com.vievu.vievusolution.json;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.vievu.vievusolution.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

public class DateTypeAdapter extends TypeAdapter<Date> {

    @Override
    public void write(JsonWriter out, Date value) throws IOException {
        if (value != null) {
            out.value(DateUtil.Formatters.UTC.PARTIAL_ISO.format(value));
        } else {
            out.nullValue();
        }
    }

    @Override
    public Date read(JsonReader in) throws IOException {
        JsonToken token = in.peek();
        switch (token) {
            case STRING :
                String date = in.nextString();
                if (date != null && !date.isEmpty()) {
                    try {
                        return DateUtil.Formatters.UTC.PARTIAL_ISO.parse(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case NULL:
                in.nextNull();
                break;
        }
        return null;
    }
}
