package com.vievu.vievusolution.json;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.vievu.vievusolution.bean.User;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class UserJsonAdapter extends TypeAdapter<User> {

    @Override
    public void write(JsonWriter out, User user) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        user.getPicture().compress(Bitmap.CompressFormat.JPEG, 95, stream);

        out
            .beginObject()
                .name(User.FIELD_USER_ID).value(user.getUserId())
                .name(User.FIELD_USER_NAME).value(user.getUserName())
                .name(User.FIELD_VIDEO_COUNT).value(user.getVideoCount())
                .name(User.FIELD_MIME_TYPE).value(user.getMimeType())
                .name(User.FIELD_PICTURE).value(Base64.encodeToString(stream.toByteArray(), Base64.NO_WRAP))
            .endObject();
        stream.close();
    }

    @Override
    public User read(JsonReader in) throws IOException {
        User user = new User();
        in.beginObject();
        while (in.hasNext()) {
            switch (in.nextName()) {
                case User.FIELD_USER_ID :
                    user.setUserId(in.nextInt());
                    break;
                case User.FIELD_USER_NAME :
                    user.setUserName(in.nextString());
                    break;
                case User.FIELD_VIDEO_COUNT :
                    user.setVideoCount(in.nextInt());
                    break;
                case User.FIELD_MIME_TYPE :
                    user.setMimeType(in.nextString());
                    break;
                case User.FIELD_PICTURE :
                    byte[] decodedString = Base64.decode(in.nextString(), Base64.DEFAULT);
                    user.setPicture(BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length));
                    break;
                default:
                    in.skipValue();
                    break;
            }
        }
        in.endObject();
        return user;
    }
}
