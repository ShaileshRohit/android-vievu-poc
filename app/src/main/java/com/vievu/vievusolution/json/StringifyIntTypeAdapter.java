package com.vievu.vievusolution.json;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class StringifyIntTypeAdapter extends TypeAdapter<Integer> {
    @Override
    public void write(JsonWriter out, Integer value) throws IOException {
        if (value != null) {
            out.value(value.toString());
        }
    }

    @Override
    public Integer read(JsonReader in) throws IOException {
        return in.nextInt();
    }
}