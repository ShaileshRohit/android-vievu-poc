package com.vievu.vievusolution.json;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class StringifyBooleanTypeAdapter extends TypeAdapter<Boolean> {
    @Override
    public void write(JsonWriter out, Boolean value) throws IOException {
        if (value != null) {
            out.value(value.toString());
        }
    }

    @Override
    public Boolean read(JsonReader in) throws IOException {
        return in.nextBoolean();
    }
}
