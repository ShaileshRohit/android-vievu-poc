package com.vievu.vievusolution.json;

import android.graphics.Bitmap;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.Date;

public class Gsons {

    public static final Gson CAMEL_CASE_PURE;
    public static final Gson CAMEL_CASE_WITH_LISTS_UNWRAPPING;
    public static final Gson CAMEL_CASE_FTP_ONLY;

    static {
        GsonBuilder builder = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE);
        CAMEL_CASE_PURE = builder
                .registerTypeAdapter(Date.class, new DateTypeAdapter())
                .registerTypeAdapter(Bitmap.class, new Base64BitmapAdapter())
                .addSerializationExclusionStrategy(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
                        Expose exposeAnnotation = fieldAttributes.getAnnotation(Expose.class);
                        return exposeAnnotation != null && !exposeAnnotation.serialize();
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        CAMEL_CASE_WITH_LISTS_UNWRAPPING = builder
            .registerTypeAdapterFactory(new ListUnwrapAdapterFactory())
            .create();

        CAMEL_CASE_FTP_ONLY = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .registerTypeAdapter(Date.class, new DateTypeAdapter())
                .registerTypeAdapter(Integer.class, new StringifyIntTypeAdapter())
                .registerTypeAdapter(Long.class, new StringifyLongTypeAdapter())
                .registerTypeAdapter(Boolean.class, new StringifyBooleanTypeAdapter())
                .registerTypeAdapter(Double.class, new StringifyDoubleTypeAdapter())
                .create();

    }
}
