package com.vievu.vievusolution;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.event.PingCompletedEvent;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.net.camera.ThumbnailLoader;

import java.util.LinkedHashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

public abstract class EventBusActivity extends BaseVievuActivity {

    private static final IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

    private static ConnectivityStateReceiver connectivityStateReceiver = new ConnectivityStateReceiver();

    private static class ConnectivityStateReceiver extends BroadcastReceiver {

        private int action;

        private ConnectivityManager connectivityManager;

        private boolean isInOnlineMode = true;

        @Override
        public void onReceive(Context context, Intent intent) {

            if (this.connectivityManager == null) {
                this.connectivityManager = (ConnectivityManager) context
                        .getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            }

            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
            if (isConnected) {
                if (!EventBus.getDefault().isRegistered(this)) {
                    action = WebClient.Actions.unique(0);
                    EventBus.getDefault().register(this);
                    WebClient.pingAsync(action);
                }
            } else {
                this.isInOnlineMode = false;
                EventBus.getDefault().post(new OfflineModeChangedEvent(this.isInOnlineMode));
                ThumbnailLoader.invalidateCache();
                CameraClient.getInstance().setRtspRequired(true);
            }
        }

        public void onEvent(PingCompletedEvent event) {
            if (event.getAction() == action) {
                EventBus.getDefault().unregister(this);
                if (this.isInOnlineMode != event.isConnected()) {
                    this.isInOnlineMode = event.isConnected();
                    EventBus.getDefault().post(new OfflineModeChangedEvent(this.isInOnlineMode));
                }
            }
        }
    }

    protected int currentAction;
    protected boolean isInBackground = true;
    protected LinkedHashMap<Class<?>, Object> undeliveredEvents = new LinkedHashMap<>();

    public boolean isInBackground() {
        return isInBackground;
    }

    public boolean isInOnlineMode() {
        return connectivityStateReceiver.isInOnlineMode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        isInBackground = false;
        for (Map.Entry<Class<?>, Object> pair : undeliveredEvents.entrySet()) {
            EventBus.getDefault().post(pair.getValue());
        }
        undeliveredEvents.clear();
        if (BuildConfig.USES_BACKEND) {
            registerReceiver(connectivityStateReceiver, filter);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isInBackground = true;
        if (BuildConfig.USES_BACKEND) {
            unregisterReceiver(connectivityStateReceiver);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public boolean keepEvent(Object event) {
        if (isInBackground) {
            // Remove is necessary to keep event chronology
            undeliveredEvents.remove(event.getClass());
            undeliveredEvents.put(event.getClass(), event);
        }
        return isInBackground;
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public void onEvent(NetworkErrorEvent event) {
        if (event.is(currentAction) && !keepEvent(event)) {
            event.displayError(this);
        }
    }
}
