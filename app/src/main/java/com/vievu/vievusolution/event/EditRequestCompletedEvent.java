package com.vievu.vievusolution.event;

import com.vievu.vievusolution.bean.ResultDescription;

public class EditRequestCompletedEvent<TEntity> extends RequestCompletedEvent<ResultDescription> {

    private TEntity changedEntity;

    public TEntity getChangedEntity() {
        return changedEntity;
    }

    public EditRequestCompletedEvent(int action, ResultDescription result, TEntity changedEntity) {
        super(action, result);
        this.changedEntity = changedEntity;
    }
}
