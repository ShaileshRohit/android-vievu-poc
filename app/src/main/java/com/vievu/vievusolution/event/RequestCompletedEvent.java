package com.vievu.vievusolution.event;

import com.vievu.vievusolution.net.EventBusCallback;

public class RequestCompletedEvent<T> extends NetworkEvent {

    protected T result;

    public T getResult() {
        return result;
    }

    public boolean contains(Class<?> type) {
        return result != null && result.getClass() == type;
    }

    public RequestCompletedEvent(T result) {
        this(EventBusCallback.ACTION_NONE, result);
    }

    public RequestCompletedEvent(int action, T result) {
        super(action);
        this.result = result;
    }
}
