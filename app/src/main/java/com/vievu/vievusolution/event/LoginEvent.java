package com.vievu.vievusolution.event;

import com.vievu.vievusolution.bean.TokenData;

public class LoginEvent {

    private String accountName;

    public String getAccountName() {
        return accountName;
    }

    private TokenData tokenData;

    public TokenData getTokenData() {
        return tokenData;
    }

    public LoginEvent(){}
    public LoginEvent(String accountName, TokenData tokenData) {
        this.accountName = accountName;
        this.tokenData = tokenData;
    }
}
