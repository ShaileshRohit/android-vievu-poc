package com.vievu.vievusolution.event;

public class UploadProgressEvent {

    private int action;
    private long bytesUploaded;
    private long bytesTotal;
    private boolean uploadCompleted;

    public UploadProgressEvent(int action, long bytesTotal) {
        this.action = action;
        this.bytesTotal = bytesTotal;
        this.bytesUploaded = 0;
    }

    public int getAction() {
        return action;
    }

    public void addBytesUploaded(long bytesUploaded) {
        this.bytesUploaded += bytesUploaded;
    }

    public long getBytesUploaded() {
        return bytesUploaded;
    }

    public long getBytesTotal() {
        return bytesTotal;
    }

    public int getPercents() {
        return (int) (((double) bytesUploaded / bytesTotal) * 100);
    }

    public boolean isUploadCompleted() {
        return uploadCompleted;
    }

    public void setUploadCompleted(boolean uploadCompleted) {
        this.uploadCompleted = uploadCompleted;
    }
}
