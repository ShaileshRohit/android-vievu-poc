package com.vievu.vievusolution.event;

import com.vievu.vievusolution.bean.ResultDescription;

public class UploadCompletedEvent {

    private int action;
    private boolean isCompleted;
    private String storageFileName;
    private Throwable cause;
    private String errorMessage;

    public UploadCompletedEvent(int action, String storageFileName) {
        isCompleted = true;
        this.action = action;
        this.storageFileName = storageFileName;
    }

    public UploadCompletedEvent(int action, ResultDescription description) {
        isCompleted = description.isApplied();
        this.action = action;
        errorMessage = description.getMessage();
    }

    public int getAction() {
        return action;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public String getStorageFileName() {
        return storageFileName;
    }

    public Throwable getCause() {
        return cause;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
