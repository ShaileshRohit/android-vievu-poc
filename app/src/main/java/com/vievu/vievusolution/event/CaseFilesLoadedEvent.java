package com.vievu.vievusolution.event;

import com.vievu.vievusolution.bean.CaseFile;

import java.util.List;

public class CaseFilesLoadedEvent extends RequestCompletedEvent<List<CaseFile>> {

    public CaseFilesLoadedEvent(int action, List<CaseFile> result) {
        super(action, result);
    }
}
