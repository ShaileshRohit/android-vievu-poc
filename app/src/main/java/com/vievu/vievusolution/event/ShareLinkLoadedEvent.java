package com.vievu.vievusolution.event;

import android.net.Uri;

import com.vievu.vievusolution.bean.ShareLink;

public class ShareLinkLoadedEvent extends RequestCompletedEvent<ShareLink> {

    public ShareLinkLoadedEvent(int action, ShareLink result) {
        super(action, result);
    }

    public Uri getShareLink() {
        return result != null ? result.getShareLink() : null;
    }
}
