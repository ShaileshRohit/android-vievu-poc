package com.vievu.vievusolution.event;

public class OfflineModeChangedEvent {

    private boolean isInOnlineMode;

    public OfflineModeChangedEvent(boolean isInOnlineMode) {
        this.isInOnlineMode = isInOnlineMode;
    }

    public boolean isInOnlineMode() {
        return isInOnlineMode;
    }
}
