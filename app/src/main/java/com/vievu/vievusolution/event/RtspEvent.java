package com.vievu.vievusolution.event;

public class RtspEvent {

    private boolean isConnected;

    public boolean isConnected() {
        return isConnected;
    }

    public RtspEvent(boolean isConnected) {
        this.isConnected = isConnected;
    }
}
