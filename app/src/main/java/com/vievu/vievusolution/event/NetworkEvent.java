package com.vievu.vievusolution.event;

public class NetworkEvent {

    protected int action;

    public int getAction() {
        return action;
    }

    public boolean is(int action) {
        return this.action == action;
    }

    public NetworkEvent(int action) {
        this.action = action;
    }
}
