package com.vievu.vievusolution.event;

import android.content.Context;
import android.widget.Toast;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.ErrorDescription;
import com.vievu.vievusolution.net.EventBusCallback;

import java.net.UnknownHostException;

public class NetworkErrorEvent extends NetworkEvent {

    private Throwable cause;

    private ErrorDescription description;

    public Throwable getCause() {
        return cause;
    }

    public ErrorDescription getDescription() {
        return description;
    }

    public NetworkErrorEvent(int action, Throwable cause, ErrorDescription description) {
        super(action);
        this.cause = cause;
        this.description = description;
    }

    public NetworkErrorEvent(Throwable cause, ErrorDescription description) {
        this(EventBusCallback.ACTION_NONE, cause, description);
    }

    public NetworkErrorEvent(int action, Throwable cause) {
        this(action, cause, null);
    }

    public void displayError(Context context) {
        String message = null;
        if (cause instanceof UnknownHostException) {
            message = context.getString(R.string.error_no_internet);
        } else if (description != null) {
            message = description.getDescription();
        }
        if (message == null) {
            message = context.getString(R.string.error_unknown);
        }
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
