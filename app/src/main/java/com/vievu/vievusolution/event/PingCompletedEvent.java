package com.vievu.vievusolution.event;

public class PingCompletedEvent {

    private boolean isConnected;
    private int action;

    public PingCompletedEvent(boolean isConnected, int action) {
        this.isConnected = true;
        this.action = action;
    }

    public int getAction() {
        return action;
    }

    public boolean isConnected() {
        return isConnected;
    }
}
