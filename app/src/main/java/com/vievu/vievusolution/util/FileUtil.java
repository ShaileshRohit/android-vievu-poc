package com.vievu.vievusolution.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;

import com.vievu.vievusolution.R;

import java.io.File;

public class FileUtil {

    private static String[] units = new String[] {
            "B",
            "KB",
            "MB",
            "GB"
    };

    public static String getShortFileSizeString(long sizeInBytes) {
        double value = sizeInBytes;
        int i = 0;
        for (; i < units.length; i++) {
            if (value < 100) {
                break;
            }
            value /= 1024;
        }
        return String.format("%.2f %s", value, units[i]);
    }

    public static String generateUniqueName(File folder, String originalName) {
        String nameOnly = originalName;
        String extension = "";
        int lastPointIndex = originalName.lastIndexOf(".");
        int version = 0;
        if (lastPointIndex > 0) {
            nameOnly = originalName.substring(0, lastPointIndex);
            extension = originalName.substring(lastPointIndex + 1);
        }
        String uniqueName;
        do {
            version++;
            uniqueName = String.format("%s_%d.%s", nameOnly, version, extension);
        } while (new File(folder, uniqueName).exists());
        return uniqueName;
    }

    public static void playVideoFile(Activity activity, Uri fileUri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(fileUri, "video/*");
        activity.startActivity(Intent.createChooser(
                intent, activity.getString(R.string.chooser_action_play)));
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static Cursor getMediaCursor(ContentResolver contentResolver, Uri fileUri) {
        String[] idParts = DocumentsContract.getDocumentId(fileUri).split(":");
        Uri contentUri = null;
        if("image".equals(idParts[0])) {
            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        } else if("video".equals(idParts[0])) {
            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else if("audio".equals(idParts[0])) {
            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        }
        if (contentUri != null) {
            String sel = MediaStore.Images.Media._ID + "=?";
            return contentResolver.query(
                    contentUri,
                    null, sel, new String[]{idParts[1]}, null);
        }
        return null;
    }
}
