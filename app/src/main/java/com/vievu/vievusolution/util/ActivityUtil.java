package com.vievu.vievusolution.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;

import com.vievu.vievusolution.R;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ActivityUtil {

    public static void hideKeyboard(Activity activity) {
        if (activity != null) {
            View view = activity.getCurrentFocus();
            hideKeyboard(view);
        }
    }

    public static void hideKeyboard(Dialog dialog) {
        if (dialog != null) {
            View view = dialog.getCurrentFocus();
            hideKeyboard(view);
        }
    }

    private static void hideKeyboard(View view) {
        if (view != null) {
            view.clearFocus();
            InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void putMapExtra(Intent intent, String key, Map<String, String> map) {
        Bundle bundle = new Bundle();
        putMapExtra(bundle, key, map);
        intent.putExtras(bundle);
    }

    public static void putMapExtra(Bundle bundle, String key, Map<String, String> map) {
        String[] array = new String[map.size() * 2];
        int i = 0;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            array[i] = entry.getKey();
            array[i + 1] = entry.getValue();
            i += 2;
        }
        bundle.putStringArray(key, array);
    }

    public static Map<String, String> getMapExtra(Bundle bundle, String key, Map<String, String> defaultValue) {
        String[] array = bundle.getStringArray(key);
        if (array != null) {
            HashMap<String, String> extra = new HashMap<>(array.length / 2);
            for (int i = 0; i < array.length; i += 2) {
                extra.put(array[i], array[i + 1]);
            }
            return extra;
        }
        return defaultValue;
    }

    public static void forceOverFlowIcon(Activity activity) {
        try {
            ViewConfiguration config = ViewConfiguration.get(activity);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            // Should not be called
            e.printStackTrace();
        }
    }

    public static void replaceFragment(Activity activity, Fragment fragment, boolean withBackStack, int containerId) {
        if (!fragment.isAdded()) {
            FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
            if (withBackStack) {
                transaction.addToBackStack(null);
            }
            transaction.replace(containerId, fragment).commit();
        }
    }

    public static void replaceFragment(Activity activity, Fragment fragment, boolean withBackStack) {
        replaceFragment(activity, fragment, withBackStack, R.id.container);
    }

    public static void replaceFragment(Activity activity, Fragment fragment) {
        replaceFragment(activity, fragment, false);
    }

    public static void tryBindNetworkAdapter(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d("Connectivity", "Binding network");
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info;
            for (Network network : connectivityManager.getAllNetworks()) {
                info = connectivityManager.getNetworkInfo(network);
                if (info != null) {
                    Log.d("Connectivity", String.format("Network: %s, type: %d", info.toString(), info.getType()));
                    if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                        Log.d("Connectivity", "Binding to network " + info.toString());
                        connectivityManager.bindProcessToNetwork(network);
                        break;
                    }
                } else {
                    Log.d("Connectivity", "Cannot get info");
                }
            }
        }
    }

    public static Network tryUnbindNetworkAdapter(Context context) {
        Network network = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d("Connectivity", "Unbinding network");
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            network = connectivityManager.getBoundNetworkForProcess();
            connectivityManager.bindProcessToNetwork(null);
        }
        return network;
    }
}
