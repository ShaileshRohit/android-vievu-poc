package com.vievu.vievusolution.util;

import android.content.Context;

import com.vievu.vievusolution.json.Gsons;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

public class JsonCacheUtil {

    public static void cacheCopy(Context context, String fileName, Object data) {
        File file = new File(context.getFilesDir(), fileName);
        try {
            FileWriter writer = new FileWriter(file, false);
            Gsons.CAMEL_CASE_PURE.toJson(data, writer);
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> T restoreFromCache(Context context, String fileName, Type type) {
        File file = new File(context.getFilesDir(), fileName);
        T result = null;
        try {
            if (file.exists()) {
                FileReader reader = new FileReader(file);
                result = Gsons.CAMEL_CASE_PURE.fromJson(reader, type);
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;

    }
}
