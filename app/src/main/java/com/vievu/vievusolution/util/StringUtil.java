package com.vievu.vievusolution.util;

import com.google.gson.reflect.TypeToken;
import com.vievu.vievusolution.json.Gsons;

import java.util.HashMap;

public class StringUtil {

    private static String[] units = new String[] {
            "B",
            "KB",
            "MB",
            "GB"
    };

    public static String getShortSizeString(long sizeInBytes) {
        double value = sizeInBytes;
        int i = 0;
        for (; i < units.length; i++) {
            if (value < 100) {
                break;
            }
            value /= 1024;
        }
        return String.format("%.2f %s", value, units[i]);
    }

    public static String serialize(HashMap<String, String> map) {
        return Gsons.CAMEL_CASE_PURE.toJson(map);
    }

    public static HashMap<String, String> deserialize(String json) {
        return Gsons.CAMEL_CASE_PURE.fromJson(json, new TypeToken<HashMap<String, String>>(){}.getType());
    }
}
