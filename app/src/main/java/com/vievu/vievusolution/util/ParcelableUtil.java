package com.vievu.vievusolution.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.vievu.vievusolution.bean.GeoLocation;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ParcelableUtil {

    public static void writeBoolean(Parcel out, boolean value) {
        out.writeByte((byte) (value ? 1 : 0));
    }

    public static boolean readBoolean(Parcel in) {
        return in.readByte() > 0;
    }

    public static void writeDate(Parcel out, Date value) {
        if (value != null) {
            out.writeByte((byte)1);
            out.writeLong(value.getTime());
        } else {
            out.writeByte((byte)0);
        }
    }

    public static Date readDate(Parcel in) {
        if (in.readByte() != 0) {
            return new Date(in.readLong());
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Parcelable> void writeList(Parcel out, List<T> list, int flags) {
        boolean hasValue = list != null && !list.isEmpty();
        writeBoolean(out, hasValue);
        if (hasValue) {
            out.writeParcelableArray(list.toArray((T[]) Array.newInstance(list.get(0).getClass(), list.size())), flags);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> ArrayList<T> readList(Parcel in, Class<T> itemClass) {
        return
            readBoolean(in) ?
                new ArrayList<>(Arrays.asList((T[]) in.readParcelableArray(itemClass.getClassLoader()))) :
                new ArrayList<>();
    }

    public static void writeGeoLocation(Parcel out, GeoLocation value) {
        boolean hasValue = value != null;
        writeBoolean(out, hasValue);
        if (hasValue) {
            out.writeDouble(value.getLongitude());
            out.writeDouble(value.getLatitude());
        }
    }

    public static GeoLocation readGeoLocation(Parcel in) {
        return readBoolean(in) ?
            new GeoLocation(in.readDouble(), in.readDouble()) :
            null;
    }
}
