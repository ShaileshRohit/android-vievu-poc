package com.vievu.vievusolution.util;

import com.annimon.stream.function.Predicate;

public class CollectionsUtil {

    public static <T> int indexOf(Iterable<T> source, Predicate<T> predicate) {
        int index = 0;
        for (T item : source) {
            if (predicate.test(item)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    public static int indexOf(int[] source, int value) {
        for (int i = 0; i < source.length; i++) {
            if (value == source[i]) {
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(long[] source, long value) {
        for (int i = 0; i < source.length; i++) {
            if (value == source[i]) {
                return i;
            }
        }
        return -1;
    }
}
