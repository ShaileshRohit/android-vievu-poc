package com.vievu.vievusolution.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.android.internal.util.Predicate;

import java.util.regex.Pattern;

public class ValidationUtil {

    private static Pattern anyLetterPattern;

    public static boolean hasAnyLetter(CharSequence input) {
        if (anyLetterPattern == null) {
            anyLetterPattern = Pattern.compile("\\w+");
        }
        return anyLetterPattern.matcher(input).find();
    }

    public static boolean hasAnyLetter(TextView view) {
        return hasAnyLetter(view.getText());
    }

    public static boolean hasAnySymbol(CharSequence input) {
        return input.length() > 0;
    }

    public static boolean hasAnySymbol(TextView view) {
        return hasAnySymbol(view.getText());
    }

    public static boolean isValidLogin(CharSequence login) {
        return login != null && login.length() > 0;
    }

    public static boolean isValidPassword(CharSequence password) {
        return password != null && password.length() > 0;
    }

    public static boolean validateEditText(EditText editText, Predicate<CharSequence> validator, int errorMessageId) {
        if (!validator.apply(editText.getText())) {
            String errorMessage = editText.getContext().getString(errorMessageId);
            editText.setError(errorMessage);
            SingleSimpleWatcher.applyTo(editText, validator, errorMessage);
            return false;
        }
        return true;
    }

    public static class SimpleWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private static class SingleSimpleWatcher extends SimpleWatcher {

        private EditText editText;
        private String errorMessage;
        Predicate<CharSequence> validator;

        public static SingleSimpleWatcher applyTo(EditText editText, final Predicate<CharSequence> validator, String errorMessage) {
            SingleSimpleWatcher watcher = new SingleSimpleWatcher();
            watcher.editText = editText;
            watcher.errorMessage = errorMessage;
            watcher.validator = validator;
            editText.removeTextChangedListener(watcher);
            editText.addTextChangedListener(watcher);
            return watcher;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            validate(editable);
        }

        private void validate(CharSequence value) {
            if (validator.apply(value)) {
                editText.setError(null);
            } else {
                editText.setError(errorMessage);
            }
        }

        @Override
        public boolean equals(Object other) {
            return other != null
                && other instanceof SingleSimpleWatcher
                && editText.equals(((SingleSimpleWatcher)other).editText);
        }

        @Override
        public int hashCode() {
            return editText.hashCode();
        }
    }
}
