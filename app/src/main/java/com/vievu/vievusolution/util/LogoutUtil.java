package com.vievu.vievusolution.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.bean.TokenData;
import com.vievu.vievusolution.data.AssignedCamerasAsyncRepository;
import com.vievu.vievusolution.data.CaseStatusesAsyncRepository;
import com.vievu.vievusolution.data.CategoriesAsyncRepository;
import com.vievu.vievusolution.data.TagsAsyncRepository;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.openid_provider.TenantActivity;

public class LogoutUtil {

    public  static final String KEY_TOKEN_EXPIRATION_TIME = "expiration_time";

    public static void  logOut(Context caller) {
        WebClient.destroy();
        UserDataRepository.getInstance().clearData(caller);
        CategoriesAsyncRepository.getInstance().clearData();
        TagsAsyncRepository.getInstance().clearData();
        CaseStatusesAsyncRepository.getInstance().clearData();
        AssignedCamerasAsyncRepository.getInstance().clearData();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(caller);
        preferences.edit().clear().apply();
        //preferences.edit().putString("TokenAccessed","0").apply();
        //Intent logoutIntent = new Intent(caller, LoginActivity.class);
        Intent logoutIntent = new Intent(caller, TenantActivity.class);
        logoutIntent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK
                |Intent.FLAG_ACTIVITY_CLEAR_TASK);
        caller.startActivity(logoutIntent);
    }

    public static void logoutIfNecessary(Context caller) {
        if (!validateAuthData(caller)) {
            Log.d("LogoutUtil", "Auth token empty or expired");
            logOut(caller);
        }
    }

    public static boolean validateAuthData(Context caller) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(caller);
        long currentTime = System.currentTimeMillis();
        if (preferences.contains(CommonConstants.KEY_AUTH_TOKEN)
            && preferences.getLong(KEY_TOKEN_EXPIRATION_TIME, currentTime) > currentTime) {
            return true;
        } else {
            PreferenceManager.getDefaultSharedPreferences(caller).edit()
                    .remove(CommonConstants.KEY_AUTH_TOKEN)
                .apply();
            return false;
        }
    }

    public static void storeAuthData(Context caller, TokenData tokenData) {
        PreferenceManager.getDefaultSharedPreferences(caller).edit()
                .putString(CommonConstants.KEY_AUTH_TOKEN, tokenData.getAccessToken())
                .putLong(KEY_TOKEN_EXPIRATION_TIME, System.currentTimeMillis() + tokenData.getExpiresIn() * 1000)
            .apply();
    }
}
