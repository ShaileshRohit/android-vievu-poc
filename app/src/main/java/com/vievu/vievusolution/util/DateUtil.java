package com.vievu.vievusolution.util;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class DateUtil {

    public static class Formats {
        public static final String PARTIAL_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        public static final String DURATION = "HH:mm:ss";
        public static final String DURATION_SHORT = "HH:mm";
        public static final String CAMERA = "yyyyMMdd'T'hhmmss";
    }

    public static class Formatters {

        public static final DateFormat DATE_TIME_SHORT = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.SHORT);
        public static final DateFormat DATE_TIME = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.MEDIUM);

        @SuppressLint("SimpleDateFormat")
        public static final DateFormat CAMERA = new SimpleDateFormat(Formats.CAMERA);

        private static void setTimezoneOffset(TimeZone timeZone, DateFormat... formats) {
            if (formats.length > 0) {
                for (DateFormat format : formats) {
                    format.setTimeZone(timeZone);
                }
            }
        }

        @SuppressLint("SimpleDateFormat")
        public static class UTC {

            public static final SimpleDateFormat PARTIAL_ISO = new SimpleDateFormat(Formats.PARTIAL_ISO);
            public static final SimpleDateFormat DURATION = new SimpleDateFormat(Formats.DURATION);
            public static final SimpleDateFormat DURATION_SHORT = new SimpleDateFormat(Formats.DURATION_SHORT);

            static {
                setTimezoneOffset(TimeZone.getTimeZone("UTC"), PARTIAL_ISO, DURATION, DURATION_SHORT);
            }
        }
    }
}
