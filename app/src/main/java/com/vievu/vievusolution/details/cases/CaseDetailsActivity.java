package com.vievu.vievusolution.details.cases;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.bean.CaseFile;
import com.vievu.vievusolution.bean.FileInfo;
import com.vievu.vievusolution.bean.VideoFileInfo;
import com.vievu.vievusolution.data.FileUrlCache;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.details.BaseDetailsActivity;
import com.vievu.vievusolution.details.BaseUploadProgressActivity;
import com.vievu.vievusolution.details.VideoPlayerFragment;
import com.vievu.vievusolution.dialog.TwoOptionDialogFragment;
import com.vievu.vievusolution.event.CaseFilesLoadedEvent;
import com.vievu.vievusolution.event.EditRequestCompletedEvent;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.ComplexEventBusCallback;
import com.vievu.vievusolution.net.EditEvenBusCallback;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.net.upload.UploadIntentService;
import com.vievu.vievusolution.util.ActivityUtil;
import com.vievu.vievusolution.util.FileUtil;

import java.io.File;
import java.net.URLConnection;

import butterknife.Bind;

public class CaseDetailsActivity extends BaseUploadProgressActivity<Case> {

    public static final int REQUEST_CODE_CASE_DETAILS = 201;

    private static final String EXTRA_REMOTE_FILE = "CaseDetailsActivity.currentRemoteFile";

    public static void start(Activity caller, Case item) {
        BaseDetailsActivity.start(caller, item, CaseDetailsActivity.class, REQUEST_CODE_CASE_DETAILS);
    }

    private CaseFile currentRemoteFile;
    @Bind(R.id.indeterminateProgressBarOverlay) View indeterminateProgressBarOverlay;
    private View verboseProgressBarOverlay;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_case_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            showFragment(new CaseDetailsFragment(), false);
        }
    }

    @Override
    protected void initialize(Bundle bundle) {
        super.initialize(bundle);
        ActivityUtil.forceOverFlowIcon(this);
        item = bundle.getParcelable(EXTRA_ITEM);
        if (bundle.containsKey(EXTRA_REMOTE_FILE)) {
            currentRemoteFile = bundle.getParcelable(EXTRA_REMOTE_FILE);
        }
        this.verboseProgressBarOverlay = progressBarOverlay;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (currentRemoteFile != null) {
            outState.putParcelable(EXTRA_REMOTE_FILE, currentRemoteFile);
        }
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().findFragmentById(R.id.container) instanceof VideoPlayerFragment) {
            setProgressOverlayVisible(false);
        }
        super.onBackPressed();
    }

    public void showCaseEditFragment() {
        showFragment(new CaseEditFragment(), true);
    }

    private void setActiveProgressOverlay(View overlay) {
        this.progressBarOverlay = overlay;
    }

    public void requestCaseFilesInfo() {
        setActiveProgressOverlay(indeterminateProgressBarOverlay);
        currentAction = WebClient.Actions.unique(WebClient.Actions.CASES_GET_FILES);
        WebClient.request().filesBy(item.getCaseId(), new ComplexEventBusCallback<>(currentAction, CaseFilesLoadedEvent::new));
    }

    public void requestCaseFile(CaseFile caseFile) {
        setActiveProgressOverlay(indeterminateProgressBarOverlay);
        currentRemoteFile = caseFile;
        Uri uri = FileUrlCache.getInstance().getFileUrl(caseFile.getStorageFileName(caseFile.isVideo()));
        if (uri == null) {
            if (caseFile.isVideo()) {
                currentAction = WebClient.Actions.unique(WebClient.Actions.VIDEOS_CONVERTED_FILE_NAME);
                WebClient.request().convertedVideoFileInfo(caseFile.getStorageFileName(), new EventBusCallback<>(currentAction));
            } else {
                currentAction = WebClient.Actions.unique(WebClient.Actions.CASES_GET_FILE_NAME);
                WebClient.request().caseFileInfo(item.getCaseId(), caseFile.getStorageFileName(), new EventBusCallback<>(currentAction));
            }
        } else {
            openFile(uri);
        }
    }

    private void openFile(Uri uri) {
        setActiveProgressOverlay(indeterminateProgressBarOverlay);
        Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof CaseDetailsFragment) {
            ((CaseDetailsFragment) fragment).onCaseFileLoaded();
        }
        if (currentRemoteFile.isVideo()) {
            setProgressOverlayVisible(true);
            showFragment(VideoPlayerFragment.create(uri), true);
        } else if (requestPermissionIfNecessary(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showDialogAfterPermission(currentRemoteFile.getFileName());
        }
    }
    private void showDialogAfterPermission(String localFileName){
        Uri uri = FileUrlCache.getInstance().getFileUrl(currentRemoteFile.getStorageFileName(currentRemoteFile.isVideo()));
        File localFile = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                localFileName);
        if (localFile.exists()) {
            String uniqueName = FileUtil.generateUniqueName(localFile.getParentFile(), localFileName);
            TwoOptionDialogFragment dialog = TwoOptionDialogFragment.create(
                    R.string.title_dialog_download_file,
                    getString(R.string.template_rename_or_replace_file_message, localFileName, uniqueName));
            dialog.setPositiveButton(R.string.button_dialog_replace, d -> {
                //noinspection ResultOfMethodCallIgnored
                localFile.delete();
                downloadFile(uri, localFileName);
            });
            dialog.setNegativeButton(R.string.button_dialog_rename, d -> downloadFile(uri, uniqueName));
            dialog.show(getFragmentManager(), null);
        } else {
            TwoOptionDialogFragment dialog = TwoOptionDialogFragment.create(
                    R.string.title_dialog_download_file,
                    getString(R.string.text_dialog_confirm_download));
            dialog.setPositiveButton(R.string.text_yes, d -> downloadFile(uri, localFileName));
            dialog.setNegativeButton(R.string.text_no, null);
            dialog.show(getFragmentManager(), null);

        }
    }

    private void downloadFile(Uri remoteUri, String localFileName) {
        String mimeType = URLConnection.guessContentTypeFromName(localFileName);
        DownloadManager.Request request = new DownloadManager.Request(remoteUri);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, localFileName);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setMimeType(mimeType == null ? "*/*" : mimeType);
        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);
    }

    public void uploadFile(Uri uri) {
        setActiveProgressOverlay(verboseProgressBarOverlay);
        setProgressOverlayVisible(true);
        currentAction = UploadIntentService.uploadCaseFile(
                this,
                UserDataRepository.getInstance().getUser().getUserId(),
                item.getCaseId(),
                uri);
    }

    private void updateFileInfoAndOpen(String storageFileName, FileInfo fileInfo) {
        FileUrlCache urlCache = FileUrlCache.getInstance();
        urlCache.addEntry(storageFileName, fileInfo);
        openFile(urlCache.getFileUrl(storageFileName));
    }

    public void onEvent(RequestCompletedEvent<FileInfo> event) {
        if (event.is(currentAction) && !keepEvent(event)) {
            if (event.contains(FileInfo.class)) {
                updateFileInfoAndOpen(currentRemoteFile.getStorageFileName(), event.getResult());
            } else if (event.contains(VideoFileInfo.class)) {
                updateFileInfoAndOpen(currentRemoteFile.getStorageFileName(true), event.getResult());
            }
        }
    }

    public void onEvent(CaseFilesLoadedEvent event) {
        if (event.is(currentAction) && !keepEvent(event)) {
            callOnFragment(
                    CaseDetailsFragment.class,
                    fragment -> fragment.onCaseFilesLoaded(event.getResult()));
        }
    }

    @Override
    protected void onEditCompleted(EditRequestCompletedEvent<Case> event) {
        event.getChangedEntity().copyTo(item);
    }

    @Override
    protected void onUploadCompletedSuccessfully() {
        requestCaseFilesInfo();
    }

    @Override
    protected boolean onNetworkErrorEvent(NetworkErrorEvent event) {
        if (event.is(currentAction)) {
            Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
            switch (WebClient.Actions.common(currentAction)) {
                case WebClient.Actions.CASES_GET_FILES :
                    if (fragment instanceof CaseDetailsFragment) {
                        ((CaseDetailsFragment) fragment).onCaseFilesLoaded(null);
                    }
                    break;
                case WebClient.Actions.VIDEOS_CONVERTED_FILE_NAME :
                case WebClient.Actions.CASES_GET_FILE_NAME :
                    if (fragment instanceof CaseDetailsFragment) {
                        ((CaseDetailsFragment) fragment).onCaseFileLoaded();
                    }
                    break;
            }

        }
        return false;
    }

    @Override
    protected int getOnCompletedMessageId() {
        return progressBarOverlay == verboseProgressBarOverlay ?
                R.string.text_upload_completed_file : R.string.text_edit_completed_case;
    }

    @Override
    public void updateItem(Case changedCase) {
        setProgressOverlayVisible(true);
        currentAction = WebClient.Actions.unique(WebClient.Actions.CASES_EDIT);
        WebClient.request().editCase(changedCase, new EditEvenBusCallback<>(currentAction, changedCase));
    }

    @Override
    protected void onPermissionGranted(String grantedPermission) {
        if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(grantedPermission)) {
            callOnFragment(CaseDetailsFragment.class, CaseDetailsFragment::onReadStorageGranted);
        } else {
            showDialogAfterPermission(currentRemoteFile.getFileName());
        }
    }
}
