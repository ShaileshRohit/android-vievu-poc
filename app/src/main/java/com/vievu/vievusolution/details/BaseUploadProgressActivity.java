package com.vievu.vievusolution.details;

import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.UserRelatedBean;
import com.vievu.vievusolution.event.UploadCompletedEvent;
import com.vievu.vievusolution.event.UploadProgressEvent;
import com.vievu.vievusolution.ui.UiUtil;
import com.vievu.vievusolution.util.FileUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class BaseUploadProgressActivity<TItem extends UserRelatedBean<TItem>> extends BaseDetailsActivity<TItem> {

    @Bind(R.id.progressBar)
    protected ProgressBar progressBar;
    @Bind(R.id.progressTextView)
    protected TextView progressTextView;

    protected abstract int getLayoutId();

    @Override
    protected void initialize(Bundle bundle) {
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        UiUtil.colorizeProgressBar(progressBar, R.color.accent);
    }

    protected abstract void onUploadCompletedSuccessfully();

    public void onEventMainThread(UploadCompletedEvent event) {
        if (event.getAction() == currentAction && !keepEvent(event)) {
            setProgressOverlayVisible(false);
            if (event.isCompleted()) {
                shortToast(getOnCompletedMessageId());
                onUploadCompletedSuccessfully();
            } else {
                shortToast(event.getErrorMessage());
                callSaveChangesError();
            }
        }
    }

    public void onEventMainThread(UploadProgressEvent event) {
        if (event.getAction() == currentAction && !keepEvent(event)) {
            if (event.isUploadCompleted()) {
                progressBar.setIndeterminate(true);
                progressTextView.setText(R.string.text_completing);
            } else {
                progressBar.setIndeterminate(false);
                progressBar.setProgress(event.getPercents());
                progressTextView.setText(
                        String.format("%s / %s",
                                FileUtil.getShortFileSizeString(event.getBytesUploaded()),
                                FileUtil.getShortFileSizeString(event.getBytesTotal())));
            }
        }
    }
}
