package com.vievu.vievusolution.details.camera;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.details.files.FileEditFragment;

public class CameraFileEditFragment extends FileEditFragment implements CompoundButton.OnCheckedChangeListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        getActivity().setTitle(R.string.title_activity_camera_video_details);
        //noinspection PointlessBooleanExpression
        if (!BuildConfig.USES_BACKEND) {
            // Only for VERIPATROL app
            neverDeleteSwitch.getToggleSwitch().setEnabled(!changedItem.isLockDownVideo());
            lockdownSwitch.setOnCheckedChangeListener(this);
        }
        return view;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_camera_file_edit;
    }

    @Override
    protected int getCurrentTitleId() {
        return R.string.title_activity_edit_metadata;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // Only for VERIPATROL app
        neverDeleteSwitch.getToggleSwitch().setChecked(isChecked);
        neverDeleteSwitch.getToggleSwitch().setEnabled(!isChecked);
    }

    @Override
    protected boolean validateViews() {
        return true;
    }
}
