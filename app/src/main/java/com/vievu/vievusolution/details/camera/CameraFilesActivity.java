package com.vievu.vievusolution.details.camera;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.vievu.vievusolution.CameraClient;
import com.vievu.vievusolution.NoCameraFragment;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.WiFiStateReceiver;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.details.BaseDetailsActivity;
import com.vievu.vievusolution.details.location.LocationFragment;
import com.vievu.vievusolution.event.EditRequestCompletedEvent;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.media.LiveViewActivity;
import com.vievu.vievusolution.net.camera.MetadataFtpClient;
import com.vievu.vievusolution.net.camera.RetryHandler;
import com.vievu.vievusolution.ptpip.dataset.FileInfo;
import com.vievu.vievusolution.util.ActivityUtil;

import java.util.List;

public class CameraFilesActivity extends BaseDetailsActivity<File> implements WiFiStateReceiver.CallbackActivity, RetryHandler {

    private FileInfo currentFileInfo;
    private boolean skipPreRecordRestore;
    private CameraFileListFragment lastCameraFileListFragment;

    private WiFiStateReceiver<CameraFilesActivity> receiver = new WiFiStateReceiver<>();

    public FileInfo getFileInfo() {
        return currentFileInfo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!CameraClient.getInstance().getPtpIpClient().isConnected())  {
            showFragment(NoCameraFragment.create(true), false);
            CameraClient.getInstance().connect();
        } else {
            showFragment(lastCameraFileListFragment = new CameraFileListFragment(), false);
        }
    }

    //region BaseDetailsActivity network callbacks

    @Override
    protected void onEditCompleted(EditRequestCompletedEvent<File> event) {
        //not used
    }

    @Override
    protected boolean onNetworkErrorEvent(NetworkErrorEvent event) {
        //not used
        return false;
    }

    @Override
    protected int getOnCompletedMessageId() {
        //not used
        return R.string.text_edit_completed_metadata;
    }

    //endregion

    @Override
    public void onStart() {
        super.onStart();
        receiver.registerWith(this);
        setProgressOverlayVisible(CameraClient.getInstance().isPreRecordRestoreRequested());
    }

    @Override
    public void onStop() {
        setProgressOverlayVisible(true);
        super.onStop();
        receiver.unregister();

        if (skipPreRecordRestore) {
            skipPreRecordRestore = false;
        } else {
            CameraClient.getInstance().restorePreRecord();
        }
    }

    @Override
    public void onBackPressed() {
        skipPreRecordRestore = true;
        super.onBackPressed();
    }

    @Override
    public void onConnectedToWiFiNetwork(String ssid) {
        ActivityUtil.tryBindNetworkAdapter(this);
        CameraClient.getInstance().connect();
        if (!isInBackground) {
            callOnFragment(LocationFragment.class, LocationFragment::onWiFiConnectionChanged);
        }
    }

    @Override
    public void onWiFiConnectionLost() {
        callOnFragment(LocationFragment.class, LocationFragment::onWiFiConnectionLost);
        receiver.handleLostConnection();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LocationFragment.REQUEST_CODE_GOOGLE_SETTINGS) {
            callOnFragment(LocationFragment.class,
                    fragment -> fragment.onActivityResult(requestCode, resultCode, data));
        }
    }

    public void onFilePicked(FileInfo fileInfo) {
        this.currentFileInfo = fileInfo;
        setProgressOverlayVisible(true);
        MetadataFtpClient.getInstance().loadMetadataAsync(fileInfo.getName());
    }

    public void onEvent(MetadataFtpClient.Event event) {
        if (event.isSuccessful()) {
            this.item = event.getMetadata();
            this.item.setCameraFileName(currentFileInfo.getName());
            if (event.isEdited()) {
                shortToast(R.string.text_edit_completed_metadata);
                ActivityUtil.hideKeyboard(this);
                getFragmentManager().popBackStack();
            } else {
                showFragment(new CameraFileDetailsFragment(), true);
            }
        } else {
            shortToast(R.string.error_unknown);
        }
        setProgressOverlayVisible(false);
    }

    public void onEventMainThread(CameraClient.Event event) {
        if (!keepEvent(event)) {
            if(event.isConnected()){
                if(getFragmentManager().getBackStackEntryCount() > 0){
                    getFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getFragmentManager().beginTransaction().remove(lastCameraFileListFragment).commit();
                }
                showFragment(lastCameraFileListFragment = new CameraFileListFragment(), false);
            } else {
                showFragment(NoCameraFragment.create(false), false);
            }
        }
    }

    public void onEvent(RequestCompletedEvent<List<Category>> event) {
        callOnFragment(CameraFileDetailsFragment.class,
                fragment -> fragment.onCategoriesLoaded(event.getResult()));
    }

    public void onEventMainThread(CameraClient.Mode event) {
        if (!keepEvent(event)) {
            setProgressOverlayVisible(false);
        }
    }

    @Override
    public void retry() {
        CameraClient.getInstance().setRtspRequired(true);
        CameraClient.getInstance().connect();
    }

    @Override
    public void updateItem(File video) {
        setProgressOverlayVisible(true);
        MetadataFtpClient.getInstance().saveMetadataAsync(currentFileInfo.getName(), video);
    }

    public void viewVideo() {
        skipPreRecordRestore = true;
        LiveViewActivity.start(this, currentFileInfo.getVideoUrl());
    }
}
