package com.vievu.vievusolution.details.files;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.annimon.stream.Objects;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.bean.GeoLocation;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.data.CategoriesAsyncRepository;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.details.BaseEditFragment;
import com.vievu.vievusolution.details.location.LocationFragment;
import com.vievu.vievusolution.dialog.TwoOptionDialogFragment;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.filtering.fragment.CasesConstraintFragment;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ui.ActionDrawableEditText;
import com.vievu.vievusolution.ui.VievuSwitch;
import com.vievu.vievusolution.util.CollectionsUtil;

import java.util.Collection;

import butterknife.Bind;
import butterknife.OnTouch;

public class FileEditFragment extends BaseEditFragment<File> implements ActionDrawableEditText.OnActionDrawableClickListener {

    @Bind(R.id.categorySpinner) Spinner categorySpinner;
    @Nullable
    @Bind(R.id.reportNumberEditText)
    EditText reportNumberEditText;
    @Nullable
    @Bind(R.id.eventNumberEditText)
    EditText eventNumberEditText;
    @Nullable
    @Bind(R.id.caseNameEditText)
    protected EditText caseNameEditText;
    @Nullable
    @Bind(R.id.geolocationEditText)
    protected ActionDrawableEditText geolocationEditText;
    @Bind(R.id.neverDeleteSwitch)
    protected VievuSwitch neverDeleteSwitch;
    @Bind(R.id.lockdownSwitch)
    protected VievuSwitch lockdownSwitch;

    private OneLineArrayListAdapter<Category> adapter;

    @Override
    protected File createChangedItem() {
        return new File();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_video_edit;
    }

    @Override
    protected int getCurrentTitleId() {
        return R.string.title_activity_edit_file;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        commentEditText.setText(changedItem.getComment());

        adapter = new OneLineArrayListAdapter<>(getActivity(), null, Category::getCategoryName);
        categorySpinner.setAdapter(adapter);
        CategoriesAsyncRepository.getInstance().getDataAsync(this::onCategoriesLoaded);
        if (reportNumberEditText != null) {
            reportNumberEditText.setText(changedItem.getReportNumber());
        }
        if (eventNumberEditText != null) {
            eventNumberEditText.setText(changedItem.getEventNumber());
        }
        if (caseNameEditText != null) {
            caseNameEditText.setText(changedItem.getCaseName());
            if (caseNameEditText instanceof ActionDrawableEditText) {
                ((ActionDrawableEditText)caseNameEditText).setOnActionDrawableClickListener(this);
            }
        }
        if (geolocationEditText != null) {
            if (changedItem.getVideoLocation() != null) {
                geolocationEditText.setText(changedItem.getVideoLocation().toString());
                geolocationEditText.setActionDrawable(R.drawable.ic_action_remove);
            } else {
                geolocationEditText.setActionDrawable(R.drawable.ic_location_active);
            }
            geolocationEditText.setOnActionDrawableClickListener(this);
        }
        neverDeleteSwitch.getToggleSwitch().setChecked(changedItem.isNeverDelete());
        lockdownSwitch.getToggleSwitch().setChecked(changedItem.isLockDownVideo());
        if(!UserDataRepository.getInstance().getPermissions().getSetLockdown()) {
            lockdownSwitch.setVisibility(View.GONE);
        }

        getDetailsActivity().setTitle(R.string.title_activity_file_details);
        return view;
    }

    @Override
    public void onDestroyView() {
        writeBackSwitchStates();
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        saveItem.setEnabled(validateViews()
            && CategoriesAsyncRepository.getInstance().isDataReady());
    }

    private void onCategoriesLoaded(Collection<Category> categories) {
        if (isAdded()) {
            adapter.swapItems(categories);
            progressBar.setVisibility(View.GONE);
            categorySpinner.setVisibility(View.VISIBLE);
            if (categories == null) {
                adapter.addItem(getDetailsActivity().getItem().getCategory());
                categorySpinner.setSelection(0);
                getVievuActivity().shortToast(R.string.error_cannot_load_categories);
                //noinspection PointlessBooleanExpression
                if (!BuildConfig.USES_BACKEND && saveItem != null) {
                    saveItem.setEnabled(true);
                }
            } else {
                int id = getDetailsActivity().getItem().getCategoryId();
                int index = CollectionsUtil.indexOf(
                        categories, category -> category.getCategoryId() == id);
                if (index < 0) {
                    String defaultCategoryName = getString(R.string.default_category);
                    index = CollectionsUtil.indexOf(
                        categories, category -> Objects.equals(category.getCategoryName(), defaultCategoryName));
                }
                categorySpinner.setSelection(index < 0 ? 0 : index);
                if (saveItem != null) {
                    saveItem.setEnabled(validateViews());
                }
            }
        }
    }

    public void onEvent(OfflineModeChangedEvent event) {
        if (saveItem != null) {
            saveItem.setEnabled(validateViews());
        }
    }

    @Nullable
    @OnTouch(R.id.geolocationEditText)
    public boolean onGeolocationTextTouch(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {

            showLocationFragment();
        }
        return false;
    }

    @Override
    public void onActionDrawableClicked(ActionDrawableEditText view) {
        if (view.getId() == R.id.caseNameEditText) {
            getDetailsActivity().checkInternetAvailability(WebClient.Actions.CASES_FIND, isConnected -> {
                if (isConnected) {
                    showSuggestionsFragment(
                        new CasesConstraintFragment(),
                        R.string.hint_case_name,
                        value -> {
                            changedItem.setCaseId(value.getCaseId());
                            changedItem.setCaseName(value.getCaseName());
                            getFragmentManager().popBackStack();
                        });
                } else {
                    showNoInternetDialog(R.string.text_video_case);
                }
            });
        } else if (geolocationEditText != null) {
            if (geolocationEditText.getText().length() == 0) {
                showLocationFragment();
            } else {
                geolocationEditText.setText(null);
                changedItem.setVideoLocation(null);
                geolocationEditText.setActionDrawable(R.drawable.ic_location_active);
            }
        }
    }

    private void showLocationFragment() {
        getDetailsActivity().checkInternetAvailability(
            WebClient.Actions.LOCATIONS_FIND,
            isConnected -> {
                if (isConnected) {
                    getDetailsActivity().setTitle(getCurrentTitleId());
                    getDetailsActivity().showFragment(
                        LocationFragment.create(
                            changedItem.getVideoLocation(),
                            latLng -> changedItem.setVideoLocation(new GeoLocation(latLng))),
                        true);
                } else {
                    showNoInternetDialog(R.string.text_video_geolocation);
                }
            });
    }

    private void showNoInternetDialog(@StringRes int resourceNameId) {
        TwoOptionDialogFragment fragment = TwoOptionDialogFragment.create(R.string.text_error,
            getString(R.string.template_error_resource_requires_internet, getString(resourceNameId)));
        fragment.setPositiveButton(R.string.button_dialog_wifi_settings,
                dialog -> startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)));
        fragment.show(getFragmentManager(), null);
    }

    private void writeBackSwitchStates() {
        changedItem.setNeverDelete(neverDeleteSwitch.getToggleSwitch().isChecked());
        changedItem.setLockDownVideo(lockdownSwitch.getToggleSwitch().isChecked());
    }

    private String getStringValue(EditText view) {
        return view != null && view.getText().length() > 0 ? view.getText().toString() : null;
    }

    protected void saveChanges() {
        Category selectedCategory = adapter.getItem(categorySpinner.getSelectedItemPosition());
        changedItem.setCategoryId(selectedCategory.getCategoryId());
        changedItem.setCategoryName(selectedCategory.getCategoryName());
        changedItem.setReportNumber(getStringValue(reportNumberEditText));
        changedItem.setEventNumber(getStringValue(eventNumberEditText));
        writeBackSwitchStates();
        if (caseNameEditText != null) {
            changedItem.setCaseName(getStringValue(caseNameEditText));
        }
        changedItem.setComment(commentEditText.getText().toString());
        getDetailsActivity().updateItem(changedItem);
    }
}
