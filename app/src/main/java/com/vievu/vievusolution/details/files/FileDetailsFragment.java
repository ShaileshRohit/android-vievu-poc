package com.vievu.vievusolution.details.files;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vievu.vievusolution.BaseVievuFragment;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.bean.Permissions;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.details.location.LocationFragment;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ui.TagLabelView;
import com.vievu.vievusolution.ui.VideoPropertyMixin;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FileDetailsFragment extends BaseVievuFragment {

    public FileDetailsActivity getVideoActivity() {
        return (FileDetailsActivity) getActivity();
    }

    @Bind(R.id.thumbnailImageView) ImageView thumbnailImageView;
    @Bind(R.id.propertiesListView) TableLayout propertiesListView;
    @Nullable
    @Bind(R.id.commentTextView) TextView commentTextView;

    protected int getLayoutId() {
        return R.layout.fragment_file_details;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);
        getVideoActivity().setTitle(R.string.title_activity_files);
        File file = getVideoActivity().getItem();
        if (file.hasThumbnail()) {
            thumbnailImageView.post(this::onThumbnailImageViewShown);
        } else {
            thumbnailImageView.setImageResource(R.drawable.placeholder_file);
        }
        onCreateView(file, view);
        return view;
    }

    protected void onCreateView(File file, View view) {
        initPropertiesTable(file);

        if (commentTextView != null) {
            commentTextView.setText(file.getComment());
        }

        if (file.getVideoLocation() != null) {
            TextView locationValueTextView = ButterKnife.findById(
                    propertiesListView.findViewById(R.id.property_row_video_location),
                    R.id.valueTextView);
            locationValueTextView.setPaintFlags(
                    locationValueTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            locationValueTextView.setOnClickListener(textView -> {
                getVideoActivity().showFragment(
                        LocationFragment.createDisplayOnly(file.getVideoLocation()), true);
                getVideoActivity().setTitle(R.string.title_activity_file_details);
            });
        }

        TagLabelView.generateLabels(ButterKnife.findById(view, R.id.tagsContainer), file);
    }

    protected void initPropertiesTable(File file) {
        VideoPropertyMixin.with(propertiesListView)
            .addVideoPropertyRow(R.string.text_video_file_name, file.getCameraFileName())
            .addVideoPropertyRow(R.string.text_video_upload_date, file.getUploadDate())
            .addVideoPropertyRow(R.string.text_video_user, file.getUserName())
            .addVideoPropertyRow(R.string.text_video_category, file.getCategoryName())
            .addVideoPropertyRow(R.string.text_video_report_number, file.getReportNumber())
            .addVideoPropertyRow(R.string.text_video_event_number, file.getEventNumber())
            .addVideoPropertyRow(R.string.text_video_case, file.getCaseName())
            .addVideoPropertyRow(R.string.text_video_never_delete, file.isNeverDelete())
            .addVideoPropertyRow(R.string.text_video_lockdown, file.isLockDownVideo())
            .withNextId(R.id.property_row_video_location)
            .addVideoPropertyRow(R.string.text_video_geolocation, file.getVideoLocation());
    }

    protected void showErrorToast(int messageId) {
        if (isAdded()) {
            getVideoActivity().shortToast(messageId);
        }
    }

    protected void onThumbnailImageViewShown() {
        if (getActivity() != null) {
            Picasso picasso = Picasso.with(getActivity());
            picasso.setLoggingEnabled(BuildConfig.DEBUG);
            picasso
                .load(WebClient.getThumbnail(
                        getVideoActivity().getItem(),
                        thumbnailImageView.getWidth(),
                        thumbnailImageView.getHeight()))
                .into(thumbnailImageView);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(getVideoActivity().getItem().getIsReadOnly()){
            return;
        }
        inflater.inflate(R.menu.menu_video_details, menu);
        Permissions permissions = UserDataRepository.getInstance().getPermissions();
        if (getVideoActivity().getItem().isRedactedVideo() || !permissions.canEditVideos()) {
            menu.findItem(R.id.action_edit).setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_edit) {
            getVideoActivity().showVideoEditFragment();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
