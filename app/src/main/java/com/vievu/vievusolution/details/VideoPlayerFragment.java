package com.vievu.vievusolution.details;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.media.MediaControlsFragment;
import com.vievu.vievusolution.ui.SurfaceVideoView;
import com.vievu.vievusolution.ui.UiUtil;

import java.io.IOException;
import java.util.Formatter;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

public class VideoPlayerFragment extends MediaControlsFragment
                                 implements MediaPlayer.OnPreparedListener,
                                            MediaPlayer.OnBufferingUpdateListener,
                                            MediaPlayer.OnSeekCompleteListener,
                                            MediaPlayer.OnCompletionListener,
                                            SurfaceHolder.Callback,
                                            SeekBar.OnSeekBarChangeListener, MediaPlayer.OnErrorListener {

    public static VideoPlayerFragment create(Uri videoUrl) {
        VideoPlayerFragment fragment = new VideoPlayerFragment();
        fragment.videoUrl = videoUrl;
        return fragment;
    }

    @Bind(R.id.rootView) View rootView;
    @Bind(R.id.videoView) SurfaceVideoView videoView;
    @Bind(R.id.playButton) ImageButton playButton;
    @Bind(R.id.seekBar) SeekBar seekBar;
    @Bind(R.id.currentTimeTextView) TextView currentTimeTextView;
    @Bind(R.id.totalTimeTextView) TextView totalTimeTextView;

    private Uri videoUrl;
    private boolean shouldResumePlaying;
    private boolean isUpdatingTime;
    private MediaPlayer mediaPlayer;
    private StringBuilder timeFormatBuilder;
    private Formatter timeFormatter;

    private AudioManager audioManager;

    private boolean isInErrorState = false;

    public VideoPlayerFragment() {
        setRetainInstance(true);
    }

    public BaseDetailsActivity<?> getDetailsActivity() {
        return (BaseDetailsActivity) getActivity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDetailsActivity().setOrientationChangeEnabled(true);
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        audioManager.requestAudioFocus(
            i -> Log.d("AUDIO_FOCUS_GAINED", String.valueOf(i)),
            AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_video_player;
    }

    @Override
    protected void initView(View view) {
        videoView.getHolder().addCallback(this);
        rootView.setOnTouchListener(this);

        UiUtil.colorizeProgressBar(seekBar, R.color.accent);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            UiUtil.colorizeDrawable(
                seekBar.getThumb(),
                getActivity().getResources().getColor(R.color.accent));
        }
        seekBar.setOnSeekBarChangeListener(this);

        timeFormatBuilder = new StringBuilder();
        timeFormatter = new Formatter(timeFormatBuilder, Locale.getDefault());
        mediaControlsLock.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            this.shouldResumePlaying = true;
            pausePlaying();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isInErrorState) {
            handleMediaPlayerError();
        } else if (mediaPlayer != null && this.shouldResumePlaying) {
            startPlaying();
            this.shouldResumePlaying = false;
        }
    }

    @Override
    public void onDestroyView() {
        rootView.setOnClickListener(null);
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        audioManager.abandonAudioFocus(i -> Log.d("AUDIO_FOCUS_ABANDONED", String.valueOf(i)));
        if (getDetailsActivity() != null) {
            getDetailsActivity().setOrientationChangeEnabled(false);
        }
    }

    @OnClick(R.id.playButton)
    public void onPlayButtonClick() {
        if (this.isInFullScreen) {
            resetHideOverlayTimer();
        }
        if (mediaPlayer.isPlaying()) {
            pausePlaying();
        } else {
            startPlaying();
        }
    }

    private void setVideoViewAspectRatio() {
        float videoWidth = mediaPlayer.getVideoWidth();
        float videoHeight = mediaPlayer.getVideoHeight();

        if (videoWidth > 0 && videoHeight > 0) {
            videoView.setVideoSize((int)videoWidth, (int)videoHeight);
        }
    }

    @Override
    public void setMediaControlsOverlayVisible(boolean isVisible) {
        super.setMediaControlsOverlayVisible(isVisible);
        if (isOverlayVisible) {
            updatePlayButton();
        }
    }

    private void updatePlayButton() {
        if (mediaPlayer != null) {
            playButton.setImageResource(
                mediaPlayer.isPlaying() ?
                    android.R.drawable.ic_media_pause :
                    android.R.drawable.ic_media_play);
        }
    }

    private void startPlaying() {
        mediaPlayer.start();
        startUpdatingTime();
        if (isOverlayVisible) {
            updatePlayButton();
        }
    }

    private void pausePlaying() {
        stopUpdatingTime();
        mediaPlayer.pause();
        if (isOverlayVisible) {
            updatePlayButton();
        }
    }

    private void handleMediaPlayerError() {
        if (!getDetailsActivity().isInBackground()) {
            getDetailsActivity().shortToast(R.string.error_media_player);
            getActivity().getFragmentManager().popBackStack();
            getDetailsActivity().setProgressOverlayVisible(false);
        }
        this.isInErrorState = true;
    }

    //region Current time updates

    private String stringForTime(int timeMs) {

        timeFormatBuilder = new StringBuilder();
        timeFormatter = new Formatter(timeFormatBuilder, Locale.getDefault());

        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours   = totalSeconds / 3600;

        timeFormatBuilder.setLength(0);
        if (hours > 0) {
            return timeFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return timeFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private void updateCurrentTime() {
        if (this.isUpdatingTime && mediaPlayer != null && mediaPlayer.isPlaying()) {
            int position = mediaPlayer.getCurrentPosition();
            currentTimeTextView.setText(stringForTime(position));
            int duration = mediaPlayer.getDuration();
            long relativePosition = duration != 0 ? 1000L * position / duration : 0;
            seekBar.setProgress((int) relativePosition);
            mediaControlsOverlay.postDelayed(this::updateCurrentTime, 500);
        }
    }

    private void startUpdatingTime() {
        if (!this.isUpdatingTime) {
            this.isUpdatingTime = true;
            mediaControlsOverlay.post(this::updateCurrentTime);
        }
    }

    private void stopUpdatingTime() {
        this.isUpdatingTime = false;
        mediaControlsOverlay.getHandler().removeCallbacks(this::updateCurrentTime);
    }

    //endregion

    //region Media Player listeners

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        getDetailsActivity().setProgressOverlayVisible(false);
        totalTimeTextView.setText(stringForTime(mediaPlayer.getDuration()));
        if (!getDetailsActivity().isInBackground()) {
            mediaPlayer.seekTo(0); //Samsung Galaxy S6 Edge with Android 7.0 workaround (bug SOFT-4716)
            startPlaying();
        } else {
            this.shouldResumePlaying = true;
        }
        setVideoViewAspectRatio();
        videoView.requestLayout();
        mediaControlsLock.setVisibility(View.GONE);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        seekBar.setSecondaryProgress(percent * 10);
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        mediaControlsLock.setVisibility(View.GONE);
        if (isInFullScreen) {
            startHideOverlayTimer();
        }
        if (shouldResumePlaying && mp.getCurrentPosition() != mp.getDuration()) {
            startPlaying();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (seekBar != null) {
            shouldResumePlaying = false;
            stopUpdatingTime();
            updatePlayButton();
            seekBar.setProgress(seekBar.getMax());
            playButton.setEnabled(false);
            seekBar.setEnabled(false);
            mediaControlsOverlay.getHandler().postDelayed(() -> {
                try {
                    if (mediaPlayer != null) {
                        mediaPlayer.seekTo(0);
                        seekBar.setProgress(0);
                        currentTimeTextView.setText(stringForTime(0));
                        playButton.setEnabled(true);
                        seekBar.setEnabled(true);
                    }
                } catch (IllegalStateException e) {
                    Log.d("VideoPlayerFragment", "cannot rewind MediaPlayer to the beginning");
                    e.printStackTrace();
                }
            }, 500);
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mediaPlayer.reset();
        mediaPlayer.release();
        mediaPlayer = null;
        handleMediaPlayerError();
        return true;
    }

    //endregion

    //region Surface callback

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (this.videoUrl != null && this.mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.reset();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(this.videoUrl.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            mediaPlayer.setScreenOnWhilePlaying(true);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnSeekCompleteListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.prepareAsync();
        } else {
            mediaControlsLock.setVisibility(View.GONE);
        }
        mediaPlayer.setDisplay(videoView.getHolder());
        setVideoViewAspectRatio();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        setVideoViewAspectRatio();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    //endregion

    //region SeekBar callbacks

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser && mediaPlayer != null) {
            currentTimeTextView.setText(stringForTime(
                (int) ((float)progress / seekBar.getMax() * mediaPlayer.getDuration())));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mediaControlsOverlay.getHandler().removeCallbacks(this::hideOverlay);
        stopUpdatingTime();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mediaControlsLock.setVisibility(View.VISIBLE);
        if (mediaPlayer != null) {
            this.shouldResumePlaying = mediaPlayer.isPlaying();
            mediaPlayer.seekTo((int) ((mediaPlayer.getDuration() * seekBar.getProgress()) / 1000L));
        }
    }

    //endregion
}
