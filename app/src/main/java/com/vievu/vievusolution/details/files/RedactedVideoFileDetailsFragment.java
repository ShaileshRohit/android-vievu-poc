package com.vievu.vievusolution.details.files;

import android.view.View;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.ui.VideoPropertyMixin;

public class RedactedVideoFileDetailsFragment extends VideoFileDetailsFragment {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_redacted_video_details;
    }

    @Override
    protected void onCreateView(File file, View view) {
        initPropertiesTable(file);

    }

    @Override
    protected void initPropertiesTable(File file) {
        VideoPropertyMixin.with(propertiesListView)
            .addVideoPropertyRow(R.string.text_video_file_name, file.getCameraFileName())
            .addVideoPropertyRow(R.string.text_video_duration, file.getDuration());
    }
}
