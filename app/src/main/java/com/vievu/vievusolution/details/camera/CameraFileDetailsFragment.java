package com.vievu.vievusolution.details.camera;

import android.graphics.Paint;
import android.net.wifi.WifiInfo;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.vievu.vievusolution.BaseVievuFragment;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.data.CategoriesAsyncRepository;
import com.vievu.vievusolution.details.location.LocationFragment;
import com.vievu.vievusolution.net.camera.ThumbnailLoader;
import com.vievu.vievusolution.ptpip.dataset.FileInfo;
import com.vievu.vievusolution.ui.TagLabelView;
import com.vievu.vievusolution.ui.VideoPropertyMixin;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CameraFileDetailsFragment extends BaseVievuFragment {

    @Bind(R.id.thumbnailImageView)
    ImageView thumbnailImageView;
    @Bind(R.id.propertiesListView)
    TableLayout propertiesListView;
    @Bind(R.id.commentTextView) TextView commentTextView;

    public CameraFilesActivity getCameraFilesActivity() {
        return (CameraFilesActivity)getActivity();
    }

    public CameraFileDetailsFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_details, container, false);
        ButterKnife.bind(this, view);
        FileInfo fileInfo = getCameraFilesActivity().getFileInfo();
        if (fileInfo != null) {
            ThumbnailLoader.getInstance()
                    .load(ThumbnailLoader.getUri(fileInfo.getId()))
                    .fit()
                    .into(thumbnailImageView);
        }
        File metadata = getCameraFilesActivity().getItem();
        initVideoPropertiesTable(metadata);
        commentTextView.setText(metadata.getComment());

        if (metadata.getVideoLocation() != null) {
            TextView locationValueTextView = valueTextViewOf(R.id.property_row_video_location);
            locationValueTextView.setPaintFlags(
                    locationValueTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            locationValueTextView.setOnClickListener(textView -> {
                getCameraFilesActivity().showFragment(
                        LocationFragment.createDisplayOnly(metadata.getVideoLocation()), true);
                getCameraFilesActivity().setTitle(R.string.title_activity_camera_video_details);
            });
        }

        if (BuildConfig.USES_BACKEND) {
            TagLabelView.generateLabels(ButterKnife.findById(view, R.id.tagsContainer), metadata);
        }

        CategoriesAsyncRepository.getInstance().getDataAsync(this::onCategoriesLoaded);

        getActivity().setTitle(R.string.title_activity_camera_video);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_camera_file_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            getCameraFilesActivity().showFragment(new CameraFileEditFragment(), true);
            return true;
        }
        return false;
    }

    @OnClick(R.id.playButton)
    void onPlayClick() {
        getCameraFilesActivity().viewVideo();
    }

    private void initVideoPropertiesTable(File metadata) {
        VideoPropertyMixin.Builder builder = VideoPropertyMixin.with(propertiesListView)
            .addVideoPropertyRow(R.string.text_video_file_name, metadata.getCameraFileName())
            .withNextId(R.id.property_row_video_category)
            .addVideoPropertyRow(R.string.text_video_category, (String)null);
        if (BuildConfig.USES_BACKEND) {
            builder
                .addVideoPropertyRow(R.string.text_video_report_number, metadata.getReportNumber())
                .addVideoPropertyRow(R.string.text_video_event_number, metadata.getEventNumber());
        }
        builder
            .addVideoPropertyRow(R.string.text_video_case, metadata.getCaseName())
            .addVideoPropertyRow(R.string.text_video_never_delete, metadata.isNeverDelete())
            .addVideoPropertyRow(R.string.text_video_lockdown, metadata.isLockDownVideo());
        if (BuildConfig.USES_BACKEND) {
            builder
                .withNextId(R.id.property_row_video_location)
                .addVideoPropertyRow(R.string.text_video_geolocation, metadata.getVideoLocation());
        }
    }

    private TextView valueTextViewOf(@IdRes int rowId) {
        return ButterKnife.findById(
                propertiesListView.findViewById(rowId),
                R.id.valueTextView);
    }

    public void onCategoriesLoaded(List<Category> categories) {
        if (propertiesListView != null) {
            TextView categoryTextView = valueTextViewOf(R.id.property_row_video_category);
            categoryTextView.setText(
                    getCategoryName(getCameraFilesActivity().getItem().getCategoryId(), categories));
        }
    }

    private String getCategoryName(int categoryId, List<Category> categories) {
        if (categories != null) {
            Optional<Category> result =
                Stream.of(categories)
                    .filter(category -> category.getCategoryId() == categoryId)
                    .findFirst();
            if (result.isPresent()) {
                return result.get().getCategoryName();
            } else {
                String name = getString(R.string.default_category);
                Stream.of(categories)
                    .filter(category -> name.equals(category.getCategoryName()))
                    .findFirst()
                    .ifPresent(category -> getCameraFilesActivity().getItem().setCategoryId(category.getCategoryId()));
                return name;
            }
        }
        return getString(R.string.default_category);
    }
}
