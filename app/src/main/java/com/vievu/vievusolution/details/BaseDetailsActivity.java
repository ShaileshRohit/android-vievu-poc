package com.vievu.vievusolution.details;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.annimon.stream.function.Consumer;
import com.vievu.vievusolution.EventBusActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.UserRelatedBean;
import com.vievu.vievusolution.details.location.LocationFragment;
import com.vievu.vievusolution.dialog.TwoOptionDialogFragment;
import com.vievu.vievusolution.event.EditRequestCompletedEvent;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.PingCompletedEvent;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.ActivityUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class BaseDetailsActivity<TItem extends UserRelatedBean<TItem>> extends EventBusActivity {

    private static final String EXTRA_IS_IN_PROGRESS = "BaseDetailsActivity.isInProgress";

    public static final String EXTRA_ITEM = "BaseDetailsActivity.item";

    protected static <TItem extends UserRelatedBean<TItem>> void start(Activity caller, TItem item, Class<? extends BaseDetailsActivity<TItem>> target, int requestCode) {
        Intent intent = new Intent(caller, target);
        intent.putExtra(EXTRA_ITEM, item);
        caller.startActivityForResult(intent, requestCode);
    }

    @Bind(R.id.progressBarOverlay)
    protected View progressBarOverlay;

    protected Consumer<Boolean> pingCallback = null;

    protected TItem item;

    public TItem getItem() {
        return item;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            initialize(savedInstanceState);
            setProgressOverlayVisible(
                   savedInstanceState.containsKey(EXTRA_IS_IN_PROGRESS)
                && savedInstanceState.getBoolean(EXTRA_IS_IN_PROGRESS));
        } else {
            initialize(getIntent().getExtras());
        }

    }

    protected void initialize(Bundle bundle) {
        ActivityUtil.forceOverFlowIcon(this);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        if (bundle != null && bundle.containsKey(EXTRA_ITEM)) {
            item = bundle.getParcelable(EXTRA_ITEM);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EXTRA_ITEM, item);
        if (progressBarOverlay.getVisibility() == View.VISIBLE) {
            outState.putBoolean(EXTRA_IS_IN_PROGRESS, true);
        }
    }

    @Override
    public void onBackPressed() {
        if (progressBarOverlay.getVisibility() != View.VISIBLE) {
            if (!callOnFragment(VideoPlayerFragment.class, fragment -> {
                    if (fragment.isInFullScreen()) {
                        fragment.setFullscreenModeEnabled(false);
                    } else {
                        super.onBackPressed();
                    }
                })) {

                Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
                if (fragment instanceof BaseEditFragment) {
                    TwoOptionDialogFragment dialog = TwoOptionDialogFragment.create(
                            R.string.title_dialog_discard_changes,
                            getString(R.string.text_dialog_discard_changes));
                    dialog.setPositiveButton(R.string.text_yes, d -> super.onBackPressed());
                    dialog.setNegativeButton(R.string.text_no, DialogInterface::dismiss);
                    dialog.show(getFragmentManager(), null);
                    return;
                }
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onPermissionGranted(String grantedPermission) {
        if (Manifest.permission.ACCESS_FINE_LOCATION.equals(grantedPermission)) {
            callOnFragment(LocationFragment.class, LocationFragment::requestGoogleSettingsIfNecessary);
        }
    }

    @Override
    protected void onPermissionDenied(String deniedPermission) {
        if (Manifest.permission.ACCESS_FINE_LOCATION.equals(deniedPermission)) {
            if (callOnFragment(LocationFragment.class, LocationFragment::onInitialLocationError)) {
                return;
            }
        }
        super.onPermissionDenied(deniedPermission);
    }

    public void showFragment(Fragment fragment, boolean withBackStack) {
        ActivityUtil.replaceFragment(this, fragment, withBackStack);
    }

    public void setProgressOverlayVisible(boolean isVisible) {
        progressBarOverlay.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public void onEvent(EditRequestCompletedEvent<TItem> event) {
        if (event.is(currentAction) && !keepEvent(event)) {
            if (event.getResult().isApplied()) {
                onEditCompleted(event);
                shortToast(getOnCompletedMessageId());
                Intent resultingIntent = new Intent();
                resultingIntent.putExtra(EXTRA_ITEM, item);
                setResult(Activity.RESULT_OK, resultingIntent);
                getFragmentManager().popBackStack();
            } else {
                shortToast(event.getResult().getMessage());
            }
            progressBarOverlay.setVisibility(View.GONE);
        }
    }

    public void onEventMainThread(PingCompletedEvent event) {
        if (event.getAction() == currentAction && !keepEvent(event)) {
            setProgressOverlayVisible(false);
            if (pingCallback != null) {
                pingCallback.accept(event.isConnected());
            }
        }
    }

    @Override
    public void onEvent(NetworkErrorEvent event) {
        if (event.is(currentAction) && !keepEvent(event)) {
            if (!onNetworkErrorEvent(event)) {
                super.onEvent(event);
                progressBarOverlay.setVisibility(View.GONE);
                callSaveChangesError();
            }
        }
    }

    protected void callSaveChangesError() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof BaseEditFragment) {
            ((BaseEditFragment) fragment).onSaveChangesError();
        }
    }

    public void setOrientationChangeEnabled(boolean isEnabled) {
        setRequestedOrientation(isEnabled ?
                ActivityInfo.SCREEN_ORIENTATION_USER :
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void checkInternetAvailability(int action, Consumer<Boolean> callback) {
        setProgressOverlayVisible(true);
        this.pingCallback = callback;
        this.currentAction = WebClient.Actions.unique(action);
        WebClient.pingAsync(this.currentAction);
    }

    protected abstract void onEditCompleted(EditRequestCompletedEvent<TItem> event);
    protected abstract boolean onNetworkErrorEvent(NetworkErrorEvent event);
    protected abstract int getOnCompletedMessageId();
    public abstract void updateItem(TItem item);
}
