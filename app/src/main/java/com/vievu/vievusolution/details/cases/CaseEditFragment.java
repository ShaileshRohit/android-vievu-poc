package com.vievu.vievusolution.details.cases;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.bean.CaseStatus;
import com.vievu.vievusolution.data.CaseStatusesAsyncRepository;
import com.vievu.vievusolution.details.BaseEditFragment;
import com.vievu.vievusolution.dialog.DateTimeDialogFragment;
import com.vievu.vievusolution.ui.ActionDrawableEditText;
import com.vievu.vievusolution.ui.ValidationMixin;
import com.vievu.vievusolution.util.CollectionsUtil;
import com.vievu.vievusolution.util.DateUtil;
import com.vievu.vievusolution.util.ValidationUtil;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import butterknife.Bind;
import butterknife.OnTextChanged;

public class CaseEditFragment extends BaseEditFragment<Case> {

    @Bind(R.id.caseStatusSpinner) Spinner caseStatusSpinner;
    @Bind(R.id.progressBar) ProgressBar progressBar;
    @Bind(R.id.summaryEditText) EditText summaryEditText;
    @Bind(R.id.incidentDateEditText) ActionDrawableEditText incidentDateEditText;
    @Bind(R.id.reportNumberEditText) EditText reportNumberEditText;
    @Bind(R.id.eventNumberEditText) EditText eventNumberEditText;

    private OneLineArrayListAdapter<CaseStatus> adapter;

    @Override
    protected Case createChangedItem() {
        return new Case();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_case_edit;
    }

    @Override
    protected int getCurrentTitleId() {
        return R.string.title_activity_edit_case;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        ValidationMixin validationMixin = ValidationMixin.create(getActivity());
        validationMixin.setColorHeader(view, R.id.summaryHeaderTextView, R.string.text_case_summary);
        validationMixin.setColorHeader(view, R.id.incidentDateHeaderTextView, R.string.text_case_incident_date);

        commentEditText.setText(changedItem.getCaseNotes());

        adapter = new OneLineArrayListAdapter<>(getActivity(), null, CaseStatus::getName);
        caseStatusSpinner.setAdapter(adapter);
        summaryEditText.setText(changedItem.getSummary().trim());
        incidentDateEditText.setText(DateUtil.Formatters.DATE_TIME_SHORT.format(changedItem.getIncidentDate()));
        incidentDateEditText.setOnActionDrawableClickListener(
            editText -> {
                DateTimeDialogFragment fragment = new DateTimeDialogFragment();
                fragment.setTitleId(R.string.text_case_incident_date);
                Date minDate = new Date(0);
                Date maxDate = new Date();
                fragment.setMinDate(minDate);
                fragment.setMaxDate(maxDate);
                if (   changedItem.getIncidentDate() != null
                    && changedItem.getIncidentDate().compareTo(minDate) > -1
                    && changedItem.getIncidentDate().compareTo(maxDate) < 1) {
                    fragment.setDate(changedItem.getIncidentDate());
                } else {
                    fragment.setDate(maxDate);
                }
                fragment.setOnDateSelectedListener(this::updateIncidentDate);
                fragment.showSingle(getFragmentManager());
            }
        );
        reportNumberEditText.setText(changedItem.getReportNumber());
        eventNumberEditText.setText(changedItem.getEventNumber());

        getDetailsActivity().setTitle(R.string.title_activity_case_details);
        CaseStatusesAsyncRepository.getInstance().getDataAsync(this::onCaseStatusesLoaded);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        saveItem.setEnabled(CaseStatusesAsyncRepository.getInstance().isDataReady() && validateViews());
    }

    private void onCaseStatusesLoaded(Collection<CaseStatus> caseStatuses) {
        if (isAdded()) {
            adapter.swapItems(caseStatuses);
            progressBar.setVisibility(View.GONE);
            caseStatusSpinner.setVisibility(View.VISIBLE);
            if (caseStatuses == null) {
                adapter.addItem(getDetailsActivity().getItem().getCaseStatus());
                caseStatusSpinner.setSelection(0);

                getDetailsActivity().shortToast(R.string.error_cannot_load_statuses);
            } else {
                String statusName = getDetailsActivity().getItem().getStatusName();
                int index = CollectionsUtil.indexOf(caseStatuses, category -> statusName.equals(category.getName()));
                caseStatusSpinner.setSelection(index > -1 ? index : 0);
                if (saveItem != null) {
                    saveItem.setEnabled(validateViews());
                }
            }
        }
    }

    private void updateIncidentDate(Date date) {
        changedItem.setIncidentDate(date);
        incidentDateEditText.setText(DateUtil.Formatters.DATE_TIME_SHORT.format(date));
        saveItem.setEnabled(validateViews());
    }

    @OnTextChanged(
            value = R.id.summaryEditText,
            callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void onSummaryTextChanged() {
        if (saveItem != null) {
            saveItem.setEnabled(validateViews());
        }
    }

    protected boolean validateViews() {
        return super.validateViews()
            && incidentDateEditText != null
            && ValidationUtil.hasAnySymbol(incidentDateEditText)
            && ValidationUtil.hasAnyLetter(summaryEditText);
    }

    protected void saveChanges() {
        CaseStatus selectedStatus = adapter.getItem(caseStatusSpinner.getSelectedItemPosition());
        changedItem.setCaseStatusId(selectedStatus.getCaseStatusId());
        changedItem.setStatusName(selectedStatus.getName());

        Calendar truncatedDate = Calendar.getInstance();
        truncatedDate.setTime(changedItem.getIncidentDate());
        truncatedDate.set(Calendar.SECOND, 0);
        truncatedDate.set(Calendar.MILLISECOND, 0);
        changedItem.setIncidentDate(truncatedDate.getTime());

        changedItem.setSummary(summaryEditText.getText().toString().trim());
        changedItem.setReportNumber(reportNumberEditText.getText().toString());
        changedItem.setEventNumber(eventNumberEditText.getText().toString());
        changedItem.setCaseNotes(commentEditText.getText().toString());
        getDetailsActivity().updateItem(changedItem);
    }
}
