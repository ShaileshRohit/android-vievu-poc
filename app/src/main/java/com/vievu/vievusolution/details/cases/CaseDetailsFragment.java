package com.vievu.vievusolution.details.cases;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.squareup.picasso.Picasso;
import com.vievu.vievusolution.BaseVievuFragment;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.UploadActivity;
import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.bean.CaseFile;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ui.TagLabelView;
import com.vievu.vievusolution.util.DateUtil;

import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.BindDimen;
import butterknife.ButterKnife;

public class CaseDetailsFragment extends BaseVievuFragment implements AdapterView.OnItemClickListener {

    public static final int REQUEST_CODE_CHOOSE_FILE = 301;

    @Bind(R.id.userImageView) ImageView userImageView;
    @Bind(R.id.summaryTextView) TextView summaryTextView;
    @Bind(R.id.commentTextView) TextView commentTextView;
    @Bind(R.id.filesCountHeader) TextView filesCountHeader;
    @Bind(R.id.progressBar) ProgressBar progressBar;

    private Picasso picasso;
    @BindDimen(R.dimen.side_user_image)
    int imageSize;

    private List<CaseFile> caseFiles;
    private OneLineArrayListAdapter<CaseFile> caseFilesAdapter;

    private MenuItem addFileItem;

    public CaseDetailsActivity getCaseActivity() {
        return (CaseDetailsActivity) getActivity();
    }

    public CaseDetailsFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        usesEvents = true;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView view = (ListView) inflater.inflate(R.layout.fragment_case_details, container, false);
        View caseDescriptionView = inflater.inflate(R.layout.view_part_case_description, view, false);
        ButterKnife.bind(this, caseDescriptionView);
        Case item = getCaseActivity().getItem();
        picasso = Picasso.with(getActivity());
        picasso.setLoggingEnabled(BuildConfig.DEBUG);
        setUserImage(item.getUserId());

        View row = initPropertyRow(caseDescriptionView, R.id.caseNameRow, R.string.text_case_name, item.getCaseName());
        shiftPropertyRow(row);
        row = initPropertyRow(caseDescriptionView, R.id.userNameRow, R.string.text_user, item.getUserName());
        shiftPropertyRow(row);
        initPropertyRow(caseDescriptionView, R.id.statusRow, R.string.text_case_status, item.getStatusName());
        initPropertyRow(caseDescriptionView, R.id.reportNumberRow, R.string.text_case_report_number, item.getReportNumber());
        initPropertyRow(caseDescriptionView, R.id.eventNumberRow, R.string.text_case_event_number, item.getEventNumber());
        initPropertyRow(caseDescriptionView, R.id.incidentDateRow, R.string.text_case_incident_date, item.getIncidentDate());
        initPropertyRow(caseDescriptionView, R.id.createdDateRow, R.string.text_case_creation_date, item.getCreationDate());
        summaryTextView.setText(item.getSummary().trim());
        commentTextView.setText(item.getCaseNotes());

        TagLabelView.generateLabels(ButterKnife.findById(caseDescriptionView, R.id.tagsContainer), item);

        getCaseActivity().setTitle(R.string.title_activity_cases);
        if (this.caseFiles == null) {
            getCaseActivity().requestCaseFilesInfo();
        } else {
            onCaseFilesLoaded(this.caseFiles);
        }

        view.addHeaderView(caseDescriptionView, null, false);
        this.caseFilesAdapter = new OneLineArrayListAdapter<>(getActivity(),
                this.caseFiles, CaseFile::getFileName, R.layout.list_item_case_file);
        view.setAdapter(this.caseFilesAdapter);
        view.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(getCaseActivity().getItem().getIsReadOnly()) {
            return;
        }
        inflater.inflate(R.menu.menu_case_details, menu);
        /*Change code*/
        menu.findItem(R.id.action_edit).setVisible(UserDataRepository.getInstance().getPermissions().canEditCases());
        this.addFileItem = menu.findItem(R.id.action_add);
        if (UserDataRepository.getInstance().getPermissions().canDownloadVideos()) {
            this.addFileItem.setVisible(getCaseActivity().isInOnlineMode());
        } else {
            this.addFileItem.setVisible(false);
            this.addFileItem = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add :
                if (getCaseActivity().requestPermissionIfNecessary(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    onReadStorageGranted();
                }
                return true;
            case R.id.action_edit :
                getCaseActivity().showCaseEditFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onReadStorageGranted() {
        Intent getContentIntent = FileUtils.createGetContentIntent();
        Intent intent = Intent.createChooser(getContentIntent, getString(R.string.chooser_action_pick_file));
        startActivityForResult(intent, REQUEST_CODE_CHOOSE_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CHOOSE_FILE && resultCode == Activity.RESULT_OK) {
            if (UserDataRepository.getInstance().getPermissions().canDownloadVideos()) {
                Case item = getCaseActivity().getItem();
                UploadActivity.startWithFile(getActivity(),
                        data.getData(), item.getCaseId(), item.getCaseName());
            } else {
                getCaseActivity().shortToast(R.string.error_permission_denied);
            }
        } else if (requestCode == UploadActivity.REQUEST_CODE_UPLOAD_FILE
                && resultCode == UploadActivity.RESULT_UPLOAD_SUCCEEDED){
            getCaseActivity().requestCaseFilesInfo();
        }
    }

    public void onCaseFilesLoaded(List<CaseFile> caseFiles) {

        if (caseFiles != null && !caseFiles.isEmpty()) {
            this.caseFiles = caseFiles;
            this.caseFilesAdapter.swapItems(this.caseFiles);
            filesCountHeader.setVisibility(View.VISIBLE);
            filesCountHeader.setText(getString(R.string.template_case_file_count, caseFiles.size()));
        } else {
            filesCountHeader.setVisibility(View.GONE);
            this.caseFilesAdapter.swapItems(null);
        }
        showProgressView(false);
    }

    public void onCaseFileLoaded() {
        getCaseActivity().setProgressOverlayVisible(false);
        updateFilesList();
    }

    public void onEvent(OfflineModeChangedEvent event) {
        if (addFileItem != null) {
            addFileItem.setVisible(event.isInOnlineMode());
        }
    }

    private void updateFilesList() {
        ListView listView = (ListView) getView();
        if (listView != null) {
            listView.clearChoices();
            this.caseFilesAdapter.notifyDataSetChanged();
        }
    }

    private void setUserImage(int userId) {
        picasso
            .load(WebClient.getAvatarUrl(userId, imageSize, imageSize))
            .placeholder(R.drawable.placeholder_user)
            .into(userImageView);
    }

    private View initPropertyRow(View view, int rowId, int propertyNameId, Date value) {
        return initPropertyRow(view, rowId, propertyNameId, value != null ? DateUtil.Formatters.DATE_TIME_SHORT.format(value) : null);
    }

    private View initPropertyRow(View view, int rowId, int propertyNameId, String value) {
        View row = ButterKnife.findById(view, rowId);
        ButterKnife.<TextView>findById(row, R.id.nameTextView).setText(propertyNameId);
        ButterKnife.<TextView>findById(row, R.id.valueTextView).setText(value);
        return row;
    }

    private void shiftPropertyRow(View row) {
        int commonPadding = row.getPaddingBottom();
        row.setPadding(0, 0, imageSize + commonPadding, commonPadding);
    }

    private void showProgressView(boolean isVisible) {
        progressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CaseFile file = caseFiles.get(position - 1);
        if(file.isVideo() || !getCaseActivity().getItem().getIsCopyDisabled()) {
            getCaseActivity().setProgressOverlayVisible(true);
            getCaseActivity().requestCaseFile(file);
        }
        else {
            updateFilesList();
        }
    }
}
