package com.vievu.vievusolution.details.files;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.FileInfo;
import com.vievu.vievusolution.bean.ShareRequest;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.bean.VideoFileInfo;
import com.vievu.vievusolution.data.FileUrlCache;
import com.vievu.vievusolution.details.BaseDetailsActivity;
import com.vievu.vievusolution.details.VideoPlayerFragment;
import com.vievu.vievusolution.details.location.LocationFragment;
import com.vievu.vievusolution.event.EditRequestCompletedEvent;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.event.ShareLinkLoadedEvent;
import com.vievu.vievusolution.net.ComplexEventBusCallback;
import com.vievu.vievusolution.net.EditEvenBusCallback;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;

import java.net.UnknownHostException;


public class FileDetailsActivity extends BaseDetailsActivity<File> {

    private static final String EXTRA_USE_CONVERTED_URL = "VideoDetailsActivity.useConvertedUrl";

    public static final int REQUEST_CODE_VIDEO_DETAILS = 101;

    public static void start(Activity caller, File file) {
        BaseDetailsActivity.start(caller, file, FileDetailsActivity.class, REQUEST_CODE_VIDEO_DETAILS);
    }

    private boolean useConvertedUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            switch (item.getFileType()) {
                case File.FILE_TYPE_VIDEO:
                    showFragment(new VideoFileDetailsFragment(), false);
                    break;
                case File.FILE_TYPE_REDACTED_VIDEO:
                    showFragment(new RedactedVideoFileDetailsFragment(), false);
                    break;
                //TODO: Add fragment for image
                default:
                    showFragment(new FileDetailsFragment(), false);
                    break;
            }
        } else {
            this.useConvertedUrl =
                   savedInstanceState.containsKey(EXTRA_USE_CONVERTED_URL)
                && savedInstanceState.getBoolean(EXTRA_USE_CONVERTED_URL);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LocationFragment.REQUEST_CODE_GOOGLE_SETTINGS) {
            callOnFragment(LocationFragment.class,
                fragment -> fragment.onActivityResult(requestCode, resultCode, data));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.useConvertedUrl) {
            outState.putBoolean(EXTRA_USE_CONVERTED_URL, true);
        }
    }

    public void showVideoEditFragment() {
        showFragment(new FileEditFragment(), true);
    }

    public void requestPlayUrl() {
        this.useConvertedUrl = true;
        Uri uri = FileUrlCache.getInstance().getFileUrl(item.getStorageFileName(true));
        if (uri == null) {
            setProgressOverlayVisible(true);
            currentAction = WebClient.Actions.unique(WebClient.Actions.VIDEOS_CONVERTED_FILE_NAME);
            WebClient.request().convertedVideoFileInfo(
                item.getStorageFileName(), new EventBusCallback<>(currentAction));
        } else {
            callOnUriLoaded(uri);
        }
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().findFragmentById(R.id.container) instanceof VideoPlayerFragment) {
            setProgressOverlayVisible(false);
        }
        super.onBackPressed();
    }

    public void requestShareUrl(ShareRequest shareRequest) {
        setProgressOverlayVisible(true);
        this.useConvertedUrl = false;
        shareRequest.setId(item.getVideoId());
        currentAction = WebClient.Actions.unique(WebClient.Actions.VIDEOS_SHARE);
        WebClient.request().shareVideoLink(
            shareRequest, new ComplexEventBusCallback<>(currentAction, ShareLinkLoadedEvent::new));
    }

    private void callOnUriLoaded(Uri uri) {
        setProgressOverlayVisible(false);
        item.setVideoStreamUri(uri);
        Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof VideoFileDetailsFragment) {
            ((VideoFileDetailsFragment) fragment).onUriLoaded(uri);
        }
    }

    public void updateItem(File changedFile) {
        setProgressOverlayVisible(true);
        currentAction = WebClient.Actions.unique(WebClient.Actions.VIDEOS_EDIT);
        WebClient.request().editVideo(changedFile, new EditEvenBusCallback<>(currentAction, changedFile));
    }

    public void onEvent(RequestCompletedEvent<FileInfo> event) {
        if (event.is(currentAction) && event.contains(VideoFileInfo.class) && !keepEvent(event)) {
            FileUrlCache urlCache = FileUrlCache.getInstance();
            String name = item.getStorageFileName(useConvertedUrl);
            urlCache.addEntry(name, event.getResult());
            callOnUriLoaded(urlCache.getFileUrl(name));
        }
    }

    public void onEvent(ShareLinkLoadedEvent event) {
        if (event.is(currentAction) && !keepEvent(event)) {
            callOnUriLoaded(event.getShareLink());
        }
    }

    @Override
    protected void onEditCompleted(EditRequestCompletedEvent<File> event) {
        event.getChangedEntity().copyTo(item);
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    @Override
    protected boolean onNetworkErrorEvent(NetworkErrorEvent event) {
        if (!(event.getCause() instanceof UnknownHostException) &&
            WebClient.Actions.common(event.getAction()) != WebClient.Actions.VIDEOS_EDIT) {

            callOnUriLoaded(null);
            return true;
        }
        return false;
    }

    @Override
    protected int getOnCompletedMessageId() {
        return R.string.text_edit_completed_video;
    }
}
