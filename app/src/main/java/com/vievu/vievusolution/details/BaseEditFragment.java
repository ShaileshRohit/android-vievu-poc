package com.vievu.vievusolution.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vievu.vievusolution.BaseVievuFragment;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.TaggedBean;
import com.vievu.vievusolution.bean.UserRelatedBean;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.filtering.fragment.ConstraintProvider;
import com.vievu.vievusolution.filtering.fragment.MultiTagConstraintFragment;
import com.vievu.vievusolution.filtering.fragment.SearchHintConfigurable;
import com.vievu.vievusolution.filtering.fragment.UsersConstraintFragment;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ui.TagLabelView;
import com.vievu.vievusolution.util.ActivityUtil;

import org.apmem.tools.layouts.FlowLayout;

import butterknife.Bind;
import butterknife.BindDimen;
import butterknife.ButterKnife;
import butterknife.OnClick;

public abstract class BaseEditFragment<TItem extends UserRelatedBean<TItem>> extends BaseVievuFragment implements View.OnClickListener {

    @Bind(R.id.progressBar)
    protected ProgressBar progressBar;
    @Nullable
    @Bind(R.id.userImageView)
    protected ImageView userImageView;
    @Nullable
    @Bind(R.id.userNameTextView)
    protected TextView userNameTextView;
    @Bind(R.id.commentEditText)
    protected EditText commentEditText;
    @Nullable
    @Bind(R.id.tagsContainer)
    protected FlowLayout tagsContainer;

    protected int menuLayoutId = R.menu.menu_edit;
    protected MenuItem saveItem;

    protected Picasso picasso;
    @BindDimen(R.dimen.side_user_image)
    protected int imageSize;
    protected TItem changedItem;

    public BaseDetailsActivity<TItem> getDetailsActivity() {
        //noinspection unchecked
        return (BaseDetailsActivity<TItem>) getActivity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        usesEvents = true;
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        changedItem = createChangedItem();
        TItem item = getDetailsActivity().getItem();
        if (item != null) {
            item.copyTo(changedItem);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);
        if (userNameTextView != null) {
            userNameTextView.setText(changedItem.getUserName());
            picasso = Picasso.with(getActivity());
            picasso.setLoggingEnabled(BuildConfig.DEBUG);
            setUserImage(changedItem.getUserId());
        }
        if (tagsContainer != null && changedItem instanceof TaggedBean) {
            TagLabelView.generateLabels(
                    tagsContainer,
                    (TaggedBean) changedItem,
                    this);
            TagLabelView tagView = new TagLabelView(getActivity(), null);
            tagView.setBackgroundResource(R.drawable.background_add_tag_label);
            tagView.getTagTextView().setText(getString(R.string.text_add));
            //noinspection deprecation
            tagView.getTagTextView().setTextColor(getResources().getColor(R.color.accent));
            tagView.getTagTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_add_cross, 0);
            tagView.setOnClickListener(this::onAddTagClicked);
            tagsContainer.addView(tagView);
        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(menuLayoutId, menu);
        saveItem = menu.findItem(R.id.action_save);
        saveItem.setEnabled(validateViews());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            ActivityUtil.hideKeyboard(getActivity());
            saveItem.setEnabled(false);
            saveChanges();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onSaveChangesError() {
        saveItem.setEnabled(validateViews());
    }

    @SuppressWarnings("NullableProblems")
    @Nullable
    @OnClick(R.id.userInfoContainer)
    void onUserInfoClicked() {
        showSuggestionsFragment(
            new UsersConstraintFragment(),
            R.string.hint_user_name,
            value -> {
                changedItem.setUserId(value.getUserId());
                changedItem.setUserName(value.getUserName());
                getFragmentManager().popBackStack();
            });
    }

    void onAddTagClicked(View addTagLabel) {
        showSuggestionsFragment(
            //new TagsConstraintFragment(),
            MultiTagConstraintFragment.create(((TaggedBean) changedItem).getTags()),
            R.string.filter_type_tag,
            newTags -> {
                ((TaggedBean) changedItem).setTags(newTags);
                getFragmentManager().popBackStack();
            });
    }

    @Override
    public void onClick(View tagView) {
        //noinspection ConstantConditions
        int index = tagsContainer.indexOfChild(tagView);
        ((TaggedBean) changedItem).getTags().remove(index);
        tagsContainer.removeViewAt(index);
    }

    public void onEvent(OfflineModeChangedEvent event) {
        if (saveItem != null) {
            saveItem.setEnabled(validateViews());
        }
    }

    protected <T> void showSuggestionsFragment(
            ConstraintProvider<T> fragment,
            int hintId,
            ConstraintProvider.OnValueChangedListener<T> onValueChangedListener) {

        if (fragment instanceof SearchHintConfigurable) {
            ((SearchHintConfigurable) fragment).setSearchHintId(hintId);
        }
        fragment.setOnValueChangedListener(onValueChangedListener);
        getDetailsActivity().setTitle(getCurrentTitleId());
        getDetailsActivity().showFragment(fragment, true);
    }

    protected void setUserImage(int userId) {
        picasso
            .load(WebClient.getAvatarUrl(userId, imageSize, imageSize))
            .placeholder(R.drawable.placeholder_user)
            .into(userImageView);
    }

    protected boolean validateViews() {
        return getDetailsActivity().isInOnlineMode();
    }

    protected abstract void saveChanges();
    protected abstract TItem createChangedItem();
    protected abstract int getLayoutId();
    protected abstract int getCurrentTitleId();
}
