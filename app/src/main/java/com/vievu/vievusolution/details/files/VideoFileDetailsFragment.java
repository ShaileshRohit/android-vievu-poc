package com.vievu.vievusolution.details.files;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.details.VideoPlayerFragment;
import com.vievu.vievusolution.ui.VideoPropertyMixin;

import butterknife.Bind;
import butterknife.OnClick;

public class VideoFileDetailsFragment extends FileDetailsFragment {

    @Bind(R.id.videoNotConvertedOverlay) TextView videoNotConvertedOverlay;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_video_details;
    }

    protected void initPropertiesTable(File file) {
        VideoPropertyMixin.with(propertiesListView)
            .addVideoPropertyRow(R.string.text_video_file_name, file.getCameraFileName())
            .addVideoPropertyRow(R.string.text_video_record_date, file.getRecordDate())
            .addVideoPropertyRow(R.string.text_video_upload_date, file.getUploadDate())
            .addVideoPropertyRow(R.string.text_video_duration, file.getDuration())
            .addVideoPropertyRow(R.string.text_video_recorded_on_camera, file.getCameraSerialNumber())
            .addVideoPropertyRow(R.string.text_video_user, file.getUserName())
            .addVideoPropertyRow(R.string.text_video_category, file.getCategoryName())
            .addVideoPropertyRow(R.string.text_video_report_number, file.getReportNumber())
            .addVideoPropertyRow(R.string.text_video_event_number, file.getEventNumber())
            .addVideoPropertyRow(R.string.text_video_case, file.getCaseName())
            .addVideoPropertyRow(R.string.text_video_digital_signature, file.getDigitalSignature())
            .addVideoPropertyRow(R.string.text_video_never_delete, file.isNeverDelete())
            .addVideoPropertyRow(R.string.text_video_lockdown, file.isLockDownVideo())
            .withNextId(R.id.property_row_video_location)
            .addVideoPropertyRow(R.string.text_video_geolocation, file.getVideoLocation());
    }

    @OnClick(R.id.playButton)
    void onPlayClick() {
        Uri videoUri = getVideoActivity().getItem().getVideoStreamUri();
        if (videoUri == null) {
            getVideoActivity().requestPlayUrl();
        } else {
            onUriLoaded(videoUri);
        }
    }

    public void onUriLoaded(Uri uri) {
        if (uri != null) {
            getVideoActivity().setProgressOverlayVisible(true);
            getVideoActivity().showFragment(VideoPlayerFragment.create(uri), true);

        } else {
            videoNotConvertedOverlay.setVisibility(View.VISIBLE);
            thumbnailImageView.setVisibility(View.GONE);
        }
    }
}
