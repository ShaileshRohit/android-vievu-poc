package com.vievu.vievusolution.details.camera;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vievu.vievusolution.BaseVievuFragment;
import com.vievu.vievusolution.CameraClient;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.CameraFilesAdapter;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.camera.ThumbnailLoader;
import com.vievu.vievusolution.ptpip.dataset.FileInfo;
import com.vievu.vievusolution.ui.CommonListMixin;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class CameraFileListFragment extends BaseVievuFragment {

    private CommonListMixin commonListMixin;
    private CameraFilesAdapter adapter = null;

    public CameraFilesActivity getCameraFilesActivity() {
        return (CameraFilesActivity)getActivity();
    }

    public CameraFileListFragment() {
        usesEvents = true;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_files_list, container, false);
        ButterKnife.bind(this, view);
        commonListMixin = CommonListMixin.create(view);
        commonListMixin.setEmptyProgressView();
        commonListMixin.getListView().addFooterView(new View(getActivity()));
        commonListMixin.getListView().setAdapter(adapter = new CameraFilesAdapter(getActivity(), null));
        CameraClient.getInstance().getVideoFilesAsync(getCameraFilesActivity()::onWiFiConnectionLost);
        getActivity().setTitle(R.string.title_activity_home);
        return view;
    }

    @OnItemClick(android.R.id.list)
    void onFileClicked(int position) {
        getCameraFilesActivity().onFilePicked(adapter.getItem(position));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        commonListMixin.unbind();
        ThumbnailLoader.getInstance().cancelTag(CameraFilesAdapter.PICASSO_TAG);
    }

    public void onEventMainThread(RequestCompletedEvent<List<FileInfo>> event) {
        if (event.is(CameraClient.Actions.GET_FILE_LIST)) {

            if (commonListMixin != null) {
                commonListMixin.setEmptyMessageView();
            }
            adapter.swapItems(event.getResult());
        }
    }
}
