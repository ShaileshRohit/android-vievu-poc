package com.vievu.vievusolution.details.location;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.annimon.stream.function.Consumer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vievu.vievusolution.BaseVievuFragment;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.adapter.AutocompletePredictionAdapter;
import com.vievu.vievusolution.bean.GeoLocation;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.details.BaseDetailsActivity;
import com.vievu.vievusolution.event.PingCompletedEvent;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ui.UiUtil;
import com.vievu.vievusolution.ui.VievuAutoCompleteTextView;
import com.vievu.vievusolution.util.ActivityUtil;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

@SuppressWarnings("MissingPermission")
public class LocationFragment extends BaseVievuFragment implements
        AutocompletePredictionAdapter.GoogleApiProvider,
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerDragListener, AdapterView.OnItemClickListener {

    private static final float INITIAL_ZOOM = 3.5f;
    private static final int DEFAULT_ZOOM = 15;
    private static final LatLng INITIAL_LOCATION = new LatLng(40, -101);

    public static final int REQUEST_CODE_GOOGLE_SETTINGS = 701;

    public static LocationFragment createDisplayOnly(@Nullable GeoLocation markerPosition) {
        return create(markerPosition, null);
    }

    public static LocationFragment create(@Nullable GeoLocation markerPosition, Consumer<LatLng> onMarkerPlacedCallback) {
        LocationFragment fragment = new LocationFragment();
        fragment.markerPosition = markerPosition != null ? markerPosition.toLatLng() : null;
        fragment.onMarkerPlacedCallback = onMarkerPlacedCallback;
        return fragment;
    }

    @Bind(R.id.map) MapView mapView;
    @Bind(R.id.searchView) VievuAutoCompleteTextView searchView;
    @Bind(R.id.progressBar) ProgressBar progressBar;
    @Bind(R.id.errorTextView) TextView errorTextView;

    private int pingAction;

    private MenuItem saveItem;

    private LatLng markerPosition;
    private Marker marker;
    private Consumer<LatLng> onMarkerPlacedCallback;

    private GoogleApiClient googleApiClient;

    public BaseDetailsActivity<File> getDetailsActivity() {
        //noinspection unchecked
        return ((BaseDetailsActivity<File>) getActivity());
    }

    public GoogleApiClient getGoogleApiClient() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                .build();
        }
        return googleApiClient;
    }

    @Override
    public LatLngBounds getSearchBounds() {
        return new LatLngBounds(
            new LatLng(markerPosition.latitude - 1, markerPosition.longitude - 1),
            new LatLng(markerPosition.latitude + 1, markerPosition.longitude + 1));
    }

    public LocationFragment() {
        usesEvents = true;
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        ButterKnife.bind(this, view);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        UiUtil.colorizeProgressBar(progressBar, R.color.accent);
        searchView.setProgressIndicator(progressBar);
        searchView.setEnabled(false);
        searchView.setOnItemClickListener(this);
        if (onMarkerPlacedCallback != null && isGooglePlayServicesAvailable()) {
            pingAction = WebClient.Actions.unique(0);
            WebClient.pingAsync(pingAction);
        } else {
            view.findViewById(R.id.searchCardView).setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (onMarkerPlacedCallback != null) {
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.getUiSettings().setMapToolbarEnabled(false);
            getGoogleApiClient().connect();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(INITIAL_LOCATION, INITIAL_ZOOM));
        } else if (markerPosition != null) {
            onInitialLocationReady(markerPosition);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        AutocompletePrediction item = (AutocompletePrediction) adapterView.getItemAtPosition(position);
        searchView.setEnabled(false);
        saveItem.setEnabled(false);
        searchView.setText(item.getFullText(null));
        marker.setVisible(false);
        PendingResult<PlaceBuffer> pendingResult = Places.GeoDataApi.getPlaceById(getGoogleApiClient(), item.getPlaceId());
        pendingResult.setResultCallback(result -> {
            if (result.getStatus().isSuccess()) {
                Place place = result.get(0);
                markerPosition = place.getLatLng();
                marker.setPosition(markerPosition);
                marker.setVisible(true);
                mapView.getMapAsync(map -> map.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(markerPosition, DEFAULT_ZOOM)));
            } else {
                marker.setVisible(true);
            }
            result.release();
            searchView.setEnabled(true);
            saveItem.setEnabled(true);
        }, 5, TimeUnit.SECONDS);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_GOOGLE_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                subscribeForLocationUpdates();
            } else {
                onInitialLocationError();
            }
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        return GoogleApiAvailability.getInstance().
                isGooglePlayServicesAvailable(getActivity()) == ConnectionResult.SUCCESS;
    }

    //region Options menu callbacks

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (onMarkerPlacedCallback != null) {
            inflater.inflate(R.menu.menu_edit, menu);
            saveItem = menu.findItem(R.id.action_save);
            saveItem.setEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            ActivityUtil.hideKeyboard(getActivity());
            onMarkerPlacedCallback.accept(marker.getPosition());
            getFragmentManager().popBackStack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //endregion

    //region Lifecycle hooks for map view

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        mapView.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    //endregion

    //region Initial location detecting callbacks

    private void onInitialLocationReady(@NonNull LatLng latLng) {
        onInitialLocationReady(latLng, DEFAULT_ZOOM);
    }

    private void onInitialLocationReady(@NonNull LatLng latLng, float zoom) {
        onInitialLocationReady(latLng, zoom, false);
    }

    private void onInitialLocationReady(@NonNull LatLng latLng, float zoom, boolean withAnimation) {
        Log.d("LocationFragment", "Location found: " + latLng.toString());
        this.markerPosition = latLng;
        if (mapView != null) {
            mapView.getMapAsync(map -> {
                marker = map.addMarker(
                    new MarkerOptions()
                        .draggable(onMarkerPlacedCallback != null)
                        .position(latLng));
                if (!withAnimation) {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                } else {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                }
                if (onMarkerPlacedCallback != null) {
                    map.setOnMapClickListener(marker::setPosition);
                    map.setOnMarkerDragListener(this);
                    onMarkerDragEnd(marker);
                    searchView.setAdapter(new AutocompletePredictionAdapter(getActivity(), this));
                    searchView.setEnabled(true);
                    saveItem.setEnabled(true);
                }
            });
        }
    }

    public void onInitialLocationError() {
        Log.d("LocationFragment", "Cannot get location");
        onInitialLocationReady(INITIAL_LOCATION, INITIAL_ZOOM);
        if (getVievuActivity() != null && !getDetailsActivity().isInBackground()) {
            getVievuActivity().shortToast(R.string.error_cannot_get_location);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("LocationFragment", "Received location update");
        if (location != null) {
            onInitialLocationReady(
                    new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM, true);
        } else {
            onInitialLocationError();
        }
        LocationServices.FusedLocationApi.removeLocationUpdates(getGoogleApiClient(), this);
    }

    //endregion

    //region Google API Client connection callbacks

    private LocationRequest locationRequest;

    private LocationRequest getLocationRequest() {
        if (locationRequest == null) {
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(5);
            locationRequest.setNumUpdates(1);
        }
        return locationRequest;
    }

    private void subscribeForLocationUpdates() {
        LocationServices.FusedLocationApi.
            requestLocationUpdates(getGoogleApiClient(), getLocationRequest(), new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult result) {
                    onLocationChanged(result.getLastLocation());
                }

                @Override
                public void onLocationAvailability(LocationAvailability locationAvailability) {
                    Log.d("LocationFragment", "Location availability changed: " + locationAvailability.isLocationAvailable());
                    if (!locationAvailability.isLocationAvailable()) {
                        onInitialLocationError();
                    }
                }
            }, Looper.getMainLooper());
    }

    public void requestGoogleSettingsIfNecessary() {
        Log.d("LocationFragment", "No last known location. Requesting access to geo data...");
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(getGoogleApiClient(),
                new LocationSettingsRequest.Builder().addLocationRequest(getLocationRequest()).build());
        result.setResultCallback(locationSettingsResult -> {
            switch (locationSettingsResult.getStatus().getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS :
                    Log.d("LocationFragment", "Registering to location updates...");
                    subscribeForLocationUpdates();
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED :
                    try {
                        locationSettingsResult.getStatus().
                                startResolutionForResult(getActivity(), REQUEST_CODE_GOOGLE_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                        Log.d("LocationFragment", "Cannot open Google settings change dialog");
                    }
                    break;
                default:
                    Log.d("LocationFragment", "Cannot change necessary Google settings");
                    onInitialLocationError();
                    break;

            }
        }, 5, TimeUnit.SECONDS);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d("LocationFragment", "Connected to Google API Client");
        if (markerPosition != null) {
            onInitialLocationReady(markerPosition);
        } else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(getGoogleApiClient());
            if (location != null) {
                onInitialLocationReady(new LatLng(location.getLatitude(), location.getLongitude()));
            } else if (getDetailsActivity().requestPermissionIfNecessary(Manifest.permission.ACCESS_FINE_LOCATION)) {
                requestGoogleSettingsIfNecessary();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int errorId) {
        onInitialLocationError();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        onInitialLocationError();
    }

    //endregion

    //region Marker drag callbacks

    @Override
    public void onMarkerDragStart(Marker marker) {}

    @Override
    public void onMarkerDrag(Marker marker) {}

    @Override
    public void onMarkerDragEnd(Marker marker) {
        if (marker == this.marker) {
            markerPosition = marker.getPosition();
        }
    }

    //endregion

    //region Wi-Fi state handling

    public void onWiFiConnectionChanged() {
        pingAction = WebClient.Actions.unique(0);
        WebClient.pingAsync(0);
    }

    public void onWiFiConnectionLost() {
        setSearchEnabled(false);
    }

    public void onEvent(PingCompletedEvent event) {
        if (event.getAction() == pingAction) {
            if (!getDetailsActivity().keepEvent(event)) {
                setSearchEnabled(event.isConnected());
            }
        }
    }

    private void setSearchEnabled(boolean isEnabled) {
        if (searchView != null) {
            if (isEnabled) {
                searchView.setVisibility(View.VISIBLE);
                errorTextView.setVisibility(View.GONE);
            } else {
                ActivityUtil.hideKeyboard(getActivity());
                searchView.setVisibility(View.INVISIBLE);
                searchView.setText(null);
                progressBar.setVisibility(View.GONE);
                errorTextView.setVisibility(View.VISIBLE);
            }
        }
    }

    //endregion
}
