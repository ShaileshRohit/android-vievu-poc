package com.vievu.vievusolution;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vievu.vievusolution.net.camera.RetryHandler;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NoCameraFragment extends BaseVievuFragment {

    public static final String EXTRA_IN_PROGRESS = "in_progress";

    public static NoCameraFragment create(boolean inProgress) {
        NoCameraFragment fragment = new NoCameraFragment();
        if (inProgress) {
            Bundle arguments = new Bundle();
            arguments.putBoolean(EXTRA_IN_PROGRESS, true);
            fragment.setArguments(arguments);
        }
        return fragment;
    }

    @Bind(R.id.progressBar) ProgressBar progressBar;
    @Bind(R.id.errorOverlayLayout) LinearLayout errorLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_no_camera, container, false);
        ButterKnife.bind(this, view);
        TextView tapsHintTextView = ButterKnife.findById(view, R.id.tapsHintTextView);
        ImageView errorImageView = ButterKnife.findById(view, R.id.errorImageView);
        ButterKnife.<TextView>findById(view, R.id.errorTextView).setText(R.string.text_error);
        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        setProgressBarVisible(getArguments() != null && getArguments().containsKey(EXTRA_IN_PROGRESS));
        if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureListener());
            errorImageView.setImageResource(R.drawable.no_camera);
            tapsHintTextView.setText(R.string.text_taps_no_camera);
            errorLayout.setOnTouchListener((v, event) -> {
                gestureDetector.onTouchEvent(event);
                return true;
            });
        } else {
            errorImageView.setImageResource(R.drawable.no_wifi);
            tapsHintTextView.setText(R.string.text_taps_no_wifi);
            errorLayout.setClickable(true);
        }
        return view;
    }

    @OnClick(R.id.errorOverlayLayout)
    public void openWiFiSettings() {
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    public void retryConnection() {
        setProgressBarVisible(true);
        if (getActivity() instanceof RetryHandler) {
            ((RetryHandler) getActivity()).retry();
        }
    }

    public boolean isProgressBarVisible() {
        return progressBar.getVisibility() == View.VISIBLE;
    }

    public void setProgressBarVisible(boolean isVisible) {
        if (progressBar != null) {
            if (isVisible) {
                progressBar.setVisibility(View.VISIBLE);
                errorLayout.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
           retryConnection();
           return super.onSingleTapConfirmed(e);
        }


        @Override
        public boolean onDoubleTap(MotionEvent e) {
            openWiFiSettings();
            return true;
        }
    }
}

