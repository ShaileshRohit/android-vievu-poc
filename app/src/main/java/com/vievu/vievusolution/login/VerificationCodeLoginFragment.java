package com.vievu.vievusolution.login;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.LoginErrorDescription;
import com.vievu.vievusolution.bean.MfaTokenData;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.LoginEventBusCallback;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.ValidationUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class VerificationCodeLoginFragment extends BaseLoginFragment {

    public static final String KEY_MFA_TYPE = "VCLF_mfa_type";
    public static final String KEY_MFA_PARAMETER = "VCLF_mfa_parameter";

    public static VerificationCodeLoginFragment create(String type, String parameter) {
        VerificationCodeLoginFragment fragment = new VerificationCodeLoginFragment();
        fragment.type = type;
        fragment.parameter = parameter;
        return fragment;
    }

    public static VerificationCodeLoginFragment tryCreate(SharedPreferences preferences) {
        VerificationCodeLoginFragment fragment = null;
        if (preferences.contains(KEY_MFA_TYPE)) {
            fragment = new VerificationCodeLoginFragment();
            fragment.type = preferences.getString(KEY_MFA_TYPE, null);
            fragment.parameter = preferences.getString(KEY_MFA_PARAMETER, null);
        }
        return fragment;
    }

    public static void cleanUp(SharedPreferences.Editor editor) {
        editor.remove(KEY_MFA_TYPE);
        editor.remove(KEY_MFA_PARAMETER);
    }

    private String type;
    private String parameter;

    private int clickedButtonId;

    @Bind(R.id.descriptionTextView) TextView descriptionTextView;
    @Bind(R.id.verificationCodeEditText) EditText verificationCodeEditText;
    @Bind(R.id.resendCodeTextView) TextView resendCodeTextView;
    @Bind(R.id.verifyButton) Button verifyButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_security_code, container, false);
        ButterKnife.bind(this, view);
        boolean isInOnlineMode = getLoginActivity().isInOnlineMode();
        resendCodeTextView.setEnabled(isInOnlineMode);
        verifyButton.setEnabled(isInOnlineMode);
        descriptionTextView.setText(Html.fromHtml(getString(
            WebClient.Login.REQUEST_CODE_METHOD_SMS.equals(type) ?
                R.string.template_verification_sms_sent :
                R.string.template_verification_email_sent,
            parameter)));
        saveState();
        return view;
    }

    //region ButterKnife bindings

    @OnEditorAction(R.id.verificationCodeEditText)
    public boolean onVerificationCodeOkClicked(int key) {
        if (EditorInfo.IME_ACTION_DONE == key) {
            onVerifyClick();
            return true;
        }
        return false;
    }

    @OnClick(R.id.verifyButton)
    public void onVerifyClick() {
        if (ValidationUtil.validateEditText(verificationCodeEditText,
                (value) -> value.length() > 0, R.string.error_invalid_verification_code)) {
            this.clickedButtonId = R.id.verifyButton;
            getLoginActivity().checkInternetConnection();
        }
    }

    @OnClick(R.id.resendCodeTextView)
    public void onResendClick() {
        this.clickedButtonId = R.id.resendCodeTextView;
        getLoginActivity().checkInternetConnection();
    }

    //endregion

    public void saveState() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
        editor.putString(KEY_MFA_TYPE, type);
        editor.putString(KEY_MFA_PARAMETER, parameter);
        editor.apply();
    }

    public void onEvent(OfflineModeChangedEvent event) {
        if (verifyButton != null) {
            resendCodeTextView.setEnabled(event.isInOnlineMode());
            verifyButton.setEnabled(event.isInOnlineMode());
        }
    }

    public void onEvent(RequestCompletedEvent<LoginErrorDescription> event) {
        if (event.contains(LoginErrorDescription.class) && !getLoginActivity().keepEvent(event)) {
            getLoginActivity().shortToast(
                event.getResult().getCode() == LoginErrorDescription.CODE_MFA_CODE_SENT ?
                    R.string.text_verification_code_resent :
                    R.string.error_unknown);
            getLoginActivity().setProgressBarVisible(false);
        }
    }

    @Override
    protected void onInternetAvailable() {
        MfaTokenData tokenData = getLoginActivity().getCurrentMfaTokenData();
        if (tokenData != null) {
            switch (clickedButtonId) {
                case R.id.resendCodeTextView:
                    WebClient.request().securityCode(
                        tokenData.getMfaToken(), type, new LoginEventBusCallback<>());
                    break;
                case R.id.verifyButton:
                    WebClient.getInstance(null).authorizeWithCode(
                        verificationCodeEditText.getText().toString(),
                        tokenData.getMfaToken(), type);
                    break;
            }
        }
    }
}
