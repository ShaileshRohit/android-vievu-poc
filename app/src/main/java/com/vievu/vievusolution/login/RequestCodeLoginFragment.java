package com.vievu.vievusolution.login;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.annimon.stream.function.Function;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.LoginEventBusCallback;
import com.vievu.vievusolution.net.WebClient;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RequestCodeLoginFragment extends BaseLoginFragment implements RadioGroup.OnCheckedChangeListener {

    public static RequestCodeLoginFragment create(HashMap<String, String> availableMethods) {
        RequestCodeLoginFragment fragment = new RequestCodeLoginFragment();
        fragment.availableMethods = availableMethods;
        return fragment;
    }

    @Bind(R.id.sendCodeButton) Button sendCodeButton;

    private String selectedMethod = WebClient.Login.REQUEST_CODE_METHOD_EMAIL;
    private HashMap<String, String> availableMethods;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_request_code, container, false);
        ButterKnife.bind(this, view);

        RadioGroup requestCodeMethodRadioGroup = (RadioGroup) view.findViewById(R.id.requestCodeMethodRadioGroup);
        requestCodeMethodRadioGroup.check(R.id.requestCodeMethodEmail);
        requestCodeMethodRadioGroup.setOnCheckedChangeListener(this);
        RadioButton radioButton;

        radioButton = (RadioButton) view.findViewById(R.id.requestCodeMethodEmail);
        radioButton.setText(availableMethods.get(WebClient.Login.REQUEST_CODE_METHOD_EMAIL));
        radioButton.setTag(WebClient.Login.REQUEST_CODE_METHOD_EMAIL);

        radioButton = (RadioButton) view.findViewById(R.id.requestCodeMethodSms);
        radioButton.setText(availableMethods.get(WebClient.Login.REQUEST_CODE_METHOD_SMS));
        radioButton .setTag(WebClient.Login.REQUEST_CODE_METHOD_SMS);

        boolean isInOnlineMode = getLoginActivity().isInOnlineMode();
        sendCodeButton.setEnabled(isInOnlineMode);
        return view;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        selectedMethod = (String) group.findViewById(checkedId).getTag();
    }

    @OnClick(R.id.sendCodeButton)
    public void onVerifyClick() {
        getLoginActivity().checkInternetConnection();
    }

    @Override
    protected void onInternetAvailable() {
        WebClient.request().securityCode(
            getLoginActivity().getCurrentMfaTokenData().getMfaToken(),
            selectedMethod,
            new LoginEventBusCallback<>());
    }

    public void onEvent(RequestCompletedEvent event) {
        if (!getLoginActivity().keepEvent(event)) {
            getLoginActivity().showFragment(
                VerificationCodeLoginFragment.create(
                    selectedMethod,
                    availableMethods.get(selectedMethod)),
                true);
            getLoginActivity().setProgressBarVisible(false);
        }
    }

    public void onEvent(OfflineModeChangedEvent event) {
        if (sendCodeButton != null) {
            sendCodeButton.setEnabled(event.isInOnlineMode());
        }
    }
}
