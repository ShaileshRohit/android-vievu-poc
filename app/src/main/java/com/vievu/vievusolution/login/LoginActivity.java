package com.vievu.vievusolution.login;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.EventBusActivity;
import com.vievu.vievusolution.MainActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.CaseStatus;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.bean.LoginErrorDescription;
import com.vievu.vievusolution.bean.MfaTokenData;
import com.vievu.vievusolution.bean.SecurityQuestion;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.data.AssignedCamerasAsyncRepository;
import com.vievu.vievusolution.data.CaseStatusesAsyncRepository;
import com.vievu.vievusolution.data.CategoriesAsyncRepository;
import com.vievu.vievusolution.data.MfaTokenRepository;
import com.vievu.vievusolution.data.TagsAsyncRepository;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.dialog.TextInputDialogFragment;
import com.vievu.vievusolution.event.LoginEvent;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.PingCompletedEvent;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.openid_provider.IdentityProvider;
import com.vievu.vievusolution.openid_provider.TokenActivity;
import com.vievu.vievusolution.util.ActivityUtil;
import com.vievu.vievusolution.util.LogoutUtil;
import com.vievu.vievusolution.util.StringUtil;

import net.openid.appauth.AppAuthConfiguration;
import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.ClientSecretBasic;
import net.openid.appauth.RegistrationRequest;
import net.openid.appauth.RegistrationResponse;
import net.openid.appauth.ResponseTypeValues;
import net.openid.appauth.browser.BrowserDescriptor;
import net.openid.appauth.browser.ExactBrowserMatcher;

import java.net.HttpURLConnection;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.RetrofitError;
import retrofit.client.Response;



public class LoginActivity extends EventBusActivity {

    private static final String KEY_MFA_METHODS = "mfa_methods";
    private static final String KEY_MFA_TOKEN_KEY = "mfa_token_key";
    private static final String TAG = "LoginActivity";
    private AuthorizationService mAuthService;

    @Bind(R.id.progressBarContainer) View progressBarContainer;
    @Bind(R.id.backButton) View backButton;

    private SecurityQuestion securityQuestion;

    private HashMap<String, String> mfaRequestCodeMethods;
    private String selectedMfaTokenKey;
    private MfaTokenData mfaTokenData;
    private MfaTokenRepository mfaTokenRepository = new MfaTokenRepository();

    public MfaTokenData selectMfaTokenData(String userName, String accountName) {
        this.selectedMfaTokenKey = MfaTokenRepository.key(userName, accountName);
        if (mfaTokenRepository.getData().containsKey(this.selectedMfaTokenKey)) {
            mfaTokenData = mfaTokenRepository.getData().get(this.selectedMfaTokenKey);
        } else {
            mfaTokenData = new MfaTokenData(null);
            mfaTokenRepository.getData().put(this.selectedMfaTokenKey, mfaTokenData);
        }
        return mfaTokenData;
    }

    public MfaTokenData getCurrentMfaTokenData() {
        if (mfaTokenData == null && mfaTokenRepository.getData().containsKey(this.selectedMfaTokenKey)) {
            mfaTokenData = mfaTokenRepository.getData().get(this.selectedMfaTokenKey);
        }
        return mfaTokenData;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUtil.tryUnbindNetworkAdapter(this);
        isAutoLogoutEnabled = false;
        setContentView(R.layout.activity_login);
        mAuthService = new AuthorizationService(this);

        //noinspection ConstantConditions
        if (LogoutUtil.validateAuthData(this)
            && UserDataRepository.restoreFromCache(this)
            && WebClient.restoreInstance(this)
            && TagsAsyncRepository.restoreFromCache(this)
            && CategoriesAsyncRepository.restoreFromCache(this)
            && CaseStatusesAsyncRepository.restoreFromCache(this)
            && AssignedCamerasAsyncRepository.restoreFromCache(this)) {

            goToMainActivity();
            return;
        }
        mfaTokenRepository.loadStoredTokens(this);
        ButterKnife.bind(this);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.selectedMfaTokenKey = preferences.getString(KEY_MFA_TOKEN_KEY, null);
        Fragment fragment = null;
        if (selectedMfaTokenKey != null) {
            this.mfaRequestCodeMethods = StringUtil.deserialize(preferences.getString(KEY_MFA_METHODS, null));
            mfaTokenData = mfaTokenRepository.getData().get(this.selectedMfaTokenKey);
            String accountName = preferences.getString(CommonConstants.KEY_ACCOUNT_NAME, null);
            if (mfaTokenData != null && accountName != null) {
                WebClient.getInstance(accountName);
                fragment = VerificationCodeLoginFragment.tryCreate(preferences);
                if (fragment == null && this.mfaRequestCodeMethods.size() > 1) {
                    fragment = RequestCodeLoginFragment.create(this.mfaRequestCodeMethods);
                }
            } else {
                preferences.edit().remove(KEY_MFA_TOKEN_KEY).apply();
                this.selectedMfaTokenKey = null;
            }
        }
        if (fragment == null) {
            fragment = new MainLoginFragment();
        } else {
            backButton.setVisibility(View.VISIBLE);
        }
        ActivityUtil.replaceFragment(this, fragment);
    }


    @OnClick(R.id.backButton)
    public void onBackButtonClick() {
        boolean hasMoreThanOneRequestMethod = this.mfaRequestCodeMethods.size() > 1;
        if (   callOnFragment(VerificationCodeLoginFragment.class, no -> cleanUpMfaData(hasMoreThanOneRequestMethod))
            && hasMoreThanOneRequestMethod) {
            showFragment(RequestCodeLoginFragment.create(this.mfaRequestCodeMethods), false);
        } else {
            backButton.setVisibility(View.GONE);
            cleanUpMfaData(false);
            showFragment(new MainLoginFragment(), false);
        }
    }

    @Override
    public void onStop() {
        mfaTokenRepository.saveData(this);
        super.onStop();
    }

    private void cleanUpMfaData(boolean cleanOnlyVerificationCodeStep) {
        SharedPreferences.Editor preferences = PreferenceManager.getDefaultSharedPreferences(this).edit();
        VerificationCodeLoginFragment.cleanUp(preferences);
        if (!cleanOnlyVerificationCodeStep) {
            preferences.remove(KEY_MFA_METHODS);
            preferences.remove(KEY_MFA_TOKEN_KEY);
            preferences.remove(KEY_MFA_METHODS);
        }
        preferences.apply();
    }

    public void checkInternetConnection() {
        ActivityUtil.hideKeyboard(this);
        setProgressBarVisible(true);
        currentAction = WebClient.Actions.unique(0);
        WebClient.pingAsync(currentAction);
    }

    public void setProgressBarVisible(boolean isVisible) {
        backButton.setEnabled(!isVisible);
        if (isVisible) {
            progressBarContainer.setVisibility(View.VISIBLE);
        } else {
            progressBarContainer.setVisibility(View.GONE);
        }
    }

    public void showFragment(Fragment fragment, boolean isNavigatingForward) {
        if (fragment.getClass() != getFragmentManager().findFragmentById(R.id.container).getClass()) {
            if (isNavigatingForward) {
                SharedPreferences.Editor preferences = PreferenceManager.getDefaultSharedPreferences(this).edit();
                preferences.putString(KEY_MFA_TOKEN_KEY, this.selectedMfaTokenKey);
                if (this.mfaRequestCodeMethods != null) {
                    preferences.putString(KEY_MFA_METHODS, StringUtil.serialize(this.mfaRequestCodeMethods));
                }
                preferences.apply();

                backButton.setVisibility(View.VISIBLE);
            }
            //ActivityUtil.replaceFragment(this, fragment);

            FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
            if (isNavigatingForward) {
                transaction.setCustomAnimations(R.animator.slide_in_from_left, R.animator.slide_out_to_right);
            } else {
                transaction.setCustomAnimations(R.animator.slide_in_from_right, R.animator.slide_out_to_left);
            }
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        }
    }

    private void goToMainActivity() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        String TokenAccessed=preferences.getString("TokenAccessed","0");
        startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
        /*if(TokenAccessed.equalsIgnoreCase("0")){
            initProvider();
        }
        else{
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }*/


    }

    private void showEmailCodeFragment() {
        showFragment(
            VerificationCodeLoginFragment.create(
                WebClient.Login.REQUEST_CODE_METHOD_EMAIL,
                this.mfaRequestCodeMethods.values().iterator().next()),
            true);
    }

    //region Event handlers

    public void onEvent(PingCompletedEvent event) {
        if (event.getAction() == currentAction) {
            if (event.isConnected()) {
                BaseLoginFragment fragment = (BaseLoginFragment) getFragmentManager().findFragmentById(R.id.container);
                fragment.onInternetAvailable();
            } else {
                shortToast(R.string.error_no_internet);
                setProgressBarVisible(false);
            }
        }
    }

    //region Data loading chain

    public void onTagsLoaded(Collection<Tag> tags) {
        TagsAsyncRepository.getInstance().cacheCopy(this);
        CategoriesAsyncRepository.getInstance().getDataAsync(this::onCategoriesLoaded);
    }

    public void onCategoriesLoaded(Collection<Category> categories) {
        CategoriesAsyncRepository.getInstance().cacheCopy(this);
        CaseStatusesAsyncRepository.getInstance().getDataAsync(this::onCaseStatusesLoaded);
    }

    public void onCaseStatusesLoaded(Collection<CaseStatus> caseStatuses) {
        CaseStatusesAsyncRepository.getInstance().cacheCopy(this);
        AssignedCamerasAsyncRepository.getInstance().getDataAsync(this::onAssignedCamerasLoaded);
    }

    public void onAssignedCamerasLoaded(Collection<String> assignedCameras) {
        AssignedCamerasAsyncRepository.getInstance().cacheCopy(this);
        goToMainActivity();
        setProgressBarVisible(false);
    }

    //endregion
    public void onEvent(LoginEvent event) {
        if (!keepEvent(event)) {
            if (event.getTokenData() != null) {
                LogoutUtil.storeAuthData(this, event.getTokenData());
                if (event.getTokenData().getMfaToken() != null) {
                    this.mfaTokenData.setMfaToken(event.getTokenData().getMfaToken());
                }
                mfaTokenData.updateTimestamp();
                cleanUpMfaData(false);
                if (UserDataRepository.getInstance().getPermissions().canUseMobileApp()) {
                    UserDataRepository.getInstance().cacheCopy(this);
                    TagsAsyncRepository.getInstance().getDataAsync(this::onTagsLoaded);
                } else {
                    shortToast(R.string.error_permission_denied);
                    setProgressBarVisible(false);
                }
            } else {
                setProgressBarVisible(false);
            }
        }
    }

    @Override
    public void onEvent(NetworkErrorEvent event) {
        if (!keepEvent(event)) {
            setProgressBarVisible(false);
            if (event.getCause() instanceof RetrofitError) {
                Response response = ((RetrofitError) event.getCause()).getResponse();
                if (response != null) {
                    switch (response.getStatus()) {
                        case HttpURLConnection.HTTP_BAD_REQUEST:
                            LoginErrorDescription errorDescription = (LoginErrorDescription)event.getDescription();
                            if (errorDescription.getMfaToken() != null) {
                                this.mfaTokenData.setMfaToken(errorDescription.getMfaToken());
                            }
                            switch (errorDescription.getCode()) {
                                case LoginErrorDescription.CODE_PASSWORD_EXPIRED:
                                    Toast.makeText(this,
                                        R.string.error_password_expired,
                                        Toast.LENGTH_LONG).show();
                                    break;
                                case LoginErrorDescription.CODE_MFA_REQUIRED:
                                    this.mfaRequestCodeMethods = errorDescription.getParameters();
                                    if (this.mfaRequestCodeMethods.size() > 1) {
                                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
                                        VerificationCodeLoginFragment.cleanUp(editor);
                                        editor.apply();
                                        showFragment(RequestCodeLoginFragment.create(this.mfaRequestCodeMethods), true);
                                    } else {
                                        showEmailCodeFragment();
                                    }
                                    break;
                                case LoginErrorDescription.CODE_MFA_CODE_SENT:
                                    this.mfaRequestCodeMethods = errorDescription.getParameters();
                                    showEmailCodeFragment();
                                    break;
                                case LoginErrorDescription.CODE_BAD_VERIFICATION_CODE:
                                    shortToast(R.string.error_bad_verification_code);
                                    break;
                                case LoginErrorDescription.CODE_USER_LOCKED:
                                    shortToast(R.string.error_user_locked);
                                    break;
                                default :
                                    shortToast(R.string.error_bad_credentials);
                                    break;
                            }
                            break;
                        case HttpURLConnection.HTTP_NOT_FOUND:
                            shortToast(R.string.error_unresolved_account_name);
                            break;
                    }
                }
            } else if (event.getCause() instanceof UnknownHostException) {
                ConnectivityManager connectivityManager =
                        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isConnected()) {
                    shortToast(R.string.error_unresolved_account_name);
                    return;
                }
            }
            super.onEvent(event);
        }
    }

    public void onEvent(SecurityQuestion event) {
        if (!keepEvent(event)) {
            setProgressBarVisible(false);
            if (event.getQuestion() != null) {
                this.securityQuestion = event;
            }
            TextInputDialogFragment fragment = TextInputDialogFragment.
                    create(R.string.title_dialog_security_question, securityQuestion.getQuestion());
            fragment.setOnInputSubmitListener(answer -> {
                setProgressBarVisible(true);
                String[] userData = MfaTokenRepository.userData(this.selectedMfaTokenKey);
                WebClient
                    .getInstance(userData[1])
                    .authorizeWithQuestion(userData[0], answer, securityQuestion.getQuestionId());
            });
            fragment.show(getFragmentManager(), null);
        }
    }

    //endregion


    // OpenID provider
    public void initProvider(){

        List<IdentityProvider> providers = IdentityProvider.getEnabledProviders(LoginActivity.this);
        IdentityProvider idp=providers.get(0);
        final AuthorizationServiceConfiguration.RetrieveConfigurationCallback retrieveCallback =
                new AuthorizationServiceConfiguration.RetrieveConfigurationCallback() {

                    @Override
                    public void onFetchConfigurationCompleted(
                            @Nullable AuthorizationServiceConfiguration serviceConfiguration,
                            @Nullable AuthorizationException ex) {
                        if (ex != null) {
                            Log.w(TAG, "Failed to retrieve configuration for " + idp.name, ex);
                        } else {
                            Log.d(TAG, "configuration retrieved for " + idp.name+ ", proceeding");
                            if (idp.getClientId() == null) {
                                // Do dynamic client registration if no client_id
                                makeRegistrationRequest(serviceConfiguration, idp);
                            } else {
                                makeAuthRequest(serviceConfiguration, idp, new AuthState());

                            }
                        }
                    }
                };

        idp.retrieveConfig(LoginActivity.this, retrieveCallback);
    }
    private AppAuthConfiguration createConfiguration(
            @Nullable BrowserDescriptor browser) {
        AppAuthConfiguration.Builder builder = new AppAuthConfiguration.Builder();

        if (browser != null) {
            builder.setBrowserMatcher(new ExactBrowserMatcher(browser));
        }

        return builder.build();
    }
    private void makeAuthRequest(
            @NonNull AuthorizationServiceConfiguration serviceConfig,
            @NonNull IdentityProvider idp,
            @NonNull AuthState authState) {

        String loginHint = "";

        if (loginHint.isEmpty()) {
            loginHint = null;
        }

        AuthorizationRequest authRequest = new AuthorizationRequest.Builder(
                serviceConfig,
                idp.getClientId(),
                ResponseTypeValues.CODE,
                idp.getRedirectUri())
                .setScope(idp.getScope())
                .setLoginHint(loginHint)
                .build();

        Log.d(TAG, "Making auth request to " + serviceConfig.authorizationEndpoint);

        mAuthService.performAuthorizationRequest(
                authRequest,
                TokenActivity.createPostAuthorizationIntent(
                        this,
                        authRequest,
                        serviceConfig.discoveryDoc,
                        authState),
                mAuthService.createCustomTabsIntentBuilder()
                        .setShowTitle(true)
                        .setToolbarColor(getColorCompat(R.color.accent))
                        .build());
    }

    private void makeRegistrationRequest(
            @NonNull AuthorizationServiceConfiguration serviceConfig,
            @NonNull final IdentityProvider idp) {

        final RegistrationRequest registrationRequest = new RegistrationRequest.Builder(
                serviceConfig,
                Arrays.asList(idp.getRedirectUri()))
                .setTokenEndpointAuthenticationMethod(ClientSecretBasic.NAME)
                .build();

        Log.d(TAG, "Making registration request to " + serviceConfig.registrationEndpoint);
        mAuthService.performRegistrationRequest(
                registrationRequest,
                new AuthorizationService.RegistrationResponseCallback() {
                    @Override
                    public void onRegistrationRequestCompleted(
                            @Nullable RegistrationResponse registrationResponse,
                            @Nullable AuthorizationException ex) {
                        Log.d(TAG, "Registration request complete");
                        if (registrationResponse != null) {
                            idp.setClientId(registrationResponse.clientId);
                            Log.d(TAG, "Registration request complete successfully");
                            // Continue with the authentication
                            makeAuthRequest(registrationResponse.request.configuration, idp,
                                    new AuthState((registrationResponse)));
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuthService.dispose();
    }
    @TargetApi(Build.VERSION_CODES.M)
    @SuppressWarnings("deprecation")
    private int getColorCompat(@ColorRes int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return getColor(color);
        } else {
            return getResources().getColor(color);
        }
    }


}
