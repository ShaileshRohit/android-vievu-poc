package com.vievu.vievusolution.login;

import com.vievu.vievusolution.BaseVievuFragment;

public abstract class BaseLoginFragment extends BaseVievuFragment {

    protected LoginActivity getLoginActivity() {
        return (LoginActivity) getActivity();
    }

    public BaseLoginFragment() {
        this.usesEvents = true;
    }

    protected abstract void onInternetAvailable();
}
