package com.vievu.vievusolution.login;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.MfaTokenData;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.ValidationUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class MainLoginFragment extends BaseLoginFragment {

    @Bind(R.id.loginEditText) EditText loginEditText;
    @Bind(R.id.passwordEditText) EditText passwordEditText;
    @Bind(R.id.accountNameEditText) EditText accountNameEditText;
    @Bind(R.id.loginButton) Button loginButton;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_main, container, false);
        ButterKnife.bind(this, view);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String accountName = preferences.getString(CommonConstants.KEY_ACCOUNT_NAME, "");
        //noinspection ConstantConditions
        if (!accountName.isEmpty()) {
            accountNameEditText.setText(accountName);
        } else {
            view.findViewById(R.id.moreButton).setVisibility(View.GONE);
            accountNameEditText.setVisibility(View.VISIBLE);
        }
        loginButton.setEnabled(getLoginActivity().isInOnlineMode());
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            passwordEditText.setTypeface(Typeface.DEFAULT);
        }
        return view;
    }

    //region ButterKnife bindings

    @OnEditorAction(R.id.passwordEditText)
    public boolean onPasswordOkClicked(int key) {
        if (EditorInfo.IME_ACTION_DONE == key) {
            onLoginClick();
            return true;
        }
        return false;
    }

    @OnClick(R.id.moreButton)
    public void onMoreClick(View moreButton) {
        moreButton.setVisibility(View.GONE);
        accountNameEditText.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.loginButton)
    public void onLoginClick() {
        boolean isValid = ValidationUtil.validateEditText(loginEditText, ValidationUtil::isValidLogin, R.string.error_invalid_login);
        isValid &= ValidationUtil.validateEditText(passwordEditText, ValidationUtil::isValidPassword, R.string.error_invalid_password);
        isValid &= ValidationUtil.validateEditText(accountNameEditText, (value) -> value.length() > 0, R.string.error_invalid_account_name);
        if (isValid) {
            getLoginActivity().checkInternetConnection();
        }
    }

    //endregion

    @Override
    protected void onInternetAvailable() {
        String userName = loginEditText.getText().toString().trim();
        loginEditText.setText(userName);
        String accountName =accountNameEditText.getText().toString().trim();
        accountName = extractHost(accountName);
        accountNameEditText.setText(accountName);
        MfaTokenData mfaTokenData = getLoginActivity().selectMfaTokenData(userName, accountName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        preferences.edit().putString(CommonConstants.KEY_ACCOUNT_NAME,accountName).apply();
        WebClient.getInstance(accountName).authorizeWithPassword(
                userName,
                passwordEditText.getText().toString(),
                mfaTokenData.getMfaToken());
    }

    public void onEvent(OfflineModeChangedEvent event) {
        if (loginButton != null) {
            //loginButton.setEnabled(event.isInOnlineMode());
            loginButton.setEnabled(true);
        }
    }

    private static String extractHost(String accountName) {
        if(accountName.startsWith("https:")){
            accountName = accountName.substring("https://".length());
        }
        if (accountName.startsWith("www.")){
            accountName = accountName.substring("www.".length());
        }
        if(accountName.endsWith("/") || accountName.endsWith("\\")){
            accountName = accountName.substring(0, accountName.length() - 1);
        }
        return accountName;
    }



}
