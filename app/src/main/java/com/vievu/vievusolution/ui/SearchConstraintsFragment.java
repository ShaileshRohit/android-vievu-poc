package com.vievu.vievusolution.ui;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.vievu.vievusolution.BaseVievuFragment;
import com.vievu.vievusolution.filtering.FilterActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.filtering.constraint.FilterConstraintValue;
import com.vievu.vievusolution.util.CollectionsUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;

import butterknife.Bind;
import butterknife.BindDimen;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

public class SearchConstraintsFragment extends BaseVievuFragment implements View.OnTouchListener {

    public static final int TOTAL_SLIDE_DURATION = 800;
    public static final int MIN_FLING_VELOCITY = 700;

    private static class ScrollFlingGestureDetector extends GestureDetector.SimpleOnGestureListener {

        private WeakReference<SearchConstraintsFragment> fragmentReference;

        public ScrollFlingGestureDetector(SearchConstraintsFragment fragment) {
            this.fragmentReference = new WeakReference<>(fragment);
        }

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            SearchConstraintsFragment fragment = fragmentReference.get();
            if (fragment != null && fragment.lastTouchedConstraint != null) {
                if (Math.abs(velocityX) > MIN_FLING_VELOCITY) {
                    final View capture = fragment.lastTouchedConstraint;
                    int width = capture.getWidth();
                    capture.setOnTouchListener(null);
                    fragment.lastTouchedConstraint = null;
                    capture.animate()
                            .translationX(e2.getX() > e1.getX() ? width : -width)
                            .setDuration((long) Math.abs(width * TOTAL_SLIDE_DURATION / velocityX))
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    fragment.removeConstraint(capture);
                                }
                            })
                            .start();
                } else {
                    float offset = e2.getX() - e1.getX();
                    fragment.lastTouchedConstraint.setTranslationX(offset);
                    fragment.completeConstraintViewTranslation(offset);
                }
                return true;
            }
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            SearchConstraintsFragment fragment = fragmentReference.get();
            if (fragment != null && fragment.lastTouchedConstraint != null) {
                fragment.lastTouchedConstraint.setTranslationX(e2.getX() - fragment.touchOriginX);
                return true;
            }
            return false;
        }
    }

    private ArrayList<FilterConstraintValue> constraints = new ArrayList<>();

    private LayoutInflater inflater;
    @BindDimen(R.dimen.common_padding_small)
    int commonPaddingSmall;
    private GestureDetector gestureDetector;
    private View lastTouchedConstraint;
    private float touchOriginX;
    @BindDimen(R.dimen.height_fragment_search_constraints_full)
    int heightFull;
    @BindDimen(R.dimen.height_fragment_search_constraints_collapsed)
    int heightCollapsed;

    @Bind(R.id.constraintContainer) LinearLayout constraintContainer;
    @Bind(R.id.scrollView) ScrollView scrollView;
    @Bind(R.id.edgeButton) View edgeButton;

    public Collection<FilterConstraintValue> getConstraints() {
        return constraints;
    }

    public boolean isExpanded() {
        return getView() != null && getView().getHeight() > heightCollapsed;
    }

    public FilterActivity getFilterActivity() {
        return (FilterActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        View view = inflater.inflate(R.layout.fragment_search_constraints, container, false);
        ButterKnife.bind(this, view);
        gestureDetector = new GestureDetector(getActivity(), new ScrollFlingGestureDetector(this));
        return view;
    }

    @OnTouch(R.id.constraintContainer)
    boolean onConstraintContainerTouch(MotionEvent event) {
        int action = event.getAction();
        if (lastTouchedConstraint != null && (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL)) {
            completeConstraintViewTranslation(event.getX() - touchOriginX);
        }
        gestureDetector.onTouchEvent(event);
        return true;
    }

    private void completeConstraintViewTranslation(float translationX) {
        int width = lastTouchedConstraint.getWidth();
        int resultingTranslation = 0;
        ViewPropertyAnimator animator = lastTouchedConstraint.animate();
        if (Math.abs(translationX) > width / 2) {
            lastTouchedConstraint.setOnTouchListener(null);
            final View capture = lastTouchedConstraint;
            lastTouchedConstraint = null;
            resultingTranslation = translationX > 0 ? width : -width;
            animator.setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    removeConstraint(capture);
                }
            });
        }
        animator
            .translationX(resultingTranslation)
            .setDuration((long) Math.abs(TOTAL_SLIDE_DURATION * (resultingTranslation - translationX) / width))
            .start();
    }

    @OnClick(R.id.clearButton)
    public void onClearClick() {
        constraints.clear();
        constraintContainer.removeAllViews();
        if (isExpanded()) {
            onEdgeClick();
        }
    }

    @OnClick(R.id.edgeButton)
    public void onEdgeClick() {

        boolean isExpanded = this.isExpanded();
        ResizeAnimation resizeAnimation = new ResizeAnimation(getView(),
                isExpanded ? heightCollapsed : heightFull);
        resizeAnimation.setDuration(400);
        resizeAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        resizeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                edgeButton.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                edgeButton.setEnabled(true);
                edgeButton.setActivated(!isExpanded);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        //noinspection ConstantConditions
        getView().startAnimation(resizeAnimation);
    }

    private boolean replaceConstraint(FilterConstraintValue constraint) {
        FilterConstraintValue oldConstraint  = constraint.findDuplicate(constraints);
        if (oldConstraint != null) {
            int position = removeConstraintOnly(oldConstraint);
            constraints.add(position, constraint);
            View view = constraintContainer.getChildAt(position);
            TextView valueTextView = (TextView) view.getTag();
            valueTextView.setText(constraint.getReadableValue());
            int y = scrollView.getScrollY();
            int viewTop = view.getTop();
            view.post(() -> scrollView.scrollTo(0, y > viewTop ? viewTop : view.getBottom()));
            return true;
        }
        return false;
    }

    private int removeConstraintOnly(FilterConstraintValue constraint) {
        if (constraint != null) {
            int position = constraints.indexOf(constraint);
            if (position > -1) {
                constraints.remove(position);
            }
            return position;
        }
        return -1;
    }

    public void addConstraint(FilterConstraintValue constraint) {
        if (!replaceConstraint(constraint)) {
            int insertPosition = getInsertPosition(constraint.getId());
            constraints.add(insertPosition, constraint);
            View view = inflater.inflate(R.layout.view_part_constraint, constraintContainer, false);
            TextView nameTextView = ButterKnife.findById(view, R.id.nameTextView);
            nameTextView.setText(getString(R.string.template_search_constraint_name, getString(constraint.getReadableName())));
            TextView valueTextView = ButterKnife.findById(view, R.id.valueTextView);
            valueTextView.setText(constraint.getReadableValue());
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
            layoutParams.setMargins(0, commonPaddingSmall, 0, commonPaddingSmall);
            view.setLayoutParams(layoutParams);
            view.setTag(valueTextView);
            view.setOnTouchListener(this);
            constraintContainer.addView(view, insertPosition);
            view.post(() -> scrollView.scrollTo(0, view.getTop()));
            if (constraints.size() == 1 && !isExpanded()) {
                onEdgeClick();
            }
        }
    }

    public void removeConstraint(View constraintView) {
        int position = constraintContainer.indexOfChild(constraintView);
        if (position > -1) {
            constraintContainer.removeViewAt(position);
            constraints.remove(position);
            if (constraints.isEmpty() && isExpanded()) {
                onEdgeClick();
            }
        }
    }

    private int getInsertPosition(int idToInsert) {
        int[] orderedIds = getFilterActivity().getFilterBundle().getOrderedIds();
        int insertionIndex = CollectionsUtil.indexOf(orderedIds, idToInsert);
        int insertPosition = 0;
        int currentIndex = -1;
        int currentId = 0;
        FilterConstraintValue constraint;
        for (;insertPosition < constraints.size(); insertPosition++) {
            constraint = constraints.get(insertPosition);
            if (currentId != constraint.getId()) {
                currentId = constraint.getId();
                currentIndex = CollectionsUtil.indexOf(orderedIds, currentId);
            }
            if (currentIndex > insertionIndex) {
                break;
            }
        }
        return insertPosition;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            lastTouchedConstraint = view;
            touchOriginX = event.getX();
        }
        return false;
    }
}
