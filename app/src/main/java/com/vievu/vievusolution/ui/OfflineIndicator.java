package com.vievu.vievusolution.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vievu.vievusolution.EventBusActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


public class  OfflineIndicator extends RelativeLayout {

    public OfflineIndicator(Context context) {
        super(context);
    }

    public OfflineIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OfflineIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private boolean isInOnlineMode = true;

    @Bind(R.id.messageTextView) TextView messageTextView;
    @Bind(R.id.content) RelativeLayout content;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (getContext() instanceof EventBusActivity) {
            EventBusActivity activity = (EventBusActivity) getContext();
            setOnlineMode(activity.isInOnlineMode());
        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        EventBus.getDefault().unregister(this);
        super.onDetachedFromWindow();
    }

    public void onEventMainThread(OfflineModeChangedEvent event) {
        setOnlineMode(event.isInOnlineMode());
    }

    @Override
    protected void onFinishInflate() {
        int childCount = getChildCount();
        View[] children = new View[childCount];
        for (int i = 0; i < childCount; i++) {
            children[i] = getChildAt(i);
        }
        this.detachAllViewsFromParent();
        final View template = LayoutInflater.from(getContext())
                .inflate(R.layout.view_offline_indicator, this, true);
        ButterKnife.bind(this, template);
        setOnlineMode(isInOnlineMode);
        for (int i = 0; i < childCount; i++) {
            content.addView(children[i]);
        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onFinishInflate();
    }

    @Override
    public void addView(View child) {
        content.addView(child);
    }

    public void setOnlineMode(boolean isInOnlineMode) {
        this.isInOnlineMode = isInOnlineMode;
        if (messageTextView != null) {
            messageTextView.setVisibility(isInOnlineMode ? GONE : VISIBLE);
        }
    }
}
