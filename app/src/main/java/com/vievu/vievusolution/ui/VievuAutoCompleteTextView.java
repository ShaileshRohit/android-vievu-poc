package com.vievu.vievusolution.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;

import com.vievu.vievusolution.R;

public class VievuAutoCompleteTextView
        extends AutoCompleteTextView
        implements View.OnTouchListener, View.OnFocusChangeListener, TextWatcher {

    protected ProgressBar progressBar;
    protected Drawable xD;
    protected Runnable delayedFiltering;

    //region Initialization

    public VievuAutoCompleteTextView(Context context) {
        super(context);
        init(context, null);
    }

    public VievuAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public VievuAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs) {
        xD = getCompoundDrawables()[2];
        if (xD == null) {
            setActionDrawable(0);
        } else {
            xD.setBounds(0, 0, xD.getIntrinsicWidth(), xD.getIntrinsicHeight());
        }
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        setActionDrawableVisible(false);
        addTextChangedListener(this);
    }

    //endregion

    //region Progress bar management

    public void setProgressIndicator(ProgressBar view) {
        progressBar = view;
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
        progressBar.setVisibility(VISIBLE);
        if (delayedFiltering != null) {
            getHandler().removeCallbacks(delayedFiltering);
        }
        delayedFiltering = () -> super.performFiltering(text, keyCode);
        getHandler().postDelayed(delayedFiltering, 1000);
    }

    @Override
    public void onFilterComplete(int count) {
        progressBar.setVisibility(GONE);
        super.onFilterComplete(count);
    }

    @Override
    public void setEnabled(boolean enabled) {
        progressBar.setVisibility(enabled ? GONE : VISIBLE);
        super.setEnabled(enabled);
    }

    //endregion

    //region Action drawable management

    protected int getDefaultActionDrawableId() {
        return R.drawable.ic_action_remove;
    }

    public void setActionDrawable(int drawableId) {
        //noinspection deprecation
        xD = getResources().getDrawable(drawableId == 0 ? getDefaultActionDrawableId() : drawableId);
        assert xD != null;
        xD.setBounds(0, 0, xD.getIntrinsicWidth(), xD.getIntrinsicHeight());
    }

    public void setActionDrawableVisible(boolean isVisible) {
        Drawable x = isVisible ? xD : null;
        setCompoundDrawables(getCompoundDrawables()[0],
                getCompoundDrawables()[1], x, getCompoundDrawables()[3]);
    }

    //endregion

    //region View callbacks listeners

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        setActionDrawableVisible(hasFocus && getText().length() > 0);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (isFocused()) {
            setActionDrawableVisible(s.length() > 0);
        }
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (getCompoundDrawables()[2] != null) {
            boolean tappedX = event.getX() > (getWidth() - getPaddingRight() - xD.getIntrinsicWidth());
            if (tappedX) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    onActionDrawableClicked();
                }
                return true;
            }
        }
        return false;
    }

    //endregion

    protected void onActionDrawableClicked() {
        setText(null);
    }
}