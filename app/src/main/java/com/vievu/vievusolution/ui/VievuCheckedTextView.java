package com.vievu.vievusolution.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

import java.lang.ref.WeakReference;


public class VievuCheckedTextView extends CheckedTextView {

    public interface OnCheckedChangedListener {
        void onCheckedChanged(VievuCheckedTextView view);
    }

    public VievuCheckedTextView(Context context) {
        super(context);
    }

    public VievuCheckedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VievuCheckedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private WeakReference<OnCheckedChangedListener> onCheckedChangedListenerReference;

    public OnCheckedChangedListener getOnCheckedChangedListener() {
        return onCheckedChangedListenerReference.get();
    }

    public void setOnCheckedChangedListener(OnCheckedChangedListener onCheckedChangedListener) {
        this.onCheckedChangedListenerReference = new WeakReference<>(onCheckedChangedListener);
    }

    @Override
    public void setChecked(boolean isChecked) {
        if (this.isChecked() != isChecked) {
            super.setChecked(isChecked);
            if (onCheckedChangedListenerReference != null) {
                OnCheckedChangedListener listener = onCheckedChangedListenerReference.get();
                if (listener != null) {
                    listener.onCheckedChanged(this);
                }
            }
        }
    }
}
