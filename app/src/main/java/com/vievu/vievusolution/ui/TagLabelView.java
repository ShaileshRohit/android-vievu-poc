package com.vievu.vievusolution.ui;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.bean.TaggedBean;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TagLabelView extends FrameLayout implements View.OnClickListener {

    public static void generateLabels(ViewGroup container, TaggedBean source) {
        generateLabels(container, source, null);
    }

    public static void generateLabels(ViewGroup container, TaggedBean source, View.OnClickListener onDeleteClickListener) {
        TagLabelView tagView;
        for (Tag tag : source.getTags()) {
            tagView = new TagLabelView(container.getContext(), null);
            tagView.getTagTextView().setText(tag.getTagName());
            if (onDeleteClickListener != null) {
                tagView.setOnDeleteClickListener(onDeleteClickListener);
            }
            container.addView(tagView);
        }
    }

    @Bind(R.id.tagTextView) TextView tagTextView;
    @Bind(R.id.deleteImageButton) ImageButton deleteImageButton;

    protected OnClickListener onDeleteClickListener;

    public TextView getTagTextView() {
        return tagTextView;
    }

    public ImageButton getDeleteImageButton() {
        return deleteImageButton;
    }

    public TagLabelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.view_part_tag_label, this);
        ButterKnife.bind(this);
    }

    @Override
    public void setBackgroundResource(@DrawableRes int backgroundResource) {
        getChildAt(0).setBackgroundResource(backgroundResource);
    }

    public void showDeleteButton() {
        deleteImageButton.setVisibility(VISIBLE);
    }

    public void setOnDeleteClickListener(OnClickListener listener) {
        showDeleteButton();
        this.onDeleteClickListener = listener;
        deleteImageButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (onDeleteClickListener != null) {
            onDeleteClickListener.onClick(this);
        }
    }
}
