package com.vievu.vievusolution.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.vievu.vievusolution.R;

public class ClearableEditText extends ActionDrawableEditText {

    public ClearableEditText(Context context) {
        super(context);
    }

    public ClearableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClearableEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void init(Context context, AttributeSet attrs) {
        super.init(context, attrs);
        this.isActionDrawableOnlyTouchable = true;
    }

    @Override
    protected void onActionDrawableClicked() {
        setText(null);
        super.onActionDrawableClicked();
    }

    @Override
    protected int getDefaultActionDrawableId() {
        return R.drawable.ic_action_remove;
    }
}
