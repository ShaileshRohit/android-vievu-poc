package com.vievu.vievusolution.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.vievu.vievusolution.R;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnCheckedChanged;

public class VievuSwitch extends RelativeLayout {

    @Bind(R.id.headerTextView) TextView headerTextView;
    @Bind(android.R.id.toggle) Switch toggleSwitch;

    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener;

    public VievuSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.view_part_vievu_switch, this);
        ButterKnife.bind(this);
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.VievuSwitch, 0, 0);
            try {
                headerTextView.setText(array.getString(R.styleable.VievuSwitch_header));
                boolean isChecked = array.getBoolean(R.styleable.VievuSwitch_checked, false);
                if (toggleSwitch.isChecked() == isChecked) {
                    // Internal callback won't be called in this case and text value won't be set
                    onCheckedChanged(isChecked);
                }
                toggleSwitch.setChecked(isChecked);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                array.recycle();
            }
        }
    }

    public TextView getHeaderTextView() {
        return headerTextView;
    }

    public Switch getToggleSwitch() {
        return toggleSwitch;
    }

    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener listener) {
        this.onCheckedChangeListener = listener;
    }

    @OnCheckedChanged(android.R.id.toggle)
    void onCheckedChanged(boolean isChecked) {
        toggleSwitch.setText(isChecked ? R.string.text_yes : R.string.text_no);
        if (this.onCheckedChangeListener != null) {
            this.onCheckedChangeListener.onCheckedChanged(toggleSwitch, isChecked);
        }
    }
}
