package com.vievu.vievusolution.ui;

import android.app.Activity;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vievu.vievusolution.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CommonListMixin {

    public static CommonListMixin create(View view) {
        CommonListMixin decorator = new CommonListMixin();
        ButterKnife.bind(decorator, view);
        decorator.listView.setEmptyView(ButterKnife.findById(view, android.R.id.empty));
        return decorator;
    }

    public static CommonListMixin create(Activity activity) {
        CommonListMixin decorator = new CommonListMixin();
        ButterKnife.bind(decorator, activity);
        decorator.listView.setEmptyView(ButterKnife.findById(activity, android.R.id.empty));
        return decorator;
    }

    @Bind(android.R.id.list) ListView listView;
    @Bind(R.id.progressEmptyView) ProgressBar progressEmptyView;
    @Bind(R.id.messageEmptyView) TextView messageEmptyView;

    private CommonListMixin() {}

    public ListView getListView() {
        return listView;
    }

    public void setEmptyMessageView() {
        showEmptyView(messageEmptyView);
    }

    public void setEmptyProgressView() {
        showEmptyView(progressEmptyView);
    }

    public void unbind() {
        ButterKnife.unbind(this);
    }

    public void setVisible(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;
        listView.setVisibility(visibility);
        ((View) messageEmptyView.getParent()).setVisibility(visibility);
    }

    protected void showEmptyView(View emptyView) {
        if (messageEmptyView != null) {
            if (messageEmptyView != emptyView) {
                messageEmptyView.setVisibility(View.GONE);
                progressEmptyView.setVisibility(View.VISIBLE);
            } else {
                messageEmptyView.setVisibility(View.VISIBLE);
                progressEmptyView.setVisibility(View.GONE);
            }
        }
    }
}
