package com.vievu.vievusolution.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.widget.LinearLayout;


public class SlidingLinearLayout extends LinearLayout {

    public SlidingLinearLayout(Context context) {
        super(context);
    }

    public SlidingLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public float getXFraction()
    {
        Point size = new Point();
        ((Activity)getContext()).getWindowManager().getDefaultDisplay().getSize(size);
        return (size.x == 0) ? 0 : getX() / (float) size.x;
    }

    public void setXFraction(float xFraction) {
        Point size = new Point();
        ((Activity)getContext()).getWindowManager().getDefaultDisplay().getSize(size);
        //noinspection ResourceType
        setX((size.x > 0) ? (xFraction * size.x) : 0);
    }
}