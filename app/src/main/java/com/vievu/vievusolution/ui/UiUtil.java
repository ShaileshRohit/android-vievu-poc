package com.vievu.vievusolution.ui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.widget.ProgressBar;

public class UiUtil {

    public static void colorizeDrawable(@NonNull Context context, @NonNull Drawable drawable, @ColorRes int colorId) {
        //noinspection deprecation
        int color = context.getResources().getColor(colorId);
        colorizeDrawable(drawable, color);
    }

    public static void colorizeDrawable(@NonNull Drawable drawable, @ColorInt int color) {
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    public static void colorizeProgressBar(@NonNull ProgressBar progressBar, @ColorRes int colorId) {
        //noinspection deprecation
        int color = progressBar.getContext().getResources().getColor(colorId);
        colorizeDrawable(progressBar.getProgressDrawable(), color);
        colorizeDrawable(progressBar.getIndeterminateDrawable(), color);
    }
}
