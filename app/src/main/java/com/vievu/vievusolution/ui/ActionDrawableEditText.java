package com.vievu.vievusolution.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.vievu.vievusolution.R;

public class ActionDrawableEditText extends EditText implements View.OnTouchListener,
        View.OnFocusChangeListener, TextWatcher {

    public interface OnActionDrawableClickListener {

        void onActionDrawableClicked(ActionDrawableEditText view);
    }

    protected boolean isActionDrawableAlwaysVisible;
    protected boolean isActionDrawableOnlyTouchable;
    protected Drawable xD;
    protected Context mContext;

    private OnActionDrawableClickListener onActionDrawableClickListener;
    private OnTouchListener onTouchListener;
    private OnFocusChangeListener onFocusChangeListener;

    public ActionDrawableEditText(Context context) {
        super(context);
        init(context, null);
    }

    public ActionDrawableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ActionDrawableEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public void setOnActionDrawableClickListener(OnActionDrawableClickListener onActionDrawableClickListener) {
        this.onActionDrawableClickListener = onActionDrawableClickListener;
    }

    @Override
    public void setOnTouchListener(OnTouchListener listener) {
        this.onTouchListener = listener;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener listener) {
        this.onFocusChangeListener = listener;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (getCompoundDrawables()[2] != null
            && event.getAction() == MotionEvent.ACTION_UP
            && (!isActionDrawableOnlyTouchable
                || event.getX() > (getWidth() - getPaddingRight() - xD.getIntrinsicWidth()))) {

            onActionDrawableClicked();
            return true;
        }
        return onTouchListener != null && onTouchListener.onTouch(view, event);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!isActionDrawableAlwaysVisible) {
            setActionDrawableVisible(hasFocus && getText().length() > 0);
        }
        if (onFocusChangeListener != null) {
            onFocusChangeListener.onFocusChange(v, hasFocus);
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (isFocused()) {
            setActionDrawableVisible(s.length() > 0);
        }
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    protected void init(Context context, AttributeSet attrs) {
        mContext=context;
        xD = getCompoundDrawables()[2];
        if (xD == null) {
            setActionDrawable(0);
        } else {
            xD.setBounds(0, 0, xD.getIntrinsicWidth(), xD.getIntrinsicHeight());
        }
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ActionDrawableEditText, 0, 0);
            try {
                isActionDrawableAlwaysVisible = array.getBoolean(
                        R.styleable.ActionDrawableEditText_actionDrawableAlwaysVisible, false);
                isActionDrawableOnlyTouchable = array.getBoolean(
                        R.styleable.ActionDrawableEditText_actionDrawableOnlyTouchable, false);
            } finally {
                array.recycle();
            }
        }
        if (!isActionDrawableAlwaysVisible) {
            setActionDrawableVisible(false);
            addTextChangedListener(this);
        }
    }

    protected void onActionDrawableClicked() {
        if (onActionDrawableClickListener != null) {
            onActionDrawableClickListener.onActionDrawableClicked(this);
        }
    }

    @DrawableRes
    protected int getDefaultActionDrawableId() {
        return R.drawable.ic_action_remove;
    }

    public void setActionDrawable(@DrawableRes int drawableId) {
        xD =getResources().getDrawable(drawableId == 0 ? getDefaultActionDrawableId() : drawableId);
        assert xD != null;
        xD.setBounds(0, 0, xD.getIntrinsicWidth(), xD.getIntrinsicHeight());
        setRightCompoundDrawable(xD);
    }

    public void setActionDrawableVisible(boolean isVisible) {
        Drawable drawable = isVisible ? xD : null;
        setRightCompoundDrawable(drawable);
    }

    private void setRightCompoundDrawable(Drawable drawable) {
        Drawable[] compoundDrawables = getCompoundDrawables();
        setCompoundDrawables(compoundDrawables[0], compoundDrawables[1], drawable, compoundDrawables[3]);
    }
}