package com.vievu.vievusolution.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MjpegView extends SurfaceView implements SurfaceHolder.Callback {

    public final static int SIZE_STANDARD   = 1;
    public final static int SIZE_BEST_FIT   = 4;
    public final static int SIZE_FULLSCREEN = 8;

    private boolean surfaceDone = false;
    private boolean displayModeChanged = false;
    private int dispWidth;
    private int dispHeight;
    private int displayMode;

    private SurfaceHolder surfaceHolder;

    private Rect destRect;

    private Paint paint = new Paint();

    public void setDisplayMode(int displayMode) {
        if (this.displayMode != displayMode) {
            this.displayMode = displayMode;
            displayModeChanged = true;
        }
    }

    public MjpegView(Context context, AttributeSet attrs) {
        super(context, attrs);
        surfaceHolder = getHolder();
        assert surfaceHolder != null;
        surfaceHolder.addCallback(this);
        setFocusable(true);
        displayMode = MjpegView.SIZE_STANDARD;
        dispWidth = getWidth();
        dispHeight = getHeight();
    }

    private Rect destRect(int bmw, int bmh) {
        int tempx;
        int tempy;
        switch (displayMode) {
            case MjpegView.SIZE_STANDARD :
                tempx = (dispWidth / 2) - (bmw / 2);
                tempy = (dispHeight / 2) - (bmh / 2);
                return new Rect(tempx, tempy, bmw + tempx, bmh + tempy);
            case MjpegView.SIZE_BEST_FIT :
                float bmasp = (float) bmw / (float) bmh;
                bmw = dispWidth;
                bmh = (int) (dispWidth / bmasp);
                if (bmh > dispHeight) {
                    bmh = dispHeight;
                    bmw = (int) (dispHeight * bmasp);
                }
                tempx = (dispWidth / 2) - (bmw / 2);
                tempy = (dispHeight / 2) - (bmh / 2);
                return new Rect(tempx, tempy, bmw + tempx, bmh + tempy);
            case MjpegView.SIZE_FULLSCREEN :
                return new Rect(0, 0, dispWidth, dispHeight);
        }
        return null;
    }


    public void drawBitmap(Bitmap bitmap) {
        if(surfaceDone && bitmap != null) {
            if (displayModeChanged || destRect == null || destRect.isEmpty()) {
                destRect = destRect(bitmap.getWidth(), bitmap.getHeight());
                displayModeChanged = false;
            }
            Canvas canvas = surfaceHolder.lockCanvas();
            if (canvas != null) {
                canvas.drawColor(Color.BLACK);
                canvas.drawBitmap(bitmap, null, destRect, paint);
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    private void setSurfaceSize(int width, int height) {
        dispWidth = width;
        dispHeight = height;
        displayModeChanged = true;
    }

    public void surfaceChanged(SurfaceHolder holder, int f, int w, int h) {
        setSurfaceSize(w, h);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        surfaceDone = false;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        surfaceDone = true;
        setWillNotDraw(false);
    }
}
