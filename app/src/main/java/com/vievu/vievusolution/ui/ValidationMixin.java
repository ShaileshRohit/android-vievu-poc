package com.vievu.vievusolution.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.vievu.vievusolution.R;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.ButterKnife;

public class ValidationMixin {

    private static ValidationMixin cachedInstance;

    public static ValidationMixin create(Activity activity) {
        if (cachedInstance == null) {
            cachedInstance = new ValidationMixin(activity);
        }
        return cachedInstance;
    }

    @BindColor(R.color.accent)
    int accentColor;
    @BindString(R.string.html_template_required_property)
    String propertyHeaderTemplate;

    private Context context;

    private ValidationMixin(Activity activity) {
        ButterKnife.bind(this, activity);
        accentColor &= 0x00FFFFFF;
        this.context = activity.getApplicationContext();
    }

    public void setColorHeader(View rootView, int headerViewId, int headerStringId) {
        TextView textView = ButterKnife.findById(rootView, headerViewId);

            textView.setText(Html.fromHtml(String.format(
                    propertyHeaderTemplate, accentColor, context.getString(headerStringId))));



    }
}
