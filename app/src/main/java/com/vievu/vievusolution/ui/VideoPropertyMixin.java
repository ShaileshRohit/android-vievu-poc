package com.vievu.vievusolution.ui;

import android.content.Context;
import android.support.annotation.IdRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.ptpip.dataset.ObjectInfo;
import com.vievu.vievusolution.util.DateUtil;

import java.util.Date;

import butterknife.ButterKnife;

public class VideoPropertyMixin {

    public static Builder with(ViewGroup container) {
        return new Builder(container);
    }

    public static class Builder {

        private String trueValue;
        private String falseValue;

        private ViewGroup container;
        private LayoutInflater inflater;

        private int nextRowId;

        private Builder(ViewGroup container) {
            this.container = container;
            Context context = container.getContext();
            this.inflater = LayoutInflater.from(context);
            this.trueValue = context.getString(R.string.text_yes);
            this.falseValue = context.getString(R.string.text_no);
        }

        public Builder addVideoPropertyRow(int propertyNameId, boolean propertyValue) {
            return addVideoPropertyRow(propertyNameId, propertyValue ? trueValue : falseValue);
        }

        public Builder addVideoPropertyRow(int propertyNameId, Date propertyValue) {
            return addVideoPropertyRow(propertyNameId,
                propertyValue != null ? DateUtil.Formatters.DATE_TIME.format(propertyValue) : "");
        }

        public Builder addVideoPropertyRow(int propertyNameId, Object propertyValue) {
            return addVideoPropertyRow(propertyNameId, propertyValue != null ? propertyValue.toString() : "");
        }

        public Builder addVideoPropertyRow(int propertyNameId, String propertyValue) {
            View view = inflater.inflate(R.layout.view_part_property_row, container, false);
            if (nextRowId != 0) {
                view.setId(nextRowId);
                nextRowId = 0;
            }
            ButterKnife.<TextView>findById(view, R.id.nameTextView).setText(propertyNameId);
            if (propertyValue != null) {
                ButterKnife.<TextView>findById(view, R.id.valueTextView).setText(propertyValue);
            }
            container.addView(view);
            return this;
        }

        public Builder withNextId(@IdRes int nextRowId) {
            this.nextRowId = nextRowId;
            return this;
        }
    }


}
