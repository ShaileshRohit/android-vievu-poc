package com.vievu.vievusolution;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.vievu.vievusolution.net.camera.ThumbnailLoader;
import com.vievu.vievusolution.ptpip.dataset.CameraStatus;

import de.greenrobot.event.EventBus;

public class VievuApplication  extends Application {
    private static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        ThumbnailLoader.init(this);

        getApplicationContext().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && !networkInfo.isConnectedOrConnecting()) {
                    Log.d("GlobalWiFiReceiver", "Network disconnected");
                    CameraClient.getInstance().disconnectImmediately();
                    EventBus.getDefault().post(new CameraStatus());
                }
            }
        }, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
    }
    public static  Context getContext(){
        return  context;
    }
}