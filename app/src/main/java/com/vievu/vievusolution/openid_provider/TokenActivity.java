/*
 * Copyright 2015 The AppAuth for Android Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vievu.vievusolution.openid_provider;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceDiscovery;
import net.openid.appauth.ClientAuthentication;
import net.openid.appauth.TokenRequest;
import net.openid.appauth.TokenResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.MainActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.CaseStatus;
import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.bean.Permissions;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.data.AssignedCamerasAsyncRepository;
import com.vievu.vievusolution.data.CaseStatusesAsyncRepository;
import com.vievu.vievusolution.data.CategoriesAsyncRepository;
import com.vievu.vievusolution.data.TagsAsyncRepository;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.event.LoginEvent;
import com.vievu.vievusolution.login.LoginActivity;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.LogoutUtil;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A sample activity to serve as a client to the Native Oauth library.
 */
public class TokenActivity extends Activity {
    private static final String TAG = "TokenActivity";
    private static final String KEY_AUTH_STATE = "authState";
    private static final String KEY_USER_INFO = "userInfo";
    private static final String EXTRA_AUTH_SERVICE_DISCOVERY = "authServiceDiscovery";
    private static final String EXTRA_AUTH_STATE = "authState";
    private static final int BUFFER_SIZE = 1024;
    private AuthState mAuthState;
    private AuthorizationService mAuthService;
    private JSONObject mUserInfoJson;
    private  ProgressDialog progressDialog=null;
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.openid_token);

        mAuthService = new AuthorizationService(this);

        preferences= PreferenceManager.getDefaultSharedPreferences(TokenActivity.this);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_AUTH_STATE)) {
                try {
                    mAuthState = AuthState.jsonDeserialize(
                            savedInstanceState.getString(KEY_AUTH_STATE));
                    preferences.edit().putString("AuthState",new Gson().toJson(mAuthState)).apply();
                } catch (JSONException ex) {
                    Log.e(TAG, "Malformed authorization JSON saved", ex);
                }
            }

            if (savedInstanceState.containsKey(KEY_USER_INFO)) {
                try {
                    mUserInfoJson = new JSONObject(savedInstanceState.getString(KEY_USER_INFO));
                } catch (JSONException ex) {
                    Log.e(TAG, "Failed to parse saved user info JSON", ex);
                }
            }
        }

        if (mAuthState == null) {
            mAuthState = getAuthStateFromIntent(getIntent());
            AuthorizationResponse response = AuthorizationResponse.fromIntent(getIntent());
            AuthorizationException ex = AuthorizationException.fromIntent(getIntent());
            preferences.edit().putString("AuthState",new Gson().toJson(mAuthState)).apply();
            preferences.edit().putString("AuthorizationResponse",new Gson().toJson(response)).apply();
            preferences.edit().putString("AuthorizationException",new Gson().toJson(ex)).apply();
            mAuthState.update(response, ex);

            WebClient.mAuthState=mAuthState;
            WebClient.response=AuthorizationResponse.fromIntent(getIntent());
            WebClient.ex=AuthorizationException.fromIntent(getIntent());

            if (response != null) {
                Log.d(TAG, "Received AuthorizationResponse.");
                //showSnackbar(R.string.exchange_notification);
                exchangeAuthorizationCode(response);
            } else {
                Log.i(TAG, "Authorization failed: " + ex);
                //showSnackbar(R.string.authorization_failed);
            }
        }

        refreshUi();
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        if (mAuthState != null) {
            state.putString(KEY_AUTH_STATE, mAuthState.jsonSerializeString());
            SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(TokenActivity.this);
            preferences.edit().putString(KEY_AUTH_STATE,mAuthState.jsonSerializeString()).apply();
        }

        if (mUserInfoJson != null) {
            state.putString(KEY_USER_INFO, mUserInfoJson.toString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressbar();
        mAuthService.dispose();

    }

    private void receivedTokenResponse(
            @Nullable TokenResponse tokenResponse,
            @Nullable AuthorizationException authException) {
        Log.d(TAG, "Token request complete");
        mAuthState.update(tokenResponse, authException);
        /*showSnackbar((tokenResponse != null)
                ? R.string.exchange_complete
                : R.string.refresh_failed);*/
        refreshUi();
    }

    private void refreshUi() {

        TextView accessTokenInfoView = (TextView) findViewById(R.id.access_token_info);


        if (mAuthState.isAuthorized()) {

            if (mAuthState.getAccessToken() == null) {
                accessTokenInfoView.setText(R.string.no_access_token_returned);
            } else {

                preferences.edit().putString("TokenAccessed","1").apply();
                preferences.edit().putString(CommonConstants.KEY_ID_TOKEN,mAuthState.getIdToken()).apply();
                preferences.edit().putString(CommonConstants.KEY_AUTH_TOKEN,mAuthState.getIdToken()).apply();

                Long expiresAt = mAuthState.getAccessTokenExpirationTime();
                //Long time= System.currentTimeMillis() + expiresAt * 1000;
                Log.d(TAG,"Time expire: "+expiresAt);
                preferences.edit().putLong(LogoutUtil.KEY_TOKEN_EXPIRATION_TIME,expiresAt).apply();

                String expiryStr;
                if (expiresAt == null) {
                    expiryStr = getResources().getString(R.string.unknown_expiry);
                } else {
                    expiryStr = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)
                            .format(new Date(expiresAt));
                }
                String tokenInfo = String.format("Access token"+mAuthState.getAccessToken()+"\n,"+getResources().getString(R.string.access_token_expires_at),
                        expiryStr);
                Log.d("Access token","tokenInfo: "+tokenInfo);
                Log.d("ID token","tokenInfo: "+mAuthState.getIdToken());
                showProgressbar();

                WebClient.getInstances(TokenActivity.this).request().getPermissoin(new Callback<UserDataRepository>() {

                    @Override
                    public void success(UserDataRepository mUserDataRepository, Response response) {
                        Log.d("UserName","UserName: "+ mUserDataRepository.getUser().getUserName());
                        //UserDataRepository.getInstance().cacheCopy(TokenActivity.this,mUserDataRepository);
                        //preferences.edit().putString(CommonConstants.KEY_USER_PERMISSION,new Gson().toJson(mUserDataRepository)).apply();
                        setPermissionData(mUserDataRepository);
                        WebClient.isIDPDONE();
                        WebClient.getInstances(TokenActivity.this);
                        callTagAPI();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        LogoutUtil.logOut(TokenActivity.this);
                    }

                });

                /*Long expiresAt = mAuthState.getAccessTokenExpirationTime();
                String expiryStr;
                if (expiresAt == null) {
                    expiryStr = getResources().getString(R.string.unknown_expiry);
                } else {
                    expiryStr = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)
                            .format(new Date(expiresAt));
                }
                Log.d("Access token","Access token: "+mAuthState.getAccessToken());
                Log.d("ID token","ID token: "+mAuthState.getIdToken());

                String tokenInfo = String.format("Access token"+mAuthState.getAccessToken()+"\n,"+
                        getResources().getString(R.string.access_token_expires_at),
                        expiryStr);
                accessTokenInfoView.setText(tokenInfo);
                startActivity(new Intent(TokenActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();*/
            }
        }





        /*refreshTokenButton.setVisibility(mAuthState.getRefreshToken() != null
                ? View.VISIBLE
                : View.GONE);
        refreshTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshAccessToken();
            }
        });*/

        /*Button viewProfileButton = (Button) findViewById(R.id.view_profile);*/

        //AuthorizationServiceDiscovery discoveryDoc = getDiscoveryDocFromIntent(getIntent());
       /* if (!mAuthState.isAuthorized()
                || discoveryDoc == null
                || discoveryDoc.getUserinfoEndpoint() == null) {
            viewProfileButton.setVisibility(View.GONE);
        } else {
            viewProfileButton.setVisibility(View.VISIBLE);
            viewProfileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            fetchUserInfo();
                            return null;
                        }
                    }.execute();
                }
            });
        }*/

       /* View userInfoCard = findViewById(R.id.userinfo_card);
        if (mUserInfoJson == null) {
            userInfoCard.setVisibility(View.INVISIBLE);
        } else {
            try {
                String name = "???";
                if (mUserInfoJson.has("name")) {
                    name = mUserInfoJson.getString("name");
                }
                ((TextView) findViewById(R.id.userinfo_name)).setText(name);

                *//*if (mUserInfoJson.has("picture")) {
                    Glide.with(TokenActivity.this)
                            .load(Uri.parse(mUserInfoJson.getString("picture")))
                            .fitCenter()
                            .into((ImageView) findViewById(R.id.userinfo_profile));
                }*//*

                ((TextView) findViewById(R.id.userinfo_json)).setText(mUserInfoJson.toString(2));

                userInfoCard.setVisibility(View.VISIBLE);
            } catch (JSONException ex) {
                Log.e(TAG, "Failed to read userinfo JSON", ex);
            }
        }*/
    }
    public void setPermissionData(UserDataRepository userPermissoinModel){
        UserDataRepository model=UserDataRepository.getInstance();
        model.setUser(userPermissoinModel.getUser());
        model.setPermissions(userPermissoinModel.getPermissions());
        model.cacheCopy(this);


    }
    public void callTagAPI()
    {

        TagsAsyncRepository.getInstance().getDataAsync(this::onTagsLoaded);
    }
    private void refreshAccessToken() {
        performTokenRequest(mAuthState.createTokenRefreshRequest());
    }

    private void exchangeAuthorizationCode(AuthorizationResponse authorizationResponse) {
        performTokenRequest(authorizationResponse.createTokenExchangeRequest());
    }

    private void performTokenRequest(TokenRequest request) {
        ClientAuthentication clientAuthentication;
        try {
            clientAuthentication = mAuthState.getClientAuthentication();
        } catch (ClientAuthentication.UnsupportedAuthenticationMethod ex) {
            Log.d(TAG, "Token request cannot be made, client authentication for the token "
                            + "endpoint could not be constructed (%s)", ex);
            return;
        }

        mAuthService.performTokenRequest(
                request,
                clientAuthentication,
                new AuthorizationService.TokenResponseCallback() {
                    @Override
                    public void onTokenRequestCompleted(
                            @Nullable TokenResponse tokenResponse,
                            @Nullable AuthorizationException ex) {
                        receivedTokenResponse(tokenResponse, ex);
                    }
                });
    }

    private void fetchUserInfo() {
        if (mAuthState.getAuthorizationServiceConfiguration() == null) {
            Log.e(TAG, "Cannot make userInfo request without service configuration");
        }

        mAuthState.performActionWithFreshTokens(mAuthService, new AuthState.AuthStateAction() {
            @Override
            public void execute(String accessToken, String idToken, AuthorizationException ex) {
                if (ex != null) {
                    Log.e(TAG, "Token refresh failed when fetching user info");
                    return;
                }

                AuthorizationServiceDiscovery discoveryDoc = getDiscoveryDocFromIntent(getIntent());
                if (discoveryDoc == null) {
                    throw new IllegalStateException("no available discovery doc");
                }

                URL userInfoEndpoint;
                try {
                    userInfoEndpoint = new URL(discoveryDoc.getUserinfoEndpoint().toString());
                } catch (MalformedURLException urlEx) {
                    Log.e(TAG, "Failed to construct user info endpoint URL", urlEx);
                    return;
                }

                InputStream userInfoResponse = null;
                try {
                    HttpURLConnection conn = (HttpURLConnection) userInfoEndpoint.openConnection();
                    conn.setRequestProperty("Authorization", "Bearer " + accessToken);
                    conn.setInstanceFollowRedirects(false);
                    userInfoResponse = conn.getInputStream();
                    String response = readStream(userInfoResponse);
                    updateUserInfo(new JSONObject(response));
                } catch (IOException ioEx) {
                    Log.e(TAG, "Network error when querying userinfo endpoint", ioEx);
                } catch (JSONException jsonEx) {
                    Log.e(TAG, "Failed to parse userinfo response");
                } finally {
                    if (userInfoResponse != null) {
                        try {
                            userInfoResponse.close();
                        } catch (IOException ioEx) {
                            Log.e(TAG, "Failed to close userinfo response stream", ioEx);
                        }
                    }
                }
            }
        });
    }

    private void updateUserInfo(final JSONObject jsonObject) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mUserInfoJson = jsonObject;
                refreshUi();
            }
        });
    }

    @MainThread
    private void showSnackbar(@StringRes int messageId) {
        /*Snackbar.make(findViewById(R.id.coordinator),
                getResources().getString(messageId),
                Snackbar.LENGTH_SHORT)
                .show();*/
    }

    private static String readStream(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        char[] buffer = new char[BUFFER_SIZE];
        StringBuilder sb = new StringBuilder();
        int readCount;
        while ((readCount = br.read(buffer)) != -1) {
            sb.append(buffer, 0, readCount);
        }
        return sb.toString();
    }

    public static PendingIntent createPostAuthorizationIntent(
            @NonNull Context context,
            @NonNull AuthorizationRequest request,
            @Nullable AuthorizationServiceDiscovery discoveryDoc,
            @NonNull AuthState authState) {
        Intent intent = new Intent(context, TokenActivity.class);
        intent.putExtra(EXTRA_AUTH_STATE, authState.jsonSerializeString());

        if (discoveryDoc != null) {
            intent.putExtra(EXTRA_AUTH_SERVICE_DISCOVERY, discoveryDoc.docJson.toString());
        }
        return PendingIntent.getActivity(context, request.hashCode(), intent, 0);
    }

    static AuthorizationServiceDiscovery getDiscoveryDocFromIntent(Intent intent) {
        if (!intent.hasExtra(EXTRA_AUTH_SERVICE_DISCOVERY)) {
            return null;
        }
        String discoveryJson = intent.getStringExtra(EXTRA_AUTH_SERVICE_DISCOVERY);
        try {
            return new AuthorizationServiceDiscovery(new JSONObject(discoveryJson));
        } catch (JSONException | AuthorizationServiceDiscovery.MissingArgumentException  ex) {
            throw new IllegalStateException("Malformed JSON in discovery doc");
        }
    }

    public static AuthState getAuthStateFromIntent(Intent intent) {
        if (!intent.hasExtra(EXTRA_AUTH_STATE)) {
            throw new IllegalArgumentException("The AuthState instance is missing in the intent.");
        }
        try {
            return AuthState.jsonDeserialize(intent.getStringExtra(EXTRA_AUTH_STATE));
        } catch (JSONException ex) {
            Log.e(TAG, "Malformed AuthState JSON saved", ex);
            throw new IllegalArgumentException("The AuthState instance is missing in the intent.");
        }
    }

    //region Data loading chain

    public void onTagsLoaded(Collection<Tag> tags) {

        TagsAsyncRepository.getInstance().cacheCopy(this);
        CategoriesAsyncRepository.getInstance().getDataAsync(this::onCategoriesLoaded);
    }

    public void onCategoriesLoaded(Collection<Category> categories) {
        CategoriesAsyncRepository.getInstance().cacheCopy(this);
        CaseStatusesAsyncRepository.getInstance().getDataAsync(this::onCaseStatusesLoaded);
    }

    public void onCaseStatusesLoaded(Collection<CaseStatus> caseStatuses) {
        CaseStatusesAsyncRepository.getInstance().cacheCopy(this);
        AssignedCamerasAsyncRepository.getInstance().getDataAsync(this::onAssignedCamerasLoaded);
    }

    public void onAssignedCamerasLoaded(Collection<String> assignedCameras) {
        AssignedCamerasAsyncRepository.getInstance().cacheCopy(this);
        goToMainActivity();

    }
    private void goToMainActivity() {
        startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
        dismissProgressbar();
    }

    public void showProgressbar()
    {
        progressDialog=new ProgressDialog(TokenActivity.this);
        progressDialog.show();
        progressDialog.setContentView(R.layout.view_part_progressbar);
    }

    public void dismissProgressbar(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

}
/*UserDataRepository model=UserDataRepository.getInstance();
        User user=new User();
        user.setUserId(userPermissoinModel.getUser().getUserId());
        user.setUserName(userPermissoinModel.getUser().getUserName());
        model.setUser(user);
        Permissions permission=new Permissions();
        permission.setCanUseMobileApp(userPermissoinModel.isCanUseMobileApp());
        permission.setViewCases(userPermissoinModel.getViewCases());
        permission.setEditCases(userPermissoinModel.isEditCases());
        permission.setViewVideos(userPermissoinModel.getViewVideos());
        permission.setSetLockdown(userPermissoinModel.isSetLockdown());
        permission.setShareVideos(userPermissoinModel.isShareVideos());
        permission.setEditVideos(userPermissoinModel.isEditVideos());
        model.setPermissions(permission);
        model.cacheCopy(this);*/
