package com.vievu.vievusolution.openid_provider;

/**
 * Created by shailesh.rohit on 16-10-2017.
 */

public class OpenIDAccountModel
{
    public boolean isIDPEnabled() {
        return this.IDPEnabled;
    }
    public void setIDPEnabled(boolean IDPEnabled) {
        this.IDPEnabled = IDPEnabled;
    }
    private boolean IDPEnabled;
    private String VSClientId;
    private String VSClientSecret;
    private String MobileClientId;
    private String MobileClientSecret;
    private String Tenant;
    private String AADInstance;
    private String AuthTokenURL;
    public void setVSClientId(String VSClientId){
        this.VSClientId = VSClientId;
    }
    public String getVSClientId(){
        return this.VSClientId;
    }
    public void setVSClientSecret(String VSClientSecret){
        this.VSClientSecret = VSClientSecret;
    }
    public String getVSClientSecret(){
        return this.VSClientSecret;
    }
    public void setMobileClientId(String MobileClientId){
        this.MobileClientId = MobileClientId;
    }
    public String getMobileClientId(){
        return this.MobileClientId;
    }
    public void setMobileClientSecret(String MobileClientSecret){this.MobileClientSecret = MobileClientSecret;}
    public String getMobileClientSecret(){
        return this.MobileClientSecret;
    }
    public void setTenant(String Tenant){
        this.Tenant = Tenant;
    }
    public String getTenant(){
        return this.Tenant;
    }
    public void setAADInstance(String AADInstance){
        this.AADInstance = AADInstance;
    }
    public String getAADInstance(){
        return this.AADInstance;
    }
    public void setAuthTokenURL(String AuthTokenURL){
        this.AuthTokenURL = AuthTokenURL;
    }
    public String getAuthTokenURL(){
        return this.AuthTokenURL;
    }
}
