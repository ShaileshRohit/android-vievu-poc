package com.vievu.vievusolution.openid_provider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.MainActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.data.AssignedCamerasAsyncRepository;
import com.vievu.vievusolution.data.CaseStatusesAsyncRepository;
import com.vievu.vievusolution.data.CategoriesAsyncRepository;
import com.vievu.vievusolution.data.TagsAsyncRepository;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.login.LoginActivity;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.LogoutUtil;
import com.vievu.vievusolution.util.ValidationUtil;

import net.openid.appauth.AppAuthConfiguration;
import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.ClientSecretBasic;
import net.openid.appauth.RegistrationRequest;
import net.openid.appauth.RegistrationResponse;
import net.openid.appauth.ResponseTypeValues;
import net.openid.appauth.browser.BrowserDescriptor;
import net.openid.appauth.browser.ExactBrowserMatcher;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by shailesh.rohit on 27-10-2017.
 */

public class TenantActivity extends Activity {

    private AuthorizationService mAuthService;
    private ProgressDialog progressDialog=null;
    private ConnectivityManager connectivityManager;
    private static final String TAG = "TenantActivity";
    @Bind(R.id.accontname)
    EditText tanutNameEditText;
    @Bind(R.id.nextButton)
    Button nextButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_account_details);
        init();
    }
    public void init() {
        ButterKnife.bind(this);
        if (this.connectivityManager == null) {
            this.connectivityManager = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        mAuthService = new AuthorizationService(this);
        nextButton.setOnClickListener(clickListener);

        if (LogoutUtil.validateAuthData(this)
                && UserDataRepository.restoreFromCache(this)
                && TagsAsyncRepository.restoreFromCache(this)
                && CategoriesAsyncRepository.restoreFromCache(this)
                && CaseStatusesAsyncRepository.restoreFromCache(this)
                && AssignedCamerasAsyncRepository.restoreFromCache(this)) {
            WebClient.getInstances(this);
            goToMainActivity();
            return;
        }

    }

    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.nextButton:
                    NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
                    boolean isValid = ValidationUtil.validateEditText(tanutNameEditText, (value) -> value.length() > 0, R.string.error_invalid_tenant_name);
                    if(isValid){
                        if(isConnected) {
                            showProgressbar();
                            WebClient.getInstances(TenantActivity.this).request().getOpenIDInfo(new Callback<OpenIDAccountModel>() {
                                @Override
                                public void success(OpenIDAccountModel openIDAccountModel, Response response) {
                                    if (progressDialog != null && progressDialog.isShowing())
                                        progressDialog.dismiss();
                                    redirect(openIDAccountModel);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    if (progressDialog != null && progressDialog.isShowing())
                                        progressDialog.dismiss();
                                }

                            });
                        }
                        else
                        {
                            Toast.makeText(TenantActivity.this, "Please connect to internet", Toast.LENGTH_SHORT).show();
                        }
                    }

                    break;
            }
        }
    };

    public void redirect(OpenIDAccountModel openIDAccountModel){
        if (openIDAccountModel != null)
        {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TenantActivity.this);
            preferences.edit().putString(CommonConstants.KEY_AUTH_INFO, new Gson().toJson(openIDAccountModel)).apply();
            preferences.edit().putBoolean(CommonConstants.KEY_IDP_ENABLED,openIDAccountModel.isIDPEnabled()).apply();

            if(!openIDAccountModel.isIDPEnabled()){
                startActivity(new Intent(TenantActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
            else {
                List<IdentityProvider> providers = IdentityProvider.getEnabledProviders(TenantActivity.this);
                IdentityProvider idp=providers.get(0);
                providers.get(0).setmTenant(openIDAccountModel.getTenant());
                providers.get(0).setClientId(openIDAccountModel.getMobileClientId());
                providers.get(0).setmDiscoveryEndpoint(Uri.parse(String.format("https://login.microsoftonline.com/%1$s/.well-known/openid-configuration?p=%2$s", openIDAccountModel.getTenant(),"")));
                providers.get(0).setmRedirectUri(Uri.parse(getString(R.string.b2c_redirect_uri)));
                initProvider(idp);
            }
        }
    }

    // OpenID provider
    public void initProvider(IdentityProvider idp){

        final AuthorizationServiceConfiguration.RetrieveConfigurationCallback retrieveCallback =
                new AuthorizationServiceConfiguration.RetrieveConfigurationCallback() {

                    @Override
                    public void onFetchConfigurationCompleted(
                            @Nullable AuthorizationServiceConfiguration serviceConfiguration,
                            @Nullable AuthorizationException ex) {
                        if (ex != null) {
                            Log.w("", "Failed to retrieve configuration for " + idp.name, ex);
                        } else {
                            Log.d("", "configuration retrieved for " + idp.name+ ", proceeding");
                            if (idp.getClientId() == null) {
                                // Do dynamic client registration if no client_id
                                makeRegistrationRequest(serviceConfiguration, idp);
                            } else {
                                makeAuthRequest(serviceConfiguration, idp, new AuthState());

                            }
                        }
                    }
                };

        idp.retrieveConfig(TenantActivity.this, retrieveCallback);
    }
    private void goToMainActivity() {
        startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
    private AppAuthConfiguration createConfiguration(
            @Nullable BrowserDescriptor browser) {
        AppAuthConfiguration.Builder builder = new AppAuthConfiguration.Builder();

        if (browser != null) {
            builder.setBrowserMatcher(new ExactBrowserMatcher(browser));
        }

        return builder.build();
    }
    private void makeAuthRequest(
            @NonNull AuthorizationServiceConfiguration serviceConfig,
            @NonNull IdentityProvider idp,
            @NonNull AuthState authState) {

        String loginHint = "";

        if (loginHint.isEmpty()) {
            loginHint = null;
        }

        AuthorizationRequest authRequest = new AuthorizationRequest.Builder(
                serviceConfig,
                idp.getClientId(),
                ResponseTypeValues.CODE,
                idp.getRedirectUri())
                .setScope(idp.getScope())
                .setLoginHint(loginHint)
                .build();

        Log.d("", "Making auth request to " + serviceConfig.authorizationEndpoint);

        mAuthService.performAuthorizationRequest(
                authRequest,
                TokenActivity.createPostAuthorizationIntent(
                        this,
                        authRequest,
                        serviceConfig.discoveryDoc,
                        authState),
                mAuthService.createCustomTabsIntentBuilder()
                        .setShowTitle(true)
                        .setToolbarColor(getColorCompat(R.color.accent))
                        .build());
    }

    private void makeRegistrationRequest(
            @NonNull AuthorizationServiceConfiguration serviceConfig,
            @NonNull final IdentityProvider idp) {

        final RegistrationRequest registrationRequest = new RegistrationRequest.Builder(
                serviceConfig,
                Arrays.asList(idp.getRedirectUri()))
                .setTokenEndpointAuthenticationMethod(ClientSecretBasic.NAME)
                .build();

        Log.d("", "Making registration request to " + serviceConfig.registrationEndpoint);
        mAuthService.performRegistrationRequest(
                registrationRequest,
                new AuthorizationService.RegistrationResponseCallback() {
                    @Override
                    public void onRegistrationRequestCompleted(
                            @Nullable RegistrationResponse registrationResponse,
                            @Nullable AuthorizationException ex) {
                        Log.d("", "Registration request complete");
                        if (registrationResponse != null) {
                            idp.setClientId(registrationResponse.clientId);
                            Log.d("", "Registration request complete successfully");
                            // Continue with the authentication
                            makeAuthRequest(registrationResponse.request.configuration, idp,
                                    new AuthState((registrationResponse)));
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuthService.dispose();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressWarnings("deprecation")
    private int getColorCompat(@ColorRes int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return getColor(color);
        } else {
            return getResources().getColor(color);
        }
    }
    public void showProgressbar()
    {
        progressDialog=new ProgressDialog(TenantActivity.this);
        progressDialog.show();
        progressDialog.setContentView(R.layout.view_part_progressbar);
    }


    @Override
    public void onBackPressed() {
        if(progressDialog==null || (progressDialog!=null && !progressDialog.isShowing()))
            super.onBackPressed();

    }
}

