package com.vievu.vievusolution;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.annimon.stream.function.Consumer;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.login.LoginActivity;
import com.vievu.vievusolution.util.LogoutUtil;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.ClientAuthentication;
import net.openid.appauth.TokenRequest;
import net.openid.appauth.TokenResponse;

import org.json.JSONObject;

public abstract class BaseVievuActivity extends Activity {

    private static final String TAG = "BaseVievuActivity";

    public static final int REQUEST_CODE_ASK_PERMISSIONS = 401;
    private static Context context;
    protected boolean isAutoLogoutEnabled = true;
    protected boolean isHomeAsUpEnabled = true;

    private static final String KEY_AUTH_STATE = "authState";
    private static final String KEY_USER_INFO = "userInfo";

    private static final String EXTRA_AUTH_SERVICE_DISCOVERY = "authServiceDiscovery";
    private static final String EXTRA_AUTH_STATE = "authState";


    private static final int BUFFER_SIZE = 1024;

    private static AuthState mAuthState;
    public static AuthorizationService mAuthService;
    private JSONObject mUserInfoJson;
    private static SharedPreferences preferences;
    public  static final String KEY_TOKEN_EXPIRATION_TIME = "expiration_time";

    public static  Context getContext(){
        return  context;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getActionBar();
        mAuthService = new AuthorizationService(this);
        context=this;
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(isHomeAsUpEnabled);
        }
         preferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (UserDataRepository.getInstance().getPermissions() == null
                && !(this instanceof LoginActivity)) {
            //seems like the app restores after crash
            LogoutUtil.logOut(this);
            finish();
        }

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuthService.dispose();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            this.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //noinspection PointlessBooleanExpression
        if (BuildConfig.USES_BACKEND && isAutoLogoutEnabled) {
            LogoutUtil.logoutIfNecessary(this);
        }
    }

    public void shortToast(int messageId) {
        Toast.makeText(this, messageId, Toast.LENGTH_SHORT).show();
    }

    public void shortToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected <T extends Fragment> boolean callOnFragment(Class<T> fragmentClass, Consumer<T> callable) {
        return callOnFragment(R.id.container, fragmentClass, callable);
    }

    protected <T extends Fragment> boolean callOnFragment(int containerId, Class<T> fragmentClass, Consumer<T> callable) {
        Fragment fragment = getFragmentManager().findFragmentById(containerId);
        if (fragment != null && fragment.getClass() == fragmentClass && callable != null) {
            //noinspection unchecked
            callable.accept((T) fragment);
            return true;
        }
        return false;
    }

    public boolean requestPermissionIfNecessary(String permission) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){
            int permissionStatus = checkSelfPermission(permission);
            if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        new String[]{ permission },
                        REQUEST_CODE_ASK_PERMISSIONS);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Currently there is no reason to pass which exactly permission was granted
                // because even for WRITE_EXTERNALS_STORAGE it returns READ_EXTERNAL_STORAGE
                onPermissionGranted(permissions[0]);
            } else {
                onPermissionDenied(permissions[0]);
            }
        }
    }

    protected void onPermissionGranted(String grantedPermission) {
        // Do nothing by default
    }

    protected void onPermissionDenied(String deniedPermission) {
        shortToast(R.string.error_permission_denied);
    }
}







/* if(preferences.getBoolean(CommonConstants.KEY_IDP_ENABLED,false)==false)
        {
            if (UserDataRepository.getInstance().getPermissions() == null
                    && !(this instanceof LoginActivity)) {
                //seems like the app restores after crash
                LogoutUtil.logOut(this);
                finish();
            }
        }*/
