package com.vievu.vievusolution.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.vievu.vievusolution.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Date;
import java.util.TimeZone;

import butterknife.ButterKnife;

public class DateTimeDialogFragment extends BasePickerDialogFragment implements Dialog.OnClickListener, TimePicker.OnTimeChangedListener, CalendarView.OnDateChangeListener {

    public interface OnDateSelectedListener {
        void onDateSelected(Date date);
    }

    public static final String TAG = DateTimeDialogFragment.class.getSimpleName();

    private DateTime defaultDate = null;
    private DateTime maxDate = null;
    private DateTime minDate = null;

    private OnDateSelectedListener onDateSelectedListener;

    private DatePicker datePicker;
    private TimePicker timePicker;

    private TimeZone timeZone;

    private int defaultSeconds = 0;
    private int defaultMillis = 0;

    public void useUpperBoundAlignment(boolean useAlignment) {
        if (useAlignment) {
            defaultSeconds = 59;
            defaultMillis = 999;
        } else {
            defaultSeconds = defaultMillis = 0;
        }
    }

    @SuppressWarnings("deprecation")
    private void setTime(DateTime date) {
        timePicker.setCurrentHour(date.getHourOfDay());
        timePicker.setCurrentMinute(date.getMinuteOfHour());
    }

    public void setDate(Date date) {
        this.setDateAndTime(new DateTime(date));
    }

    public void setDateAndTime(DateTime date) {
        this.defaultDate = date;
        if (datePicker != null && defaultDate != null) {

            datePicker.getCalendarView().setDate(defaultDate.getMillis());
            setTime(defaultDate);
        }
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public void setMaxDate(Date maxDate) {
        setMaxDate(new DateTime(maxDate.getTime()));
    }

    public void setMaxDate(DateTime maxDate) {
        this.maxDate = maxDate;
        if (datePicker != null) {
            // This condition may be true only if time part of selected date is after max date
            if (maxDate.isBefore(datePicker.getCalendarView().getDate())) {
                // Only date part is used for comparison, so it's safe to leave time as is
                datePicker.setMaxDate(datePicker.getCalendarView().getDate());
                // Calendar's view date cannot be set to max, because it has the same date part as max date
                // See CalendarViewLegacyDelegate.setDate(long, boolean, boolean) for details

            } else {
                datePicker.setMaxDate(this.maxDate.getMillis());
            }
        }
    }

    public void setMinDate(Date minDate) {
        setMinDate(new DateTime(minDate.getTime()));
    }

    public void setMinDate(DateTime minDate) {
        this.minDate = minDate;
        if (datePicker != null) {
            //This condition may be true only if time part of selected date is before min date
            if (minDate.isAfter(datePicker.getCalendarView().getDate())) {
                //Only date part is used for comparison, so it's safe to leave time as is
                datePicker.setMinDate(datePicker.getCalendarView().getDate());
                // Calendar's view date cannot be set to min, because it has the same date part as min date
                // See CalendarViewLegacyDelegate.setDate(long, boolean, boolean) for details
            } else {
                datePicker.setMinDate(this.minDate.getMillis());
            }
        }
    }

    public void setOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
        this.onDateSelectedListener = onDateSelectedListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_date_picker,null,false);
        datePicker = ButterKnife.findById(view, R.id.datePicker);
        timePicker = ButterKnife.findById(view, R.id.timePicker);
        timePicker.setIs24HourView(DateFormat.is24HourFormat(getActivity()));
        if (defaultDate != null) {



            datePicker.getCalendarView().setDate(defaultDate.plusDays(1).getMillis());
            if (maxDate != null && defaultDate.isAfter(maxDate)) {
                setDateAndTime(maxDate);
            } else if (minDate != null && defaultDate.isBefore(minDate)) {
                setDateAndTime(minDate);
            } else {
                setDateAndTime(defaultDate);
            }
        } else {
            datePicker.getCalendarView().setDate(0);
            datePicker.getCalendarView().setDate(DateTime.now().withTime(0, 0, 0, 0).getMillis());
        }
        if (maxDate != null) {
            datePicker.setMaxDate(maxDate.getMillis());
        }
        if (minDate != null) {
            datePicker.setMinDate(minDate.getMillis());
        }
        customizeDividers((ViewGroup) ((ViewGroup) datePicker.getChildAt(0)).getChildAt(0));
        ViewGroup timePickerRoot = (ViewGroup) timePicker.getChildAt(0);
        customizeDividers(timePickerRoot);
        customizeDividers((ViewGroup) timePickerRoot.getChildAt(0));
        timePicker.setOnTimeChangedListener(this);
        datePicker.getCalendarView().setOnDateChangeListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.Dialog).
            setView(view).
            setNegativeButton(android.R.string.cancel, null).
            setPositiveButton(android.R.string.ok, this);
        applyTitle(builder);
        return builder.create();
    }

    @Override
    public void onClick(@NonNull DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_POSITIVE &&
            datePicker != null &&
            onDateSelectedListener != null) {

            @SuppressWarnings("deprecation")
            DateTime newDateTime =
                    new DateTime(datePicker.getCalendarView().getDate())
                    .withTime(timePicker.getCurrentHour(), timePicker.getCurrentMinute(), defaultSeconds, defaultMillis);
            setDateAndTime(newDateTime);
            if (timeZone != null) {
                newDateTime = newDateTime.withZoneRetainFields(DateTimeZone.forTimeZone(timeZone));
            }
            onDateSelectedListener.onDateSelected(newDateTime.toDate());
        }
    }

    @Override
    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
        if (timePicker != null) {
            //noinspection deprecation
            correctTimePickerValue(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
        }
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        correctTimePickerValue(hourOfDay, minute);
    }

    private void correctTimePickerValue(int hourOfDay, int minute) {
        if (maxDate != null || minDate != null) {
            DateTime newDateTime =
                    new DateTime(datePicker.getCalendarView().getDate())
                    .withTime(hourOfDay, minute, defaultSeconds, defaultMillis);
            if (maxDate != null && newDateTime.isAfter(maxDate)) {
                setDateAndTime(maxDate);
            } else if (minDate != null && newDateTime.isBefore(minDate)) {
                setDateAndTime(minDate);
            }
        }
    }

    public void showSingle(FragmentManager manager) {
        showSingle(manager, TAG);
    }
}
