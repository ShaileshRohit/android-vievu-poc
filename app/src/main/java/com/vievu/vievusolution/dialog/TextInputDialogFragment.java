package com.vievu.vievusolution.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.annimon.stream.function.Consumer;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.util.ActivityUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;

public class TextInputDialogFragment extends BaseDialogFragment implements DialogInterface.OnClickListener {

    @Bind(R.id.inputEditText) EditText inputEditText;

    private Consumer<String> onInputSubmitListener;

    public void setOnInputSubmitListener(Consumer<String> onInputSubmitListener) {
        this.onInputSubmitListener = onInputSubmitListener;
    }

    public static TextInputDialogFragment create(int titleId, String message) {
        return BaseDialogFragment.createWithTexts(new TextInputDialogFragment(), titleId, message);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_text_input, null);
        ButterKnife.bind(this, view);
        inputEditText.getBackground().setColorFilter(getResources().getColor(R.color.accent), PorterDuff.Mode.SRC_IN);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.Dialog)
                .setView(view)
                .setPositiveButton(android.R.string.ok, this);
        applyTexts(builder);
        return builder.create();
    }

    @OnEditorAction(R.id.inputEditText)
    public boolean onPasswordOkClicked(int key) {
        if (EditorInfo.IME_ACTION_DONE == key) {
            onClick(null, 0);
            this.dismiss();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        ActivityUtil.hideKeyboard(getActivity());
        if (onInputSubmitListener != null) {
            onInputSubmitListener.accept(inputEditText.getText().toString());
        }
    }
}