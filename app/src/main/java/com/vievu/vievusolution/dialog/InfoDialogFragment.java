package com.vievu.vievusolution.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;

import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.vievu.vievusolution.R;

public class InfoDialogFragment extends BaseDialogFragment {

    public static InfoDialogFragment create(@StringRes int titleId, @Nullable String message) {
        return BaseDialogFragment.createWithTexts(new InfoDialogFragment(), titleId, message);
    }

    public static InfoDialogFragment create(@StringRes int titleId, @LayoutRes int layoutId, @Nullable Consumer<View> viewInitializer) {
        InfoDialogFragment fragment = BaseDialogFragment.createWithTexts(new InfoDialogFragment(), titleId, null);
        fragment.layoutId = layoutId;
        fragment.viewInitializer = viewInitializer;
        return fragment;
    }


    private int layoutId;
    private Consumer<View> viewInitializer;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.Dialog)
                .setPositiveButton(android.R.string.ok, null);
        applyTexts(builder);
        if (layoutId != 0) {
            View view = getActivity().getLayoutInflater().inflate(layoutId, null);
            if (viewInitializer != null) {
                viewInitializer.accept(view);
            }
            builder.setView(view);
        }
        return builder.create();
    }
}
