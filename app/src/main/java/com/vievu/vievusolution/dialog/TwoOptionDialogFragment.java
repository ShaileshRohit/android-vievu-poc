package com.vievu.vievusolution.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.StringRes;

import com.vievu.vievusolution.R;

public class TwoOptionDialogFragment extends BaseDialogFragment implements DialogInterface.OnClickListener {

    public interface OnClickListener {
        void onClick(DialogInterface dialog);
    }

    public static TwoOptionDialogFragment create(int titleId, String message) {
        return BaseDialogFragment.createWithTexts(new TwoOptionDialogFragment(), titleId, message);
    }

    private int positiveTextId = android.R.string.ok;
    private OnClickListener positiveOnClickListener;
    private int negativeTextId = android.R.string.cancel;
    private OnClickListener negativeOnClickListener;

    public TwoOptionDialogFragment() {
        setRetainInstance(true);
    }

    public void setPositiveButton(@StringRes int textId, OnClickListener positiveOnClickListener) {
        this.positiveTextId = textId;
        this.positiveOnClickListener = positiveOnClickListener;
    }

    public void setNegativeButton(@StringRes int textId, OnClickListener negativeOnClickListener) {
        this.negativeTextId = textId;
        this.negativeOnClickListener = negativeOnClickListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.Dialog)
                .setPositiveButton(positiveTextId, this)
                .setNegativeButton(negativeTextId, this);
        applyTexts(builder);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            if (positiveOnClickListener != null) {
                positiveOnClickListener.onClick(dialog);
            }
        } else if (negativeOnClickListener != null) {
            negativeOnClickListener.onClick(dialog);
        }
    }
}
