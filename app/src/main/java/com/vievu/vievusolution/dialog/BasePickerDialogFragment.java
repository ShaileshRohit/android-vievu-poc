package com.vievu.vievusolution.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import com.vievu.vievusolution.R;

import java.lang.reflect.Field;

public abstract class BasePickerDialogFragment extends BaseDialogFragment {

    protected int titleId;

    public void setTitleId(int titleId) {
        this.titleId = titleId;
        Dialog dialog = getDialog();
        if (dialog != null && titleId != 0) {
            dialog.setTitle(titleId);
        }
    }

    protected void applyTitle(AlertDialog.Builder builder) {
        if (titleId != 0) {
            builder.setTitle(titleId);
        }
    }

    protected void customizeDividers(ViewGroup root) {
        try {
            Field selectionDividerField = NumberPicker.class.getDeclaredField("mSelectionDivider");
            Field pressedDrawableField = NumberPicker.class.getDeclaredField("mVirtualButtonPressedDrawable");
            Drawable accentColor = getResources().getDrawable(R.color.accent);
            for (int i = 0; i < root.getChildCount(); i++) {
                View nPicker = root.getChildAt(i);
                if (nPicker.getClass().equals(NumberPicker.class)) {
                    updateColorField(nPicker, selectionDividerField, accentColor);
                    updateColorField(nPicker, pressedDrawableField, accentColor);
                }
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    protected void updateColorField(View target, Field field, Drawable color) {
        field.setAccessible(true);
        try {
            field.set(target, color);
        } catch (IllegalArgumentException | Resources.NotFoundException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    protected void showSingle(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            this.show(manager, tag);
        }
    }
}
