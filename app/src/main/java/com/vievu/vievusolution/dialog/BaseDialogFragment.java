package com.vievu.vievusolution.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;

import com.vievu.vievusolution.R;

public abstract class BaseDialogFragment extends DialogFragment {

    protected static final String ARG_TITLE_ID = "title_id";
    protected static final String ARG_MESSAGE = "message";

    public static <T extends BaseDialogFragment> T createWithTexts(T fragment, @StringRes int titleId, @Nullable String message) {
        Bundle arguments = fragment.getArguments();
        if (arguments == null) {
            arguments = new Bundle();
        }
        arguments.putInt(ARG_TITLE_ID, titleId);
        arguments.putString(ARG_MESSAGE, message);
        fragment.setArguments(arguments);
        return fragment;
    }

    protected void applyTexts(AlertDialog.Builder builder) {
        int titleId = getArguments().getInt(ARG_TITLE_ID);
        if (titleId != 0) {
            builder.setTitle(titleId);
        }
        String message = getArguments().getString(ARG_MESSAGE);
        if (message != null) {
            builder.setMessage(message);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
            View titleDivider = dialog.findViewById(titleDividerId);
            if (titleDivider != null) {
                titleDivider.setBackgroundColor(getResources().getColor(R.color.accent));
            }
        }
    }
}
