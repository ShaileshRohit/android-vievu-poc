package com.vievu.vievusolution.data;

import android.content.Context;

import com.vievu.vievusolution.bean.CaseStatus;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.JsonCacheUtil;

import java.util.List;

public class CaseStatusesAsyncRepository extends CachingAsyncRepository<List<CaseStatus>> {

    private static final String CACHE_FILE_NAME = "case_statuses.json";

    private static CaseStatusesAsyncRepository instance;

    public static CaseStatusesAsyncRepository getInstance() {
        if (instance == null) {
            instance = new CaseStatusesAsyncRepository();
        }
        return instance;
    }

    @Override
    protected String getCacheFileName() {
        return CACHE_FILE_NAME;
    }

    public static boolean restoreFromCache(Context context) {
        CaseStatusesAsyncRepository cachedInstance = JsonCacheUtil.
                restoreFromCache(context, CACHE_FILE_NAME, CaseStatusesAsyncRepository.class);
        if (cachedInstance != null) {
            CaseStatusesAsyncRepository.instance = cachedInstance;
            return true;
        }
        return false;
    }

    @Override
    protected int getLoadEventAction() {
        return WebClient.Actions.LOAD_CASE_STATUSES;
    }

    @Override
    protected void loadData(EventBusCallback<List<CaseStatus>> callback) {
        WebClient.request().caseStatuses(callback);
    }
}
