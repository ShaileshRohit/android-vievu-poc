package com.vievu.vievusolution.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.reflect.TypeToken;
import com.vievu.vievusolution.bean.MfaTokenData;
import com.vievu.vievusolution.util.JsonCacheUtil;

import org.joda.time.DateTime;

import java.util.HashMap;

public class MfaTokenRepository {

    public static String key(@NonNull String userName, @NonNull String accountName) {
        return String.format("%s:%s", userName.toLowerCase(), accountName.toLowerCase());
    }

    public static String[] userData(@NonNull String key) {
        return key.split(":");
    }

    private static final String CACHE_FILE_NAME = "mfa_tokens.json";

    private HashMap<String, MfaTokenData> data;

    public HashMap<String, MfaTokenData> getData() {
        return data;
    }

    public void loadStoredTokens(Context context) {
        this.data = JsonCacheUtil.restoreFromCache(
            context, CACHE_FILE_NAME,
            new TypeToken<HashMap<String, MfaTokenData>>(){}.getType());
        if (data == null) {
            data = new HashMap<>(1);
        }
    }

    public void saveData(Context context) {
        if (data != null) {
            JsonCacheUtil.cacheCopy(context, CACHE_FILE_NAME, data);
        }
    }
}
