package com.vievu.vievusolution.data;

import android.content.Context;

import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.JsonCacheUtil;

import java.util.List;

public class TagsAsyncRepository extends CachingAsyncRepository<List<Tag>> {

    private static final String CACHE_FILE_NAME = "tags.json";

    private static TagsAsyncRepository instance;

    public static TagsAsyncRepository getInstance() {
        if (instance == null) {
            instance = new TagsAsyncRepository();
        }
        return instance;
    }

    @Override
    protected String getCacheFileName() {
        return CACHE_FILE_NAME;
    }

    public static boolean restoreFromCache(Context context) {
        TagsAsyncRepository cachedInstance = JsonCacheUtil.
                restoreFromCache(context, CACHE_FILE_NAME, TagsAsyncRepository.class);
        if (cachedInstance != null) {
            TagsAsyncRepository.instance = cachedInstance;
            return true;
        }
        return false;
    }

    @Override
    protected int getLoadEventAction() {
        return WebClient.Actions.LOAD_TAGS;
    }

    @Override
    protected void loadData(EventBusCallback<List<Tag>> callback) {
        WebClient.request().tags(callback);
    }
}