package com.vievu.vievusolution.data;

import android.content.Context;

import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.JsonCacheUtil;

import java.util.List;

public class AssignedCamerasAsyncRepository extends CachingAsyncRepository<List<String>> {

    private static final String CACHE_FILE_NAME = "assigned_cameras.json";

    private static AssignedCamerasAsyncRepository instance;

    public static AssignedCamerasAsyncRepository getInstance() {
        if (instance == null) {
            instance = new AssignedCamerasAsyncRepository();
        }
        return instance;
    }

    @Override
    protected String getCacheFileName() {
        return CACHE_FILE_NAME;
    }

    public static boolean restoreFromCache(Context context) {
        AssignedCamerasAsyncRepository cachedInstance = JsonCacheUtil.
                restoreFromCache(context, CACHE_FILE_NAME, AssignedCamerasAsyncRepository.class);
        if (cachedInstance != null) {
            AssignedCamerasAsyncRepository.instance = cachedInstance;
            return true;
        }
        return false;
    }

    @Override
    protected int getLoadEventAction() {
        return WebClient.Actions.LOAD_ASSIGNED_CAMERAS;
    }

    @Override
    protected void loadData(EventBusCallback<List<String>> callback) {
        WebClient.request().assignedCameras(callback);
    }
}