package com.vievu.vievusolution.data;

import com.annimon.stream.function.Consumer;
import com.google.gson.annotations.Expose;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.NetworkEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.EventBusCallback;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public abstract class BaseAsyncRepository<T> {

    protected T data;
    @Expose(serialize = false, deserialize = false)
    protected ArrayList<Consumer<T>> callbacks = new ArrayList<>();

    protected abstract int getLoadEventAction();
    protected abstract void loadData(EventBusCallback<T> callback);

    protected void invokeCallback(Consumer<T> callback) {
        if (callback != null) {
            callback.accept(data);
        }
    }

    protected void onEventReceived(NetworkEvent event, T data) {
        if (event.is(getLoadEventAction())) {
            onDataLoaded(data);
        }
    }

    protected void onDataLoaded(T data) {
        this.data = data;
        for (Consumer<T> callback : callbacks) {
            invokeCallback(callback);
        }
        callbacks.clear();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(RequestCompletedEvent<T> event) {
        onEventReceived(event, event.getResult());
    }

    public void onEvent(NetworkErrorEvent event) {
        onEventReceived(event, null);
    }

    public boolean isDataReady() {
        return data != null;
    }

    public void getDataAsync(Consumer<T> callback) {
        if (isDataReady()) {
            invokeCallback(callback);
        } else {
            callbacks.add(callback);
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
            }
            loadData(new EventBusCallback<>(getLoadEventAction()));
        }
    }

    public T getData() {
        return data;
    }

    public void clearData() {
        this.data = null;
    }
}
