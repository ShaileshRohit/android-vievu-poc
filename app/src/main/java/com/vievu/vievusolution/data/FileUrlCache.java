package com.vievu.vievusolution.data;

import android.net.Uri;

import com.vievu.vievusolution.bean.FileInfo;

import java.util.Date;
import java.util.HashMap;

public class FileUrlCache {

    public static FileUrlCache instance;

    public static FileUrlCache getInstance() {
        if (instance == null) {
            instance = new FileUrlCache();
        }
        return instance;
    }

    private HashMap<String, FileInfo> cache = new HashMap<>();

    public void addEntry(String fileName, FileInfo info) {
        cache.put(fileName, info);
    }

    public Uri getFileUrl(String fileName) {
        FileInfo info = cache.get(fileName);
        if (info != null) {
            Uri uri = info.buildFileUri(fileName);
            if (info.getExpirationDate() != null && info.getExpirationDate().after(new Date())) {
                return uri;
            } else {
                cache.remove(fileName);
            }
        }
        return null;
    }
}
