package com.vievu.vievusolution.data;

import android.content.Context;

import com.vievu.vievusolution.util.JsonCacheUtil;

public abstract class CachingAsyncRepository<T> extends BaseAsyncRepository<T> {

    protected abstract String getCacheFileName();

    public void cacheCopy(Context context) {
        JsonCacheUtil.cacheCopy(context, getCacheFileName(), this);
    }
}
