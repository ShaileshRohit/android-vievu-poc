package com.vievu.vievusolution;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.details.BaseUploadProgressActivity;
import com.vievu.vievusolution.details.location.LocationFragment;
import com.vievu.vievusolution.event.EditRequestCompletedEvent;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.net.upload.UploadIntentService;

public class UploadActivity extends BaseUploadProgressActivity<File> {

    public static final int REQUEST_CODE_CHOOSE_FILE = 501;
    public static final int REQUEST_CODE_UPLOAD_FILE = 502;
    public static final int RESULT_UPLOAD_SUCCEEDED = Activity.RESULT_FIRST_USER + 501;

    public static final String EXTRA_FILE_URI = "file_uri";

    public static void startWithFile(Activity caller, Uri fileUri, int caseId, String caseName) {
        Intent intent = new Intent(caller, UploadActivity.class);
        intent.putExtra(CommonConstants.EXTRA_CASE_ID, caseId);
        intent.putExtra(CommonConstants.EXTRA_CASE_NAME, caseName);
        intent.putExtra(EXTRA_FILE_URI, fileUri);
        caller.startActivityForResult(intent, REQUEST_CODE_UPLOAD_FILE);
    }

    private Uri currentFileUri;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_upload;
    }

    @Override
    protected void initialize(Bundle bundle) {
        super.initialize(bundle);
        item = new File();
        Fragment fragment;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(EXTRA_FILE_URI)) {
            fragment = MetadataFragment.create(
                    extras.getInt(CommonConstants.EXTRA_CASE_ID),
                    extras.getString(CommonConstants.EXTRA_CASE_NAME));
            currentFileUri = extras.getParcelable(EXTRA_FILE_URI);
            showFragment(fragment, false);
        } else {
            Intent getContentIntent = FileUtils.createGetContentIntent();
            Intent intent = Intent.createChooser(getContentIntent, getString(R.string.chooser_action_pick_file));
            startActivityForResult(intent, REQUEST_CODE_CHOOSE_FILE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case LocationFragment.REQUEST_CODE_GOOGLE_SETTINGS:
                callOnFragment(LocationFragment.class,
                        fragment -> fragment.onActivityResult(requestCode, resultCode, data));
                break;
            case REQUEST_CODE_CHOOSE_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    currentFileUri = data.getData();
                    showFragment(new MetadataFragment(), false);
                } else {
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onEditCompleted(EditRequestCompletedEvent<File> event) {
        setProgressOverlayVisible(false);
    }

    @Override
    protected boolean onNetworkErrorEvent(NetworkErrorEvent event) {
        setProgressOverlayVisible(false);
        callSaveChangesError();
        return false;
    }

    @Override
    protected int getOnCompletedMessageId() {
        return R.string.text_upload_completed_file;
    }

    @Override
    protected void onUploadCompletedSuccessfully() {
        setResult(RESULT_UPLOAD_SUCCEEDED);
        finish();
    }

    @Override
    public void updateItem(File video) {
        setProgressOverlayVisible(true);
        video.setUserId(UserDataRepository.getInstance().getUser().getUserId());
        currentAction = UploadIntentService.uploadVideo(this, currentFileUri, video);
    }

    @Override
    public void setProgressOverlayVisible(boolean isVisible) {
        if (isVisible) {
            progressBarOverlay.setVisibility(View.VISIBLE);
        } else {
            progressBarOverlay.setVisibility(View.GONE);
            progressTextView.setText(R.string.text_preparing);
            progressBar.setIndeterminate(true);

        }
    }
}
