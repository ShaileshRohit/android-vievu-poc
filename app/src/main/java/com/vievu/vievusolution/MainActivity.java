package com.vievu.vievusolution;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.vievu.vievusolution.adapter.MainMenuAdapter;
import com.vievu.vievusolution.bean.Permissions;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.dialog.InfoDialogFragment;
import com.vievu.vievusolution.event.OfflineModeChangedEvent;
import com.vievu.vievusolution.ptpip.constants.CameraMode;
import com.vievu.vievusolution.ptpip.dataset.CameraStatus;
import com.vievu.vievusolution.util.ActivityUtil;
import com.vievu.vievusolution.util.LogoutUtil;

import java.security.MessageDigest;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

import static com.microsoft.azure.storage.Constants.QueryConstants.SIGNATURE;


public class MainActivity extends EventBusActivity implements WiFiStateReceiver.CallbackActivity {

    private static final int REQUEST_CODE_CAMERA_PAGE = 901;

    @Bind(R.id.bottomBar) View bottomBar;
    @Bind(R.id.chargeLevelImageView) ImageView chargeLevelImageView;
    @Bind(R.id.storageStatusTextView) TextView storageStatusTextView;
    @Bind(R.id.progressBarOverlay) View progressBarOverlay;

    private MainMenuAdapter adapter;
    private WiFiStateReceiver<MainActivity> receiver = new WiFiStateReceiver<>(false);

    private MenuItem cameraMenuItem;
    private MenuItem logoutMenuItem = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.isHomeAsUpEnabled = false;
        super.onCreate(savedInstanceState);
        ActivityUtil.forceOverFlowIcon(this);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        if (UserDataRepository.getInstance().getPermissions() != null) {
            ButterKnife
                    .<ListView>findById(this, android.R.id.list)
                    .setAdapter(adapter = new MainMenuAdapter(this));
            restorePreRecord();
        } else {
            //seems like the app restores after crash
            LogoutUtil.logOut(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setIcon(R.drawable.vievu_logo);
        cameraMenuItem = menu.findItem(R.id.action_camera);
        if (BuildConfig.USES_BACKEND) {
            logoutMenuItem = menu.findItem(R.id.action_logout);
            logoutMenuItem.setVisible(isInOnlineMode());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_logout:
                LogoutUtil.logOut(this);
                return true;
            case R.id.action_camera:
                startActivity(new Intent(this, AssignedCameraActivity.class));
                return true;
            case R.id.action_about:
                showAboutDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!isProgressOverlayVisible()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAMERA_PAGE) {
            restorePreRecord();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        receiver.registerWith(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        receiver.unregister();
    }

    @Override
    public void onConnectedToWiFiNetwork(String ssid) {
    }

    @Override
    public void onWiFiConnectionLost() {
        setProgressOverlayVisible(false);
        bottomBar.setVisibility(View.GONE);
    }

    @OnItemClick(android.R.id.list)
    protected void onListItemClick(int position) {
        adapter.getItem(position).handleClick(this);
    }

    public boolean isProgressOverlayVisible() {
        return progressBarOverlay != null && progressBarOverlay.getVisibility() == View.VISIBLE;
    }

    public void setProgressOverlayVisible(boolean isVisible) {
        if (progressBarOverlay != null) {
            progressBarOverlay.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
        if (cameraMenuItem != null) {
            cameraMenuItem.setEnabled(!isVisible);
        }
    }

    @Override
    protected void onPermissionGranted(String grantedPermission) {
        goTo(UploadActivity.class);
    }

    public void onEventMainThread(CameraStatus cameraStatus) {
        if (cameraStatus.getCapacity() > 0) {
            bottomBar.setVisibility(View.VISIBLE);
            chargeLevelImageView.setImageLevel(cameraStatus.getChargeLevel());
            storageStatusTextView.setText(cameraStatus.getDisplayedStorageStatus());
        } else {
            bottomBar.setVisibility(View.GONE);
        }
    }

    public void onEventMainThread(CameraClient.Event event) {
        if (!event.isConnected()
            && progressBarOverlay.getVisibility() == View.VISIBLE
            && !keepEvent(event)) {

            setProgressOverlayVisible(false);
        }
    }

    public void onEventMainThread(CameraClient.Mode mode) {
        if (!keepEvent(mode)) {
            short modeValue = mode.getValue();
            if (modeValue == CameraMode.VIDEO_OFF || modeValue == CameraMode.ERROR) {
                setProgressOverlayVisible(false);
            }
        }
    }

    public void onEventMainThread(OfflineModeChangedEvent event) {
        if (!isInBackground) {
            adapter.notifyDataSetChanged();
        }
        if (BuildConfig.USES_BACKEND && logoutMenuItem != null) {
            logoutMenuItem.setVisible(event.isInOnlineMode());
        }
    }

    public void goTo(Class<? extends Activity> activityClass) {
        ActivityUtil.tryUnbindNetworkAdapter(this);
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }

    public void goToCameraPage(Class<? extends Activity> activityClass) {
        Intent intent = new Intent(this, activityClass);
        startActivityForResult(intent, REQUEST_CODE_CAMERA_PAGE);
    }

    public void checkPermissionsBeforeUpload() {

        if(requestPermissionIfNecessary(Manifest.permission.READ_EXTERNAL_STORAGE)){
            goTo(UploadActivity.class);
        }
    }

    private void showAboutDialog() {
        InfoDialogFragment fragment = InfoDialogFragment.create(
            R.string.action_about,
            R.layout.dialog_about,
            view -> ((TextView)view).setText(getString(
                        R.string.template_copyright_and_version,
                        BuildConfig.VERSION_NAME)));
        fragment.show(getFragmentManager(), null);
    }

    private void restorePreRecord() {
        ActivityUtil.tryBindNetworkAdapter(this);
        if (CameraClient.getInstance().isPreRecordRestoreRequested()
            || CameraClient.getInstance().restorePreRecord()) {
            setProgressOverlayVisible(true);
        }
        ActivityUtil.tryUnbindNetworkAdapter(this);
    }
}
