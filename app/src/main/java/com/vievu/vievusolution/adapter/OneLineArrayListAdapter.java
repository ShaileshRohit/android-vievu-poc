package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.annimon.stream.function.Function;

import java.util.List;

public class OneLineArrayListAdapter<T> extends ArrayListAdapter<T, SimpleViewHolder> {

    protected Function<T, CharSequence> getText;

    public OneLineArrayListAdapter(Context context, List<T> items) {
        this(context, items, Object::toString, android.R.layout.simple_list_item_1);
    }

    public OneLineArrayListAdapter(Context context, List<T> items, Function<T, CharSequence> getText) {
        this(context, items, getText, android.R.layout.simple_list_item_1);
    }

    public OneLineArrayListAdapter(Context context, List<T> items, Function<T, CharSequence> getText, int layoutId) {
        super(context, layoutId, items);
        this.getText = getText;
    }

    @Override
    protected SimpleViewHolder createHolder(View view) {
        return new SimpleViewHolder();
    }

    @Override
    protected View createView(ViewGroup parent) {
        TextView view = (TextView) super.createView(parent);
        view.setSingleLine();
        view.setEllipsize(TextUtils.TruncateAt.END);
        return view;
    }

    @Override
    protected void initView(int position, SimpleViewHolder viewHolder) {
        viewHolder.textView.setText(getText.apply(getItem(position)));
    }
}
