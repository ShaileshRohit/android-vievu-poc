package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.File;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.DateUtil;

import java.util.List;

import butterknife.Bind;

public class FileAdapter extends ArrayListAdapter<File, FileAdapter.ViewHolder> {

    private int width;
    private int height;

    protected static class ViewHolder {
        @Bind(R.id.imageView) ImageView imageView;
        @Bind(R.id.nameTextView) TextView nameTextView;
        @Bind(R.id.dateTextView) TextView dateTextView;
        @Bind(R.id.ownerTextView) TextView ownerTextView;
    }

    public FileAdapter(Context context, List<File> files) {
        super(context, R.layout.list_item_video, files);
        width = context.getResources().getDimensionPixelSize(R.dimen.width_video_thumbnail_small);
        height = context.getResources().getDimensionPixelSize(R.dimen.height_video_thumbnail_small);
    }

    @Override
    protected ViewHolder createHolder(View view) {
        return new ViewHolder();
    }

    @Override
    protected void initView(int position, ViewHolder viewHolder) {
        File file = getItem(position);
        if (file.hasThumbnail()) {
            Picasso picasso = Picasso.with(context);
            picasso.setLoggingEnabled(BuildConfig.DEBUG);
            picasso
                .load(WebClient.getThumbnail(file, width, height))
                .into(viewHolder.imageView);
        } else {
            viewHolder.imageView.setImageResource(R.drawable.thumbnail_file);
        }

        viewHolder.nameTextView.setText(file.getCameraFileName());
        viewHolder.dateTextView.setText(DateUtil.Formatters.DATE_TIME.format(file.getRecordDate()));
        viewHolder.ownerTextView.setText(file.getUserName());
    }
}