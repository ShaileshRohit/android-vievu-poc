package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Case;
import com.vievu.vievusolution.util.DateUtil;

import java.util.List;

import butterknife.Bind;

public class CaseAdapter extends ArrayListAdapter<Case, CaseAdapter.ViewHolder> {

    protected static class ViewHolder {
        @Bind(R.id.caseNameTextView) TextView caseNameTextView;
        @Bind(R.id.caseStatusTextView) TextView caseStatusTextView;
        @Bind(R.id.caseDateTextView) TextView caseDateTextView;
        @Bind(R.id.caseSummaryTextView) TextView caseSummaryTextView;
        @Bind(R.id.caseOwnerTextView) TextView caseOwnerTextView;
    }

    public CaseAdapter(Context context, List<Case> cases) {
        super(context, R.layout.list_item_case, cases);
    }

    @Override
    protected ViewHolder createHolder(View view) {
        return new ViewHolder();
    }

    @Override
    protected void initView(int position, ViewHolder viewHolder) {
        Case item = getItem(position);
        viewHolder.caseNameTextView.setText(item.getCaseName());
        viewHolder.caseStatusTextView.setText(item.getStatusName());
        viewHolder.caseDateTextView.setText(DateUtil.Formatters.DATE_TIME_SHORT.format(item.getIncidentDate()));
        viewHolder.caseSummaryTextView.setText(item.getSummary().trim());
        viewHolder.caseOwnerTextView.setText(item.getUserName());
    }
}