package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.util.DateUtil;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GalleryAdapter extends SimpleCursorAdapter {

    public static class ViewHolder {

        @Bind(R.id.thumbnailImageView) ImageView thumbnailImageView;
        @Bind(R.id.durationTextView) TextView durationTextView;
        private int fileId;

        public int getFileId() {
            return fileId;
        }

        public void setFileId(int fileId) {
            this.fileId = fileId;
        }
    }

    public GalleryAdapter(Context context, Cursor cursor, String[] from) {
        super(context, R.layout.grid_item_video, cursor, from, null, 0);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            ButterKnife.bind(holder, view);
            view.setTag(holder);
        }
        int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.VideoColumns._ID));
        holder.setFileId(id);
        loadThumbnail(id, holder.thumbnailImageView, context);
        holder.durationTextView.setText(loadDuration(cursor));
    }

    protected void loadThumbnail(int id, ImageView imageView, Context context) {
        imageView.setImageBitmap(
            MediaStore.Video.Thumbnails.getThumbnail(
                context.getContentResolver(),
                id,
                MediaStore.Video.Thumbnails.MINI_KIND,
                null));
    }

    protected String loadDuration(Cursor cursor) {
        long millis = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.VideoColumns.DURATION));
        return DateUtil.Formatters.UTC.DURATION.format(new Date(millis));
    }
}
