package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;

public abstract class ArrayListAdapter<TItem, THolder> extends BaseAdapter {

    protected Context context;
    protected LayoutInflater inflater;
    protected int itemLayoutId;
    protected List<TItem> items;

    public ArrayListAdapter(Context context, int itemLayoutId, List<TItem> items) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.itemLayoutId = itemLayoutId;
        this.items = items != null ? items : new ArrayList<>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public TItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<TItem> getItems() {
        return items;
    }

    protected abstract THolder createHolder(View view);

    protected abstract void initView(int position, THolder viewHolder);

    protected View createView(ViewGroup parent) {
        return inflater.inflate(itemLayoutId, parent, false);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        THolder viewHolder;
        if (convertView == null) {
            convertView = createView(parent);
            viewHolder = createHolder(convertView);
            ButterKnife.bind(viewHolder, convertView);
            convertView.setTag(viewHolder);
        } else {
            //noinspection unchecked
            viewHolder = (THolder) convertView.getTag();
        }
        initView(position, viewHolder);
        return convertView;
    }

    public void addItem(TItem item) {
        if (item != null) {
            this.items.add(item);
            notifyDataSetChanged();
        }
    }

    public void addItems(Collection<TItem> items) {
        if (items != null) {
            this.items.addAll(items);
            notifyDataSetChanged();
        }
    }

    public void swapItems(Collection<TItem> items) {
        if (this.items != items) {
            this.items.clear();
            if (items != null) {
                this.items.addAll(items);
            }
        }
        notifyDataSetChanged();
    }

    public void replaceItems(List<TItem> items) {
        this.items = items != null ? items : new ArrayList<>();
        notifyDataSetChanged();
    }
}