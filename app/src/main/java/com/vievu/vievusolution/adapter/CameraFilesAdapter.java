package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.net.camera.ThumbnailLoader;
import com.vievu.vievusolution.ptpip.dataset.FileInfo;

import java.util.List;

import butterknife.Bind;

public class CameraFilesAdapter extends ArrayListAdapter<FileInfo, CameraFilesAdapter.ViewHolder> {

    public static final String PICASSO_TAG = "camera_file_thumbnail_request";

    public CameraFilesAdapter(Context context, List<FileInfo> items) {
        super(context, R.layout.list_item_camera_file, items);
    }

    static class ViewHolder {
        @Bind(R.id.imageView) ImageView imageView;
        @Bind(R.id.nameTextView) TextView nameTextView;
    }

    @Override
    protected ViewHolder createHolder(View view) {
        return new ViewHolder();
    }

    @Override
    protected void initView(int position, ViewHolder viewHolder) {
        FileInfo fileInfo = getItem(position);
        ThumbnailLoader.getInstance()
            .load(ThumbnailLoader.getUri(fileInfo.getId()))
            .tag(PICASSO_TAG)
            .fit()
            .into(viewHolder.imageView);
        viewHolder.nameTextView.setText(fileInfo.getName());
    }
}
