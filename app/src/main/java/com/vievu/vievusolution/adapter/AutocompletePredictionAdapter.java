package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;
import com.vievu.vievusolution.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;

public class AutocompletePredictionAdapter extends ArrayListAdapter<AutocompletePrediction, AutocompletePredictionAdapter.ViewHolder> implements Filterable {

    public interface GoogleApiProvider {
        GoogleApiClient getGoogleApiClient();
        LatLngBounds getSearchBounds();
    }

    public static class ViewHolder {
        @Bind(R.id.primaryTextView) TextView primaryTextView;
        @Bind(R.id.secondaryTextView) TextView secondaryTextView;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null) {
                ArrayList<AutocompletePrediction> resultsList = getAutocomplete(constraint);
                if (resultsList != null) {
                    results.values = resultsList;
                    results.count = resultsList.size();
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
                //noinspection unchecked
                swapItems((ArrayList<AutocompletePrediction>)results.values);
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    };

    private GoogleApiProvider googleApiProvider;

    public AutocompletePredictionAdapter(@NonNull Context context, @NonNull GoogleApiProvider apiProvider) {
        super(context, R.layout.list_item_place, null);
        this.googleApiProvider = apiProvider;
    }

    @Override
    protected ViewHolder createHolder(View view) {
        return new ViewHolder();
    }

    @Override
    protected void initView(int position, ViewHolder viewHolder) {
        AutocompletePrediction item = items.get(position);
        viewHolder.primaryTextView.setText(item.getPrimaryText(null).toString());
        viewHolder.secondaryTextView.setText(item.getSecondaryText(null).toString());
    }

    private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint) {
        if (googleApiProvider.getGoogleApiClient().isConnected()
            && constraint != null
            && constraint.length() > 2) {

            Log.d("AutocompleteAdapter", "Getting predictions for: " + constraint);
            PendingResult<AutocompletePredictionBuffer> results =
                Places.GeoDataApi.getAutocompletePredictions(
                        googleApiProvider.getGoogleApiClient(),
                        constraint.toString(),
                        googleApiProvider.getSearchBounds(),
                        new AutocompleteFilter.Builder().setTypeFilter(Place.TYPE_GEOCODE).build());

            AutocompletePredictionBuffer autocompletePredictions = results.await(5, TimeUnit.SECONDS);

            if (autocompletePredictions.getStatus().isSuccess()) {
                Log.d("AutocompleteAdapter", String.format("Loaded %d options", autocompletePredictions.getCount()));
                return DataBufferUtils.freezeAndClose(autocompletePredictions);
            } else {
                Log.d("AutocompleteAdapter", "Cannot load predictions: " + autocompletePredictions.getStatus());
                autocompletePredictions.release();
            }
        }
        return null;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
}
