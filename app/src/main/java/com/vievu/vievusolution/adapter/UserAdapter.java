package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.net.WebClient;

import java.util.List;

import butterknife.Bind;
import butterknife.BindDimen;

public class UserAdapter extends ArrayListAdapter<User, UserAdapter.ViewHolder> {

    protected static class ViewHolder {

        @Bind(R.id.imageView) ImageView imageView;
        @Bind(R.id.nameTextView) TextView nameTextView;
    }

    @BindDimen(R.dimen.side_user_image_small)
    int imageSize;
    private Picasso picasso;

    public UserAdapter(Context context, List<User> users) {
        super(context, R.layout.list_item_user, users);
        picasso = Picasso.with(context);
        picasso.setLoggingEnabled(BuildConfig.DEBUG);
    }

    @Override
    protected UserAdapter.ViewHolder createHolder(View view) {
        return new ViewHolder();
    }

    @Override
    protected void initView(int position, ViewHolder viewHolder) {
        User user = getItem(position);
        picasso
            .load(WebClient.getAvatarUrl(user.getUserId(), imageSize, imageSize))
            .placeholder(R.drawable.placeholder_user)
            .into(viewHolder.imageView);
        viewHolder.nameTextView.setText(user.getUserName());
    }
}
