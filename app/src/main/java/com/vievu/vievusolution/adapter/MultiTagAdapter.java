package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Tag;
import com.vievu.vievusolution.ui.UiUtil;
import com.vievu.vievusolution.ui.VievuCheckedTextView;

import java.lang.reflect.Field;
import java.util.List;

import butterknife.Bind;

public class MultiTagAdapter extends ArrayListAdapter<Tag, MultiTagAdapter.ViewHolder> {

    public static class ViewHolder extends SimpleViewHolder {

        int id;

        public int getId() {
            return id;
        }
    }

    public MultiTagAdapter(Context context, List<Tag> tags, VievuCheckedTextView.OnCheckedChangedListener onItemCheckedListener) {
        super(context, R.layout.list_item_multiple_choice, tags);
        this.onItemCheckedListener = onItemCheckedListener;
    }

    private Field checkMarkDrawableField;
    private VievuCheckedTextView.OnCheckedChangedListener onItemCheckedListener;

    private Drawable getCheckMarkDrawable(CheckedTextView view) {
        try {
            if (checkMarkDrawableField == null) {
                checkMarkDrawableField = CheckedTextView.class.getDeclaredField("mCheckMarkDrawable");
                checkMarkDrawableField.setAccessible(true);
            }
            return (Drawable) checkMarkDrawableField.get(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getTagId();
    }

    @Override
    protected ViewHolder createHolder(View view) {
        return new ViewHolder();
    }

    @Override
    protected void initView(int position, ViewHolder viewHolder) {
        Tag item = getItem(position);
        viewHolder.textView.setText(item.getTagName());
        viewHolder.id = item.getTagId();
    }

    @Override
    protected View createView(ViewGroup parent) {
        VievuCheckedTextView view = (VievuCheckedTextView) super.createView(parent);
        Drawable checkMarkDrawable;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            checkMarkDrawable = view.getCheckMarkDrawable();
        } else {
            checkMarkDrawable = getCheckMarkDrawable(view);
        }
        if (checkMarkDrawable != null) {
            UiUtil.colorizeDrawable(context, checkMarkDrawable, R.color.accent);
        }
        view.setOnCheckedChangedListener(onItemCheckedListener);
        return view;
    }
}
