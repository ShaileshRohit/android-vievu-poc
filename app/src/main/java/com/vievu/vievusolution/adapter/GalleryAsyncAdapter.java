package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import com.vievu.vievusolution.R;

import java.util.HashMap;

import butterknife.ButterKnife;

public class GalleryAsyncAdapter extends GalleryAdapter {

    private static class LoadThumbnailTask extends AsyncTask<Void, Void, Bitmap> {

        private int origId;
        private Context context;
        private ImageView view;

        public LoadThumbnailTask(int origId, Context context, ImageView view) {
            this.origId = origId;
            this.context = context.getApplicationContext();
            this.view = view;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            return MediaStore.Video.Thumbnails.getThumbnail(
                    context.getContentResolver(),
                    origId,
                    MediaStore.Video.Thumbnails.MINI_KIND,
                    null);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (!isCancelled()) {
                if (bitmap != null) {
                    view.setImageBitmap(bitmap);
                } else {
                    view.setImageResource(R.drawable.photo_placeholder);
                }
            }
        }

        public void cancelLoading() {
            MediaStore.Video.Thumbnails.cancelThumbnailRequest(context.getContentResolver(), origId);
            cancel(false);
        }
    }

    private HashMap<ImageView, LoadThumbnailTask> loadThumbnailTasks = new HashMap<>();

    public GalleryAsyncAdapter(Context context, Cursor cursor, String[] from) {
        super(context, cursor, from);
    }

    @Override
    protected void loadThumbnail(int id, ImageView imageView, Context context) {
        imageView.setImageResource(R.drawable.photo_placeholder);
        LoadThumbnailTask task = loadThumbnailTasks.get(imageView);
        if (task != null) {
            task.cancelLoading();
        }
        task = new LoadThumbnailTask(id, context, imageView);
        task.execute();
        loadThumbnailTasks.put(imageView, task);
    }
}
