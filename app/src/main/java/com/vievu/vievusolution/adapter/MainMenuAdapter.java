package com.vievu.vievusolution.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;

import com.annimon.stream.function.Consumer;
import com.vievu.vievusolution.MainActivity;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.bean.Permissions;
import com.vievu.vievusolution.data.UserDataRepository;
import com.vievu.vievusolution.details.camera.CameraFilesActivity;
import com.vievu.vievusolution.filtering.FilterActivity;
import com.vievu.vievusolution.filtering.FilterBundle;
import com.vievu.vievusolution.media.LiveViewActivity;

import java.util.ArrayList;

import butterknife.Bind;

public class MainMenuAdapter extends ArrayListAdapter<MainMenuAdapter.MainMenuItem, MainMenuAdapter.ViewHolder> {

    public static class MainMenuItem {

        private int id;
        private int drawable;
        private boolean requiresOnlineMode;
        private Consumer<MainActivity> clickHandler;

        MainMenuItem(int id, int drawable, boolean requiresOnlineMode, Consumer<MainActivity> clickHandler) {
            this.id = id;
            this.drawable = drawable;
            this.clickHandler = clickHandler;
            this.requiresOnlineMode = requiresOnlineMode;
        }

        public int getId() {
            return id;
        }

        public int getDrawable() {
            return drawable;
        }

        boolean requiresOnlineMode() {
            return requiresOnlineMode;
        }

        public void handleClick(MainActivity activity) {
            if (!requiresOnlineMode || activity.isInOnlineMode()) {
                clickHandler.accept(activity);
            }
        }
    }

    protected static class ViewHolder extends SimpleViewHolder {
        @Bind(android.R.id.icon) ImageView imageView;
        @Bind(android.R.id.background) View background;
    }

    public MainMenuAdapter(Context context) {
        super(context, R.layout.list_item_main_menu, null);
        this.items = new ArrayList<>();
        Permissions permissions = UserDataRepository.getInstance().getPermissions();
        if (permissions != null) {
            items.add(new MainMenuItem(
                    R.string.main_menu_live_view,
                    R.drawable.ic_live_view,
                    false,
                    (activity) -> activity.goToCameraPage(LiveViewActivity.class)));
            if (permissions.canEditVideos()) {
                items.add(new MainMenuItem(
                        R.string.main_menu_metadata,
                        R.drawable.ic_metadata,
                        false,
                        (activity) -> activity.goToCameraPage(CameraFilesActivity.class)));
            }
            if (permissions.canViewVideos()) {
                items.add(new MainMenuItem(
                        R.string.main_menu_videos,
                        R.drawable.ic_video,
                        true,
                        (activity) -> FilterActivity.start(activity, FilterBundle.FILTER_BUNDLE_VIDEOS)));
            }
            if (permissions.canViewCases()) {
                items.add(new MainMenuItem(
                        R.string.main_menu_case_files,
                        R.drawable.ic_case,
                        true,
                        (activity) -> FilterActivity.start(activity, FilterBundle.FILTER_BUNDLE_CASES)));
            }
            if (permissions.canDownloadVideos()) {
                items.add(new MainMenuItem(
                        R.string.main_menu_upload,
                        R.drawable.ic_upload,
                        true,
                        MainActivity::checkPermissionsBeforeUpload));
            }
        }
    }

    @Override
    protected ViewHolder createHolder(View view) {
        return new ViewHolder();
    }

    @Override
    protected void initView(int position, ViewHolder viewHolder) {
        MainMenuItem item = getItem(position);
        boolean isEnabled = !item.requiresOnlineMode() || ((MainActivity)context).isInOnlineMode();
        viewHolder.textView.setText(item.getId());
        viewHolder.textView.setEnabled(isEnabled);
        viewHolder.background.setEnabled(isEnabled);
        viewHolder.imageView.setImageResource(item.getDrawable());
        viewHolder.imageView.setEnabled(isEnabled);
        viewHolder.imageView.setAlpha(isEnabled ? 1.0f : 0.3f);
    }
}






/*if(preferences.getBoolean(CommonConstants.KEY_IDP_ENABLED,false))
        {
            UserPermissoinModel model=new Gson().fromJson(preferences.getString(CommonConstants.KEY_USER_PERMISSION,""),UserPermissoinModel.class);
            permissions=new Permissions();
            permissions.setCanEditVideos(model.isEditVideos());
            permissions.setVideosAccess(model.getViewVideos());
            permissions.setViewCases(model.getViewCases());
            permissions.setCanDownloadVideos(model.isDownloadVideos());
        }
        else {
          permissions= UserDataRepository.getInstance().getPermissions();
        }*/