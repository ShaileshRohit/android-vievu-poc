package com.vievu.vievusolution;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.media.MediaService;
import com.vievu.vievusolution.media.mjpeg.RtspClient;
import com.vievu.vievusolution.ptpip.PtpIpClient;
import com.vievu.vievusolution.ptpip.constants.CameraMode;
import com.vievu.vievusolution.ptpip.constants.FileFormats;
import com.vievu.vievusolution.ptpip.dataset.CameraStatus;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;

@SuppressWarnings("WeakerAccess")
public class CameraClient {

    //region Singleton

    private static CameraClient instance;

    public static CameraClient getInstance() {
        if (instance == null) {
            instance = new CameraClient();
        }
        return instance;
    }

    //endregion

    //region Execution engine

    public interface Callback {
        void run() throws IOException;
    }

    public static class Event {

        private boolean isConnected;

        public boolean isConnected() {
            return isConnected;
        }

        Event(boolean isConnected) {
            this.isConnected = isConnected;
        }
    }

    private static class ReportingTask extends AsyncTask<Callable<?>, Void, Object> {

        @Override
        protected final Object doInBackground(Callable<?>... params) {
            try {
                return params[0].call();
            } catch (Exception e) {
                e.printStackTrace();
                instance.ptpIpClient.disconnect(true);
                instance.rtspClient.disconnect();
            }
            return new Event(false);
        }

        @Override
        protected void onPostExecute(Object event) {
            if (event != null) {
                EventBus.getDefault().post(event);
            }
        }
    }

    private static class Task extends AsyncTask<Callback, Void, Object> {

        @Override
        protected final Object doInBackground(Callback... params) {
            try {
                if (instance.ptpIpClient.isConnected()) {
                    run(params[0]);
                    return null;
                } else if (!instance.rtspRequired || instance.rtspClient.reconnect()) {
                    Log.d("CameraClient", "Mandatory RTSP");
                    instance.rtspRequired = false;
                    if (instance.ptpIpClient.connect(
                            CommonConstants.DEFAULT_CAMERA_IP,
                            CommonConstants.DEFAULT_PORT_PTPIP)) {
                        EventBus.getDefault().post(new Event(true));
                        run(params[0]);
                        return null;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.d("CameraClient", "Seems that PTP-IP is disconnected prematurely");
                e.printStackTrace();
            }
            handleError(params[1]);
            return null;
        }

        private void run(Callback callback) throws IOException {
            if (callback != null) {
                callback.run();
            }
        }

        private void handleError(Callback callback) {
            instance.ptpIpClient.disconnect(true);
            instance.rtspClient.disconnect();
            try {
                run(callback);
            } catch (IOException e) {
                e.printStackTrace();
            }
            CameraClient.getInstance().setRtspRequired(true);
            Log.d("CameraClient", "Execution failed");
            EventBus.getDefault().post(new Event(false));
        }
    }

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    private void execute(Callback onSuccess) {
        execute(onSuccess, null);
    }

    private void execute(Callback onSuccess, Callback onError) {
        new Task().executeOnExecutor(executor, onSuccess, onError);
    }

    //endregion

    public static class Actions {
        public static final int GET_FILE_LIST = -0x11000000;
    }

    public static class Mode {

        @SuppressLint("SupportAnnotationUsage")
        @CameraMode.Value
        private short value;

        @SuppressLint("SupportAnnotationUsage")
        @CameraMode.Value
        public short getValue() {
            return value;
        }

        public boolean isError() {
            return value == CameraMode.ERROR;
        }

        public Mode(@SuppressLint("SupportAnnotationUsage") @CameraMode.Value short value) {
            this.value = value;
        }
    }

    private boolean rtspRequired = true;
    private boolean preRecordRestoreRequested = false;
    private boolean mediaServiceRequested = false;

    private PtpIpClient ptpIpClient = new PtpIpClient();

    private RtspClient rtspClient = new RtspClient();

    private CameraClient() {}

    public ExecutorService getExecutor() {
        return executor;
    }

    public PtpIpClient getPtpIpClient() {
        return ptpIpClient;
    }

    public RtspClient getRtspClient() {
        return rtspClient;
    }

    public boolean isPreRecordRestoreRequested() {
        return preRecordRestoreRequested;
    }

    public boolean isMediaServiceRequested() {
        return mediaServiceRequested;
    }

    public void setRtspRequired(boolean rtspRequired) {
        this.rtspRequired = rtspRequired;
    }

    public void connect() {
        execute(null);
    }

    public void compareAndSetCameraMode(@SuppressLint("SupportAnnotationUsage") @CameraMode.Value short newMode) {
        execute(() -> {
            ptpIpClient.compareAndSetCameraMode(newMode);
            EventBus.getDefault().post(new Mode(newMode));
        });
    }

    public void requestMediaService(String videoUrl) {
        Log.d("CameraClient", "Requesting MediaService...");
        if (!this.mediaServiceRequested) {
            this.mediaServiceRequested = true;
            execute(() -> {
                Log.d("CameraClient", "Cleaning up...");
                if (rtspClient.isConnected()) {
                    rtspClient.stop();
                } else {
                    rtspClient.disconnect();
                }

                if (videoUrl != null && !CommonConstants.DEFAULT_VIDEO_URI.equals(videoUrl)) {
                    Log.d("CameraClient", "MediaService: Requesting direct video");
                    ptpIpClient.compareAndSetCameraMode(CameraMode.SHARE);
                } else {
                    Log.d("CameraClient", "MediaService: Requesting live stream, camera mode is unchanged");
                }
                Log.d("CameraClient", "Connecting RTSP client...");
                rtspClient.connect(videoUrl);
                this.mediaServiceRequested = false;
            },
            () -> this.mediaServiceRequested = false);
        } else {
            Log.d("CameraClient", "MediaService already requested");
        }
    }

    public boolean restorePreRecord() {
        Log.d("CameraClient", "Requesting pre-record restore...");
        if (!this.preRecordRestoreRequested) {
            this.preRecordRestoreRequested = true;
            execute(
                () -> {
                    Log.d("CameraClient", "Restoring pre-record...");
                    short mode = ptpIpClient.getCameraMode();
                    Log.d("CameraClient", String.format("Current mode: %d", mode));
                    CameraStatus cameraStatus = ptpIpClient.getCameraStatus();
                    EventBus.getDefault().post(cameraStatus);
                    Log.d("CameraClient", "Current camera status: " + cameraStatus);
                    if (mode != CameraMode.VIDEO_OFF && mode != CameraMode.ERROR) {
                        Log.d("CameraClient", "Change mode required");
                        Log.d("CameraClient", "RTSP 1 " + rtspClient.reconnect());
                        ptpIpClient.setCameraMode(CameraMode.VIDEO_OFF);
                        Log.d("CameraClient", "Mode changed");
                        Log.d("CameraClient", "RTSP 2 " + rtspClient.reconnect());
                        mode = CameraMode.VIDEO_OFF;
                    }
                    this.preRecordRestoreRequested = false;
                    EventBus.getDefault().post(new Mode(mode));
                },
                () -> this.preRecordRestoreRequested = false);
            return true;
        } else {
            Log.d("CameraClient", "Pre-record restore already requested");
            return false;
        }
    }

    public void getVideoFilesAsync(Callback onError) {
        execute(() -> {
            Log.d("CameraClient", "File list: Checking camera mode");
            ptpIpClient.compareAndSetCameraMode(CameraMode.SHARE);
            RequestCompletedEvent event = new RequestCompletedEvent<>(
                Actions.GET_FILE_LIST,
                Stream
                    .of(ptpIpClient.getFileList(new short[]{FileFormats.AVI, FileFormats.MPEG}))
                    .sorted((first, second) -> first.getDateCreated().compareTo(second.getDateCreated()))
                    .collect(Collectors.toList()));
            EventBus.getDefault().post(event);
        }, onError);
    }

    public void getStorageInfoAsync() {
        execute(() -> EventBus.getDefault().post(ptpIpClient.getCameraStatus()));
    }

    public void stopVideoAsync() {
        execute(rtspClient::stop);
    }

    public void disconnectAsync() {
        execute(this::disconnectImmediately);
    }

    public void disconnectImmediately() {
        ptpIpClient.disconnect(false);
        rtspClient.dispose();
        rtspClient.disconnect();
        setRtspRequired(true);
    }
}
