package com.vievu.vievusolution.collector;

import com.annimon.stream.Collector;
import com.annimon.stream.function.BiConsumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Supplier;

/**
 * Joins a sequence of T items into single {@code String}
 * @param <T> Item type of supplied collection.
 */
public class JoinWithSeparatorCollector<T> implements Collector<T, StringBuilder, String> {

    private String separator;
    private Function<T, String> toString;

    /**
     * Joins a sequence of T items into single {@code String}.
     * Inserts {@param separator} between every two elements.
     *
     * @param separator A separator between items. For example space or comma.
     * @param toString A function to convert each of supplied T items to a string.
     */
    public JoinWithSeparatorCollector(String separator, Function<T, String> toString) {
        this.separator = separator;
        this.toString = toString;
    }

    @Override
    public Supplier<StringBuilder> supplier() {
        return StringBuilder::new;
    }

    @Override
    public BiConsumer<StringBuilder, T> accumulator() {
        return (t, u) -> t.append(separator).append(toString.apply(u));
    }

    @Override
    public Function<StringBuilder, String> finisher() {
        return (builder) -> builder.delete(0, separator.length()).toString();
    }
}
