package com.vievu.vievusolution.collector;

import com.annimon.stream.Collector;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiConsumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Supplier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * Transforms collection of TValue items into a map { TKey -> TResult }
 *
 * Groups collection of TValue items to map { TKey -> [TValue, ..., TValue] } and then reduces
 * each of [TValue, ..., TValue] arrays to single TResult result. Provides result as map { TKey -> TResult }
 * @param <TKey> Key type of resulting map.
 * @param <TValue> Item type of supplied collection.
 * @param <TResult> Value type of resulting map.
 */

public class GroupByReduceCollector<TKey, TValue, TResult> implements Collector<TValue, Map<TKey, List<TValue>>, Map<TKey, TResult>> {

    private Function<TValue, TKey> keyFunction;
    private Collector<TValue, ?, TResult> resultCollector;

    /**
     * Transforms collection of TValue items into a map { TKey -> TResult }
     * @param keyFunction A function to extract key from each of TValue items.
     * @param resultCollector A collector to reduce each group of TValue items with the same TKey key to one TResult value.
     */
    public GroupByReduceCollector(
            Function<TValue, TKey> keyFunction,
            Collector<TValue, ?, TResult> resultCollector) {
        this.keyFunction = keyFunction;
        this.resultCollector = resultCollector;
    }

    @Override
    public Supplier<Map<TKey, List<TValue>>> supplier() {
        return HashMap<TKey, List<TValue>>::new;
    }

    @Override
    public BiConsumer<Map<TKey, List<TValue>>, TValue> accumulator() {
        return (accumulator, item) -> {
            TKey key = keyFunction.apply(item);
            List<TValue> values = accumulator.get(key);
            if (values == null) {
                values = new ArrayList<>();
                accumulator.put(key, values);
            }
            values.add(item);
        };
    }

    @Override
    public Function<Map<TKey, List<TValue>>, Map<TKey, TResult>> finisher() {
        return (acc) -> {
            HashMap<TKey, TResult> results = new HashMap<>(acc.size());
            Stream.of(acc.entrySet())
                    .forEach(entry -> results.put(
                            entry.getKey(),
                            Stream.of(entry.getValue()).collect(resultCollector)));
            return results;
        };
    }
}
