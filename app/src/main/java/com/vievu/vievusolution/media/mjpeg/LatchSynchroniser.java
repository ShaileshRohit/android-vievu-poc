package com.vievu.vievusolution.media.mjpeg;

import java.util.concurrent.CountDownLatch;

public interface LatchSynchroniser {

    boolean isPaused();

    CountDownLatch getLatch();

    void onClientError(int id, int cause);
}
