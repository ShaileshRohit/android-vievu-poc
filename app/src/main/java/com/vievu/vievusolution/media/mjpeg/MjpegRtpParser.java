package com.vievu.vievusolution.media.mjpeg;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.Semaphore;

public class MjpegRtpParser {

    public interface OnJpegLoadedListener {

        void onJpegLoaded(FrameData framedata);
    }

    public static class FrameData {
        public boolean isShown;
        public int frameTime;
        public Bitmap frame;
    }

    public static class InMemoryFrameBuffer {

        private boolean isLoggingEnabled;
        private int writePosition;
        private int readPosition;
        private Semaphore semaphore;
        private FrameData[] frames;
        private BitmapFactory.Options frameOptions = new BitmapFactory.Options();

        public void setLoggingEnabled(boolean loggingEnabled) {
            isLoggingEnabled = loggingEnabled;
        }

        InMemoryFrameBuffer(int capacity) {
            semaphore = new Semaphore(capacity);
            frames = new FrameData[capacity];
            for (int i = 0; i < capacity; i++) {
                frames[i] = new FrameData();
            }

            frameOptions.inMutable = true;
            frameOptions.inSampleSize = 1;
        }

        public void put(int frameTime, UnsafeByteArrayOutputStream bitmapStream) throws InterruptedException {
            if (isLoggingEnabled) {
                Log.d("InMemoryFrameBuffer", String.format("BEFORE PUT: Quotas: %d, timestamp: %d, read: %d, write: %d",
                        semaphore.availablePermits(), frameTime, readPosition, writePosition));
            }
            semaphore.acquire();
            FrameData frameData = frames[writePosition];
            frameData.frameTime = frameTime;
            frameOptions.inBitmap = frameData.frame;
            frameData.frame = BitmapFactory.decodeStream(
                new ByteArrayInputStream(bitmapStream.getInnerBuffer(), 0, bitmapStream.getCount()),
                null,
                frameOptions);
            frameData.isShown = false;
            this.writePosition = advancePosition(this.writePosition);
        }

        public FrameData peek() {
            return frames[this.readPosition];
        }

        public void release() {
            frames[this.readPosition].isShown = true;
            this.readPosition = advancePosition(this.readPosition);
            semaphore.release();
            if (isLoggingEnabled) {
                Log.d("InMemoryFrameBuffer", String.format("AFTER READ: Quotas: %d, read: %d, write: %d",
                        semaphore.availablePermits(), readPosition, writePosition));
            }
        }

        private int advancePosition(int position) {
            return (position == frames.length - 1) ? 0 : position + 1;
        }

        public void recycle() {
            frameOptions.inBitmap = null;
            for (FrameData frameData : frames) {
                if (frameData.frame != null) {
                    frameData.frame.recycle();
                }
            }
        }
    }

    private static final int FRAME_BUFFER_SIZE = 5;

    //region Tables

    private byte[] lum_dc_codelens = new byte[] {0, 1, 5, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0 };

    private byte[] lum_dc_symbols = new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

    private byte[] lum_ac_codelens = new byte[] { 0, 2, 1, 3, 3, 2, 4, 3, 5, 5, 4, 4, 0, 0, 1, 0x7d };

    private byte[] lum_ac_symbols = new byte[] {
            0x01, 0x02, 0x03, 0x00, 0x04, 0x11, 0x05, 0x12,
            0x21, 0x31, 0x41, 0x06, 0x13, 0x51, 0x61, 0x07,
            0x22, 0x71, 0x14, 0x32, (byte)0x81, (byte)0x91, (byte)0xa1, 0x08,
            0x23, 0x42, (byte)0xb1, (byte)0xc1, 0x15, 0x52, (byte)0xd1, (byte)0xf0,
            0x24, 0x33, 0x62, 0x72, (byte)0x82, 0x09, 0x0a, 0x16,
            0x17, 0x18, 0x19, 0x1a, 0x25, 0x26, 0x27, 0x28,
            0x29, 0x2a, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
            0x3a, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
            0x4a, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
            0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
            0x6a, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
            0x7a, (byte)0x83, (byte)0x84, (byte)0x85, (byte)0x86, (byte)0x87, (byte)0x88, (byte)0x89,
            (byte)0x8a, (byte)0x92, (byte)0x93, (byte)0x94, (byte)0x95, (byte)0x96, (byte)0x97, (byte)0x98,
            (byte)0x99, (byte)0x9a, (byte)0xa2, (byte)0xa3, (byte)0xa4, (byte)0xa5, (byte)0xa6, (byte)0xa7,
            (byte)0xa8, (byte)0xa9, (byte)0xaa, (byte)0xb2, (byte)0xb3, (byte)0xb4, (byte)0xb5, (byte)0xb6,
            (byte)0xb7, (byte)0xb8, (byte)0xb9, (byte)0xba, (byte)0xc2, (byte)0xc3, (byte)0xc4, (byte)0xc5,
            (byte)0xc6, (byte)0xc7, (byte)0xc8, (byte)0xc9, (byte)0xca, (byte)0xd2, (byte)0xd3, (byte)0xd4,
            (byte)0xd5, (byte)0xd6, (byte)0xd7, (byte)0xd8, (byte)0xd9, (byte)0xda, (byte)0xe1, (byte)0xe2,
            (byte)0xe3, (byte)0xe4, (byte)0xe5, (byte)0xe6, (byte)0xe7, (byte)0xe8, (byte)0xe9, (byte)0xea,
            (byte)0xf1, (byte)0xf2, (byte)0xf3, (byte)0xf4, (byte)0xf5, (byte)0xf6, (byte)0xf7, (byte)0xf8,
            (byte)0xf9, (byte)0xfa };

    private byte[] chm_dc_codelens = new byte[] { 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0 };

    private byte[] chm_dc_symbols = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

    private byte[] chm_ac_codelens = new byte[] { 0, 2, 1, 2, 4, 4, 3, 4, 7, 5, 4, 4, 0, 1, 2, 0x77 };

    private byte[] chm_ac_symbols = new byte[] {
            0x00, 0x01, 0x02, 0x03, 0x11, 0x04, 0x05, 0x21,
            0x31, 0x06, 0x12, 0x41, 0x51, 0x07, 0x61, 0x71,
            0x13, 0x22, 0x32, (byte)0x81, 0x08, 0x14, 0x42, (byte)0x91,
            (byte)0xa1, (byte)0xb1, (byte)0xc1, 0x09, 0x23, 0x33, 0x52, (byte)0xf0,
            0x15, 0x62, 0x72, (byte)0xd1, 0x0a, 0x16, 0x24, 0x34,
            (byte)0xe1, 0x25, (byte)0xf1, 0x17, 0x18, 0x19, 0x1a, 0x26,
            0x27, 0x28, 0x29, 0x2a, 0x35, 0x36, 0x37, 0x38,
            0x39, 0x3a, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
            0x49, 0x4a, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58,
            0x59, 0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
            0x69, 0x6a, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
            0x79, 0x7a, (byte)0x82, (byte)0x83, (byte)0x84, (byte)0x85, (byte)0x86, (byte)0x87,
            (byte)0x88, (byte)0x89, (byte)0x8a, (byte)0x92, (byte)0x93, (byte)0x94, (byte)0x95, (byte)0x96,
            (byte)0x97, (byte)0x98, (byte)0x99, (byte)0x9a, (byte)0xa2, (byte)0xa3, (byte)0xa4, (byte)0xa5,
            (byte)0xa6, (byte)0xa7, (byte)0xa8, (byte)0xa9, (byte)0xaa, (byte)0xb2, (byte)0xb3, (byte)0xb4,
            (byte)0xb5, (byte)0xb6, (byte)0xb7, (byte)0xb8, (byte)0xb9, (byte)0xba, (byte)0xc2, (byte)0xc3,
            (byte)0xc4, (byte)0xc5, (byte)0xc6, (byte)0xc7, (byte)0xc8, (byte)0xc9, (byte)0xca, (byte)0xd2,
            (byte)0xd3, (byte)0xd4, (byte)0xd5, (byte)0xd6, (byte)0xd7, (byte)0xd8, (byte)0xd9, (byte)0xda,
            (byte)0xe2, (byte)0xe3, (byte)0xe4, (byte)0xe5, (byte)0xe6, (byte)0xe7, (byte)0xe8, (byte)0xe9,
            (byte)0xea, (byte)0xf2, (byte)0xf3, (byte)0xf4, (byte)0xf5, (byte)0xf6, (byte)0xf7, (byte)0xf8,
            (byte)0xf9, (byte)0xfa };

    //endregion

    //region RFC 2435 table methods

    private byte[] staticDataOne = new byte[] {(byte)0xff, (byte)0xc0, 0, 17, 8, 0, 0, 0, 0, 3, 0, 0, 0, 1, 0x11, 1, 2, 0x11, 1 };

    private byte[] staticDataTwo = new byte[] { (byte)0xff, (byte)0xda, 0, 12, 3, 0, 0, 1, 0x11, 2, 0x11, 0, 63, 0 };

    private void prepareBitmapStream(int height, int width, int type) throws IOException {
        this.outputStream.write(0xFF);
        this.outputStream.write(0xD8);
        MakeQuantHeader(this.outputStream, luma, 0);
        MakeQuantHeader(this.outputStream, chroma, 1);

        staticDataOne[5] = (byte)((height >> 8) & 255);
        staticDataOne[6] = (byte)(height & 255);
        staticDataOne[7] = (byte)((width >> 8) & 255);
        staticDataOne[8] = (byte)(width & 255);
        staticDataOne[11] = (byte)(type == 0 ? 0x21 : 0x22);

        this.outputStream.write(staticDataOne);

        MakeHuffmanHeader(this.outputStream, lum_dc_codelens,
                lum_dc_codelens.length,
                lum_dc_symbols,
                lum_dc_symbols.length, 0, 0);
        MakeHuffmanHeader(this.outputStream, lum_ac_codelens,
                lum_ac_codelens.length,
                lum_ac_symbols,
                lum_ac_symbols.length, 0, 1);
        MakeHuffmanHeader(this.outputStream, chm_dc_codelens,
                chm_dc_codelens.length,
                chm_dc_symbols,
                chm_dc_symbols.length, 1, 0);
        MakeHuffmanHeader(this.outputStream, chm_ac_codelens,
                chm_ac_codelens.length,
                chm_ac_symbols,
                chm_ac_symbols.length, 1, 1);
        this.outputStream.write(staticDataTwo);
    }

    private void appendBitmapFinalizer(ByteArrayOutputStream stream) {
        stream.write(0xFF);
        stream.write(0xD9);
    }

    private void MakeQuantHeader(ByteArrayOutputStream stream, byte[] table, int tableId) throws IOException {
        byte[] quantTable = new byte[64 + 5];
        quantTable[0] = (byte)0xff;
        quantTable[1] = (byte)0xdb;
        quantTable[2] = 0;
        quantTable[3] = 67;
        quantTable[4] = (byte)tableId;
        System.arraycopy(table, 0, quantTable, 5, 64);
        stream.write(quantTable);
    }

    private void MakeHuffmanHeader(ByteArrayOutputStream stream, byte[] codelens, int ncodes,
                                   byte[] symbols, int nsymbols, int tableNo, int tableClass) throws IOException {
        stream.write(0xff);
        stream.write(0xc4);
        stream.write(0);
        stream.write(3 + ncodes + nsymbols);
        stream.write((tableClass << 4) | tableNo);
        stream.write(codelens);
        stream.write(symbols);
    }

    //endregion

    private UnsafeByteArrayOutputStream outputStream = new UnsafeByteArrayOutputStream();

    private int size = 0;

    private byte[] luma = new byte[64];

    private byte[] chroma = new byte[64];

    private boolean corrupted = false;

    private int frameTime;
    private int rtpOffset = 2000000000;
    private InMemoryFrameBuffer frameBuffer = new InMemoryFrameBuffer(FRAME_BUFFER_SIZE);

    public InMemoryFrameBuffer getFrameBuffer() {
        return frameBuffer;
    }

    public void setRtpOffset(int rtpOffset) {
        this.rtpOffset = rtpOffset;
    }

    void nextPacket(byte[] rtpPacket, int length) throws IOException, InterruptedException {
        int bitmapOffset = 12;
        boolean endFrameMarker = (rtpPacket[1] & 128) == 128;
        boolean hasExtensionHeader = (rtpPacket[0] & 16) == 16;
        if (hasExtensionHeader) {
            // Append extension header description length (4) and its length itself (14-15th bytes)
            bitmapOffset +=
                4 + 4 * (
                ((rtpPacket[14] <<  8) & 0x0000FF00) |
                ( rtpPacket[15]        & 0x000000FF));
        }
        int dataOffset =
                ((rtpPacket[bitmapOffset + 1] << 16) & 0x00FF0000) |
                ((rtpPacket[bitmapOffset + 2] <<  8) & 0x0000FF00) |
                ( rtpPacket[bitmapOffset + 3]        & 0x000000FF);
        corrupted |= size != dataOffset;
        int bitmapPayloadOffset = bitmapOffset + 8;
        if (dataOffset == 0) {
            reset();
            // Append Restart Marker header length (4 bytes) and Quantization Tables length (128 bytes)
            bitmapPayloadOffset += 132;
            System.arraycopy(rtpPacket, bitmapOffset + 12, luma, 0, 64);
            System.arraycopy(rtpPacket, bitmapOffset + 12 + 64, chroma, 0, 64);
            prepareBitmapStream(
                    rtpPacket[bitmapOffset + 7] << 3, // Frame height
                    rtpPacket[bitmapOffset + 6] << 3, // Frame width
                    rtpPacket[bitmapOffset + 4]);     // Image type
            frameTime =
                    ((rtpPacket[4] << 24) & 0xFF000000) |
                    ((rtpPacket[5] << 16) & 0x00FF0000) |
                    ((rtpPacket[6] <<  8) & 0x0000FF00) |
                    ( rtpPacket[7]        & 0x000000FF);
            frameTime -= rtpOffset;
            // Convert timestamp to millis assuming it's sampled at 90000 Hz frequency as described in RFC 5371
            frameTime /= 90;
        }
        size += (length - bitmapPayloadOffset);
        if (!corrupted) {
            outputStream.write(rtpPacket, bitmapPayloadOffset, length - bitmapPayloadOffset);
            if (endFrameMarker) {
                appendBitmapFinalizer(outputStream);

                frameBuffer.put(frameTime, outputStream);
                outputStream.reset();
            }
        }
    }

    void reset() {
        corrupted = false;
        outputStream.reset();
        size = 0;
    }
}
