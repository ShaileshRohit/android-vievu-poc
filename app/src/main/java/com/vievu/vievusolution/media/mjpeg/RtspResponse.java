package com.vievu.vievusolution.media.mjpeg;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("WeakerAccess")
public class RtspResponse {

    public static RtspResponse getFromReader(BufferedReader reader) throws IOException {

        RtspResponse response = new RtspResponse();
        Pattern beginPattern = Pattern.compile("RTSP/1.0 (\\d+) (\\w+)");
        String line = reader.readLine();
        Matcher matcher = beginPattern.matcher(line);
        if (matcher.matches()) {
            response.code = Integer.parseInt(matcher.group(1));
            response.status = matcher.group(2);
            if (response.code == 200) {
                RtspHeader header;
                response.headers = new HashMap<>();
                while (!(line = reader.readLine()).isEmpty()) {
                    header = RtspHeader.parse(line);
                    response.headers.put(header.getName(), header);
                }
                RtspHeader contentLength = response.header(RtspHeader.HEADER_CONTENT_LENGTH);
                if (contentLength != null) {
                    int length = Integer.parseInt(contentLength.getData());
                    char[] data = new char[length];
                    //noinspection ResultOfMethodCallIgnored
                    reader.read(data, 0, length);
                    response.body = new String(data);
                }
            }
        }
        return response;
    }

    private int code;
    private String status;
    private HashMap<String, RtspHeader> headers;
    private String body;

    public int getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public HashMap<String, RtspHeader> getHeaders() {
        return headers;
    }

    public String getBody() {
        return body;
    }

    public RtspHeader header(String name) {
        return headers != null ? headers.get(name) : null;
    }

    private RtspResponse() {}

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("RTSP/1.0 ");
        builder.append(code).append(' ').append(status).append('\n');
        if (headers != null) {
            for (RtspHeader header : headers.values()) {
                builder.append(header).append('\n');
            }
        }
        builder.append(body);
        return builder.toString();
    }
}
