package com.vievu.vievusolution.media.mjpeg;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;


import com.vievu.vievusolution.media.MediaService;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Pcms16beReceiver extends Thread {

    private static final int MAX_AUDIO_FRAGMENT_LENGTH = 1500;

    private static final int AUDIO_SAMPLE_RATE = 44100;

    private final int id;

    private final DatagramSocket audioSocket;

    private final LatchSynchroniser synchroniser;

    private boolean isPlayingAudio = false;

    private short[] audioBuffer = new short[MAX_AUDIO_FRAGMENT_LENGTH / 2];

    private byte[] rawBuffer = new byte[MAX_AUDIO_FRAGMENT_LENGTH];

    public Pcms16beReceiver(int id, DatagramSocket audioSocket, LatchSynchroniser synchroniser) {
        this.id = id;
        this.audioSocket = audioSocket;
        this.synchroniser = synchroniser;
    }

    @Override
    public void run() {
        if (!isPlayingAudio) {
            isPlayingAudio = true;
            DatagramPacket packet;
            int actualLength;
            int bufferSize = AudioTrack.getMinBufferSize(
                    AUDIO_SAMPLE_RATE,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT);
            AudioTrack audioTrack = new AudioTrack(
                    AudioManager.STREAM_MUSIC,
                    AUDIO_SAMPLE_RATE,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    bufferSize,
                    AudioTrack.MODE_STREAM);
            audioTrack.play();
            try {
                while (!isInterrupted()) {
                    if (synchroniser.isPaused()) {
                        synchroniser.getLatch().await();
                    }
                    packet = new DatagramPacket(rawBuffer, MAX_AUDIO_FRAGMENT_LENGTH);
                    audioSocket.receive(packet);
                    actualLength = extractAudio(packet.getLength());
                    audioTrack.write(audioBuffer, 0, actualLength);
                }
            } catch (IOException e) {
                e.printStackTrace();
                synchroniser.onClientError(id, MediaService.CAUSE_INTERRUPTED);
                //TODO : Add causes
            } catch (InterruptedException e) {
                e.printStackTrace();
                synchroniser.onClientError(id, MediaService.CAUSE_ERROR);
                //TODO : Add causes
            }
            audioTrack.stop();
            isPlayingAudio = false;
        }
    }

    private int extractAudio(int packetLength) {
        int j = 0;
        int payloadOffset = 12;
        int extensionMarker = 1 << 4;
        if ((rawBuffer[0] & extensionMarker) != 0) {
            int extensionLength = (rawBuffer[14] & 0xFF) << 8 | (rawBuffer[15] & 0xFF);
            payloadOffset += extensionLength * 4 + 4;
        }
        for (int i = payloadOffset; i < packetLength; i += 2, j++) {
            audioBuffer[j] = (short)((rawBuffer[i] & 0xFF) << 8 | (rawBuffer[i + 1] & 0xFF));
        }
        return j;
    }
}
