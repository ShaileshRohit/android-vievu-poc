package com.vievu.vievusolution.media;

import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;

import com.vievu.vievusolution.CameraClient;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.media.mjpeg.MjpegReceiver;
import com.vievu.vievusolution.media.mjpeg.MjpegRtpParser;
import com.vievu.vievusolution.media.mjpeg.RtspClient;
import com.vievu.vievusolution.ui.MjpegView;

import butterknife.Bind;

public class LiveViewFragment extends MediaControlsFragment implements MjpegRtpParser.OnJpegLoadedListener, LiveViewActivity.BackPressInterceptor {

    @Bind(R.id.mjpegView) MjpegView mjpegView;

    protected boolean isPlaying = false;
    protected boolean isAutoPlayEnabled = true;

    protected Bitmap lastBitmap;
    protected BitmapProcessingThread bitmapDrawingThread;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_live_view;
    }

    protected LiveViewActivity getLiveViewActivity() {
        return (LiveViewActivity) getActivity();
    }

    public LiveViewFragment() {
        setRetainInstance(true);
    }

    @Override
    protected void initView(View view) {
        mjpegView.setDisplayMode(MjpegView.SIZE_BEST_FIT);
        mjpegView.setOnTouchListener(this);

        startMediaProcessing();
    }

    @Override
    public boolean onBackPressed(LiveViewActivity activity) {
        if (isInFullScreen) {
            setFullscreenModeEnabled(false);
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        removeOverlayCallbacks();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onDestroyInternal();
    }

    @Override
    public void onJpegLoaded(MjpegRtpParser.FrameData frameData) {
        this.lastBitmap = frameData.frame;
        if (mjpegView != null && !lastBitmap.isRecycled()) {
            mjpegView.drawBitmap(lastBitmap);
        }
    }

    protected void removeOverlayCallbacks() {
        if (mediaControlsOverlay != null) {
            Handler handler = mediaControlsOverlay.getHandler();
            if (handler != null) {
                handler.removeCallbacks(this::hideOverlay);
            }
        }
    }

    protected void onDestroyInternal() {
        stopMediaProcessing();
    }

    public void startMediaProcessing() {
        MediaService service = getLiveViewActivity().getMediaService();
        if (service != null) {
            RtspClient rtspClient = CameraClient.getInstance().getRtspClient();
            service.startAudioReceiver(rtspClient);
            MjpegReceiver receiver = service.startVideoReceiver(rtspClient);
            if (receiver != null) {
                if (bitmapDrawingThread != null) {
                    bitmapDrawingThread.interrupt();
                }
                bitmapDrawingThread = new BitmapProcessingThread();
                bitmapDrawingThread.start();
                service.resume();
                isPlaying = true;
            }
        }
    }

    protected void stopMediaProcessing() {
        setFullscreenModeEnabled(false);
        MediaService service = getLiveViewActivity().getMediaService();
        if (service != null) {
            service.pause();
            if (service.getVideoReceiver() != null
                && service.getVideoReceiver().getRtpParser() != null
                && service.getVideoReceiver().getRtpParser().getFrameBuffer() != null) {

                service.getVideoReceiver().getRtpParser().getFrameBuffer().recycle();
            }
            service.stopSelf();
            CameraClient.getInstance().stopVideoAsync();
        }
        if (bitmapDrawingThread != null && bitmapDrawingThread.isAlive()) {
            bitmapDrawingThread.interrupt();
        }
        isPlaying = false;
    }

    protected class BitmapProcessingThread extends Thread {

        @Override
        public void run() {
            if (getLiveViewActivity() != null && getLiveViewActivity().getMediaService() != null) {
                MjpegReceiver receiver = getLiveViewActivity().getMediaService().getVideoReceiver();
                if (receiver != null) {
                    MjpegRtpParser.InMemoryFrameBuffer frameBuffer = receiver.getRtpParser().getFrameBuffer();
                    MjpegRtpParser.FrameData frameData;
                    while (!isInterrupted()) {
                        frameData = frameBuffer.peek();
                        if (frameData.frame != null && !frameData.isShown) {
                            onJpegLoaded(frameData);
                            frameBuffer.release();
                        }
                    }
                }
            }
        }
    }
}
