package com.vievu.vievusolution.media;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.vievu.vievusolution.BaseVievuFragment;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.ui.UiUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public abstract class MediaControlsFragment extends BaseVievuFragment implements View.OnTouchListener {

    protected static final int TIMEOUT_MEDIA_CONTROLS_INACTIVITY = 5000;

    protected boolean isOverlayVisible = true;
    protected boolean isInFullScreen = false;
    protected boolean isLocked = false;
    protected GestureDetector gestureDetector;
    protected Runnable hideOverlayCallback = this::hideOverlay;

    @Nullable
    @Bind(R.id.mediaProgressIndicator) protected ProgressBar mediaProgressIndicator;
    @Bind(R.id.mediaControlsOverlay) protected View mediaControlsOverlay;
    @Bind(R.id.mediaControlsLockOverlay) protected View mediaControlsLock;
    @Bind(R.id.fullscreenButton) ImageButton fullscreenButton;

    public boolean isInFullScreen() {
        return isInFullScreen;
    }

    protected abstract int getLayoutId();
    protected abstract void initView(View view);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);
        initView(view);
        setFullscreenModeEnabled(isInFullScreen);
        if (!isOverlayVisible && mediaControlsOverlay != null) {
            mediaControlsOverlay.post(() ->  {
                if (mediaControlsOverlay != null) {
                    mediaControlsOverlay.setTranslationY(mediaControlsOverlay.getHeight());
                }
            });
        } else if (isInFullScreen) {
            startHideOverlayTimer();
        }
        gestureDetector = new GestureDetector(
                getActivity(),
                new GestureDetector.SimpleOnGestureListener(){
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        setMediaControlsOverlayVisible(!isOverlayVisible);
                        return true;
                    }
                });
        if (mediaProgressIndicator != null) {
            UiUtil.colorizeProgressBar(mediaProgressIndicator, R.color.accent);
        }
        setMediaControlsLocked(this.isLocked);
        return view;
    }

    @Override
    public boolean onTouch(View view, @NonNull MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return true;
    }

    @OnClick(R.id.fullscreenButton)
    public void onFullscreenButtonClick() {
        resetHideOverlayTimer();
        setFullscreenModeEnabled(!isInFullScreen);
    }

    public void setMediaControlsOverlayVisible(boolean isVisible) {
        if (this.isOverlayVisible != isVisible) {
            isOverlayVisible = isVisible;
            if (mediaControlsOverlay != null) {
                stopHideOverlayTimer();
                mediaControlsLock.setVisibility(View.VISIBLE);
                mediaControlsOverlay
                    .animate()
                    .translationY(isVisible ? 0 : mediaControlsOverlay.getHeight())
                    .setInterpolator(new AccelerateDecelerateInterpolator())
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(@NonNull Animator animation) {
                            if (getView() != null) {
                                if (!isLocked) {
                                    mediaControlsLock.setVisibility(View.GONE);
                                }
                                if (isInFullScreen) {
                                    startHideOverlayTimer();
                                }
                            }
                        }
                    })
                    .start();
            }
        }
    }

    protected void startHideOverlayTimer() {
        if (isOverlayVisible && mediaControlsOverlay != null) {
            mediaControlsOverlay.postDelayed(hideOverlayCallback, TIMEOUT_MEDIA_CONTROLS_INACTIVITY);
        }
    }

    protected void stopHideOverlayTimer() {
        if (mediaControlsOverlay != null && mediaControlsOverlay.getHandler() != null) {
            mediaControlsOverlay.getHandler().removeCallbacks(hideOverlayCallback);
        }
    }

    protected void resetHideOverlayTimer() {
        if (isOverlayVisible && mediaControlsOverlay != null) {
            mediaControlsOverlay.getHandler().removeCallbacks(hideOverlayCallback);
            mediaControlsOverlay.postDelayed(hideOverlayCallback, TIMEOUT_MEDIA_CONTROLS_INACTIVITY);
        }
    }

    public void hideOverlay() {
        setMediaControlsOverlayVisible(false);
    }

    @SuppressWarnings("ConstantConditions")
    public void setFullscreenModeEnabled(boolean isEnabled) {
        this.isInFullScreen = isEnabled;
        Window window = getActivity().getWindow();
        if (isEnabled) {
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getActivity().getActionBar().hide();
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getActivity().getActionBar().show();
        }
        if (fullscreenButton != null) {
            fullscreenButton.setImageResource(
                isEnabled ?
                    R.drawable.ic_action_return_from_full_screen :
                    R.drawable.ic_action_full_screen);
        }
    }

    protected void setMediaControlsLocked(boolean isLocked) {
        this.isLocked = isLocked;
        if (mediaProgressIndicator != null) {
            int newVisibility = isLocked ? View.VISIBLE : View.GONE;
            mediaProgressIndicator.setVisibility(newVisibility);
            mediaControlsLock.setVisibility(newVisibility);
        }
    }
}
