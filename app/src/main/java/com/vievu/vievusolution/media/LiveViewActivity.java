package com.vievu.vievusolution.media;

import android.app.Activity;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MenuItem;

import com.annimon.stream.Stream;
import com.vievu.vievusolution.CameraClient;
import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.EventBusActivity;
import com.vievu.vievusolution.NoCameraFragment;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.WiFiStateReceiver;
import com.vievu.vievusolution.event.RtspEvent;
import com.vievu.vievusolution.net.camera.RetryHandler;
import com.vievu.vievusolution.ptpip.constants.CameraMode;
import com.vievu.vievusolution.util.ActivityUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class LiveViewActivity extends EventBusActivity implements WiFiStateReceiver.CallbackActivity, RetryHandler {

    public static void start(Activity caller, String videoUrl) {
        Intent intent = new Intent(caller, LiveViewActivity.class);
        intent.putExtra(EXTRA_VIDEO_URL, videoUrl);
        caller.startActivity(intent);
    }

    interface BackPressInterceptor {
        boolean onBackPressed(LiveViewActivity activity);
    }

    private enum State {
        ERROR,
        INITIAL,
        WAITING,
        LIVE_VIEW,
        DIRECT_VIDEO_PLAY,
        DIRECT_VIDEO_IDLE,
        WAIT_AND_LEAVE;

        public boolean oneOf(State... states) {
            return Stream.of(states).anyMatch(state -> state == this);
        }
    }

    private static final String EXTRA_VIDEO_URL = "video_url";
    private static final String EXTRA_LAST_SSID = "last_ssid";
    private static final String EXTRA_CURRENT_SATE = "current_sate";
    private static final String EXTRA_HAS_THUMBNAIL = "has_thumbnail";
    private static final String EXTRA_HAS_PENDING_CHANGE = "has_pending_change ";

    private static final String THUMBNAIL_FILENAME = "thumbnail.jpeg";

    public static final String TAG = "LiveViewActivity";

    private String videoUrl;
    private String lastSSID;

    private State currentState;
    private boolean hasPendingFragmentChange;

    private boolean hasDirectVideoThumbnail;
    private String thumbnailFilePath;
    private WiFiStateReceiver<LiveViewActivity> receiver = new WiFiStateReceiver<>();
    private MediaService mediaService;

    private ServiceConnection mediaServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (mediaService == null) {
                mediaService = ((MediaService.MediaServiceProvider) iBinder).getService();
                if (isLiveView()) {
                    if (currentState == State.LIVE_VIEW) {
                        callOnFragment(LiveViewFragment.class, LiveViewFragment::startMediaProcessing);
                    } else {
                        goToState(State.LIVE_VIEW);
                    }
                } else if (!CameraClient.getInstance().isPreRecordRestoreRequested()) {
                    goToState(State.DIRECT_VIDEO_PLAY);
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };

    private boolean isLiveView() {
        return videoUrl == null || CommonConstants.DEFAULT_VIDEO_URI.equals(videoUrl);
    }

    public MediaService getMediaService() {
        return mediaService;
    }

    //region Activity lifecycle callbacks

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        this.thumbnailFilePath = new File(getCacheDir(), THUMBNAIL_FILENAME).getPath();

        Bundle bundle;
        if (savedInstanceState != null) {
            currentState = (State) savedInstanceState.getSerializable(EXTRA_CURRENT_SATE);
            bundle = savedInstanceState;
        } else {
            bundle = getIntent().getExtras();
            goToState(State.INITIAL);
        }

        if (bundle != null) {
            lastSSID = bundle.getString(EXTRA_LAST_SSID);
            videoUrl = bundle.getString(EXTRA_VIDEO_URL);
            hasDirectVideoThumbnail = bundle.getBoolean(EXTRA_HAS_THUMBNAIL);
            hasPendingFragmentChange |= bundle.getBoolean(EXTRA_HAS_PENDING_CHANGE);
            if (!isLiveView()) {
                setTitle(R.string.title_activity_camera_video_details);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Any onStop state change must be saved here in advance, but applied in onStop
        if (   !isChangingConfigurations()
            && currentState != State.WAIT_AND_LEAVE) {
            outState.putBoolean(EXTRA_HAS_PENDING_CHANGE, true);
            outState.putSerializable(EXTRA_CURRENT_SATE, State.WAITING);
        } else {
            outState.putBoolean(EXTRA_HAS_PENDING_CHANGE, hasPendingFragmentChange);
            outState.putSerializable(EXTRA_CURRENT_SATE, currentState);
        }

        outState.putString(EXTRA_VIDEO_URL, videoUrl);
        outState.putString(EXTRA_LAST_SSID, lastSSID);
        outState.putBoolean(EXTRA_HAS_THUMBNAIL, hasDirectVideoThumbnail);
    }

    @Override
    public void onStart() {
        super.onStart();
        receiver.registerWith(this);
        if (MediaService.isBoundToAppContext()) {
            bindMediaService();
            MediaService.unbindFromAppContext(this);
        }
        if (hasPendingFragmentChange) {
            Log.d(TAG, "Applying pending fragment change in state " + currentState);
            hasPendingFragmentChange = false;
            applyStateFragmentChange(currentState);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home && !isLiveView()) {
            this.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
        if (   !(fragment instanceof BackPressInterceptor)
            || !((BackPressInterceptor) fragment).onBackPressed(this)) {
            if (currentState.oneOf(State.ERROR, State.LIVE_VIEW, State.DIRECT_VIDEO_IDLE)) {
                super.onBackPressed();
            } else if (currentState == State.DIRECT_VIDEO_PLAY){
                goToState(State.WAIT_AND_LEAVE);
                CameraClient.getInstance().compareAndSetCameraMode(CameraMode.SHARE);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        receiver.unregister();
        if (isChangingConfigurations()) {
            if (mediaService != null) {
                MediaService.bindToAppContext(this);
            }
        } else if (currentState != State.WAIT_AND_LEAVE) {
            CameraClient.getInstance().stopVideoAsync();
            if (!isLiveView() && currentState != State.ERROR) {
                CameraClient.getInstance().restorePreRecord();
            }
            // State change won't be applied properly after onSaveInstanceState
            goToState(State.WAITING);
        }
        unbindMediaService();
    }

    @Override
    protected void onDestroy() {
        if (!isChangingConfigurations()) {
            if (hasDirectVideoThumbnail) {
                //noinspection ResultOfMethodCallIgnored
                new File(this.thumbnailFilePath).delete();
            }
            unbindMediaService();
        }
        super.onDestroy();
    }

    //endregion

    //region Wi-Fi state monitoring callbacks

    @Override
    public void onConnectedToWiFiNetwork(String ssid) {
        ActivityUtil.tryBindNetworkAdapter(this);
        this.lastSSID = ssid;
        if (   (isLiveView() && currentState != State.LIVE_VIEW)
            || currentState.oneOf(State.ERROR, State.INITIAL)) {
            retry();
        }
    }

    @Override
    public void onWiFiConnectionLost() {
        this.lastSSID = null;
        CameraClient.getInstance().disconnectImmediately();
        goToState(State.ERROR);
        unbindMediaService();
    }

    //endregion

    //region EvetBus handlers

    public void onEventMainThread(CameraClient.Event event) {
        Log.d(TAG, String.format("Received CameraClient.Event (connected %b)", event.isConnected()));
        if (!event.isConnected()) {
            goToState(State.ERROR);
        }
    }

    public void onEventMainThread(RtspEvent event) {
        Log.d(TAG, String.format("Received MediaService.Event (running %b)", event.isConnected()));
        if (event.isConnected()) {
            if (currentState != State.WAIT_AND_LEAVE && !bindMediaService() && isLiveView()) {
                callOnFragment(LiveViewFragment.class, LiveViewFragment::startMediaProcessing);
            }
        } else {
            unbindMediaService();
            goToState(State.ERROR);
        }
    }

    public void onEventMainThread(CameraClient.Mode event) {
        if (event.getValue() == CameraMode.VIDEO_OFF || event.getValue() == CameraMode.SHARE) {
            if (currentState == State.WAIT_AND_LEAVE) {
                finish();
            } else if (!isLiveView()
                    && currentState == State.WAITING
                    && !CameraClient.getInstance().isPreRecordRestoreRequested()){
                goToState(State.DIRECT_VIDEO_IDLE);
            }
        }
    }

    //endregion

    //region State management

    private State goToState(State state) {
        if (state != currentState) {
            Log.d(TAG, "Switching state to " + state);
            if (isFragmentChangedRequired(state)) {
                if (!isInBackground) {
                    applyStateFragmentChange(state);
                } else {
                    this.hasPendingFragmentChange = true;
                    Log.d(TAG, "Activity is in background. Submitted pending fragment change");
                }
            }
            this.currentState = state;
        } else {
            Log.d(TAG, "Remaining in state " + state);
        }
        return currentState;
    }

    private boolean isFragmentChangedRequired(State newState) {
        if (newState == State.DIRECT_VIDEO_PLAY
            && currentState == State.DIRECT_VIDEO_IDLE) {
            callOnFragment(DirectVideoFragment.class, LiveViewFragment::startMediaProcessing);
            return false;
        }
        return true;
    }

    private void applyStateFragmentChange(State state) {
        Fragment fragment = null;
        switch (state) {
            case INITIAL:
            case WAITING:
            case WAIT_AND_LEAVE:
                fragment = NoCameraFragment.create(true);
                break;
            case ERROR:
                fragment = NoCameraFragment.create(false);
                break;
            case LIVE_VIEW:
                fragment = new LiveViewFragment();
                break;
            case DIRECT_VIDEO_PLAY:
                fragment = DirectVideoFragment.create(true, null);
                break;
            case DIRECT_VIDEO_IDLE:
                fragment = DirectVideoFragment.create(
                        false,
                        hasDirectVideoThumbnail ? BitmapFactory.decodeFile(this.thumbnailFilePath) : null);
                break;
        }
        showFragment(fragment);
    }

    public void showFragment(Fragment fragment) {
        if (!isInBackground) {
            setRequestedOrientation(fragment instanceof NoCameraFragment ?
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT :
                    ActivityInfo.SCREEN_ORIENTATION_USER);
            ActivityUtil.replaceFragment(this, fragment);
        }
    }

    //endregion

    private boolean bindMediaService() {
        if (mediaService == null && currentState != State.WAIT_AND_LEAVE) {
            bindService(new Intent(this, MediaService.class), mediaServiceConnection, BIND_AUTO_CREATE);
            return true;
        }
        return false;
    }

    private void unbindMediaService() {
        if (this.mediaService != null) {
            this.mediaService = null;
            unbindService(mediaServiceConnection);
        }
    }

    @Override
    public void retry() {
        if (currentState != State.INITIAL) {
            goToState(State.WAITING);
        }
        CameraClient.getInstance().requestMediaService(this.videoUrl);
    }

    public void restartToPlay(Bitmap thumbnail) {
        if (!currentState.oneOf(State.WAIT_AND_LEAVE, State.WAITING)) {
            if (thumbnail != null) {
                hasDirectVideoThumbnail = true;
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(this.thumbnailFilePath);
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, out);
                } catch (Exception e) {
                    e.printStackTrace();
                    hasDirectVideoThumbnail = false;
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        hasDirectVideoThumbnail = false;
                    }
                }

            }
            unbindMediaService();
            CameraClient.getInstance().compareAndSetCameraMode(CameraMode.SHARE);
            goToState(State.WAITING);
        }
    }

    public void replayDirectVideo() {
        CameraClient.getInstance().requestMediaService(this.videoUrl);
    }
}