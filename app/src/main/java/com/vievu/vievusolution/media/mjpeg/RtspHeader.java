package com.vievu.vievusolution.media.mjpeg;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class RtspHeader {

    public static final String HEADER_PUBLIC = "Public";
    public static final String HEADER_TRANSPORT = "Transport";
    public static final String HEADER_SESSION = "Session";
    public static final String HEADER_CONTENT_LENGTH = "Content-Length";
    public static final String HEADER_RANGE = "Range";
    public static final String HEADER_RTP_INFO = "RTP-Info";
    private String name;
    private String data;
    private LinkedList<HashMap<String, String>> values;

    public String getName() {
        return name;
    }

    public String getData() {
        return data;
    }

    public List<HashMap<String, String>> getValues() {
        return values;
    }

    private RtspHeader() {}

    public static RtspHeader parse(String rawHeader) {
        RtspHeader header = new RtspHeader();
        String[] headerStructure = rawHeader.split(": ");
        header.name = headerStructure[0];
        header.data = headerStructure[1];
        String[] items = header.data.split(",");
        header.values = new LinkedList<>();
        for (String item : items) {
            String[] values = item.split(";");
            if (values.length > 0) {
                header.values.add(new HashMap<>(values.length));
                for (String value : values) {
                    if (value.contains("=")) {
                        String[] valueStructure = value.split("=");
                        header.values.getLast().put(valueStructure[0], valueStructure[1]);
                    } else {
                        header.values.getLast().put(value, null);
                    }
                }
            }
        }
        return header;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", name, data);
    }
}
