package com.vievu.vievusolution.media.mjpeg;

import java.io.ByteArrayOutputStream;

public class UnsafeByteArrayOutputStream extends ByteArrayOutputStream {

    public int getCount() {
        return count;
    }

    public byte[] getInnerBuffer() {
        return buf;
    }
}
