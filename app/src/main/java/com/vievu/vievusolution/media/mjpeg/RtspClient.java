package com.vievu.vievusolution.media.mjpeg;

import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;

import com.vievu.vievusolution.CameraClient;
import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.event.RtspEvent;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

@SuppressWarnings("WeakerAccess")
public class RtspClient {

    public static final String RTSP_TRACK_VIDEO = "track1";
    public static final String RTSP_TRACK_AUDIO = "track2";

    public static class PlayEvent {

        private boolean isPlaying;

        public boolean isPlaying() {
            return isPlaying;
        }

        public PlayEvent(boolean isPlaying) {
            this.isPlaying = isPlaying;
        }
    }

    public static class StopEvent {}

    public static class RtpTimeChangedEvent {

        private String trackName;
        private int value;

        public RtpTimeChangedEvent(String trackName, int value) {
            this.trackName = trackName;
            this.value = value;
        }

        public String getTrackName() {
            return trackName;
        }

        public int getValue() {
            return value;
        }
    }

    private String rtspUrl = CommonConstants.DEFAULT_VIDEO_URI;

    private BufferedWriter writer;
    private BufferedReader reader;

    private int seq = 0;
    private String session;

    private Pattern lengthPattern = Pattern.compile("a=range:npt=0-((?:\\d|\\.)+)");
    private Pattern trackUrlPattern = Pattern.compile("^.*/(.*?)$");
    private double currentLength = -1;

    private DatagramSocket videoSocket;
    private DatagramSocket videoControlSocket;
    private DatagramSocket audioSocket;
    private Socket rtspSocket;

    private boolean isBusy = false;
    private boolean isConnected = false;
    private boolean isPlaying = false;

    private HandlerThread handlerThread;
    private Handler handler;

    private HandlerThread rtcpAwaitThread;
    private Handler rtcpAwaitHandler;

    private boolean sendDisconnectedEvent = false;
    private boolean isEventsEnabled = true;

    public RtspClient() {
        this(true);
    }

    public RtspClient(boolean isEventsEnabled) {
        this.isEventsEnabled = isEventsEnabled;
    }

    private Handler getHandler() {
        if (handler == null) {
            if (handlerThread == null) {
                handlerThread = new HandlerThread("MjpegThread");
                handlerThread.start();
            }
            handler = new Handler(handlerThread.getLooper());
        }
        return handler;
    }

    private Handler getRtcpAwaitHandler() {
        if (rtcpAwaitHandler == null) {
            if (rtcpAwaitThread == null) {
                rtcpAwaitThread = new HandlerThread("RtcpAwaitThread");
                rtcpAwaitThread.start();
            }
            rtcpAwaitHandler = new Handler(rtcpAwaitThread.getLooper());
        }
        return rtcpAwaitHandler;
    }

    public DatagramSocket getVideoSocket() {
        return videoSocket;
    }

    public DatagramSocket getAudioSocket() {
        return audioSocket;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public double getCurrentLength() {
        return currentLength;
    }

    public boolean connect(String videoUrl) {
        this.rtspUrl = videoUrl != null ? videoUrl : CommonConstants.DEFAULT_VIDEO_URI;
        if (!isConnected) {
            sendConnectSequence();
        } else {
            Log.d("RtspClient", "Connection task rejected: already connected");
        }
        return isConnected;
    }

    public void connectAsync(final String videoUrl) {
        getHandler().post(() -> connect(videoUrl));
    }

    public void playAsync(Double startTime) {
        executePlaybackAction(() -> playSync(startTime));
    }

    public void pauseAsync() {
        executePlaybackAction(this::pauseSync);
    }

    public void seekToAsync(double position) {
        executePlaybackAction(() -> {
            if (isPlaying) {
                pauseSync();
            }
            playSync(position);
        });
    }

    private void executePlaybackAction(CameraClient.Callback action) {
        getHandler().postAtFrontOfQueue(() -> {
            try {
                action.run();
                EventBus.getDefault().post(new PlayEvent(this.isPlaying));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Tries to send TEARDOWN request and calls {@link #disconnect()} no matter of TEARDOWN result.
     */
    public void teardownAsync() {
        getHandler().postAtFrontOfQueue(this::sendTeardown);
    }

    public void teardownAsync(boolean sendDisconnectedEvent) {
        this.sendDisconnectedEvent = sendDisconnectedEvent;
        teardownAsync();
    }

    public void teardown(boolean sendDisconnectedEvent) {
        this.sendDisconnectedEvent = sendDisconnectedEvent;
        sendTeardown();
    }

    private boolean sendConnectSequence() {
        RtspResponse response = null;
        this.isBusy = true;
        this.session = null;
        this.seq = 1;
        try {

            rtspSocket = new Socket();
            rtspSocket.setSoTimeout(CommonConstants.TIMEOUT_SOCKET);
            rtspSocket.connect(
                    new InetSocketAddress(
                            CommonConstants.DEFAULT_CAMERA_IP,
                            CommonConstants.DEFAULT_PORT_RTSP),
                    CommonConstants.TIMEOUT_SOCKET);
            rtspSocket.setKeepAlive(true);

            writer = new BufferedWriter(
                    new OutputStreamWriter(rtspSocket.getOutputStream()));
            reader = new BufferedReader(
                    new InputStreamReader(rtspSocket.getInputStream()));
            sendOptions();
            response = getResponse();
            if (response != null) {
                RtspHeader header = response.header(RtspHeader.HEADER_PUBLIC);
                if (header != null && header.getData().contains("DESCRIBE")) {
                    sendDescribe();
                    if ((response = getResponse()) != null) {
                        videoSocket = sendSetupTrack("track1");
                        boolean hasAudio = false;
                        if (response.getBody() != null) {
                            Log.d("RtspClient", response.toString());
                            hasAudio = response.getBody().contains("track2");
                            this.currentLength = parseParameter(response.getBody(), lengthPattern);
                        }
                        if (connectSocket(videoSocket, response = getResponse())) {
                            if (currentLength > -1) {
                                this.videoControlSocket = new DatagramSocket(videoSocket.getLocalPort() + 1);
                                videoControlSocket.connect(new InetSocketAddress(CommonConstants.DEFAULT_CAMERA_IP, videoSocket.getPort() + 1));
                                getRtcpAwaitHandler().post(this::waitForRtcpMessage);
                            }
                            if (hasAudio) {
                                audioSocket = sendSetupTrack("track2");
                                connectSocket(audioSocket, response = getResponse());
                            }
                            sendPlay(0d);
                            if ((response = getResponse()) != null) {
                                isConnected = true;
                                getHandler().postDelayed(this::sendKeepAlive, CommonConstants.TIMEOUT_KEEP_ALIVE);
                            }
                        }
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (!isConnected) {
            if (response != null) {
                Log.d("RtspClient", "Last response:\n" + response.toString());
            } else {
                Log.d("RtspClient", "No response");
            }
        } else {
            Log.d("RtspClient", "RTSP connected");
        }
        if (isEventsEnabled) {
            EventBus.getDefault().post(new RtspEvent(isConnected));
        }
        isBusy = false;
        return isConnected;
    }

    @SuppressWarnings("UnusedAssignment")
    public boolean reconnect() {
        if (isConnected()) {
            sendDisconnectedEvent = false;
            dispose();
            disconnect();
        }
        this.rtspUrl = CommonConstants.DEFAULT_VIDEO_URI;
        RtspResponse response;
        this.isBusy = true;
        this.session = null;
        this.seq = 1;
        try {

            rtspSocket = new Socket();
            rtspSocket.setSoTimeout(CommonConstants.TIMEOUT_SOCKET);
            rtspSocket.connect(
                    new InetSocketAddress(
                            CommonConstants.DEFAULT_CAMERA_IP,
                            CommonConstants.DEFAULT_PORT_RTSP),
                    CommonConstants.TIMEOUT_SOCKET);
            rtspSocket.setKeepAlive(true);

            writer = new BufferedWriter(
                    new OutputStreamWriter(rtspSocket.getOutputStream()));
            reader = new BufferedReader(
                    new InputStreamReader(rtspSocket.getInputStream()));
            sendOptions();
            response = getResponse();
            if (response != null) {
                RtspHeader header = response.header(RtspHeader.HEADER_PUBLIC);
                if (header != null && header.getData().contains("DESCRIBE")) {
                    sendDescribe();
                    if ((response = getResponse()) != null) {
                        videoSocket = sendSetupTrack("track1");
                        if (connectSocket(videoSocket, response = getResponse())) {
                            sendPlay(0d);
                            if ((response = getResponse()) != null) {
                                teardown(false);
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        isBusy = false;
        return false;
    }

    private void waitForRtcpMessage() {
        try {
            videoControlSocket.receive(new DatagramPacket(new byte[100], 100));
            Log.d("RtcpAwaitThread", "Received RTCP packet");
            if (isEventsEnabled) {
                EventBus.getDefault().post(new StopEvent());
            }
//            if (!isBusy) {
//                teardownAsync();
//            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeSocket(videoControlSocket);
        }
    }

    private double parseParameter(@NonNull String body, @NonNull Pattern pattern) {
        Matcher matcher = pattern.matcher(body);
        if (matcher.find()) {
            return Double.parseDouble(matcher.group(1));
        }
        return -1;
    }

    private void sendKeepAlive() {
        try {
            Log.d("MjpegConnector", String.format("KeepAliveLoop: sending GET_PARAMETER: seq = %d", seq + 1));
            writer.write(getBaseWithSession("GET_PARAMETER"));
            submitRequest();
            RtspResponse response = getResponse();
            if (response != null) {
                Log.d("MjpegConnector", "KeepAliveLoop: Received response\n" + response.toString());
                getHandler().postDelayed(this::sendKeepAlive, CommonConstants.TIMEOUT_KEEP_ALIVE);
                return;
            } else {
                Log.d("MjpegConnector", "KeepAliveLoop: no response");
            }
        } catch (IOException e) {
            Log.d("MjpegConnector", "KeepAliveLoop: error in keep alive loop");
            e.printStackTrace();
        }
        sendTeardown();
        if (isEventsEnabled) {
            EventBus.getDefault().post(new RtspEvent(false));
        }
    }

    private void sendTeardown() {
        if (writer != null && rtspSocket != null && !rtspSocket.isClosed()) {
            isBusy = true;
            if (rtcpAwaitHandler != null) {
                getRtcpAwaitHandler().removeCallbacksAndMessages(null);
            }
            try {
                writer.write(getBaseWithSession("TEARDOWN"));
                submitRequest();
                Log.d("RtspClient", "Teardown sent");
                RtspResponse response = getResponse();
                if (response != null) {
                    Log.d("RtspClient", response.toString());
                }
            } catch (IOException ex) {
                Log.d("MjpegConnector", "Disconnect: Can't disconnect gracefully (TEARDOWN failed)");
                ex.printStackTrace();
            }
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        disconnect();
    }

    public void stop() {
        dispose();
        sendTeardown();
        //disconnect();
    }

    /**
     * Closes all sockets and drops RTSP connection without sending TEARDOWN request and keeping alive worker threads.
     */
    public void disconnect(){
        isBusy = true;
        isConnected = false;
        session = null;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        if (rtcpAwaitHandler != null) {
            rtcpAwaitHandler.removeCallbacksAndMessages(null);
        }
        closeSocket(audioSocket);
        audioSocket = null;
        closeSocket(videoSocket);
        videoSocket = null;
        closeSocket(videoControlSocket);
        videoControlSocket = null;
        if (rtspSocket != null && !rtspSocket.isClosed()) {
            try {
                rtspSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        rtspSocket = null;
        isBusy = false;
        if (sendDisconnectedEvent) {
            sendDisconnectedEvent = false;
            if (isEventsEnabled) {
                EventBus.getDefault().post(new RtspEvent(false));
            }
        }
        Log.d("RtspClient", "Client disconnected");
    }

    /**
     * Stops the working thread and releases all resources.
     * Doesn't call {@link #disconnect()} automatically.
     */
    public void dispose() {
        isBusy = true;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handlerThread.quit();
            handlerThread = null;
            handler = null;
        }
        if (rtcpAwaitHandler != null) {
            rtcpAwaitHandler.removeCallbacksAndMessages(null);
            rtcpAwaitThread.quit();
            rtcpAwaitThread = null;
            rtcpAwaitHandler = null;
        }
        isBusy = false;
        Log.d("RtspClient", "Client disposed");
    }

    //region Socket handling

    private boolean connectSocket(DatagramSocket socket, RtspResponse response) throws IOException {
        if (response != null) {
            RtspHeader header = response.header(RtspHeader.HEADER_SESSION);
            if (header != null) {
                session = header.getData();
                header = response.header(RtspHeader.HEADER_TRANSPORT);
                if (header != null) {
                    int serverPort = Integer.parseInt(
                            header.getValues().get(0).get("server_port").split("-")[0]);
                    socket.connect(new InetSocketAddress(CommonConstants.DEFAULT_CAMERA_IP, serverPort));
                    return true;
                }
            }
        }
        return false;
    }

    private void closeSocket(DatagramSocket socket) {
        if (socket != null && !socket.isClosed()) {
            socket.close();
        }
    }

    //endregion

    //region RTSP methods

    private String getBase(String method, String url) {
        return String.format("%s %s RTSP/1.0\r\nCSeq: %d\r\nUser-Agent: me\r\n", method, url, seq++);
    }

    private String getBase(String method) {
        return getBase(method, rtspUrl);
    }

    private String getBaseWithSession(String method) {
        return String.format("%sSession: %s\r\n", getBase(method), session);
    }

    private void submitRequest() throws IOException {
        writer.write("\r\n");
        writer.flush();
    }

    private void sendOptions() throws IOException {
        writer.write(getBase("OPTIONS"));
        submitRequest();
    }

    private void sendDescribe() throws IOException {
        writer.write(getBase("DESCRIBE") + "Accept: application/sdp\r\n");
        submitRequest();
    }

    private DatagramSocket sendSetupTrack(String trackName) throws IOException {
        DatagramSocket rtpSocket = new DatagramSocket(0);
        rtpSocket.setSoTimeout(CommonConstants.TIMEOUT_SOCKET);
        int localPort = rtpSocket.getLocalPort();
        writer.write(String.format(
                "%sTransport: RTP/AVP;unicast;client_port=%d-%d\r\n%s",
                getBase("SETUP", rtspUrl + "/" + trackName), localPort, localPort + 1,
                session != null ? String.format("Session: %s\r\n", session) : ""));
        submitRequest();
        return rtpSocket;
    }

    private void sendPlay(Double startTime) throws IOException {
        String request = getBaseWithSession("PLAY");
        if (startTime != null) {
            request = String.format("%sRange: npt=%f-\r\n", request, startTime);
        }
        writer.write(request);
        submitRequest();
        this.isPlaying = true;
    }

    private void playSync(Double startTime) throws IOException {
        sendPlay(startTime);
        RtspResponse response = getResponse();
        if (response != null && response.getHeaders() != null) {
            RtspHeader header = response.getHeaders().get(RtspHeader.HEADER_RTP_INFO);
            if (header != null) {
                for (HashMap<String, String> value : header.getValues()) {
                    Matcher matcher = trackUrlPattern.matcher(value.get("url"));
                    if (matcher.matches()) {
                        EventBus.getDefault().post(new RtpTimeChangedEvent(
                                matcher.group(1),
                                Integer.parseInt(value.get("rtptime")))
                        );
                    }
                }
            }
            Log.d("RtspClient", response.toString());
        }
    }

    private void pauseSync() throws IOException {
        sendSimpleCommand("PAUSE");
        getResponse();
        this.isPlaying = false;
    }

    private boolean sendSimpleCommand(String command) {
        try {
            writer.write(getBaseWithSession(command));
            submitRequest();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private RtspResponse getResponse() throws IOException {

        for (int i = 0; i < CommonConstants.RETRY_COUNT; i++) {
            if (!reader.ready()) {
                try {
                    Thread.sleep(CommonConstants.TIMEOUT_RETRY, 0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                break;
            }
        }
        if (reader.ready()) {
            return RtspResponse.getFromReader(reader);
        }
        return null;
    }

    //endregion
}
