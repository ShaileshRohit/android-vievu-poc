package com.vievu.vievusolution.media;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.IBinder;
import android.util.Log;

import com.vievu.vievusolution.event.RtspEvent;
import com.vievu.vievusolution.media.mjpeg.LatchSynchroniser;
import com.vievu.vievusolution.media.mjpeg.MjpegReceiver;
import com.vievu.vievusolution.media.mjpeg.Pcms16beReceiver;
import com.vievu.vievusolution.media.mjpeg.RtspClient;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import de.greenrobot.event.EventBus;

public class MediaService extends Service implements LatchSynchroniser {

    public static final int ID_MJPEG_RECEIVER = 401;
    public static final int ID_PCM_U8_RECEIVER = 402;

    public static final int CAUSE_INTERRUPTED = 411;
    public static final int CAUSE_ERROR = 412;

    public static class Event {

        private MediaServiceProvider provider;

        public MediaServiceProvider getProvider() {
            return provider;
        }

        public boolean isRunning() {
            return provider != null;
        }

        public Event(MediaServiceProvider provider) {
            this.provider = provider;
        }
    }

    //region Service binding

    private MediaServiceProvider mediaServiceProvider = new MediaServiceProvider();

    @Override
    public IBinder onBind(Intent intent) {
        return mediaServiceProvider;
    }

    public class MediaServiceProvider extends android.os.Binder {

        private MediaService service = MediaService.this;

        MediaService getService() {
            return service;
        }
    }

    //endregion

    //region Binding to app

    public static void bindToAppContext(Context context) {
        if (isBoundToAppContext.compareAndSet(false, true)) {
            context.getApplicationContext().bindService(
                new Intent(context, MediaService.class), appContextServiceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    public static void unbindFromAppContext(Context context) {
        if (isBoundToAppContext.compareAndSet(true, false)) {
            Log.d("MediaService", "Detaching from app context...");
            context.getApplicationContext().unbindService(appContextServiceConnection);
        }
    }

    public static boolean isBoundToAppContext() {
        return isBoundToAppContext.get();
    }

    private static AtomicBoolean isBoundToAppContext = new AtomicBoolean(false);
    private static ServiceConnection appContextServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("MediaService", "Attached to app context");
            isBoundToAppContext.set(true);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("MediaService", "Detached from app context");
            isBoundToAppContext.set(false);
        }
    };

    //endregion

    private CountDownLatch latch;

    private boolean isPaused;

    private Pcms16beReceiver audioReceiver;
    private MjpegReceiver videoReceiver;
    private AudioManager audioManager;

    //region Receivers

    public MjpegReceiver startVideoReceiver(RtspClient rtspClient) {
        if (rtspClient.isConnected()) {
            stopVideoReceiver();
            videoReceiver = new MjpegReceiver(ID_MJPEG_RECEIVER, rtspClient.getVideoSocket(), this);
            videoReceiver.start();
            Log.d("MediaService", "MjpegReceiver was started");
            return videoReceiver;
        }
        return null;
    }

    public void stopVideoReceiver() {
        if (videoReceiver != null) {
            videoReceiver.interrupt();
            videoReceiver = null;
            Log.d("MediaService", "MjpegReceiver was stopped");
        }
    }

    public MjpegReceiver getVideoReceiver() {
        return videoReceiver;
    }

    public Pcms16beReceiver startAudioReceiver(RtspClient rtspClient) {
        if (rtspClient.isConnected()) {
            stopAudioReceiver();
            if (rtspClient.getAudioSocket() != null) {
                audioManager.requestAudioFocus(
                        i -> Log.d("AUDIO_FOCUS_GAINED", String.valueOf(i)),
                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                audioReceiver = new Pcms16beReceiver(ID_PCM_U8_RECEIVER, rtspClient.getAudioSocket(), this);
                audioReceiver.start();
                Log.d("MediaService", "Pcms16beReceiver was started");
                return audioReceiver;
            }
        }
        return null;
    }

    public void stopAudioReceiver() {
        if (audioReceiver != null) {
            audioReceiver.interrupt();
            audioReceiver = null;
            audioManager.abandonAudioFocus(i -> Log.d("AUDIO_FOCUS_ABANDONED", String.valueOf(i)));
            Log.d("MediaService", "Pcms16beReceiver was stopped");
        }
    }

    public Pcms16beReceiver getAudioReceiverStarted() {
        return audioReceiver;
    }

    //endregion

    public MediaService() {
        Log.d("MediaService", "Instance created (constructor)");
        pause();
    }

    //region Lifecycle methods

    @Override
    public void onCreate() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        stopAudioReceiver();
        stopVideoReceiver();
        mediaServiceProvider.service = null;
        EventBus.getDefault().post(new Event(null));
        super.onDestroy();
        Log.d("MediaService", "Service destroyed");
    }

    //endregion

    //region Event handlers

    public void onEvent(RtspEvent event) {
        if (!event.isConnected()) {
            pause();
            stopSelf();
        }
    }

    public void onEvent(RtspClient.RtpTimeChangedEvent event) {
        if (RtspClient.RTSP_TRACK_VIDEO.equals(event.getTrackName())) {
            videoReceiver.getRtpParser().setRtpOffset(event.getValue());
        }
        Log.d("MediaService", String.format(
                "rtptime of %s updated: %d", event.getTrackName(), event.getValue()));
    }

    //endregion

    //region LatchSynchroniser implementation

    public void pause() {
        if (!isPaused) {
            latch = new CountDownLatch(1);
            isPaused = true;
            Log.d("MediaService", "MediaService is paused");
        } else {
            Log.d("MediaService", "MediaService is already paused");
        }
    }

    public void resume() {
        Log.d("MediaService", "MediaService resumed");
        isPaused = false;
        latch.countDown();
    }

    @Override
    public boolean isPaused() {
        return isPaused;
    }

    @Override
    public CountDownLatch getLatch() {
        return latch;
    }

    @Override
    public void onClientError(int id, int cause) {
        stopSelf();
    }

    //endregion
}