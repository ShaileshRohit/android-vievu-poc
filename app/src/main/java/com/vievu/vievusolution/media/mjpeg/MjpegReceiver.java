package com.vievu.vievusolution.media.mjpeg;


import android.util.Log;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.media.MediaService;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class MjpegReceiver extends Thread {

    private static final int MAX_JPEG_FRAGMENT_LENGTH = 1500;

    private MjpegRtpParser mjpegParser = new MjpegRtpParser();

    private int id;

    private LatchSynchroniser synchroniser;

    private DatagramSocket videoSocket;

    private boolean isPlayingVideo = false;

    public MjpegReceiver(int id, DatagramSocket videoSocket, LatchSynchroniser synchroniser) {
        this.id = id;
        this.videoSocket = videoSocket;
        this.synchroniser = synchroniser;
    }

    public MjpegRtpParser getRtpParser() {
        return this.mjpegParser;
    }

    @Override
    public void run() {
        if (!isPlayingVideo) {
            isPlayingVideo = true;
            mjpegParser.reset();
            byte[] buffer = new byte[MAX_JPEG_FRAGMENT_LENGTH];
            DatagramPacket packet;
            boolean hasError = false;
            int eagainCounter = 0;
            try {
                while (!isInterrupted()) {
                    if (synchroniser.isPaused()) {
                        synchroniser.getLatch().await();
                    }
                    packet = new DatagramPacket(buffer, MAX_JPEG_FRAGMENT_LENGTH);
                    try {
                        videoSocket.receive(packet);
                        mjpegParser.nextPacket(buffer, packet.getLength());
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        if ((ex.getCause() == null && !synchroniser.isPaused() && !ex.getMessage().contains("Try again"))
                            || (ex.getCause() != null && !ex.getCause().getMessage().contains("EAGAIN"))
                            || eagainCounter > CommonConstants.RETRY_COUNT) {

                            hasError = true;
                            break;
                        } else {
                            eagainCounter++;
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (hasError) {
                synchroniser.onClientError(id, MediaService.CAUSE_ERROR);
                //TODO : Add causes
            }
            isPlayingVideo = false;
            Log.d("MjpegReceiver", "Loop finished");
        } else {
            Log.d("MjpegReceiver", "Loop not started");
        }
    }
}
