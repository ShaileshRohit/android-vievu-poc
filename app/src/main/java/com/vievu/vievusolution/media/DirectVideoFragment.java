package com.vievu.vievusolution.media;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.vievu.vievusolution.CameraClient;
import com.vievu.vievusolution.R;
import com.vievu.vievusolution.media.mjpeg.MjpegRtpParser;
import com.vievu.vievusolution.media.mjpeg.RtspClient;
import com.vievu.vievusolution.ui.UiUtil;
import com.vievu.vievusolution.util.DateUtil;

import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.Bind;
import butterknife.OnClick;

public class DirectVideoFragment extends LiveViewFragment implements SeekBar.OnSeekBarChangeListener {

    public static DirectVideoFragment create(boolean isAutoPlayEnabled, Bitmap thumbnail) {
        DirectVideoFragment fragment = new DirectVideoFragment();
        fragment.isAutoPlayEnabled = isAutoPlayEnabled;
        fragment.isPlaying = fragment.isAutoPlayEnabled;
        fragment.isStartedPlaying = fragment.isAutoPlayEnabled;
        fragment.lastBitmap = thumbnail;
        return fragment;
    }

    @Bind(R.id.playButton) ImageButton playButton;
    @Bind(R.id.progressBar) SeekBar progressBar;
    @Bind(R.id.elapsedTimeTextView) TextView elapsedTimeTextView;
    @Bind(R.id.totalTimeTextView) TextView totalTimeTextView;

    private double videoLengthMs = -1;
    private long offsetMs = 0;

    private boolean isStartedPlaying = false;
    private boolean forcePauseMode = false;

    private AtomicBoolean areProgressUpdatesEnabled = new AtomicBoolean(true);

    protected int getLayoutId() {
        return R.layout.fragment_direct_video;
    }

    public DirectVideoFragment() {
        super();

        this.usesEvents = true;
        this.isPlaying = true;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setPlaying(this.isPlaying);
        setupProgressViews();
        progressBar.setEnabled(isAutoPlayEnabled || isStartedPlaying);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (lastBitmap != null && mjpegView != null) {
            mjpegView.post(() -> {
                if (mjpegView != null) {
                    mjpegView.drawBitmap(lastBitmap);
                }
            });
        }
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        this.videoLengthMs = CameraClient.getInstance().getRtspClient().getCurrentLength() * 1000;
        setupProgressViews();
    }

    @Override
    public void onJpegLoaded(MjpegRtpParser.FrameData frameData) {
        super.onJpegLoaded(frameData);
        if (this.forcePauseMode) {
            this.forcePauseMode = false;
            CameraClient.getInstance().getRtspClient().pauseAsync();
        }
        Activity activity = getActivity();
        if (activity != null && areProgressUpdatesEnabled.get()) {
            activity.runOnUiThread(() -> {
                if (progressBar != null) {
                    long newTime = frameData.frameTime + offsetMs;
                    progressBar.setProgress((int) (newTime / videoLengthMs * 100));
                    elapsedTimeTextView.setText(
                            DateUtil.Formatters.UTC.DURATION.format(new Date(newTime)));
                }
            });
        }
    }

    private void setupProgressViews() {
        if (this.videoLengthMs > 0) {
            UiUtil.colorizeProgressBar(progressBar, R.color.accent);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                //noinspection deprecation
                UiUtil.colorizeDrawable(
                    progressBar.getThumb(),
                    getActivity().getResources().getColor(R.color.accent));
            }
            setupTimeTextViews();
            progressBar.setOnSeekBarChangeListener(this);

        } else {
            progressBar.setVisibility(View.GONE);
            elapsedTimeTextView.setVisibility(View.GONE);
            totalTimeTextView.setVisibility(View.GONE);
        }
    }

    private void setupTimeTextViews() {
        DateFormat formatter = DateUtil.Formatters.UTC.DURATION;
        elapsedTimeTextView.setText(formatter.format(new Date(0)));
        totalTimeTextView.setText(formatter.format(
            new Date((long) CameraClient.getInstance().getRtspClient().getCurrentLength() * 1000)));
    }

    private void setImageButtonEnabled(ImageButton imageButton, boolean isEnabled) {
        imageButton.setEnabled(isEnabled);
        imageButton.setAlpha(isEnabled ? 1.0f : 0.5f);
    }

    private void updateTimeOffset() {
        this.offsetMs = (long) (progressBar.getProgress() * videoLengthMs / 100);

        //TODO: Remove this workaround, when the camera will be able to seek to zero second positions
        if (this.offsetMs < 1000) {
            this.offsetMs = 1000;
        }
    }

    @Override
    public void startMediaProcessing() {
        if (this.isAutoPlayEnabled && !this.isStartedPlaying) {
            this.isPlaying = false;
        }
        super.startMediaProcessing();
        this.isStartedPlaying |= this.isPlaying;
        if (isPlaying) {
            setPlaying(true);
            setMediaControlsLocked(false);
        }
    }

    public void start() {
        Log.d("DirectVideoFragment", "Start: begin auto play");
        setPlaying(true);
    }

    public void setPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
        if (playButton != null) {
            playButton.setImageResource(isPlaying ? R.drawable.ic_action_pause : R.drawable.ic_action_play);
            setImageButtonEnabled(playButton, true);
            progressBar.setEnabled(true);
        }
    }

    @OnClick(R.id.playButton)
    public void onPlayButtonClick() {
        setMediaControlsLocked(true);
        resetHideOverlayTimer();
        if (isPlaying) {
            getLiveViewActivity().getMediaService().pause();
            CameraClient.getInstance().getRtspClient().pauseAsync();
        } else if (this.isStartedPlaying){
            updateTimeOffset();
            CameraClient.getInstance().getRtspClient().playAsync(null);
        } else {
            getLiveViewActivity().replayDirectVideo();
        }
    }

    public void onEventMainThread(RtspClient.StopEvent event) {
        Log.d("DirectVideoFragment", "Received video stop event");
        setPlaying(false);
        if (progressBar != null) {
            progressBar.setProgress(progressBar.getMax());
            mediaControlsLock.setVisibility(View.VISIBLE);
            progressBar.setEnabled(false);
        }
        CameraClient.getInstance().stopVideoAsync();
        getLiveViewActivity().restartToPlay(this.lastBitmap);
    }

    public void onEventMainThread(RtspClient.PlayEvent event) {
        Log.d("DirectVideoFragment", "Received video play event: " + event.isPlaying());
        setPlaying(event.isPlaying() && !this.forcePauseMode);
        if (event.isPlaying()) {
            getLiveViewActivity().getMediaService().resume();
        } else {
            getLiveViewActivity().getMediaService().pause();
        }
        setMediaControlsLocked(this.forcePauseMode);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isPlaying && !getActivity().isChangingConfigurations()) {
            getLiveViewActivity().restartToPlay(this.lastBitmap);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        elapsedTimeTextView.setText(
            DateUtil.Formatters.UTC.DURATION.format(
                new Date((long) (i * videoLengthMs  / 100))));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        areProgressUpdatesEnabled.set(false);
        stopHideOverlayTimer();
        this.forcePauseMode = !this.isPlaying;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        setMediaControlsLocked(true);
        onProgressChanged(null, seekBar.getProgress(), false);
        getLiveViewActivity().getMediaService().pause();
        updateTimeOffset();
        CameraClient.getInstance().getRtspClient().seekToAsync(this.offsetMs / 1000);
        areProgressUpdatesEnabled.set(true);
        startHideOverlayTimer();
    }
}
