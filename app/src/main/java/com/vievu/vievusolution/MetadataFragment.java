package com.vievu.vievusolution;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vievu.vievusolution.details.files.FileEditFragment;
import com.vievu.vievusolution.ui.ActionDrawableEditText;

import butterknife.ButterKnife;

public class MetadataFragment extends FileEditFragment {

    public static MetadataFragment create(int caseId, String caseName) {
        MetadataFragment fragment = new MetadataFragment();
        Bundle arguments = new Bundle(2);
        arguments.putInt(CommonConstants.EXTRA_CASE_ID, caseId);
        arguments.putString(CommonConstants.EXTRA_CASE_NAME, caseName);
        fragment.setArguments(arguments);
        return fragment;
    }

    public MetadataFragment() {
        this.menuLayoutId = R.menu.menu_upload;
    }

    @Override
    protected int getCurrentTitleId() {
        return R.string.title_activity_metadata;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        //noinspection ConstantConditions
        ButterKnife.findById(view, R.id.userInfoContainer).setVisibility(View.GONE);
        if (getArguments() != null && getArguments().containsKey(CommonConstants.EXTRA_CASE_ID)) {
            changedItem.setCaseId(getArguments().getInt(CommonConstants.EXTRA_CASE_ID));
            changedItem.setCaseName(getArguments().getString(CommonConstants.EXTRA_CASE_NAME));
            getDetailsActivity().setTitle(R.string.title_activity_case_details);
            if (caseNameEditText != null) {
                caseNameEditText.setText(changedItem.getCaseName());
                if (caseNameEditText instanceof ActionDrawableEditText) {
                    ((ActionDrawableEditText)caseNameEditText).setActionDrawableVisible(false);
                }
            }
        } else {
            getDetailsActivity().setTitle(R.string.title_activity_home);
        }

        return view;
    }

    @Override
    protected void setUserImage(int userId) {
        //Suppress avatar loading
    }

    @Override
    protected boolean validateViews() {
        return true;
    }
}
