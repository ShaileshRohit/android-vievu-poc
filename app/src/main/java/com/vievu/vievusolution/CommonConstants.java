package com.vievu.vievusolution;

public interface CommonConstants {

    String DEFAULT_ENCODING = "UTF-16LE";
    String DEFAULT_CAMERA_IP = "172.10.0.1";
    //xnjString DEFAULT_CAMERA_IP = "192.168.1.1";
    int DEFAULT_PORT_PTPIP = 15740;
    int DEFAULT_PORT_RTSP = 554;
    String DEFAULT_VIDEO_URI = "rtsp://" + DEFAULT_CAMERA_IP + "/MJPG?W=640&H=360&Q=50&BR=3000000";
    String DEFAULT_FTP_LOGIN = "wificam";
    String DEFAULT_FTP_PASSWORD = "wificam";

    int TIMEOUT_RETRY = 1000;
    int TIMEOUT_SOCKET = 5000;
    int TIMEOUT_KEEP_ALIVE = 40000;
    int TIMEOUT_LOCATION = 5000;

    int RETRY_COUNT = 5;

    String KEY_ACCOUNT_NAME = "account_name";
    String KEY_AUTH_TOKEN = "auth_token";
    String KEY_AUTH_INFO = "account_info";
    String KEY_ID_TOKEN = "id_token";
    String KEY_IDP_ENABLED = "IDP_Enabled";

    String EXTRA_FILTER_BUNDLE_ID = "filter_bundle_id";
    String EXTRA_CASE_ID = "case_id";
    String EXTRA_CASE_NAME = "case_name";
}
