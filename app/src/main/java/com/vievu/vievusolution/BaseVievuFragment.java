package com.vievu.vievusolution;

import android.app.Fragment;
import android.os.Bundle;

import com.vievu.vievusolution.util.ActivityUtil;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class BaseVievuFragment extends Fragment {

    protected boolean usesEvents = false;

    protected BaseVievuActivity getVievuActivity() {
        return (BaseVievuActivity)getActivity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (usesEvents) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ActivityUtil.hideKeyboard(getActivity());
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (usesEvents) {
            EventBus.getDefault().unregister(this);
        }
    }
}
