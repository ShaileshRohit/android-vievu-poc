package com.vievu.vievusolution.ptpip.constants;

public class OperationCodes {
    public static final short Undefined = 0x1000;
    public static final short GetDeviceInfo = 0x1001;
    public static final short OpenSession = 0x1002;
    public static final short CloseSession = 0x1003;
    public static final short GetStorageIDs = 0x1004;
    public static final short GetStorageInfo = 0x1005;
    public static final short GetNumObjects = 0x1006;
    public static final short GetObjectHandles = 0x1007;
    public static final short GetObjectHandlesExt = (short)0x9805;
    public static final short GetObjectInfo = 0x1008;
    public static final short GetObject = 0x1009;
    public static final short GetThumb = 0x100A;
    public static final short DeleteObject = 0x100B;
    public static final short SendObjectInfo = 0x100C;
    public static final short SendObject = 0x100D;
    public static final short InitiateCapture = 0x100E;
    public static final short FormatStore = 0x100F;
    public static final short ResetDevice = 0x1010;
    public static final short SelfTest = 0x1011;
    public static final short SetObjectProtection = 0x1012;
    public static final short PowerDown = 0x1013;
    public static final short GetDevicePropDesc = 0x1014;
    public static final short GetDevicePropValue = 0x1015;
    public static final short SetDevicePropValue = 0x1016;
    public static final short ResetDevicePropValue = 0x1017;
    public static final short TerminateOpenCapture = 0x1018;
    public static final short MoveObject = 0x1019;
    public static final short CopyObject = 0x101A;
    public static final short GetPartialObject = 0x101B;
    public static final short InitiateOpenCapture = 0x101C;
    public static final short iCatchHostRequest = (short)0x9601;
    public static final short iCatchInitiateOpenCapture = (short)0x9602;
    public static final short GetAllDevicePropDesc = (short)0x9614;
}
