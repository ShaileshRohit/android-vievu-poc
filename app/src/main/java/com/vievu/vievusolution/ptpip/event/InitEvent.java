package com.vievu.vievusolution.ptpip.event;

import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.request.PtpIpRequest;

public class InitEvent extends PtpIpRequest {

    private int sessionId;

    public InitEvent(int sessionId) {
        super(12, 3);
        this.sessionId = sessionId;
    }

    @Override
    public byte[] toBytes() {
        byte[] data = super.toBytes();
        ByteHelper.dumpInt(data, sessionId, 8);
        return data;
    }
}
