package com.vievu.vievusolution.ptpip.request;

import com.vievu.vievusolution.ptpip.constants.OperationCodes;
import com.vievu.vievusolution.ptpip.parameter.IntParameter;

public class OpenSession extends OperationRequest {

    public OpenSession(int sessionId) {
        super(OperationCodes.OpenSession);
        transactionId = sessionId;
        appendParameter(new IntParameter(1));
        //appendParameter(new IntParameter(transactionId));
    }
}
