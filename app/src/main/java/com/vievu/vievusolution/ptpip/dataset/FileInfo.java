package com.vievu.vievusolution.ptpip.dataset;

import android.graphics.Bitmap;
import android.util.SparseArray;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.constants.FileFormats;
import com.vievu.vievusolution.ptpip.parameter.IPtpIpParameter;
import com.vievu.vievusolution.ptpip.parameter.IntCastable;
import com.vievu.vievusolution.ptpip.parameter.ParameterFactory;
import com.vievu.vievusolution.ptpip.parameter.ShortParameter;
import com.vievu.vievusolution.ptpip.parameter.StringParameter;
import com.vievu.vievusolution.util.DateUtil;

import java.text.ParseException;
import java.util.Date;

public class FileInfo {

    public static final int PROPERTY_STORAGE_ID = 0xDC01;
    public static final int PROPERTY_FILE_TYPE = 0xDC02;
    public static final int PROPERTY_FILENAME = 0xDC07;
    public static final int PROPERTY_SIZE = 0xDC04;
    public static final int PROPERTY_WIDTH = 0xDC87;
    public static final int PROPERTY_HEIGHT = 0xDC88;
    public static final int PROPERTY_COLOR_DEPTH = 0xDCD3;
    public static final int PROPERTY_DATE_CREATED = 0xDC08;
    public static final int PROPERTY_DATE_MODIFIED = 0xDC09;

    public static class FileInfoProperty {

        public int fileInfoId;

        public short propertyCode;

        public short propertyType;

        public IPtpIpParameter value;

        public int getSize() {
            return 8 + (value == null ? 0 : value.getSize());
        }

        @Override
        public String toString() {
            if (value instanceof StringParameter) {
                return ((StringParameter)value).getValue();
            } else {
                return super.toString();
            }
        }
    }

    public static FileInfoProperty parseProperty(byte[] data, int offset) {
        FileInfoProperty property = new FileInfoProperty();
        property.fileInfoId = ByteHelper.getInt(data, offset);
        offset += 4;
        property.propertyCode = ByteHelper.getShort(data, offset);
        offset += 2;
        property.propertyType = ByteHelper.getShort(data, offset);
        offset += 2;
        property.value = ParameterFactory.create(data, offset, property.propertyType & 0xFFFF);
        return property;
    }

    private int id;
    private SparseArray<FileInfoProperty> properties;
    private Bitmap thumbnail;
    private boolean isInvalid;
    private Date dateCreated;

    public boolean isInvalid() {
        return isInvalid;
    }

    public void markAsInvalid() {
        this.isInvalid = true;
    }

    public SparseArray<FileInfoProperty> getProperties() {
        return properties;
    }

    public int getId() {
        return id;
    }

    public int getSize() {
        return ((IntCastable) get(PROPERTY_SIZE).value).getInt();
    }

    public String getName() {
        return this.get(FileInfo.PROPERTY_FILENAME).toString();
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isImage() {
        short fileType = ((ShortParameter)properties.
                get(FileInfo.PROPERTY_FILE_TYPE).value).getValue();
        return fileType == FileFormats.EXIF_JPEG;
    }

    public Date getDateCreated() {
        if (dateCreated == null) {
            try {
                dateCreated = DateUtil.Formatters.CAMERA.parse(
                    get(FileInfo.PROPERTY_DATE_CREATED).toString());
            } catch (ParseException e) {
                e.printStackTrace();
                dateCreated = new Date(0);
            }
        }
        return dateCreated;
    }

    public FileInfo(int id) {
        this.id = id;
        properties = new SparseArray<>();
    }

    public void addProperty(FileInfoProperty property) {
        properties.append(property.propertyCode & 0xFFFF, property);
    }

    public FileInfoProperty get(int code) {
        return properties.get(code);
    }

    public String getVideoUrl() {
        return String.format("rtsp://%s/VIDEO/%s", CommonConstants.DEFAULT_CAMERA_IP, getName());
    }
}
