package com.vievu.vievusolution.ptpip;

import com.vievu.vievusolution.ptpip.request.PtpIpRequest;

public class OperationMessage extends PtpIpRequest {

    protected short code;

    protected int transactionId;

    public short getCode() {
        return code;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public OperationMessage(int length, int type) {
        super(length, type);
    }
}