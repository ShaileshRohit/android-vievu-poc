package com.vievu.vievusolution.ptpip.parameter;

public abstract class Form {

    public static final int FORM_EMPTY = 0;

    public static final int FORM_RANGE = 1;

    public static final int FORM_ENUM = 2;

    public abstract int getFormType();
}
