package com.vievu.vievusolution.ptpip.response;

import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.OperationMessage;
import com.vievu.vievusolution.ptpip.constants.ResponseCodes;

public class OperationResponse extends OperationMessage {

    protected static final int PAYLOAD_OFFSET = 6;

    public OperationResponse(int length, byte[] payload) {
        super(length, 7);
        code = ByteHelper.getShort(payload, 0);
        transactionId = ByteHelper.getInt(payload, 2);
    }

    public boolean isOk() {
        return code == ResponseCodes.OK;
    }
}
