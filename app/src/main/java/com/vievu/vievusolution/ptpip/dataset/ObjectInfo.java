package com.vievu.vievusolution.ptpip.dataset;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class ObjectInfo {

    //region Properties

    private int storageId;
    private short format;
    private short protection;
    private int objectCompressedSize;
    private short thumbFormat;
    private int thumbCompressedSize;
    private int thumbPixWidth;
    private int thumbPixHeight;
    private int imagePixWidth;
    private int imagePixHeight;
    private int imageBitDepth;
    private int parent;
    private short associationType;
    private int associationDesc;
    private int sequenceNumber;
    private String fileName;
    private String captured;
    private String modified;
    private String keywords;

    public int getStorageId() {
        return storageId;
    }

    public short getFormat() {
        return format;
    }

    public short getProtection() {
        return protection;
    }

    public int getObjectCompressedSize() {
        return objectCompressedSize;
    }

    public short getThumbFormat() {
        return thumbFormat;
    }

    public int getThumbCompressedSize() {
        return thumbCompressedSize;
    }

    public int getThumbPixWidth() {
        return thumbPixWidth;
    }

    public int getThumbPixHeight() {
        return thumbPixHeight;
    }

    public int getImagePixWidth() {
        return imagePixWidth;
    }

    public int getImagePixHeight() {
        return imagePixHeight;
    }

    public int getImageBitDepth() {
        return imageBitDepth;
    }

    public int getParent() {
        return parent;
    }

    public short getAssociationType() {
        return associationType;
    }

    public int getAssociationDesc() {
        return associationDesc;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public String getCaptured() {
        return captured;
    }

    public String getModified() {
        return modified;
    }

    public String getKeywords() {
        return keywords;
    }

    //endregion

    public ObjectInfo(byte[] data) {
        int offset = 0;
        storageId = ByteHelper.getInt(data, offset);
        offset += 4;
        format = ByteHelper.getShort(data, offset);
        offset += 2;
        protection = ByteHelper.getShort(data, offset);
        offset += 2;
        objectCompressedSize = ByteHelper.getInt(data, offset);
        offset += 4;
        thumbFormat = ByteHelper.getShort(data, offset);
        offset += 2;
        thumbCompressedSize = ByteHelper.getInt(data, offset);
        offset += 4;
        thumbPixWidth = ByteHelper.getInt(data, offset);
        offset += 4;
        thumbPixHeight = ByteHelper.getInt(data, offset);
        offset += 4;
        imagePixWidth = ByteHelper.getInt(data, offset);
        offset += 4;
        imagePixHeight = ByteHelper.getInt(data, offset);
        offset += 4;
        imageBitDepth = ByteHelper.getInt(data, offset);
        offset += 4;
        parent = ByteHelper.getInt(data, offset);
        offset += 4;
        associationType = ByteHelper.getShort(data, offset);
        offset += 2;
        associationDesc = ByteHelper.getInt(data, offset);
        offset += 4;
        sequenceNumber = ByteHelper.getInt(data, offset);
        offset += 4;
        fileName = ByteHelper.getUnicodePtpString(data, offset);
        offset += ByteHelper.getPtpStringLength(fileName);
        captured = ByteHelper.getUnicodePtpString(data, offset);
        offset += ByteHelper.getPtpStringLength(captured);
        modified = ByteHelper.getUnicodePtpString(data, offset);
        offset += ByteHelper.getPtpStringLength(modified);
        keywords = ByteHelper.getUnicodePtpString(data, offset);
    }
}
