package com.vievu.vievusolution.ptpip.parameter;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class ByteArrayParameter implements IPtpIpParameter{

    private byte[] value;

    public byte[] getValue() {
        return value;
    }

    @Override
    public int getSize() {
        return value.length + 4;
    }

    @Override
    public byte[] dump(byte[] destination, int offset) {
        int size = getSize();
        byte[] data = new byte[size];
        ByteHelper.dumpInt(destination, value.length, offset);
        System.arraycopy(value, 0, data, offset + 4, size);
        return data;
    }

    public ByteArrayParameter(byte[] data, int offset) {
        int size = ByteHelper.getInt(data, offset);
        value = new byte[size];
        System.arraycopy(data, offset + 4, value, 0, size);
    }
}
