package com.vievu.vievusolution.ptpip;

import com.vievu.vievusolution.CommonConstants;

import java.io.UnsupportedEncodingException;

public class ByteHelper {

    public static int getInt(byte[] data) {
        return getInt(data, 0);
    }

    public static int getInt(byte[] data, int offset) {
        return (data[offset + 3] & 0xFF) << 24 |
               (data[offset + 2] & 0xFF) << 16 |
               (data[offset + 1] & 0xFF) <<  8 |
               (data[offset] & 0xFF);
    }

    public static short getShort(byte[] data, int offset) {
        return (short)(
               (data[offset + 1] & 0xFF) <<  8 |
               (data[offset] & 0xFF));
    }

    public static long getLong(byte[] data, int offset) {
        return ((long)(getInt(data, offset + 4))) << 32 |
               ((long) getInt(data, offset));
    }

    public static byte[] dumpInt(byte[] destination, int value, int offset) {

        for (int i = 0; i < 4; i++)
        {
            destination[offset + i] = (byte)((value >> (i * 8)) & 0xFF);
        }
        return destination;
    }

    public static byte[] dumpLong(byte[] destination, long value, int offset) {

        for (int i = 0; i < 8; i++)
        {
            destination[offset + i] = (byte)((value >> (i * 8)) & 0xFF);
        }
        return destination;
    }

    public static byte[] dumpShort(byte[] destination, short value, int offset) {

        destination[offset] = (byte)(value & 0xFF);
        destination[offset + 1] = (byte)((value & 0xFF00) >> 8);
        return destination;
    }

    public static String getUnicodePtpString(byte[] data, int offset) {

        try {
            int byteLength = data[offset] & 0xFF;
            return byteLength > 0 ?
                    new String(data, offset + 1, byteLength * 2 - 2, CommonConstants.DEFAULT_ENCODING) : null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new IllegalStateException(
                    "UnsupportedEncodingException when trying to use " +
                    CommonConstants.DEFAULT_ENCODING);
        }
    }

    public static int getPtpStringLength(String value) {
        return value != null ? value.length() * 2 + 3 : 1;
    }

    public static int asInt(short value) {
        return value & 0xFFFF;
    }
}
