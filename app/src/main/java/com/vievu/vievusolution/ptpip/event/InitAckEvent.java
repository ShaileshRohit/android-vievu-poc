package com.vievu.vievusolution.ptpip.event;

import com.vievu.vievusolution.ptpip.PtpIpMessage;

public class InitAckEvent extends PtpIpMessage {
    public InitAckEvent() {
        super(8, 4);
    }
}
