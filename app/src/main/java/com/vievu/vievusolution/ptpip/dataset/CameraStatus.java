package com.vievu.vievusolution.ptpip.dataset;

import com.vievu.vievusolution.util.StringUtil;

public class CameraStatus {

    private byte chargeLevel;
    private long freeSpace = 0;
    private long capacity = 0;

    public byte getChargeLevel() {
        return chargeLevel;
    }

    public void setChargeLevel(byte chargeLevel) {
        this.chargeLevel = chargeLevel;
    }

    public long getFreeSpace() {
        return freeSpace;
    }

    public void addFreeSpace(long freeSpace) {
        this.freeSpace += freeSpace;
    }

    public long getCapacity() {
        return capacity;
    }

    public void addCapacity(long capacity) {
        this.capacity += capacity;
    }

    public String getDisplayedStorageStatus() {
        return String.format("%s / %s",
                StringUtil.getShortSizeString(this.freeSpace),
                StringUtil.getShortSizeString(this.capacity));
    }

    @Override
    public String toString() {
        return String.format("%d%%, %d / %d", chargeLevel, freeSpace, capacity);
    }
}
