package com.vievu.vievusolution.ptpip.response.data;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class DataStartPacket extends DataPacketBase {

    public DataStartPacket(int length, byte[] payload) {
        super(length, 9, payload);
        dataSize = ByteHelper.getInt(payload, 4);
    }

    public DataStartPacket(int dataSize) {
        super(20, 9);
        this.dataSize = dataSize;
    }

    @Override
    public byte[] toBytes() {
        byte[] result = super.toBytes();
        ByteHelper.dumpInt(result, dataSize, 12);
        return result;
    }
}
