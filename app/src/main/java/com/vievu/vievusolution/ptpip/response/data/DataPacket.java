package com.vievu.vievusolution.ptpip.response.data;

import com.vievu.vievusolution.ptpip.response.ResponseFactory;

import java.io.IOException;
import java.io.OutputStream;

public class DataPacket extends DataPacketBase {

    public boolean isLast() {
        return type == ResponseFactory.TYPE_DATA_END;
    }

    public byte[] payload;

    public void copyPayload(byte[] destination, int offset) {
        System.arraycopy(payload, 4, destination, offset, getDataSize());
    }

    public void flushPayload(OutputStream destination) throws IOException {
        destination.write(payload, 4, getDataSize());
    }

    public DataPacket(byte[] data) {
        super(data.length + 12, ResponseFactory.TYPE_DATA_END);
        this.payload = data;
        this.dataSize = data.length;
    }

    public DataPacket(int length, int type, byte[] payload) {
        super(length, type, payload);
        this.payload = payload;
        this.dataSize = payload.length - 4;
    }

    @Override
    public byte[] toBytes() {
        byte[] result = super.toBytes();
        System.arraycopy(payload, 0, result, 12, payload.length);
        return result;
    }
}
