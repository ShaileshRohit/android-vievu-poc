package com.vievu.vievusolution.ptpip;

public class PtpIpMessage {

    protected int length;

    protected int type;

    public int getLength() {
        return length;
    }

    public int getType() {
        return type;
    }

    public PtpIpMessage(int length, int type) {
        this.length = length;
        this.type = type;
    }
}
