package com.vievu.vievusolution.ptpip.response;

import com.vievu.vievusolution.ptpip.dataset.DevicePropertyInfo;
import com.vievu.vievusolution.ptpip.parameter.ParameterFactory;

public class DevicePropertyInfoResponse extends OperationResponse {

    private DevicePropertyInfo propertyInfo;

    public DevicePropertyInfo getPropertyInfo() {
        return propertyInfo;
    }

    public DevicePropertyInfoResponse(int length, byte[] payload, ParameterFactory parameterFactory) {
        super(length, payload);
        propertyInfo = new DevicePropertyInfo(payload, PAYLOAD_OFFSET, parameterFactory);
    }
}
