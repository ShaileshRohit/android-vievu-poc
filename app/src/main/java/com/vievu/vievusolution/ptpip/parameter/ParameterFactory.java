package com.vievu.vievusolution.ptpip.parameter;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class ParameterFactory {

    public static final int TYPE_BYTE = 1;
    public static final int TYPE_UBYTE = 2;

    public static final int TYPE_SHORT = 3;
    public static final int TYPE_USHORT = 4;

    public static final int TYPE_INT = 5;
    public static final int TYPE_UINT = 6;

    public static final int TYPE_LONG = 7;
    public static final int TYPE_ULONG = 8;

    public static final int TYPE_INT128 = 9;
    public static final int TYPE_UINT128 = 0xA;

    public static final int TYPE_UBYTE_ARRAY = 0x4002;

    public static final int TYPE_STRING = 0xFFFF;

    private int factoryType = -1;

    public int getFactoryType() {
        return factoryType;
    }

    public void setFactoryType(int factoryType) {
        this.factoryType = factoryType;
    }

    public ParameterFactory() {
    }

    public ParameterFactory(int type) {
        this.factoryType = type;
    }

    public IPtpIpParameter create(byte[] data, int offset) {
        return create(data, offset, this.factoryType);
    }

    public static IPtpIpParameter create(byte[] data, int offset, int type) {
        switch (type) {
            case TYPE_BYTE:
            case TYPE_UBYTE:
                return new ByteParameter(data[offset]);
            case TYPE_SHORT:
            case TYPE_USHORT:
                return new ShortParameter(ByteHelper.getShort(data, offset));
            case TYPE_INT:
            case TYPE_UINT:
                return new IntParameter(ByteHelper.getInt(data, offset));
            case TYPE_STRING:
                return new StringParameter(ByteHelper.getUnicodePtpString(data, offset));
            case TYPE_UBYTE_ARRAY:
                return new ByteArrayParameter(data, offset);
            case TYPE_LONG:
            case TYPE_ULONG:
                return new LongParameter(ByteHelper.getLong(data, offset));
            case TYPE_INT128:
            case TYPE_UINT128:
                return new ParameterStub(16);
        }
        return null;
    }
}
