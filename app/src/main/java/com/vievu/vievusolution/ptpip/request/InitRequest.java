package com.vievu.vievusolution.ptpip.request;

import com.vievu.vievusolution.CommonConstants;

import java.io.UnsupportedEncodingException;
import java.util.Random;

public class InitRequest extends PtpIpRequest {

    private byte[] name;

    public InitRequest(String name) throws UnsupportedEncodingException {
        super(0, 1);
        this.name = name.getBytes(CommonConstants.DEFAULT_ENCODING);
        length = this.name.length + 30;
    }

    @Override
    public byte[] toBytes() {
        byte[] data = super.toBytes();
        byte[] guid = new byte[16];
        new Random().nextBytes(guid);
        System.arraycopy(guid, 0, data, 8, 16);
        System.arraycopy(name, 0, data, 24, length - 30);
        data[length - 4] = 1;
        return data;
    }
}
