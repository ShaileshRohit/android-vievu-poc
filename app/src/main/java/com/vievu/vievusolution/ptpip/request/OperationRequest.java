package com.vievu.vievusolution.ptpip.request;

import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.OperationMessage;
import com.vievu.vievusolution.ptpip.parameter.IPtpIpParameter;

import java.util.LinkedList;

public class OperationRequest extends OperationMessage {

    protected LinkedList<IPtpIpParameter> parameters;

    public OperationRequest setTransactionId(int transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public OperationRequest(short operationCode) {
        //super(22, 6);
        super(18, 6);
        this.code = operationCode;
    }

    public OperationRequest appendParameter(IPtpIpParameter parameter) {
        if (parameters == null) {
            parameters = new LinkedList<IPtpIpParameter>();
        }
        parameters.add(parameter);
        length += parameter.getSize();
        return this;
    }

    @Override
    public byte[] toBytes() {
        byte[] data = super.toBytes();
        data[8] = 1;                                                // Flag "Awaits incoming data"
        int offset = 12;
        ByteHelper.dumpShort(data, code, offset);
        offset += 2;
        ByteHelper.dumpInt(data, transactionId, offset);
        offset += 4;
        if (parameters != null) {
            for (IPtpIpParameter parameter : parameters) {
                parameter.dump(data, offset);
                offset += parameter.getSize();
            }
        }
        //data[length - 4] = 1;
        return data;
    }
}
