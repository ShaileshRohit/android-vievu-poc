package com.vievu.vievusolution.ptpip.dataset;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class StorageInfo {

    //region Properties

    private short storageType;
    private short fileSystemType;
    private short accessCapability;
    private long capacity;
    private long freeSpace;
    private int freeSpaceInImages;
    private String description;
    private String volumeLabel;

    public short getStorageType() {
        return storageType;
    }

    public short getFileSystemType() {
        return fileSystemType;
    }

    public short getAccessCapability() {
        return accessCapability;
    }

    public long getCapacity() {
        return capacity;
    }

    public long getFreeSpace() {
        return freeSpace;
    }

    public int getFreeSpaceInImages() {
        return freeSpaceInImages;
    }

    public String getDescription() {
        return description;
    }

    public String getVolumeLabel() {
        return volumeLabel;
    }

    //endregion

    public StorageInfo(byte[] data) {

        int offset = 0;
        storageType = ByteHelper.getShort(data, offset);
        offset += 2;
        fileSystemType = ByteHelper.getShort(data, offset);
        offset += 2;
        accessCapability = ByteHelper.getShort(data, offset);
        offset += 2;
        capacity = ((long)ByteHelper.getInt(data, offset + 4)) << 32 |
                   (ByteHelper.getInt(data, offset) & 0xFFFFFFFFL);
        offset += 8;
        freeSpace = ((long)ByteHelper.getInt(data, offset + 4)) << 32 |
                    (ByteHelper.getInt(data, offset) & 0xFFFFFFFFL);
        offset += 8;
        freeSpaceInImages = ByteHelper.getInt(data, offset);
        offset += 4;
        description = ByteHelper.getUnicodePtpString(data, offset);
        offset += ByteHelper.getPtpStringLength(description);
        volumeLabel = ByteHelper.getUnicodePtpString(data, offset);
    }
}
