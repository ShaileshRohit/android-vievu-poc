package com.vievu.vievusolution.ptpip.parameter;

public interface IPtpIpParameter {

    int getSize();

    byte[] dump(byte[] dest, int offset);
}
