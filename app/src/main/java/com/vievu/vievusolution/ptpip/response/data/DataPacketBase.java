package com.vievu.vievusolution.ptpip.response.data;

import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.request.PtpIpRequest;

public abstract class DataPacketBase extends PtpIpRequest {

    protected int transactionId;

    protected int dataSize;

    public int getDataSize() {
        return dataSize;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public DataPacketBase(int length, int type, byte[] payload) {
        super(length, type);
        transactionId = ByteHelper.getInt(payload);
    }

    public DataPacketBase(int length, int type) {
        super(length, type);
    }

    @Override
    public byte[] toBytes() {
        byte[] result = super.toBytes();
        ByteHelper.dumpInt(result, transactionId, 8);
        return result;
    }
}
