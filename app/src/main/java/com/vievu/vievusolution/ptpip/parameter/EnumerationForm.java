package com.vievu.vievusolution.ptpip.parameter;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class EnumerationForm extends Form{

    private IPtpIpParameter[] values;

    public IPtpIpParameter[] getValues() {
        return values;
    }

    public EnumerationForm(byte[] data, int offset, ParameterFactory factory) {

        int size = ByteHelper.getShort(data, offset);
        values = new IPtpIpParameter[size];
        offset += 2;
        for (int i = 0; i < size; i++) {
            values[i] = factory.create(data, offset);
            offset += values[i].getSize();
        }
    }

    @Override
    public int getFormType() {
        return Form.FORM_ENUM;
    }
}
