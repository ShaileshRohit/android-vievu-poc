package com.vievu.vievusolution.ptpip.event;

import com.vievu.vievusolution.ptpip.response.OperationResponse;


public class PtpIpEvent extends OperationResponse {

    private byte[] payload;

    public byte[] getPayload() {
        return payload;
    }

    public PtpIpEvent(int length, byte[] payload) {
        super(length, payload);
        int size = payload.length - PAYLOAD_OFFSET;
        this.payload = new byte[size];
        System.arraycopy(payload, PAYLOAD_OFFSET, this.payload, 0, size);
    }
}
