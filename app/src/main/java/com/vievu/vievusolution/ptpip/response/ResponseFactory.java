package com.vievu.vievusolution.ptpip.response;

import android.util.Log;

import com.vievu.vievusolution.ptpip.PtpIpMessage;
import com.vievu.vievusolution.ptpip.event.InitAckEvent;
import com.vievu.vievusolution.ptpip.event.PtpIpEvent;
import com.vievu.vievusolution.ptpip.response.data.DataPacket;
import com.vievu.vievusolution.ptpip.response.data.DataStartPacket;

public class ResponseFactory {

    public static final int TYPE_INIT_ACK = 2;

    public static final int TYPE_INIT_ACK_EVENT = 4;

    public static final int TYPE_OPERATION_RESPONSE = 7;

    public static final int TYPE_EVENT = 8;

    public static final int TYPE_DATA_START = 9;

    public static final int TYPE_DATA = 0xA;

    public static final int TYPE_DATA_END = 0xC;

    private OperationFactory operationFactory = new OperationFactory();

    private short requestCode;

    public void setExpectedResult(short requestCode) {
        this.requestCode = requestCode;
    }

    public PtpIpMessage create(int length, int type, byte[] payload) {

        switch (type) {
            case TYPE_INIT_ACK :
                return new InitAckResponse(length, payload);
            case TYPE_INIT_ACK_EVENT :
                return new InitAckEvent();
            case TYPE_EVENT :
                return new PtpIpEvent(length, payload);
            case TYPE_OPERATION_RESPONSE :
                OperationResponse response;
                if (requestCode != 0) {
                    response = operationFactory.create(requestCode, length, payload);
                    requestCode = 0;
                } else {
                    response = new OperationResponse(length, payload);
                }
                if (response != null) {
                    Log.d("ResponseFactory", String.format("Decoded response 0x%h", response.getCode()));
                } else {
                    Log.d("ResponseFactory", "Decoded response failed");
                }
                return response;
            case TYPE_DATA_START :
                return new DataStartPacket(length, payload);
            case TYPE_DATA:
            case TYPE_DATA_END:
                return new DataPacket(length, type, payload);
        }
        return null;
    }
}
