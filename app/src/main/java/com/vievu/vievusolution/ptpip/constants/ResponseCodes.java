package com.vievu.vievusolution.ptpip.constants;

public class ResponseCodes {
    public final static short Undefined = 0x2000;
    public final static short OK = 0x2001;
    public final static short GeneralError = 0x2002;
    public final static short SessionNotOpen = 0x2003;
    public final static short InvalidTransactionID = 0x2004;
    public final static short OperationNotSupported = 0x2005;
    public final static short ParameterNotSupported = 0x2006;
    public final static short IncompleteTransfer = 0x2007;
    public final static short InvalidStorageID = 0x2008;
    public final static short InvalidObjectHandle = 0x2009;
    public final static short DevicePropNotSupported = 0x200A;
    public final static short InvalidObjectFormatCode = 0x200B;
    public final static short StoreFull = 0x200C;
    public final static short ObjectWriteProtected = 0x200D;
    public final static short StoreReadOnly = 0x200E;
    public final static short AccessDenied = 0x200F;
    public final static short NoThumbnailPresent = 0x2010;
    public final static short SelfTestFailed = 0x2011;
    public final static short PartialDeletion = 0x2012;
    public final static short StoreNotAvailable = 0x2013;
    public final static short SpecificationByFormatUnsupported = 0x2014;
    public final static short NoValidObjectInfo = 0x2015;
    public final static short InvalidCodeFormat = 0x2016;
    public final static short UnknownVendorCode = 0x2017;
    public final static short CaptureAlreadyTerminated = 0x2018;
    public final static short DeviceBusy = 0x2019;
    public final static short InvalidParentObject = 0x201A;
    public final static short InvalidDevicePropFormat = 0x201B;
    public final static short InvalidDevicePropValue = 0x201C;
    public final static short InvalidParameter = 0x201D;
    public final static short SessionAlreadyOpen = 0x201E;
    public final static short TransactionCancelled = 0x201F;
    public final static short SpecificationOfDestinationUnsupported = 0x2020;
}
