package com.vievu.vievusolution.ptpip.parameter;

public class ByteParameter implements IPtpIpParameter, IntCastable{

    private byte value;

    public byte getValue() {
        return value;
    }

    public ByteParameter(byte value) {
        this.value = value;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public byte[] dump(byte[] destination, int offset) {
        destination[offset] = value;
        return destination;
    }

    @Override
    public int getInt() {
        return value & 0xFF;
    }
}
