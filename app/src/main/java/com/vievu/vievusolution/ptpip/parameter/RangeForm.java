package com.vievu.vievusolution.ptpip.parameter;

public class RangeForm extends Form {

    private IPtpIpParameter min;

    private IPtpIpParameter max;

    private IPtpIpParameter step;

    public IPtpIpParameter getMin() {
        return min;
    }

    public IPtpIpParameter getMax() {
        return max;
    }

    public IPtpIpParameter getStep() {
        return step;
    }

    public RangeForm(byte[] data, int offset, ParameterFactory factory) {

        min = factory.create(data, offset);
        max = factory.create(data, offset += min.getSize());
        step = factory.create(data, offset += max.getSize());
    }

    @Override
    public int getFormType() {
        return Form.FORM_RANGE;
    }
}
