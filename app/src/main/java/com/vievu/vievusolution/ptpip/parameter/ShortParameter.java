package com.vievu.vievusolution.ptpip.parameter;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class ShortParameter  implements IPtpIpParameter, IntCastable{

    private short value;

    public short getValue() {
        return value;
    }

    public ShortParameter(short value) {
        this.value = value;
    }

    @Override
    public int getSize() {
        return 2;
    }

    @Override
    public byte[] dump(byte[] dest, int offset) {
        return ByteHelper.dumpShort(dest, value, offset);
    }

    @Override
    public int getInt() {
        return value & 0xFFFF;
    }
}