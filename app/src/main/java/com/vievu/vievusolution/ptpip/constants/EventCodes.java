package com.vievu.vievusolution.ptpip.constants;

public class EventCodes {
    public static final short ObjectAdded = 0x4002;
    public static final short ObjectRemoved = 0x4003;
    public static final short StoreAdded = 0x4004;
    public static final short StoreRemoved = 0x4005;
    public static final short DevicePropChanged = 0x4006;
    public static final short DeviceInfoChanged = 0x4008;
    public static final short RequestObjectTransfer = 0x4009;
    public static final short CaptureComplete = 0x400D;
}
