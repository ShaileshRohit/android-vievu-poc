package com.vievu.vievusolution.ptpip.response;

import com.vievu.vievusolution.ptpip.constants.DeviceProperties;
import com.vievu.vievusolution.ptpip.parameter.ParameterFactory;

public class OperationFactory {

    public OperationResponse create(short requestedCommand, int length, byte[] payload) {
        switch (requestedCommand) {
            case DeviceProperties.DateTime :
                return new DevicePropertyInfoResponse(
                        length, payload, new ParameterFactory(ParameterFactory.TYPE_STRING));
        }
        return null;
    }
}
