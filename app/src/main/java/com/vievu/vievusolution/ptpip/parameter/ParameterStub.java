package com.vievu.vievusolution.ptpip.parameter;

public class ParameterStub implements IPtpIpParameter {

    private int size;

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public byte[] dump(byte[] dest, int offset) {
        return dest;
    }

    public ParameterStub(int size) {
        this.size = size;
    }
}
