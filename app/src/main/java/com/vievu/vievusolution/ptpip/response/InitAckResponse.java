package com.vievu.vievusolution.ptpip.response;

import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.PtpIpMessage;

public class InitAckResponse extends PtpIpMessage {

    private int sessionId;

    private byte[] guid;

    public byte[] getGuid() {
        return guid;
    }

    public int getSessionId() {
        return sessionId;
    }

    public InitAckResponse(int length, byte[] payload) {
        super(length, 2);
        sessionId = ByteHelper.getInt(payload);
        guid = new byte[16];
        System.arraycopy(payload, 4, guid, 0, 16);
    }
}
