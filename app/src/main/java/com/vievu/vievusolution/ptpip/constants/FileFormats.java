package com.vievu.vievusolution.ptpip.constants;

public class FileFormats {
    public static final short Undefined = 0x3000;     //  Undefined non-image object
    public static final short Association = 0x3001;   //  Association (e.g. folder)
    public static final short Script = 0x3002;        //  Device-model-specific script
    public static final short Executable = 0x3003;    //  Device-model-specific binary executable
    public static final short Text = 0x3004;          //  Text file
    public static final short HTML = 0x3005;          //  HyperText Markup Language file (text)
    public static final short DPOF = 0x3006;          //  Digital Print Order Format file (text)
    public static final short AIFF = 0x3007;          //  Audio clip
    public static final short WAV = 0x3008;           //  Audio clip
    public static final short MP3 = 0x3009;           //  Audio clip
    public static final short AVI = 0x300A;           //  Video clip
    public static final short MPEG = 0x300B;          //  Video clip
    public static final short ASF = 0x300C;           //  Microsoft Advanced Streaming Format (video)
    public static final short MOV = 0x300D;           //  Camera video Format (video)
    public static final short Unknown = 0x3800;       //  Unknown image object
    public static final short EXIF_JPEG = 0x3801;     //  Exchangeable File Format, JEIDA standard
    public static final short TIFF_EP = 0x3802;       //  Tag Image File Format for Electronic Photography
    public static final short FlashPix = 0x3803;      //  Structured Storage Image Format
    public static final short BMP = 0x3804;           //  Microsoft Windows Bitmap file
    public static final short CIFF = 0x3805;          //  Canon Camera Image File Format
    public static final short Reserved0x3806 = 0x3806;     //  Reserved
    public static final short GIF = 0x3807;           //  Graphics Interchange Format
    public static final short JFIF = 0x3808;          //  JPEG File Interchange Format
    public static final short PCD = 0x3809;           //  PhotoCD Image Pac
    public static final short PICT = 0x380A;          //  Quickdraw Image Format
    public static final short PNG = 0x380B;           //  Portable Network Graphics
    public static final short Reserved0x380C = 0x380C;     //  Reserved
    public static final short TIFF = 0x380D;          //  Tag Image File Format
    public static final short TIFF_IT = 0x380E;       //  Tag Image File Format for Information Technology (graphic arts)
    public static final short JP2 = 0x380F;           //  JPEG2000 Baseline File Format
    public static final short JPX = 0x3810;           //  JPEG2000 Extended File Format
}
