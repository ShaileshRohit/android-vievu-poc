package com.vievu.vievusolution.ptpip.constants;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class CameraMode {

    @IntDef({ERROR,
            VIDEO_OFF,
            SHARE,
            CAMERA,
            APPLICATION,
            EDIT,
            BACKWARD,
            VIDEO_ON })
    @Retention(RetentionPolicy.SOURCE)
    public @interface Value {}

    public static final short ERROR = -1;
    public static final short VIDEO_OFF = 1;
    public static final short SHARE = 2;
    public static final short CAMERA = 3;
    public static final short APPLICATION = 4;
    public static final short EDIT = 5;
    public static final short BACKWARD = 6;
    public static final short VIDEO_ON = 0x11;
}
