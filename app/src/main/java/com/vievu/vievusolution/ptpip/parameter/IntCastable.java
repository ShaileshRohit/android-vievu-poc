package com.vievu.vievusolution.ptpip.parameter;

public interface IntCastable extends IPtpIpParameter {

    int getInt();
}
