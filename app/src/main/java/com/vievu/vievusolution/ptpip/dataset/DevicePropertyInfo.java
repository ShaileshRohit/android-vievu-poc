package com.vievu.vievusolution.ptpip.dataset;

import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.parameter.EnumerationForm;
import com.vievu.vievusolution.ptpip.parameter.Form;
import com.vievu.vievusolution.ptpip.parameter.IPtpIpParameter;
import com.vievu.vievusolution.ptpip.parameter.ParameterFactory;
import com.vievu.vievusolution.ptpip.parameter.RangeForm;

public class DevicePropertyInfo {

    //region Properties

    private short code;
    private short dataType;
    private boolean readonly;
    private IPtpIpParameter defaultValue;
    private IPtpIpParameter currentValue;
    private Form form;

    public short getCode() {
        return code;
    }

    public short getDataType() {
        return dataType;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public IPtpIpParameter getDefaultValue() {
        return defaultValue;
    }

    public IPtpIpParameter getCurrentValue() {
        return currentValue;
    }

    public Form getForm() {
        return form;
    }

    public void setCurrentValue(IPtpIpParameter currentValue) {
        this.currentValue = currentValue;
    }

    //endregion

    public DevicePropertyInfo(byte[] data, int initialOffset, ParameterFactory parameterFactory) {

        int offset = initialOffset;
        code = ByteHelper.getShort(data, offset);
        offset += 2;
        dataType = ByteHelper.getShort(data, offset);
        offset += 2;
        readonly = data[offset] == 0;
        offset++;
        parameterFactory.setFactoryType(dataType & 0xFFFF);
        defaultValue = parameterFactory.create(data, offset);
        offset += defaultValue.getSize();
        currentValue = parameterFactory.create(data, offset);
        offset += currentValue.getSize();
        switch (data[offset++]) {
            case Form.FORM_RANGE :
                this.form = new RangeForm(data, offset, parameterFactory);
                break;
            case Form.FORM_ENUM :
                this.form = new EnumerationForm(data, offset, parameterFactory);
                break;
        }
    }
}
