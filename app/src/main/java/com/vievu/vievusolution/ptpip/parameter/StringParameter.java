package com.vievu.vievusolution.ptpip.parameter;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.ptpip.ByteHelper;

import java.io.UnsupportedEncodingException;

public class StringParameter implements IPtpIpParameter {

    private String value;

    private int size;

    public String getValue() {
        return value;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public byte[] dump(byte[] dest, int offset) {
        try {
            if (value != null) {
                dest[offset] = (byte)(value.length() + 1);
                byte[] rawString = value.getBytes(CommonConstants.DEFAULT_ENCODING);
                System.arraycopy(rawString, 0, dest, offset + 1, rawString.length);
            } else {
                dest[offset] = 0;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new IllegalStateException(
                    "UnsupportedEncodingException when trying to use " +
                    CommonConstants.DEFAULT_ENCODING);
        }
        return  dest;
    }

    public StringParameter(String value) {
        this.value = value;
        this.size = ByteHelper.getPtpStringLength(value);
    }

    @Override
    public String toString() {
        return value;
    }
}
