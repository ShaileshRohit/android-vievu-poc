package com.vievu.vievusolution.ptpip.parameter;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class IntParameter implements IPtpIpParameter, IntCastable{

    private int value;

    @Override
    public int getInt() {
        return value;
    }

    public IntParameter(int value) {
        this.value = value;
    }

    @Override
    public int getSize() {
        return 4;
    }

    @Override
    public byte[] dump(byte[] dest, int offset) {
        return ByteHelper.dumpInt(dest, value, offset);
    }


}
