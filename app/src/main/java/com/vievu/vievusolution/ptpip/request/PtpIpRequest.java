package com.vievu.vievusolution.ptpip.request;

import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.PtpIpMessage;

public class PtpIpRequest extends PtpIpMessage {

    public PtpIpRequest(int length, int type) {
        super(length, type);
    }

    public byte[] toBytes() {

        byte[] data = new byte[length];
        ByteHelper.dumpInt(data, length, 0);
        ByteHelper.dumpInt(data, type, 4);
        return data;
    }
}
