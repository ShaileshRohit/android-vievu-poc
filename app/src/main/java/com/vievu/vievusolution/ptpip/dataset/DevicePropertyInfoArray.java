package com.vievu.vievusolution.ptpip.dataset;

import android.util.SparseArray;

import com.vievu.vievusolution.ptpip.ByteHelper;
import com.vievu.vievusolution.ptpip.parameter.ParameterFactory;

public class DevicePropertyInfoArray {

    private SparseArray<DevicePropertyInfo> properties;

    public SparseArray<DevicePropertyInfo> getProperties() {
        return properties;
    }

    public DevicePropertyInfoArray(byte[] data) {
        int offset  = 0, length;
        properties = new SparseArray<DevicePropertyInfo>();
        ParameterFactory parameterFactory = new ParameterFactory();
        DevicePropertyInfo info;
        while (offset < data.length) {
            length = ByteHelper.getInt(data, offset);
            info = new DevicePropertyInfo(data, offset + 4, parameterFactory);
            properties.append(info.getCode() & 0xFFFF, info);
            offset += length;
        }
    }

    public DevicePropertyInfoArray() {
        properties = new SparseArray<DevicePropertyInfo>();
    }

    public void addProperty(DevicePropertyInfo property) {
        properties.put(property.getCode() & 0xFFFF, property);
    }

    public DevicePropertyInfo get(short code) {
        return properties.get(code & 0xFFFF);
    }
}
