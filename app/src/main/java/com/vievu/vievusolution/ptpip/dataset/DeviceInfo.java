package com.vievu.vievusolution.ptpip.dataset;

import com.vievu.vievusolution.ptpip.ByteHelper;

import java.io.UnsupportedEncodingException;

public class DeviceInfo {

    //region Properties

    private short standardVersion;
    private int versionExtensionID;
    private short vendorExtensionVersion;
    private String vendorExtensionDesc;
    private short functionalMode;
    private short[] operations;
    private short[] events;
    private short[] deviceProperties;
    private short[] captureFormats;
    private short[] imageFormats;

    public short getStandardVersion() {
        return standardVersion;
    }

    public int getVersionExtensionID() {
        return versionExtensionID;
    }

    public short getVendorExtensionVersion() {
        return vendorExtensionVersion;
    }

    public String getVendorExtensionDesc() {
        return vendorExtensionDesc;
    }

    public short getFunctionalMode() {
        return functionalMode;
    }

    public short[] getOperations() {
        return operations;
    }

    public short[] getEvents() {
        return events;
    }

    public short[] getDeviceProperties() {
        return deviceProperties;
    }

    public short[] getCaptureFormats() {
        return captureFormats;
    }

    public short[] getImageFormats() {
        return imageFormats;
    }

    //endregion

    public DeviceInfo(byte[] data) {
        int offset = 0;
        standardVersion = ByteHelper.getShort(data, offset);
        offset += 2;
        versionExtensionID = ByteHelper.getInt(data, offset);
        offset += 4;
        vendorExtensionVersion = ByteHelper.getShort(data, offset);
        offset += 2;
        vendorExtensionDesc = ByteHelper.getUnicodePtpString(data, offset);
        offset += ByteHelper.getPtpStringLength(vendorExtensionDesc);
        functionalMode = ByteHelper.getShort(data, offset);
        offset += 2;
        operations = getArray(data, offset);
        offset += (this.operations.length * 2 + 4);
        events = getArray(data, offset);
        offset += (this.events.length * 2 + 4);
        deviceProperties = getArray(data, offset);
        offset += (this.deviceProperties.length * 2 + 4);
        captureFormats = getArray(data, offset);
        offset += (this.captureFormats.length * 2 + 4);
        imageFormats = getArray(data, offset);
    }

    private short[] getArray(byte[] data, int offset)
    {
        int length = ByteHelper.getInt(data, offset);
        offset += 4;
        short[] array = new short[length];
        for (int i = 0; i < length; i++, offset += 2)
        {
            array[i] = ByteHelper.getShort(data, offset);
        }
        return array;
    }
}
