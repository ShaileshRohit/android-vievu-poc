package com.vievu.vievusolution.ptpip;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import com.vievu.vievusolution.CommonConstants;
import com.vievu.vievusolution.ptpip.constants.CameraMode;
import com.vievu.vievusolution.ptpip.constants.DeviceProperties;
import com.vievu.vievusolution.ptpip.constants.OperationCodes;
import com.vievu.vievusolution.ptpip.constants.ResponseCodes;
import com.vievu.vievusolution.ptpip.dataset.CameraStatus;
import com.vievu.vievusolution.ptpip.dataset.DeviceInfo;
import com.vievu.vievusolution.ptpip.dataset.DevicePropertyInfo;
import com.vievu.vievusolution.ptpip.dataset.DevicePropertyInfoArray;
import com.vievu.vievusolution.ptpip.dataset.FileInfo;
import com.vievu.vievusolution.ptpip.dataset.StorageInfo;
import com.vievu.vievusolution.ptpip.event.InitEvent;
import com.vievu.vievusolution.ptpip.event.PtpIpEvent;
import com.vievu.vievusolution.ptpip.parameter.ByteParameter;
import com.vievu.vievusolution.ptpip.parameter.IPtpIpParameter;
import com.vievu.vievusolution.ptpip.parameter.IntParameter;
import com.vievu.vievusolution.ptpip.parameter.ParameterFactory;
import com.vievu.vievusolution.ptpip.parameter.ShortParameter;
import com.vievu.vievusolution.ptpip.request.InitRequest;
import com.vievu.vievusolution.ptpip.request.OpenSession;
import com.vievu.vievusolution.ptpip.request.OperationRequest;
import com.vievu.vievusolution.ptpip.request.PtpIpRequest;
import com.vievu.vievusolution.ptpip.response.InitAckResponse;
import com.vievu.vievusolution.ptpip.response.OperationResponse;
import com.vievu.vievusolution.ptpip.response.ResponseFactory;
import com.vievu.vievusolution.ptpip.response.data.DataPacket;
import com.vievu.vievusolution.ptpip.response.data.DataPacketBase;
import com.vievu.vievusolution.ptpip.response.data.DataStartPacket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

@SuppressWarnings("WeakerAccess")
public class PtpIpClient {

    public interface FileTransferListener {
        void onProgressUpdate(long completed, long total);

        void onCompleted(short code);
    }

    private ResponseFactory responseFactory = new ResponseFactory();

    private Socket dataSocket;

    private Socket eventSocket;

    private int sessionId;

    private int transactionId = 2;

    private AsyncTask<Void, PtpIpEvent, Void> eventLoop;

    private boolean isExhaustiveLoggingEnabled;

    //region Private methods

    private boolean setUpEventConnection(String ipAddress, int port) throws IOException {
        eventSocket = createSocket(ipAddress, port, 0);
        InitEvent initEvent = new InitEvent(sessionId);
        eventSocket.getOutputStream().write(initEvent.toBytes());
        PtpIpMessage initAckEvent = readPtpIpMessage(eventSocket.getInputStream());
        if (initAckEvent != null && initAckEvent.getType() == ResponseFactory.TYPE_INIT_ACK_EVENT) {
            eventLoop = new AsyncTask<Void, PtpIpEvent, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    PtpIpEvent event;
                    try {
                        while (!isCancelled()) {
                            event = (PtpIpEvent)readPtpIpMessageLockable(eventSocket.getInputStream());
                            if (event != null) {
                                publishProgress(event);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onProgressUpdate(PtpIpEvent... values) {
                    EventBus.getDefault().post(values[0]);
                }
            };
            eventLoop.execute();
            return true;
        }
        return false;
    }

    private Socket createSocket(String ipAddress, int port, int timeout) throws IOException {
        Socket socket = new Socket();
        if (timeout > 0) {
            socket.setSoTimeout(timeout);
            socket.connect(new InetSocketAddress(ipAddress, port), timeout);
        } else {
            socket.connect(new InetSocketAddress(ipAddress, port));
        }
        socket.setKeepAlive(true);
        socket.setTcpNoDelay(true);
        return socket;
    }

    @SuppressWarnings("unchecked")
    private <T extends PtpIpMessage> T readDataStreamMessage() throws IOException {
        return  (T)readPtpIpMessage(dataSocket.getInputStream());
    }

    private PtpIpMessage readPtpIpMessageLockable(InputStream stream) throws IOException {

        byte[] buffer = new byte[8];
        int dataSize = stream.read(buffer, 0, 8);
        int length = ByteHelper.getInt(buffer);
        int type = ByteHelper.getInt(buffer, 4);
        byte counter = 0;
        if (length > 8) {
            buffer = new byte[length - 8];
            do {
                try {
                    dataSize += stream.read(buffer, dataSize - 8, length - dataSize);
                    counter = 0;
                } catch (SocketTimeoutException ste) {
                    if (++counter > CommonConstants.RETRY_COUNT) {
                        throw ste;
                    }
                }
            } while (dataSize < length);
        }
        return responseFactory.create(length, type, buffer);
    }

    private PtpIpMessage readPtpIpMessage(InputStream stream) throws IOException {
        try {
            for (int i = 0; i < CommonConstants.RETRY_COUNT; i++) {
                if (stream.available() > 0) {
                    return readPtpIpMessageLockable(stream);
                }
                Thread.sleep(CommonConstants.TIMEOUT_RETRY, 0);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private DataStartPacket readDataStartPacket(boolean throwOnWrongResponse) throws IOException {
        final PtpIpMessage message = readDataStreamMessage();
        if (message != null) {
            if (message.getType() == ResponseFactory.TYPE_OPERATION_RESPONSE) {
                int intCode = ((OperationResponse) message).getCode() & 0xFFFF;
                if (intCode == ResponseCodes.NoThumbnailPresent) {
                    return new DataStartPacket(0);
                } else if (intCode != ResponseCodes.OK && throwOnWrongResponse) {
                    throw new RuntimeException(String.format("Request failed (code: 0x%h)", intCode));
                } else {
                    return null;
                }
            }
        } else {
            Log.d("PtpIpClient", "Receive data: no start packet");
        }
        return  (DataStartPacket)message;
    }

    private byte[] receiveData(boolean throwOnWrongResponse) throws IOException {

        DataStartPacket startPacket = readDataStartPacket(throwOnWrongResponse);
        if (startPacket != null) {
            int dataSize = startPacket.getDataSize();
            byte[] buffer = new byte[dataSize];
            DataPacket dataPacket;
            int offset = 0;
            do {
                dataPacket = readDataStreamMessage();
                dataPacket.copyPayload(buffer, offset);
                offset += dataPacket.getDataSize();
            } while (!dataPacket.isLast());
            return buffer;
        }
        return null;
    }

    private short saveData(OutputStream outputStream,
                           FileTransferListener listener) throws IOException {
        PtpIpMessage message = readDataStreamMessage();
        if (message != null) {
            if (message.getType() == ResponseFactory.TYPE_DATA_START) {
                DataStartPacket startPacket = (DataStartPacket)message;
                int fileSize = startPacket.getDataSize();
                int completed = 0;
                DataPacket dataPacket;
                do {
                    dataPacket = readDataStreamMessage();
                    dataPacket.flushPayload(outputStream);
                    if (listener != null) {
                        completed += dataPacket.getDataSize();
                        listener.onProgressUpdate(completed, fileSize);
                    }
                } while (!dataPacket.isLast());
                OperationResponse response = readDataStreamMessage();
                if (listener != null) {
                    listener.onCompleted(response.getCode());
                }
                return response.getCode();
            } else if (message.getType() == ResponseFactory.TYPE_OPERATION_RESPONSE) {
                short code = ((OperationResponse) message).getCode();
                if (listener != null) {
                    listener.onCompleted(code);
                }
                return code;
            }
        }
        return -1;
    }

    private boolean contains(short[] array, short value) {
        for (short item : array) {
            if (item == value) {
                return true;
            }
        }
        return false;
    }

    //endregion

    //region Public methods

    public PtpIpClient setExhaustiveLoggingEnabled(boolean isEnabled) {
        this.isExhaustiveLoggingEnabled = isEnabled;
        return this;
    }

    public boolean connect(String ipAddress, int port) throws IOException {

        dataSocket = createSocket(ipAddress, port, CommonConstants.TIMEOUT_SOCKET);
        OutputStream outDataStream = dataSocket.getOutputStream();
        PtpIpRequest request = new InitRequest("vievu");
        outDataStream.write(request.toBytes());
        PtpIpMessage response = readDataStreamMessage();
        if (response != null && response.getType() == ResponseFactory.TYPE_INIT_ACK) {
            sessionId = ((InitAckResponse)response).getSessionId();
            if (setUpEventConnection(ipAddress, port)) {
                request = new OpenSession(sessionId);
                outDataStream.write(request.toBytes());
                response = readDataStreamMessage();
                if (response != null && response.getType() == ResponseFactory.TYPE_OPERATION_RESPONSE) {
                    short code = ((OperationResponse)response).getCode();
                    transactionId = 2;
                    return code == ResponseCodes.OK || code == ResponseCodes.SessionAlreadyOpen;
                }
            }
        }
        disconnect(false);
        return false;
    }

    public DeviceInfo getDeviceInfo() throws IOException {
        send(OperationCodes.GetDeviceInfo);
        byte[] data = receiveData(true);
        DeviceInfo info = new DeviceInfo(data);
        OperationResponse response = readDataStreamMessage();
        return info;
    }

    public DevicePropertyInfoArray getAllProperties() throws IOException {

        send(OperationCodes.GetAllDevicePropDesc);
        DevicePropertyInfoArray infoArray = new DevicePropertyInfoArray(receiveData(true));
        OperationResponse response = readDataStreamMessage();
        return infoArray;
    }

    public PtpIpMessage sendData(byte[] data) throws IOException {
        Log.d("PtpIpClient", "Sending data...");
        DataPacketBase packet = new DataStartPacket(data.length);
        packet.setTransactionId(transactionId);
        OutputStream outputStream = dataSocket.getOutputStream();
        byte[] binaryPacket = packet.toBytes();
        logBinary(data);
        outputStream.write(binaryPacket);
        packet = new DataPacket(data);
        packet.setTransactionId(transactionId);
        binaryPacket = packet.toBytes();
        logBinary(data);
        outputStream.write(binaryPacket );
        return readDataStreamMessage();
    }

    public DevicePropertyInfo getPropertyInfo(short propertyCode, int propertyType) throws IOException {
        sendWithIntParameter(OperationCodes.GetDevicePropDesc, propertyCode & 0xFFFF);
        DevicePropertyInfo result = new DevicePropertyInfo(receiveData(true), 0, new ParameterFactory());
        readDataStreamMessage();
        return result;
    }

    @SuppressWarnings("unchecked")
    public <T extends IPtpIpParameter> T getProperty(short propertyCode, int propertyType) throws IOException {
        sendWithIntParameter(OperationCodes.GetDevicePropValue, propertyCode & 0xFFFF);
        T result = (T)ParameterFactory.create(receiveData(true), 0, propertyType);
        readDataStreamMessage();
        return result;
    }

    public PtpIpMessage setProperty(short propertyCode, IPtpIpParameter value) throws IOException {

        sendWithIntParameter(OperationCodes.SetDevicePropValue, propertyCode & 0xFFFF);
        transactionId--;
        byte[] data = new byte[value.getSize()];
        value.dump(data, 0);
        PtpIpMessage message = sendData(data);
        transactionId++;
        return message;
    }

    @SuppressLint("SupportAnnotationUsage")
    @CameraMode.Value
    public short getCameraMode() throws IOException {
        ShortParameter parameter = getProperty(
                DeviceProperties.CameraMode, ParameterFactory.TYPE_SHORT);
        //noinspection WrongConstant
        return parameter != null ? parameter.getValue() : CameraMode.ERROR;
    }

    public void compareAndSetCameraMode(@SuppressLint("SupportAnnotationUsage") @CameraMode.Value short newMode) throws IOException {
        Log.d("PtpIpClient", "Changed mode requested: " + newMode);
        short currentMode = getCameraMode();
        if (currentMode != newMode) {
            Log.d("PtpIpClient", "Changing from: " + currentMode);
            setCameraMode(newMode);
        } else {
            Log.d("PtpIpClient", "Already in this mode");
        }
    }

    public PtpIpMessage setCameraMode(@SuppressLint("SupportAnnotationUsage") @CameraMode.Value short newState) throws IOException {
        Log.d("PtpIpClient", "Setting camera mode to: " + String.valueOf(newState));
        PtpIpMessage result = setProperty(DeviceProperties.CameraMode, new ShortParameter(newState));
        if (result != null) {
            forceEvents();
        }
        return result;
    }

    public List<FileInfo> getFileList(short[] types) throws IOException {

        Log.d("PtpIpClient", "Loading file list");
        IntParameter ffParameter = new IntParameter(0xFFFFFFFF);
        IntParameter zeroParameter = new IntParameter(0);
        OperationRequest request = new OperationRequest(OperationCodes.GetObjectHandlesExt).
                setTransactionId(transactionId++).
                appendParameter(ffParameter).
                appendParameter(zeroParameter).
                appendParameter(ffParameter).
                appendParameter(zeroParameter).
                appendParameter(zeroParameter);
        dataSocket.getOutputStream().write(request.toBytes());
        byte[] data = receiveData(true);
        if (data != null) {
            PtpIpMessage message = readDataStreamMessage();
            int offset = 4;
            int currentId = -1;
            FileInfo.FileInfoProperty property;
            FileInfo fileInfo = null;
            ArrayList<FileInfo> fileList = new ArrayList<>();
            while (offset < data.length) {
                property = FileInfo.parseProperty(data, offset);
                if (property.fileInfoId != currentId) {
                    currentId = property.fileInfoId;
                    fileInfo = new FileInfo(currentId);
                }
                //noinspection ConstantConditions
                fileInfo.addProperty(property);
                if ((property.propertyCode & 0xFFFF) == FileInfo.PROPERTY_FILE_TYPE) {
                    short fileType = ((ShortParameter) property.value).getValue();
                    if (types == null || contains(types, fileType)) {
                        fileList.add(fileInfo);
                    }
                }
                offset += property.getSize();
            }
            return fileList;
        } else {
            disconnect(false);
        }
        return null;
    }

    public short getThumbnail(int fileId, OutputStream outputStream) throws IOException {
        sendWithIntParameter(OperationCodes.GetThumb, fileId);
        return saveData(outputStream, null);
    }

    public CameraStatus getCameraStatus() throws IOException {
        CameraStatus status = new CameraStatus();
        ByteParameter parameter = getProperty(DeviceProperties.BatteryLevel, ParameterFactory.TYPE_BYTE);
        status.setChargeLevel(parameter.getValue());
        send(OperationCodes.GetStorageIDs);
        byte[] data = receiveData(true);
        readDataStreamMessage();
        int offset = 0;
        int length = ByteHelper.getInt(data, offset);
        offset += 4;
        int[] array = new int[length];
        StorageInfo info;
        for (int i = 0; i < length; i++, offset += 4) {
            array[i] = ByteHelper.getInt(data, offset);
            sendWithIntParameter(OperationCodes.GetStorageInfo, array[i]);
            info = new StorageInfo(receiveData(true));
            readDataStreamMessage();
            status.addCapacity(info.getCapacity());
            status.addFreeSpace(info.getFreeSpace());
        }
        return status;
    }

    public PtpIpMessage beginCapture() throws IOException {
        return setCameraMode(CameraMode.VIDEO_ON);
    }

    public PtpIpMessage endCapture() throws IOException {
        PtpIpMessage message = setCameraMode(CameraMode.VIDEO_OFF);
        forceEvents();
        return message;
    }

    public PtpIpMessage capture() throws IOException {
        send(OperationCodes.InitiateCapture);
        return readDataStreamMessage();
    }

    public void getObject(int objectId, OutputStream outputStream, FileTransferListener listener) throws IOException {
        sendWithIntParameter(OperationCodes.GetObject, objectId);
        saveData(outputStream, listener);
    }

    public void getObjectInfo(int objectId) throws IOException {
        sendWithIntParameter(OperationCodes.GetObjectInfo, objectId);
        receiveData(true);
        readDataStreamMessage();
    }

    public short deleteObject(int objectId) throws IOException {
        sendWithIntParameter(OperationCodes.DeleteObject, objectId);
        OperationResponse response = readDataStreamMessage();
        return response != null ? response.getCode() : -1;
    }

    public void forceEvents() throws IOException {
        sendWithIntParameter(OperationCodes.iCatchHostRequest, 0xD001 & 0xFFFF);
        OperationMessage message = readDataStreamMessage();
    }

    public void formatStorage() throws IOException {
        send(OperationCodes.GetStorageIDs);
        byte[] data = receiveData(true);
        readDataStreamMessage();
        int length = ByteHelper.getInt(data);
        int offset = 4;
        for (int i = 0; i < length; i++, offset += 4) {
            sendWithIntParameter(OperationCodes.FormatStore, ByteHelper.getInt(data, offset));
            readDataStreamMessage();
        }
    }

    private void logBinary(byte[] data) {
        if (isExhaustiveLoggingEnabled) {
            StringBuilder builder = new StringBuilder("[ ");
            if (data != null) {
                for (byte item : data) {
                    builder.append("0x").append(Integer.toHexString(item & 0xFF)).append(", ");
                }
                builder.deleteCharAt(builder.length() - 2).append(']');
            } else {
                builder.append("null ]");
            }
            Log.d("PtpIpClient", builder.toString());
        }
    }

    public void send (short operationCode) throws IOException{
        Log.d("PtpIpClient", String.format("Sending request 0x%h", operationCode));
        byte[] data = new OperationRequest(operationCode).setTransactionId(transactionId++).toBytes();
        logBinary(data);
        dataSocket.getOutputStream().write(data);
    }

    public void sendWithIntParameter (short operationCode, int parameter) throws IOException{
        Log.d("PtpIpClient", String.format("Sending request 0x%h with param 0x%h", operationCode, parameter));
        byte[] data = new OperationRequest(operationCode).
                setTransactionId(transactionId++).
                appendParameter(new IntParameter(parameter)).
                toBytes();
        logBinary(data);
        dataSocket.getOutputStream().write(data);
    }

    public void sendStorageInfoRequest() throws IOException {
        sendWithIntParameter(OperationCodes.GetStorageInfo, 0x20001);
        readDataStreamMessage();
    }

    public boolean isConnected() {
        return dataSocket != null && !dataSocket.isClosed();
    }

    public void disconnect(boolean sendCloseRequest) {
        if (eventLoop != null) {
            eventLoop.cancel(true);
            eventLoop = null;
        }
        if (isConnected()) {
            if (sendCloseRequest) {
                try {
                    setCameraMode(CameraMode.APPLICATION);
                    send(OperationCodes.CloseSession);
                    readDataStreamMessage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (isConnected()) {
                try {
                    dataSocket.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dataSocket = null;
        }
        if (eventSocket != null && !eventSocket.isClosed()) {
            try {
                eventSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //endregion
}
