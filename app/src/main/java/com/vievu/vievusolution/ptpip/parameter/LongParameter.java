package com.vievu.vievusolution.ptpip.parameter;

import com.vievu.vievusolution.ptpip.ByteHelper;

public class LongParameter implements IPtpIpParameter, IntCastable{

    private long value;

    @Override
    public int getInt() {
        return (int)value;
    }

    public long getValue() {
        return value;
    }

    public LongParameter(long value) {
        this.value = value;
    }

    @Override
    public int getSize() {
        return 8;
    }

    @Override
    public byte[] dump(byte[] dest, int offset) {
        return ByteHelper.dumpLong(dest, value, offset);
    }


}
