package com.vievu.vievusolution.bean;

import java.util.List;

public class CategoryCollection {
    private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }
}
