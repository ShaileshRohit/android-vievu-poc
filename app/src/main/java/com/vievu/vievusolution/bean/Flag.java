package com.vievu.vievusolution.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class Flag implements Parcelable{

    private int flagId;
    private String summary;
    private long timeSpan;

    protected Flag(Parcel in) {
        flagId = in.readInt();
        summary = in.readString();
        timeSpan = in.readLong();
    }

    public static final Creator<Flag> CREATOR = new Creator<Flag>() {
        @Override
        public Flag createFromParcel(Parcel in) {
            return new Flag(in);
        }

        @Override
        public Flag[] newArray(int size) {
            return new Flag[size];
        }
    };

    public int getFlagId() {
        return flagId;
    }

    public void setFlagId(int flagId) {
        this.flagId = flagId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public long getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(long timeSpan) {
        this.timeSpan = timeSpan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(flagId);
        dest.writeString(summary);
        dest.writeLong(timeSpan);
    }
}
