package com.vievu.vievusolution.bean;

public class TokenData {

    private String accessToken;
    private String mfaToken;
    private int expiresIn;
    private User user;
    private Permissions permissions;

    public String getAccessToken() {
        return accessToken;
    }

    public String getMfaToken() {
        return mfaToken;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public User getLoggedUser() {
        return user;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    public boolean isValid() {
        return accessToken != null && expiresIn > 0;
    }
}
