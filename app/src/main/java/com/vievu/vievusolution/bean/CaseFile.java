package com.vievu.vievusolution.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class CaseFile implements Parcelable {

    public static final int FILE_TYPE_IMAGE = 0;
    public static final int FILE_TYPE_VIDEO = 1;
    public static final int FILE_TYPE_DOCUMENT = 2;
    public static final int FILE_TYPE_OTHER = 3;
    public static final int FILE_TYPE_REDACTED_VIDEO = 4;

    private int caseFileId;
    private String fileName;
    private String storageFileName;
    private int userId;
    private int fileType;

    public int getCaseFileId() {
        return caseFileId;
    }

    public String getFileName() {
        return fileName;
    }

    public String getStorageFileName() {
        return storageFileName;
    }

    public String getStorageFileName(boolean markAsConverted) {
        return markAsConverted ? File.markAsConverted(storageFileName) : storageFileName;
    }

    public int getUserId() {
        return userId;
    }

    public boolean isVideo() {
        return this.fileType == FILE_TYPE_VIDEO
            || this.fileType == FILE_TYPE_REDACTED_VIDEO;
    }

    //region Parcelable implementation

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(caseFileId);
        dest.writeString(fileName);
        dest.writeString(storageFileName);
        dest.writeInt(userId);
        dest.writeInt(fileType);
    }

    public static final Creator<CaseFile> CREATOR = new Creator<CaseFile>() {
        @Override
        public CaseFile createFromParcel(Parcel in) {
            CaseFile caseFile = new CaseFile();
            caseFile.caseFileId = in.readInt();
            caseFile.fileName = in.readString();
            caseFile.storageFileName = in.readString();
            caseFile.userId = in.readInt();
            caseFile.fileType = in.readInt();
            return caseFile;
        }

        @Override
        public CaseFile[] newArray(int size) {
            return new CaseFile[size];
        }
    };

    //endregion
}
