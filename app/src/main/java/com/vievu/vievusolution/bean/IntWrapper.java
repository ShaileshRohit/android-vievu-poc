package com.vievu.vievusolution.bean;

public class IntWrapper {

    private int value;
    private String readableValue;

    public int getValue() {
        return value;
    }

    public String getReadableValue() {
        return readableValue;
    }

    public IntWrapper(int value, String readableValue) {
        this.value = value;
        this.readableValue = readableValue;
    }
}
