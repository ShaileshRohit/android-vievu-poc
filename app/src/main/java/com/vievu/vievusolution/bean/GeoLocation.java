package com.vievu.vievusolution.bean;

import com.google.android.gms.maps.model.LatLng;

public class GeoLocation {

    private double longitude;
    private double latitude;

    public GeoLocation() {}

    public GeoLocation(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public GeoLocation(LatLng latLng) {
        this.longitude = latLng.longitude;
        this.latitude = latLng.latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public GeoLocation copy() {
        return new GeoLocation(longitude, latitude);
    }

    public LatLng toLatLng() {
        return new LatLng(latitude, longitude);
    }

    @Override
    public String toString() {
        return String.format("%f, %f", latitude, longitude);
    }
}
