package com.vievu.vievusolution.bean;

public class Permissions {

    public static final int VIDEOS_NONE = 0;
    public static final int VIDEOS_OWN = 1;
    public static final int VIDEOS_SAME_ROLE = 2;
    public static final int VIDEOS_SAME_LOCATION = 3;
    public static final int VIDEOS_ALL = 4;

    public static final int CASES_NONE = 0;
    public static final int CASES_OWN = 1;
    public static final int CASES_ALL = 2;

    private int viewVideos;
    private boolean canUseMobileApp;
    private boolean downloadVideos;
    private boolean editVideos;
    private boolean shareVideos;
    private int viewCases;
    private boolean editCases;
    private boolean setLockdown = true;

    public int getViewVideos() {
        return this.viewVideos;
    }

    public void setViewVideos(int viewVideos) {
        this.viewVideos = viewVideos;
    }

    public boolean isCanUseMobileApp() {
        return this.canUseMobileApp;
    }

    public void setCanUseMobileApp(boolean canUseMobileApp) {
        this.canUseMobileApp = canUseMobileApp;
    }

    public boolean isDownloadVideos() {
        return this.downloadVideos;
    }

    public void setDownloadVideos(boolean downloadVideos) {
        this.downloadVideos = downloadVideos;
    }

    public boolean isEditVideos() {
        return this.editVideos;
    }

    public void setEditVideos(boolean editVideos) {
        this.editVideos = editVideos;
    }

    public boolean isShareVideos() {
        return this.shareVideos;
    }

    public void setShareVideos(boolean shareVideos) {
        this.shareVideos = shareVideos;
    }

    public int getViewCases() {
        return this.viewCases;
    }

    public void setViewCases(int viewCases) {
        this.viewCases = viewCases;
    }

    public boolean isEditCases() {
        return this.editCases;
    }

    public void setEditCases(boolean editCases) {
        this.editCases = editCases;
    }

    public boolean isSetLockdown() {
        return this.setLockdown;
    }





    public boolean canViewVideos() {
        return viewVideos != VIDEOS_NONE;
    }

    public void setVideosAccess(int level) {
        this.viewVideos = level;
    }

    public boolean canUseMobileApp() {
        return canUseMobileApp;
    }

    public boolean canDownloadVideos() {
        return downloadVideos;
    }

    public void setCanDownloadVideos(boolean isPossible) {
        this.downloadVideos = isPossible;
    }

    public boolean canEditVideos() {
        return editVideos;
    }

    public void setCanEditVideos(boolean isPossible) {
        this.editVideos = isPossible;
    }

    public boolean canShareVideos() {
        return shareVideos;
    }

    public boolean canViewCases() {
        return viewCases != CASES_NONE;
    }

    public void setCasesAccess(int level) {
        this.viewCases = level;
    }

    public boolean canEditCases() {
        return editCases;
    }

    public boolean getSetLockdown(){
        return setLockdown;
    }

    public void setSetLockdown(boolean value){
        this.setLockdown = value;
    }
}
