package com.vievu.vievusolution.bean;

import com.vievu.vievusolution.util.DateUtil;

import java.util.Date;
import java.util.TimeZone;

public class DateWrapper {

    private Date date;
    private boolean isStartDate;

    public Date getDate() {
        return date;
    }

    public boolean isStartDate() {
        return isStartDate;
    }

    public DateWrapper(Date date, boolean isStartDate) {
        this.date = date;
        this.isStartDate = isStartDate;
    }

    @Override
    public String toString() {
        return DateUtil.Formatters.UTC.PARTIAL_ISO.format(date);
    }
}
