package com.vievu.vievusolution.bean;

import android.content.Context;
import android.support.annotation.StringRes;

public class ResultDescription {

    public static ResultDescription forError(Context context, @StringRes int errorMessageId) {
        return new ResultDescription(false, context.getString(errorMessageId));
    }

    private boolean result;
    private String message;

    public ResultDescription() {
    }

    public ResultDescription(boolean result, String message) {
        this.result = result;
        this.message = message;
    }

    public boolean isApplied() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
