package com.vievu.vievusolution.bean;

import android.net.Uri;

import com.vievu.vievusolution.util.DateUtil;

import java.text.ParseException;

public class VideoFileInfo extends FileInfo {

    private String videoUrl;

    @Override
    public Uri buildFileUri(String fileName) {
        if (uri == null) {
            if (videoUrl != null) {
                uri = Uri.parse(videoUrl);
                extractExpirationDate(uri);
            } else {
                uri = super.buildFileUri(fileName);
            }
        }
        return uri;
    }
}
