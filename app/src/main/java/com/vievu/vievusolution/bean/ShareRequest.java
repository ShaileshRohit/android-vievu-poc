package com.vievu.vievusolution.bean;

import com.vievu.vievusolution.util.DateUtil;

import java.util.Date;

@SuppressWarnings("FieldCanBeLocal")
public class ShareRequest {

    private int id;
    private String expirationDate;
    private String description;

    public void setId(int id) {
        this.id = id;
    }

    public ShareRequest(Date expirationDate, String description) {
        this.expirationDate = DateUtil.Formatters.UTC.PARTIAL_ISO.format(expirationDate);
        this.description = description;
    }
}
