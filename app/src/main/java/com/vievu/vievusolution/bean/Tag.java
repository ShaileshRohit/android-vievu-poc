package com.vievu.vievusolution.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class Tag implements Parcelable {

    private int tagId;
    private String tagName;



    public int getTagId() {
        return tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public Tag() {
    }

    public Tag(int tagId, String tagName) {
        this.tagId = tagId;
        this.tagName = tagName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(tagId);
        dest.writeString(tagName);
    }

    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            Tag tag = new Tag();
            tag.tagId = in.readInt();
            tag.tagName = in.readString();
            return tag;
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };
}
