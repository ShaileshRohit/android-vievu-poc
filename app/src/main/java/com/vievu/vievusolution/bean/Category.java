package com.vievu.vievusolution.bean;

public class Category {

    private int categoryId;
    private String categoryName;
    private int retentionPeriod;

    public Category() {}

    public Category(int categoryId, String categoryName) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public int getRetentionPeriod() {
        return retentionPeriod;
    }
}
