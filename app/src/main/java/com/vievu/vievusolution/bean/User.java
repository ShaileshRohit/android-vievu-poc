package com.vievu.vievusolution.bean;

import android.graphics.Bitmap;

public class User {

    public static final String FIELD_USER_ID = "UserId";
    public static final String FIELD_USER_NAME = "UserName";
    public static final String FIELD_VIDEO_COUNT = "VideoCount";
    public static final String FIELD_MIME_TYPE = "MimeType";
    public static final String FIELD_PICTURE = "Picture";

    private int userId;
    private String userName;
    private Bitmap picture;
    private int videoCount;
    private String mimeType;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Bitmap getPicture() {
        return picture;
    }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }

    public int getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(int videoCount) {
        this.videoCount = videoCount;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
