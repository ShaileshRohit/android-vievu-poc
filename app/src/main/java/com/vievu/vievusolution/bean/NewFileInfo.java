package com.vievu.vievusolution.bean;

import android.net.Uri;

public class NewFileInfo extends FileInfo {

    private String storageFileName;

    public String getStorageFileName() {
        return storageFileName;
    }

    @Override
    public Uri buildFileUri(String fileName) {
        return super.buildFileUri(storageFileName);
    }
}
