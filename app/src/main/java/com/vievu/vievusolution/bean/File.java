package com.vievu.vievusolution.bean;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.vievu.vievusolution.BuildConfig;
import com.vievu.vievusolution.util.ParcelableUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class File implements UserRelatedBean<File>, TaggedBean {

    public static final int FILE_TYPE_IMAGE = 0;
    public static final int FILE_TYPE_VIDEO = 1;
    public static final int FILE_TYPE_DOCUMENT = 2;
    public static final int FILE_TYPE_OTHER = 3;
    public static final int FILE_TYPE_REDACTED_VIDEO = 4;

    public static String markAsConverted(String storageFileName) {
        return ":" + storageFileName;
    }

    private int videoId;
    private int userId;
    private int categoryId;
    private int caseId;
    private int redactVideoId;
    private String reportNumber;
    private String eventNumber;
    private boolean neverDelete;
    private boolean lockDownVideo;
    private boolean isReadOnly;
    private boolean isCopyDisabled;
    private GeoLocation videoLocation;
    private String comment;
    //Note: This field is relevant only for VERIPATROL app
    private String caseNumber;

    private String fileId;

    private int fileType;

    @Expose(serialize = false)
    private Date recordDate;
    @Expose(serialize = false)
    private Date uploadDate;
    @Expose(serialize = false)
    private String cameraFileName;
    @Expose(serialize = false)
    private String storageFileName;
    @Expose(serialize = false)
    private boolean recordOnCamera;
    @Expose(serialize = false)
    private String cameraSerialNumber;
    @Expose(serialize = false)
    private String digitalSignature;
    @Expose(serialize = false)
    private Uri videoStreamUri;
    @Expose(serialize = false)
    private String duration;
    @Expose(serialize = false)
    private Date incidentDate;

    @Expose(serialize = false)
    private String userName;
    @Expose(serialize = false)
    private String categoryName;
    @Expose(serialize = false)
    private String caseName;

    private List<Tag> tags = new ArrayList<>();
    private List<Flag> videoFlags = new ArrayList<>();
    private List<MasterlogRecord> masterlogRecords = new ArrayList<>();

    public int getVideoId() {
        return videoId;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public String getCameraFileName() {
        return cameraFileName;
    }

    public void setCameraFileName(String cameraFileName) {
        this.cameraFileName = cameraFileName;
    }

    public String getStorageFileName() {
        return storageFileName;
    }

    public String getStorageFileName(boolean markAsConverted) {
        return markAsConverted ? markAsConverted(storageFileName) : storageFileName;
    }

    public int getCaseId() {
        return caseId;
    }

    public void setCaseId(int caseId) {
        this.caseId = caseId;
    }

    public int getRedactVideoId() {
        return redactVideoId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isRecordOnCamera() {
        return recordOnCamera;
    }

    public String getCameraSerialNumber() {
        return cameraSerialNumber;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getEventNumber() {
        return eventNumber;
    }

    public void setEventNumber(String eventNumber) {
        this.eventNumber = eventNumber;
    }

    public String getDigitalSignature() {
        return digitalSignature;
    }

    public GeoLocation getVideoLocation() {
        return videoLocation;
    }

    public void setVideoLocation(GeoLocation videoLocation) {
        this.videoLocation = videoLocation;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isNeverDelete() {
        return neverDelete;
    }

    public void setNeverDelete(boolean neverDelete) {
        this.neverDelete = neverDelete;
    }

    public boolean isLockDownVideo() {
        return lockDownVideo;
    }

    public void setLockDownVideo(boolean lockDownVideo) {
        this.lockDownVideo = lockDownVideo;
    }

    public boolean getIsReadOnly() {
        return isReadOnly;
    }

    public boolean getIsCopyDisabled() {
        return isCopyDisabled;
    }

    public Uri getVideoStreamUri() {
        return videoStreamUri;
    }

    public void setVideoStreamUri(Uri videoStreamUri) {
        this.videoStreamUri = videoStreamUri;
    }

    public String getDuration() {
        return duration;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getFileType() {
        return fileType;
    }

    public boolean hasThumbnail() {
        return fileType == FILE_TYPE_IMAGE
            || fileType == FILE_TYPE_VIDEO
            || fileType == FILE_TYPE_REDACTED_VIDEO;
    }

    public String getCaseName() {
        //noinspection ConstantConditions
        return BuildConfig.USES_BACKEND ? caseName : caseNumber;
    }

    public void setCaseName(String caseName) {
        if (BuildConfig.USES_BACKEND) {
            this.caseName = caseName;
        } else {
            this.caseNumber = caseName;
        }
    }

    public boolean isRedactedVideo() {
        return redactVideoId != 0;
    }

    public Category getCategory() {
        return new Category(categoryId, categoryName);
    }

    public NewVideo getNewVideoProxy() {
        return new NewVideo(this);
    }

    public List<Tag> getTags() {
        return tags;
    }

    @Override
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void copyTo(File other) {
        other.videoId = this.videoId;
        other.userId = this.userId;
        other.categoryId = this.categoryId;
        other.caseId = this.caseId;
        other.redactVideoId = this.redactVideoId;
        other.reportNumber = this.reportNumber;
        other.eventNumber = this.eventNumber;
        if (this.videoLocation != null) {
            other.videoLocation = this.videoLocation.copy();
        } else {
            other.videoLocation = null;
        }
        other.comment = this.comment;
        other.neverDelete = this.neverDelete;
        other.lockDownVideo = this.lockDownVideo;
        other.isReadOnly = this.isReadOnly;
        other.isCopyDisabled = this.isCopyDisabled;
        other.userName = this.userName;
        other.categoryName = this.categoryName;
        other.caseName = this.caseName;
        other.caseNumber = this.caseNumber;
        other.fileType = this.fileType;
        if (tags != null) {
            other.tags = new ArrayList<>(this.tags);
        }
        if (videoFlags != null) {
            other.videoFlags = new ArrayList<>(this.videoFlags);
        }
        if (masterlogRecords != null) {
            other.masterlogRecords = new ArrayList<>(this.masterlogRecords);
        }
    }

    //region Parcelable implementation

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(videoId);
        out.writeInt(userId);
        out.writeInt(categoryId);
        out.writeInt(caseId);
        out.writeInt(redactVideoId);
        out.writeString(reportNumber);
        out.writeString(eventNumber);
        ParcelableUtil.writeBoolean(out, neverDelete);
        ParcelableUtil.writeBoolean(out, lockDownVideo);
        ParcelableUtil.writeBoolean(out, isReadOnly);
        ParcelableUtil.writeBoolean(out, isCopyDisabled);
        ParcelableUtil.writeGeoLocation(out, videoLocation);
        out.writeString(comment);
        out.writeString(caseNumber);
        out.writeInt(fileType);

        ParcelableUtil.writeDate(out, recordDate);
        ParcelableUtil.writeDate(out, uploadDate);
        out.writeString(cameraFileName);
        out.writeString(storageFileName);
        ParcelableUtil.writeBoolean(out, recordOnCamera);
        out.writeString(cameraSerialNumber);
        out.writeString(digitalSignature);
        out.writeString(duration);
        ParcelableUtil.writeDate(out, incidentDate);

        out.writeString(userName);
        out.writeString(categoryName);
        out.writeString(caseName);

        ParcelableUtil.writeList(out, tags, flags);
        ParcelableUtil.writeList(out, videoFlags, flags);
        ParcelableUtil.writeList(out, masterlogRecords, flags);
    }

    public static final Parcelable.Creator<File> CREATOR = new Parcelable.Creator<File>() {
        public File createFromParcel(Parcel in) {
            File file = new File();
            file.videoId = in.readInt();
            file.userId = in.readInt();
            file.categoryId = in.readInt();
            file.caseId = in.readInt();
            file.redactVideoId = in.readInt();
            file.reportNumber = in.readString();
            file.eventNumber = in.readString();
            file.neverDelete = ParcelableUtil.readBoolean(in);
            file.lockDownVideo = ParcelableUtil.readBoolean(in);
            file.isReadOnly = ParcelableUtil.readBoolean(in);
            file.isCopyDisabled = ParcelableUtil.readBoolean(in);
            file.videoLocation = ParcelableUtil.readGeoLocation(in);
            file.comment = in.readString();
            file.caseNumber = in.readString();
            file.fileType = in.readInt();

            file.recordDate = ParcelableUtil.readDate(in);
            file.uploadDate = ParcelableUtil.readDate(in);
            file.cameraFileName = in.readString();
            file.storageFileName = in.readString();
            file.recordOnCamera = ParcelableUtil.readBoolean(in);
            file.cameraSerialNumber = in.readString();
            file.digitalSignature = in.readString();
            file.duration = in.readString();
            file.incidentDate = ParcelableUtil.readDate(in);

            file.userName = in.readString();
            file.categoryName = in.readString();
            file.caseName = in.readString();

            file.tags = ParcelableUtil.readList(in, Tag.class);
            file.videoFlags = ParcelableUtil.readList(in, Flag.class);
            file.masterlogRecords = ParcelableUtil.readList(in, MasterlogRecord.class);
            return file;
        }

        public File[] newArray(int size) {
            return new File[size];
        }
    };

    //endregion
}
