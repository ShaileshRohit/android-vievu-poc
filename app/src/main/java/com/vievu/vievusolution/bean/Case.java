package com.vievu.vievusolution.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.vievu.vievusolution.util.ParcelableUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Case implements UserRelatedBean<Case>, TaggedBean {

    private int caseId;
    private int userId;
    private int caseStatusId;
    private Date incidentDate;
    private String summary;
    private String eventNumber;
    private String reportNumber;
    private String caseNotes;
    private boolean isReadOnly;
    private boolean isCopyDisabled;

    @Expose(serialize = false)
    private String caseName;
    @Expose(serialize = false)
    private Date creationDate;

    @Expose(serialize = false)
    private String userName;
    @Expose(serialize = false)
    private String statusName;

    private List<Tag> tags = new ArrayList<>();

    public int getCaseId() {
        return caseId;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getEventNumber() {
        return eventNumber;
    }

    public void setEventNumber(String eventNumber) {
        this.eventNumber = eventNumber;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public int getCaseStatusId() {
        return caseStatusId;
    }

    public void setCaseStatusId(int caseStatusId) {
        this.caseStatusId = caseStatusId;
    }

    public String getCaseName() {
        return caseName;
    }

    public String getCaseNotes() {
        return caseNotes;
    }

    public void setCaseNotes(String caseNotes) {
        this.caseNotes = caseNotes;
    }

    public boolean getIsReadOnly() {
        return isReadOnly;
    }

    public boolean getIsCopyDisabled() {
        return isCopyDisabled;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public CaseStatus getCaseStatus() {
        return new CaseStatus(caseStatusId, statusName);
    }

    public List<Tag> getTags() {
        return tags;
    }

    @Override
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void copyTo(Case other) {
        other.caseId = this.caseId;
        other.userId = this.userId;
        other.caseStatusId = this.caseStatusId;
        other.incidentDate = this.incidentDate;
        other.summary = this.summary;
        other.reportNumber = this.reportNumber;
        other.eventNumber = this.eventNumber;
        other.caseNotes = this.caseNotes;
        other.isReadOnly = this.isReadOnly;
        other.isCopyDisabled = this.isCopyDisabled;
        other.userName = this.userName;
        other.statusName = this.statusName;
        if (tags != null) {
            other.tags = new ArrayList<>(this.tags);
        }
    }

    //region Parcelable implementation

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(caseId);
        out.writeInt(userId);
        out.writeInt(caseStatusId);
        ParcelableUtil.writeDate(out, incidentDate);
        out.writeString(summary);
        out.writeString(reportNumber);
        out.writeString(eventNumber);
        out.writeString(caseNotes);
        ParcelableUtil.writeBoolean(out, isReadOnly);
        ParcelableUtil.writeBoolean(out, isCopyDisabled);

        out.writeString(caseName);
        ParcelableUtil.writeDate(out, creationDate);

        out.writeString(userName);
        out.writeString(statusName);

        ParcelableUtil.writeList(out, tags, flags);
    }

    public static final Parcelable.Creator<Case> CREATOR = new Parcelable.Creator<Case>() {
        public Case createFromParcel(Parcel in) {
            Case item = new Case();
            item.caseId = in.readInt();
            item.userId = in.readInt();
            item.caseStatusId = in.readInt();
            item.incidentDate = ParcelableUtil.readDate(in);
            item.summary = in.readString();
            item.reportNumber = in.readString();
            item.eventNumber = in.readString();
            item.caseNotes = in.readString();
            item.isReadOnly = ParcelableUtil.readBoolean(in);
            item.isCopyDisabled = ParcelableUtil.readBoolean(in);
            item.caseName = in.readString();
            item.creationDate = ParcelableUtil.readDate(in);
            item.userName = in.readString();
            item.statusName = in.readString();
            item.tags = ParcelableUtil.readList(in, Tag.class);
            return item;
        }

        public Case[] newArray(int size) {
            return new Case[size];
        }
    };

    //endregion
}
