package com.vievu.vievusolution.bean;

import android.net.Uri;

import com.google.gson.annotations.Expose;

public class ShareLink {

    private String shareLink;
    @Expose(deserialize = false)
    private Uri uri;

    public Uri getShareLink() {
        if (uri == null) {
            uri = Uri.parse(shareLink);
        }
        return uri;
    }
}
