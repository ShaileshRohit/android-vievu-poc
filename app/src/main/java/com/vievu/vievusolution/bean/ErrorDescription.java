package com.vievu.vievusolution.bean;

import java.util.HashMap;

public class ErrorDescription {

    private String error;
    private String errorDescription;

    public String getError() {
        return error;
    }

    public String getDescription() {
        return errorDescription;
    }

}
