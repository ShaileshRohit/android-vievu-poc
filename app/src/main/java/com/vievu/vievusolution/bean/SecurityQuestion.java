package com.vievu.vievusolution.bean;

public class SecurityQuestion {

    private int securityQuestionId;
    private String securityQuestion;

    public int getQuestionId() {
        return securityQuestionId;
    }

    public String getQuestion() {
        return securityQuestion;
    }
}
