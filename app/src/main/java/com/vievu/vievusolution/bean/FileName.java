package com.vievu.vievusolution.bean;

public class FileName {

    private int fileNameId;
    private String fileName;

    public int getFileNameId() {
        return fileNameId;
    }

    public String getFileName() {
        return fileName;
    }
}
