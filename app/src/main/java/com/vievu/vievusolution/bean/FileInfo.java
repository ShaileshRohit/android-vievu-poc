package com.vievu.vievusolution.bean;

import android.net.Uri;

import org.joda.time.format.ISODateTimeFormat;

import java.util.Date;

public class FileInfo {

    protected Uri uri;
    protected Date expirationDate;
    protected String fileSas;
    protected String containerUri;
    protected String sASDuration;

    public String getFileSAS() {
        return fileSas;
    }

    public String getContainerUri() {
        return containerUri;
    }

    public String getSASDuration() {
        return sASDuration;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public Uri buildFileUri(String fileName) {
        if (uri == null) {
            uri = Uri.parse(containerUri)
                    .buildUpon()
                    .appendPath(fileName)
                    .encodedQuery(fileSas.substring(1))
                .build();
            extractExpirationDate(uri);
        }
        return uri;
    }

    protected void extractExpirationDate(Uri uri) {
        expirationDate = new Date(ISODateTimeFormat.dateTimeParser().parseMillis(uri.getQueryParameter("se")));
    }
}
