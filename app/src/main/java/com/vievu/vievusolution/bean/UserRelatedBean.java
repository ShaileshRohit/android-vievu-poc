package com.vievu.vievusolution.bean;

import android.os.Parcelable;

public interface UserRelatedBean<T> extends Parcelable {
    void copyTo(T other);
    int getUserId();
    void setUserId(int userId);
    String getUserName();
    void setUserName(String userName);
}
