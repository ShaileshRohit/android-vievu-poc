package com.vievu.vievusolution.bean;

public class Location {

    private int locationId;
    private String locationName;

    public int getLocationId() {
        return locationId;
    }

    public String getLocationName() {
        return locationName;
    }
}
