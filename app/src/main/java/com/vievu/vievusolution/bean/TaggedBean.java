package com.vievu.vievusolution.bean;

import java.util.List;

public interface TaggedBean {
    List<Tag> getTags();
    void setTags(List<Tag> tags);
}
