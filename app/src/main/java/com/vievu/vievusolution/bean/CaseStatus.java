package com.vievu.vievusolution.bean;

public class CaseStatus {

    private int caseStatusId;
    private String name;

    public CaseStatus() {}

    public CaseStatus(int caseStatusId, String name) {
        this.caseStatusId = caseStatusId;
        this.name = name;
    }

    public int getCaseStatusId() {
        return caseStatusId;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
