package com.vievu.vievusolution.bean;

import java.util.ArrayList;

public class LimitedArrayList<T> extends ArrayList<T> {

    private int limit;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
