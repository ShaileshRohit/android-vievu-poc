package com.vievu.vievusolution.bean;

public class MfaTokenData {

    private String mfaToken;
    private long lastLogin;

    public MfaTokenData(String mfaToken) {
        this.mfaToken = mfaToken;
    }

    public String getMfaToken() {
        return mfaToken;
    }

    public void setMfaToken(String mfaToken) {
        this.mfaToken = mfaToken;
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public void updateTimestamp() {
        lastLogin = System.currentTimeMillis();
    }
}
