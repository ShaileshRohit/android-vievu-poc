package com.vievu.vievusolution.bean;

import java.util.HashMap;

public class LoginErrorDescription extends ErrorDescription {

    public static final int CODE_PASSWORD_EXPIRED = 1;
    public static final int CODE_MFA_REQUIRED = 3;
    public static final int CODE_BAD_VERIFICATION_CODE = 4;
    public static final int CODE_MFA_CODE_SENT = 5;
    public static final int CODE_USER_LOCKED = 6;

    private int code;
    private HashMap<String, String> parameters;
    private String mfaToken;

    public int getCode() {
        return code;
    }

    public HashMap<String, String> getParameters() {
        return parameters;
    }

    public String getMfaToken() {
        return mfaToken;
    }
}
