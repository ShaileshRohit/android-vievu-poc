package com.vievu.vievusolution.bean;

import java.util.Date;
import java.util.List;

@SuppressWarnings("FieldCanBeLocal")
public class NewVideo implements TaggedBean {

    private int userId;
    private int categoryId;
    private int caseId;
    private String reportNumber;
    private String eventNumber;
    private boolean neverDelete;
    private boolean lockDownVideo;
    private String comment;
    private Date recordDate;
    private Date uploadDate;
    private String cameraFileName;
    private String storageFileName;
    private boolean recordOnCamera;
    private String digitalSignature;
    private GeoLocation videoLocation;
    private List<Tag> tags;

    public NewVideo(File video) {
        this.userId = video.getUserId();
        this.categoryId = video.getCategoryId();
        this.caseId = video.getCaseId();
        this.reportNumber = video.getReportNumber();
        this.eventNumber = video.getEventNumber();
        this.neverDelete = video.isNeverDelete();
        this.lockDownVideo = video.isLockDownVideo();
        this.comment = video.getComment();
        this.uploadDate = new Date(System.currentTimeMillis());
        this.recordOnCamera = false;
        if (video.getVideoLocation() != null) {
            this.videoLocation = video.getVideoLocation().copy();
        }
        this.tags = video.getTags();
        //TODO: Calculate signature when it will be useful
        this.digitalSignature = null;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public String getCameraFileName() {
        return cameraFileName;
    }

    public void setCameraFileName(String cameraFileName) {
        this.cameraFileName = cameraFileName;
    }

    public void setStorageFileName(String storageFileName) {
        this.storageFileName = storageFileName;
    }

    @Override
    public List<Tag> getTags() {
        return tags;
    }

    @Override
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
