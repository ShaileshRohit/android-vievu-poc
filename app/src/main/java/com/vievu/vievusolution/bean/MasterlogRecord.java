package com.vievu.vievusolution.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterlogRecord implements Parcelable{
    private String eventId;
    private String eventMessage;
    private String eventType;
    private String fileName;
    private String ownerId;
    private String timestamp;

    protected MasterlogRecord(Parcel in) {
        eventId = in.readString();
        eventMessage = in.readString();
        eventType = in.readString();
        fileName = in.readString();
        ownerId = in.readString();
        timestamp = in.readString();
    }

    public static final Creator<MasterlogRecord> CREATOR = new Creator<MasterlogRecord>() {
        @Override
        public MasterlogRecord createFromParcel(Parcel in) {
            return new MasterlogRecord(in);
        }

        @Override
        public MasterlogRecord[] newArray(int size) {
            return new MasterlogRecord[size];
        }
    };

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventId);
        dest.writeString(eventMessage);
        dest.writeString(eventType);
        dest.writeString(fileName);
        dest.writeString(ownerId);
        dest.writeString(timestamp);
    }
}
