package com.vievu.vievusolution;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.vievu.vievusolution.adapter.OneLineArrayListAdapter;
import com.vievu.vievusolution.data.AssignedCamerasAsyncRepository;
import com.vievu.vievusolution.event.NetworkErrorEvent;
import com.vievu.vievusolution.event.PingCompletedEvent;
import com.vievu.vievusolution.event.RequestCompletedEvent;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.ptpip.dataset.CameraStatus;
import com.vievu.vievusolution.util.ActivityUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AssignedCameraActivity extends EventBusActivity {

    @Bind(R.id.connectedCameraTextView) TextView connectedCameraTextView;
    @Bind(R.id.progressBarOverlay) View progressBarOverlay;
    @Bind(R.id.assignedCamerasListView) ListView assignedCamerasListView;

    private boolean isLoadingAssignedCameras = true;
    private boolean isLoadingCameraSSID = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_camera);
        ButterKnife.bind(this);
        progressBarOverlay.setVisibility(View.VISIBLE);
        //CameraClient.getInstance().getStorageInfoAsync();
        currentAction = WebClient.Actions.LOAD_ASSIGNED_CAMERAS;
        WebClient.pingAsync(currentAction);
    }

    private void displayConnectedCameraSSID() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        connectedCameraTextView.setText(
            Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 ?
                wifiInfo.getSSID() :
                wifiInfo.getSSID().replaceAll("^\"(.*)\"$", "$1"));
        isLoadingCameraSSID = false;
        updateProgressOverlay();
    }

    private void displayNoConnectedCamera() {
        connectedCameraTextView.setText(R.string.text_camera_no_connected);
        isLoadingCameraSSID = false;
        updateProgressOverlay();
    }

    private void updateProgressOverlay() {
        progressBarOverlay.setVisibility(
            isLoadingAssignedCameras || isLoadingCameraSSID ? View.VISIBLE : View.GONE);
    }

    private void displayAssignedCameras(List<String> assignedCameras) {
        if (assignedCameras != null && !assignedCameras.isEmpty()) {
            assignedCamerasListView.setAdapter(new OneLineArrayListAdapter<>(this, assignedCameras));
        } else {
            findViewById(R.id.noAssignedCamerasTextView).setVisibility(View.VISIBLE);
        }
        isLoadingAssignedCameras = false;
        ActivityUtil.tryBindNetworkAdapter(this);
        CameraClient.getInstance().getStorageInfoAsync();
    }

    public void onEvent(PingCompletedEvent event) {
        if (event.getAction() == currentAction && !keepEvent(event)) {
            if (event.isConnected()) {
                AssignedCamerasAsyncRepository.getInstance().clearData();
            }
            AssignedCamerasAsyncRepository.getInstance().getDataAsync(this::displayAssignedCameras);
        }
    }

    public void onEvent(RequestCompletedEvent<List<String>> event) {
        if (event.is(currentAction) && !keepEvent(event)) {
            displayAssignedCameras(event.getResult());
        }
    }

    public void onEvent(NetworkErrorEvent event) {
        if (event.is(currentAction) && !keepEvent(event)) {
            displayAssignedCameras(null);
            event.displayError(this);
        }
    }

    public void onEventMainThread(CameraStatus cameraStatus) {
        ActivityUtil.tryUnbindNetworkAdapter(this);
        if (cameraStatus.getCapacity() > 0) {
            displayConnectedCameraSSID();
        } else {
            displayNoConnectedCamera();
        }
    }

    public void onEventMainThread(CameraClient.Event event) {
        if (!event.isConnected()) {
            displayNoConnectedCamera();
        }
    }
}
