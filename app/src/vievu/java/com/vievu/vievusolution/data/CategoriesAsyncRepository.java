package com.vievu.vievusolution.data;

import android.content.Context;

import com.vievu.vievusolution.bean.Category;
import com.vievu.vievusolution.net.EventBusCallback;
import com.vievu.vievusolution.net.WebClient;
import com.vievu.vievusolution.util.JsonCacheUtil;

import java.util.List;

public class CategoriesAsyncRepository extends CachingAsyncRepository<List<Category>> {

    private static final String CACHE_FILE_NAME = "categories.json";

    private static CategoriesAsyncRepository instance;

    public static CategoriesAsyncRepository getInstance() {
        if (instance == null) {
            instance = new CategoriesAsyncRepository();
        }
        return instance;
    }

    @Override
    protected String getCacheFileName() {
        return CACHE_FILE_NAME;
    }

    public static boolean restoreFromCache(Context context) {
        CategoriesAsyncRepository cachedInstance = JsonCacheUtil.
                restoreFromCache(context, CACHE_FILE_NAME, CategoriesAsyncRepository.class);
        if (cachedInstance != null) {
            CategoriesAsyncRepository.instance = cachedInstance;
            return true;
        }
        return false;
    }

    @Override
    protected int getLoadEventAction() {
        return WebClient.Actions.LOAD_CATEGORIES;
    }

    @Override
    protected void loadData(EventBusCallback<List<Category>> callback) {
        WebClient.request().categories(callback);
    }
}
