package com.vievu.vievusolution.data;

import android.content.Context;
import android.os.Environment;

import com.google.gson.stream.JsonWriter;
import com.vievu.vievusolution.bean.Permissions;
import com.vievu.vievusolution.bean.User;
import com.vievu.vievusolution.json.Gsons;
import com.vievu.vievusolution.util.JsonCacheUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;

public class UserDataRepository {

    private static final String FILE_NAME = "user_data.json";

    private static UserDataRepository instance;

    public static UserDataRepository getInstance() {
        if (instance == null) {
            instance = new UserDataRepository();
        }
        return instance;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private User user;

    public void setPermissions(Permissions permissions) {
        this.permissions = permissions;
    }

    private Permissions permissions;

    public void initialize(User user, Permissions permissions) {
        this.user = user;
        this.permissions = permissions;
    }

    public User getUser() {
        return user;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    public void clearData(Context context) {
        File file = new File(context.getFilesDir(), FILE_NAME);
        //noinspection ResultOfMethodCallIgnored
        file.delete();
        this.user = null;
        this.permissions = null;
    }

    public void cacheCopy(Context context) {
        JsonCacheUtil.cacheCopy(context, FILE_NAME, this);
    }


    public static boolean restoreFromCache(Context context) {
        instance = JsonCacheUtil.restoreFromCache(context, FILE_NAME, UserDataRepository.class);
        return instance != null;
    }
}
