-dontwarn java.lang.invoke.*

-keepattributes Signature
-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }

-keep class com.vievu.vievusolution.bean.**
-keepclassmembers class com.vievu.vievusolution.bean.** { *; }

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn retrofit.**
-dontwarn retrofit.appengine.UrlFetchClient
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

-dontwarn org.joda.convert.**

-keepclassmembers class ** {
    public void onEvent*(**);
}

-dontwarn com.microsoft.azure.storage.table.**
-dontwarn android.app.Notification
-dontwarn org.apache.http.**
-keep class org.acra.** { *; }

-dontwarn de.greenrobot.event.util.ErrorDialog*
-keep class com.ipaulpro.afilechooser.** { *; }